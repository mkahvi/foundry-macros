// Delete active effects from selected tokens and their source actors (if unlinked)

const sourceActors = new Set();

const clearAEs = async (actor) => {
	if (!actor) return;
	const AEs = actor.effects;
	if (AEs.size) {
		if (actor.token) sourceActors.add(actor.id);
		else return actor?.deleteEmbeddedDocuments('ActiveEffect', AEs.map(ae => ae.id));
	}
}

const p0 = [], p1 = [];

// Delete from selected tokens
for (const token of canvas.tokens.controlled) {
	const actor = token.actor;
	if (actor) p0.push(clearAEs(actor));
}

await Promise.allSettled(p0);

// Delete also from source actors
for (const actorId of Array.from(sourceActors)) {
	const actor = game.actors.get(actorId);
	if (actor) p1.push(clearAEs(actor));
}

await Promise.allSettled(p1);
