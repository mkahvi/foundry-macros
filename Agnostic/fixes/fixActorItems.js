/**
 * Foundry 0.8 migration fix.
 *
 * Locate and fix actors with items that have missing or malformed .name entry or duplicate IDs.
 *
 * INSTRUCTIONS/NOTES:
 * The fixes may take a little bit of time to take hold, for Foundry reasons.
 * Refresh and check if errors persist. If they do, run the script again and refresh once it's done.
 * If the errors persists after third refresh and run, there's probably something else going on than what this script fixes.
 *
 * Thanks to Kim/fyorl of Foundry staff for helping make this work correctly!
 */

async function fixActor(actor) {
	let brokenEntries = 0;
	/** @type {String[]} */
	const brokenIds = [];
	let items = actor.toObject().items;
	const origItemCount = items.length;
	// fix missing names
	items.forEach(i => {
		if ((i.name?.trim() ?? '') === '') {
			brokenEntries++;
			i.name = '----- MISSING NAME -----';
			brokenIds.push(i._id);
		}
	});

	// Find items with duplicate IDs
	const itemsWithDuplicateIDs = items.filter(i => items.filter(si => si._id === i._id).length > 1);
	if (itemsWithDuplicateIDs.length > 0) {
		// Remove items with duplicate IDs from main item list
		items = items.filter(i => !itemsWithDuplicateIDs.find(i2 => i._id === i2._id));
	}

	let fixed = false;
	let update, insert;

	// Update as necessary
	if (brokenEntries > 0 || origItemCount !== items.length) {
		update = await actor.update({ items }, { recursive: false, diff: false });
		fixed = true;
	}

	if (itemsWithDuplicateIDs.length > 0) {
		itemsWithDuplicateIDs.forEach(i => delete i._id);
		insert = await actor.createEmbeddedDocuments('Item', itemsWithDuplicateIDs);
		fixed = true;
	}

	if (fixed)
		console.log(`${actor.name} [${actor.id}] fixed ${brokenEntries} name(s) and ${itemsWithDuplicateIDs.length} duplicate ID(s)`);

	return [fixed, brokenIds, update, insert];
}

async function fixAllTokens() {
	for (const scene of game.scenes) {
		const tokens = scene.tokens.contents;
		for (const token of tokens) {
			if (token.isLinked) continue; // Linked are handled elsewhere
			if (!(token.data.actorData?.items?.length > 0)) continue; // No item overrides
			await fixActor(token.actor);
		}
	}
}

class ProgresBar {
	div = document.createElement('div');
	label = document.createElement('label');
	bar = document.createElement('progress');
	constructor() {
		this.div.append(this.label, this.bar);
		this.div.style.cssText += 'pointer-events:none;position:absolute;top:200px;background-color:#6d4848;color:white;display:flex;flex-direction:column;width:100%;z-index:100;justify-content:center;text-align:center;padding:1em;align-self:center;width:40%;height:20px;transition:opacity 0.5s;';
		this.label.style.fontSize = '1.5em';
		this.label.textContent = 'Fixing Actors...';
		document.body.append(this.div);
	}

	setText(text) {
		this.label.textContent = text;
	}

	update(value = 0) {
		this.bar.value = value;
	}

	setMax(max = 0) {
		this.bar.max = max;
	}

	hide() {
		setTimeout(() => {
			this.div.style.opacity = '0'
			setTimeout(() => this.delete(), 500);
		}, 500);
	}

	delete() {
		this.div.remove();
	}
}

let b;

async function fixAllActors() {
	let totalFixed = 0, totalFailed = 0;

	ui.notifications.info('Fixing all actors...');

	let progress = 0;
	const _fixActor = async (actor, token) => {
		const name = token?.name ?? actor.name;
		try {
			const [fixed, ids, update, insert] = await fixActor(actor);
			// await Promise.all([update, insert]); // just to be sure, JS is weird.
			if (fixed) totalFixed++;
		}
		catch (err) {
			console.error(`Error while fixing actor: ${name} [${actor.id}]:`, err);
			totalFailed++;
		}
		finally {
			b.update(++progress);
		}
	};

	const actors = game.actors.contents;
	const tokens = game.scenes.reduce((all, scene) => [
		...all, ...scene.tokens.contents.filter(t => {
			if (t.isLinked) return false; // Linked are handled elsewhere
			if (!t.actor) return false; // Missing actor. Probably deleted.
			if (!(t.data.actorData?.items?.length > 0)) return false; // No item overrides
			return true;
		})
	], []);

	b.setMax(actors.length + tokens.length);
	b.setText(`Fixing ${actors.length} actors and ${tokens.length} tokens`);

	// Fix actors
	for (const actor of actors) {
		await _fixActor(actor);
	}
	// Fix unlinked tokens
	for (const token of tokens)
		await _fixActor(token.actor, token);

	const msg = `Fixed ${totalFixed} out of ${actors.length} actors and ${tokens.length} unlinked tokens found; ${totalFailed} failed to be fixed.`;
	console.log(msg);
	ui.notifications.info(msg);
}

try {
	b = new ProgresBar();
	await fixAllActors();
}
finally {
	b?.hide();
}
