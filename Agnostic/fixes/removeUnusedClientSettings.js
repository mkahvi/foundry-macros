/**
 * Deletes client-side settings for modules that are not active.
 *
 * Affects currently logged in user only.
 *
 * THIS CAN NOT BE UNDONE!
 *
 * TODO:
 * - Add confirmation dialog?
 */

// Module IDs to ignore
const ignoreIds = [];

// Always ignore core Foundry settings and game system settings
ignoreIds.push('core', game.system.id);

const settings = game.settings.storage.get('client');

const keys = Array.fromRange(settings.length).map(n => settings.key(n).split('.'));

const dataToDelete = [];

// Collect data
for (const [packId, ...setting] of keys) {
	if (ignoreIds.includes(packId)) continue;
	if (game.modules.get(packId)?.active) continue;
	const key = setting.length ? (packId + '.' + setting.join('.')) : packId;
	const data = settings.getItem(key);
	dataToDelete.push({ key, data, bytes: data?.length ?? 0, kb: Math.round((data?.length ?? 0) / 100) / 10, json: data?.[0] === "{" && data?.at(-1) === "}"});
}

if (dataToDelete.length === 0) return ui.notifications.info("No relevant client-side settings found.");

// Sort
dataToDelete.sort((a, b) => a.key.localeCompare(b.key));

const template = `
<form>
	<p style='background:#ffa887;padding:3px 10px;'><i class="fa-solid fa-triangle-exclamation"></i> These are NOT world specific settings. If you use any in other worlds, don't delete them!</p>
	<ul style='list-style:none;margin:0;padding:0;'>
	{{#each entries}}
		<li class='flexrow' style='align-items:center;flex-flow:nowrap;gap:0 0.3em;'>
			<label class='flexrow' style='align-items:center;flex-flow:nowrap;flex:2;'>
				<input type='checkbox' name='entries.{{key}}' checked>
				{{key}}
				</label>
			<span style='white-space:nowrap;flex:0;font-size:var(--font-size-11);'>({{kb}} kB)</span>
			{{#if json}}
			<textarea disabled readonly style='flex:3;'>{{data}}</textarea>
			{{else}}
			<input style='flex:3;' value='{{data}}' disabled readonly>
			{{/if}}
		</li>
	{{/each}}
	</ul>
</form>
`;

// Query user
const result = await Dialog.wait({
	title: 'Delete Client-side Settings',
	content: Handlebars.compile(template)({ entries: dataToDelete }),
	buttons: {
		delete: {
			label: 'Delete Selected',
			callback: (html) => new FormDataExtended(html.querySelector('form')).object,
		},
	},
	close: () => null,
},
{
	jQuery: false,
	width: 760,
});

if (result === null) return;

const deleteKeys = Object.entries(result)
	.filter(([_, enabled]) => enabled)
	.map(([k]) => k.slice("entries.".length));

if (deleteKeys.length == 0) return;

let deleted = 0, savings = 0;
for (let skey of deleteKeys) {
	savings += dataToDelete.find(({ key }) => key === skey)?.kb ?? 0;
	settings.removeItem(skey);
	deleted += 1;
}

ui.notifications.info(`Deleted ${deleted} client-side settings, saving at least ${savings} kB`);
