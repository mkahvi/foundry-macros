/**
 * Deletes world settings for modules that are not active.
 *
 * THIS CAN NOT BE UNDONE. MAKE BACKUP OF THE WORLD IF UNCERTAIN!
 *
 * TODO:
 * - Add confirmation dialog?
 */

// Module IDs to ignore
const ignoreIds = [];

// Always ignore core Foundry settings and game system settings
ignoreIds.push('core', game.system.id);

const settings = game.collections.get('Setting');

const keys = settings.map(s => s.key.split('.'));

let deleted = 0, savings = 0;
for (const [packId, ...setting] of keys) {
	if (ignoreIds.includes(packId)) continue;
	if (game.modules.get(packId)?.active) continue;
	const key = packId + '.' + setting.join('.');
	const s = settings.find(s => s.key == key);
	savings += JSON.stringify(s.toObject()).length;
	await s.delete();
	deleted += 1;
}

ui.notifications.info(`Deleted ${deleted} settings, saving at least ${Math.round(savings / 100) / 10} kB`);
