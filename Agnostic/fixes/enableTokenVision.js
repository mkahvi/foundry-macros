/**
 * Enable Token Vision for all tokens that have certain minimum player permission level.
 *
 * Compatibility:
 * - Foundry v10
 */

// Adjust current scene, too?
const adjustCurrentScene = true;
// Adjust all scenes.
const adjustAllScenes = false;
// Disable vision for everything else
const disableOthers = false;
// Minimum permission needed to enable vision
const minPerm = CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER;

const testPermissions = (actor) => {
	if (!actor) return false;
	const { ownership } = actor;
	if (ownership.default >= minPerm) return true;
	const perms = Object.entries(actor.ownership).filter(([uid, level]) => uid !== 'default' && !game.users.get(uid).isGM);
	return perms.some(([_, level]) => level >= minPerm);
}

// Stats
let prototypes = 0, tokens = 0;

const needUpdate = (actor, hasPerm = false, enabled = false) => {
	if (!hasPerm && !disableOthers) return false;
	const desiredState = hasPerm || !disableOthers;
	return enabled !== desiredState;
}

const actorUpdates = [];
for (const actor of game.actors) {
	const hasPerm = testPermissions(actor);
	const sightEnabled = actor.prototypeToken?.sight?.enabled ?? false;
	if (needUpdate(actor, hasPerm, sightEnabled)) {
		const desiredState = hasPerm || !disableOthers;
		actorUpdates.push({ _id: actor.id, prototypeToken: { sight: { enabled: desiredState } } });
	}
}

if (actorUpdates.length) {
	prototypes += actorUpdates.length;
	await Actor.updateDocuments(actorUpdates);
}

const sceneUpdates = [];

const fixSceneTokens = async (scene) => {
	if (!scene) return;

	const tokenUpdates = [];
	for (const token of game.scenes.viewed.tokens) {
		const hasPerm = testPermissions(actor);
		const sightEnabled = token.sight.enabled ?? false;
		if (needUpdate(actor, hasPerm, sightEnabled)) {
			const desiredState = hasPerm || !disableOthers;
			tokenUpdates.push({ _id: token.id, sight: { enabled: desiredState } });
		}
	}

	if (tokenUpdates.length) {
		tokens += tokenUpdates.length;
		return scene.update({ tokens: tokenUpdates });
	}
}

if (adjustAllScenes) {
	for (const scene of game.scenes)
		await fixSceneTokens(scene);
}
else if (adjustCurrentScene) {
	await fixSceneTokens(game.scenes.viewed);
}

if (sceneUpdates.length) {
	await Scene.updateDocuments(sceneUpdates);
}

ui.notifications.info(`All fixed; Updated: ${prototypes} prototypes, ${tokens} tokens.`);
