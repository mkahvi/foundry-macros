/**
 * Fixes actor data including keys with delete prefix (`-=key`).
 *
 * MAKE BACKUPS BEFORE RUNNING THIS
 * If used system or modules have bugs in update handling, this WILL trigger those bugs.
 * (system & module devs, if this breaks your shit, please fix your shit)
 *
 * Usage:
 * In dev console:
 * > game.macros.getName("fixActorInvalidKeys").execute({ actor: actorYouWantFixed });
 *
 * To clean all base actors:
 * > const m = game.macros.getName("fixActorInvalidKeys");
 * > for (let a of game.actors)
 * >   await m.execute({ actor:a })
 *
 * To clean all unlinked actors
 * > for (let s of game.scenes)
 * >  for (let a of s.tokens.map(t => t.actor).filter(a => a && !a.isLinked))
 * >    await m.execute({ actor: a })
 *
 * Executing it normally will fix actor for currently selected token.
 *
 * Passing `dry:true` in execute scope will only test what could be deleted but does not commit it.
 *
 * Compatibility (tested):
 * - Foundry v11.315
 */

// If true, print logging even if nothing needs to be done
const verbose = false;

let logged = false;
function log(...msgs) {
	if (!logged) {
		console.group('Actor:', actor.name, actor.uuid);
		logged = true;
	}
	console.log(...msgs);
}

let pending = false;
function removeBadKeys(obj, key = null, path = null) {
	const curpath = path || key !== null ? [path, key].filter(k => k !== null).join('.') : null;

	if (obj === null || obj === undefined) return obj;
	if (['bigint', 'number', 'string', 'boolean'].includes(typeof obj)) return obj;

	if (Array.isArray(obj)) return obj.map((e, idx) => removeBadKeys(e, idx, curpath));

	const data = foundry.utils.flattenObject(obj);
	const badKeys = Object.keys(data).filter(k => k.includes('-='));
	for (const bkey of badKeys) {
		log('-', curpath + '.' + bkey);
		pending = true;
		delete data[bkey];
	}

	for (const [skey, value] of Object.entries(data)) {
		if (value === undefined) {
			// This likely is only possible within arrays, or with datamodels
			log('-', [path, key, skey].filter(k => k !== null).join('.'), value);
			pending = true;
			delete data[skey];
		}
		else data[skey] = removeBadKeys(value, skey, curpath);
	}

	return foundry.utils.expandObject(data);
}

let updateData;
try {
	updateData = removeBadKeys(actor.toObject());
	// console.log(updateData);
	if (!pending) {
		if (verbose) log('No bad data found!');
	}
	else if (scope.dry === true) {
		if (verbose) log('+ Update skipped');
	}
	else {
		await actor.update(updateData, { diff: false, recursive: false });
		log('+ Updated!');
	}
}
catch (err) {
	console.error(err);
	console.error('- Failed!');
}
finally {
	console.groupEnd();
}
