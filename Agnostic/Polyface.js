/**
 * Polyface
 * Easily swap token appearance between pre-configured choices.
 *
 * Compatibility:
 * - Foundry v11
 *
 * Usage:
 * - Configuration: Hold shift
 * - Reset: Hold shift and control
 * - Select: Hold control
 * - Cycle: Default
 *
 * Configuration opens if the token hasn't been configured yet.
 *
 * TODO:
 * - Somehow preload the alternatives?
 */

// Always open face selector instead of cycling
const alwaysOpenSelector = false;

// Save configuration to actor. Otherwise saved on token.
const saveOnActor = true;

const faceConfigTemplate = `
<style>
.app.polyface ol.face-list {
	list-style: none;
	margin: 0;
	padding: 0;

	display: flex;
	flex-flow: column nowrap;

	gap: 3px;
}
.app.polyface ol.face-list input {
	height: 1.5em;
}
.app.polyface ol > li {
	gap: 3px;
}
.app.polyface ol > li > img {
	flex: 0 64px;
	width: 64px;
	height: 64px;
}
.app.polyface ol > li .sizing {
	gap: 3px;
}
.app.polyface ol > li .sizing {

}
.app.polyface ol > li .details {
	gap: 3px;
}
.app.polyface ol > li .controls {
	flex: 0 1rem;
	display: flex;
	flex-flow: column nowrap;
	justify-content: space-evenly;
}
.app.polyface ol > li .controls a.command .fa-fw {
	height: 1.25em;
	width: 1.25em;
	pointer-events: none;
}
.app.polyface ol > li .controls .disabled {
	opacity: 0.4;
}
.app.polyface ol > li input::placeholder {
	opacity: 0.6;
}
.app.polyface form.closing {
	cursor: wait;
}
.app.polyface form.closing > * {
	pointer-events: none;
}
</style>

<form autocomplete="off">
	<!-- FACES -->
	<ol class="face-list">
		{{#each faces}}
		<li class="flexrow face" data-index="{{@index}}">
			<img src="{{img}}" data-tooltip="{{img}}">
			<div class="flexcol details">
				<input type="text" name="faces.{{@index}}.img" value="{{img}}" {{disabled @first}}>
				<div class="sizing flexrow">
					<label>
						Width:
						<input type="number" class="width" name="faces.{{@index}}.w" value="{{w}}" placeholder="{{@root.default.w}}" step="1" min="1" max="5" {{disabled @first}}>
					</label>
					<label>
						Height:
						<input type="number" class="height" name="faces.{{@index}}.h" value="{{h}}" placeholder="{{@root.default.h}}" step="1" min="1" max="5" {{disabled @first}}>
					</label>
					<label>
						Scale:
						<input type="number" class="scale" name="faces.{{@index}}.s" value="{{s}}" placeholder="{{@root.default.s}}" min="0.2" max="3" {{disabled @first}}>
					</label>
				</div>
			</div>
			<div class="controls">
				<a class="command {{#if @first}}disabled{{/if}}" data-action="delete"><i class="fa-solid fa-trash-can fa-fw"></i></a>
				<a class="command {{#if (lt @index 2)}}disabled{{/if}}" data-action="shift" data-direction="up"><i class="fa-solid fa-angle-up fa-fw"></i></a>
				<a class="command {{#if (or @first @last)}}disabled{{/if}}" data-action="shift" data-direction="down"><i class="fa-solid fa-angle-down fa-fw"></i></a>
			</div>
		</li>
		{{/each}}
	</ol>

	{{#if faces.length}}
	<hr>
	{{/if}}

	<footer>
		<div class="form-group">
			<label>Add New Face:</label>
			<div class="form-fields">
				<input type="text" name="newface">
				{{filePicker type="image" target="newface"}}
			</div>
		</div>

		<div class="buttons flexrow">
			<button type="button" data-action="save">
				<i class="fa-solid fa-floppy-disk"></i>
				Save Configuration
			</button>
			<button type="button" data-action="delete">
				<i class="fa-solid fa-trash-can"></i>
				Delete Configuration
			</button>
		</div>
	</footer>
</form>
`;

const faceSelectTemplate = `
<style>
.app.dialog.face-select ol {
	list-style: none;
	padding: 0;
	margin: 0;

	display: flex;
	flex-flow: row wrap;
	gap: 3px;
} 
.app.dialog.face-select ol li img {
	flex: 0 64px;
	height: 64px;
	width: 64px;

	border: 3px solid black;
}
.app.dialog.face-select ol li img.current {
	border-color: goldenrod;
}
.app.dialog.face-select ol li img:hover {
	border-color: red;
}
</style>
<ol>
{{#each faces}}
	<li>
		<img data-index="{{@index}}" src="{{img}}" class="{{#if (eq @root.current @index)}}current{{/if}}">
	</li>
{{/each}}
</ol>
`;

/**
 * @param {TokenDocument} token
 */
function getOriginalFace(token) {
	const sourceData = token.actor?.prototypeToken ?? token;
	return {
		img: sourceData.texture.src,
		h: sourceData.height,
		w: sourceData.width,
		s: Math.max(sourceData.texture.scaleX, sourceData.texture.scaleY), // TODO
	};
}

function getCurrent(token) {
	return token.getFlag('world', 'polyface')?.current ?? 0;
}

/**
 * @param {TokenDocument} token
 * @returns {{faces:object[]}}
 */
function getConfig(token) {
	const doc = saveOnActor && token.actor ? token.actor : token;
	return doc.getFlag('world', 'polyface') ?? { faces: [] };
}

class FaceConfig extends FormApplication {
	constructor(object, options) {
		super(object, options);
		this.config = deepClone(getConfig(this.token));
	}

	/** @type {Token} */
	get token() {
		return this.object;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: 'Polyface',
			editable: true,
			classes: [...options.classes, 'polyface'],
			submitOnChange: true,
			closeOnSubmit: false,
			submitOnClose: false,
			width: 360,
			height: 'auto'
		};
	}

	getData() {
		const originalFace = getOriginalFace(this.token);

		return {
			token: this.token,
			faces: [originalFace, ...this.config.faces],
			default: {
				h: originalFace.h,
				w: originalFace.w,
				s: originalFace.s,
			}
		};
	}

	_deleteFace(event, idx) {
		if (!(idx >= 1)) throw new Error('OUT!');
		idx -= 1;
		console.log('Deleting', idx, this.config.faces[idx]?.img);

		const deleteFace = (idx) => {
			this.config.faces.splice(idx, 1);
			this.render();
		};

		if (event.shiftKey)
			return void deleteFace(idx);

		new Dialog({
			title: 'Delete Face',
			// TODO: Display the image also
			content: this.config.faces[idx].img,
			buttons: {
				keep: {
					label: 'Keep',
				},
				delete: {
					label: 'Delete',
					callback: () => deleteFace(idx),
				}
			},
			default: 'delete'
		},
		{
			jQuery: false,
		}).render(true);
	}

	_shiftFace(event, idx, direction) {
		if (!(idx >= 1 && ['up', 'down'].includes(direction))) throw new Error('OUT!');
		idx -= 1;

		const offset = direction === 'up' ? -1 : 1;
		/** @type {Object[]} */
		const faces = this.config.faces;
		const [face] = faces.splice(idx, 1);
		faces.splice(idx + offset, 0, face);
		this.render();
	}

	_onClickControl(event) {
		const el = event.target;
		if (el.tagName !== 'A' || !el.classList.contains('command')) return;
		event.preventDefault();

		if (el.classList.contains('disabled')) return;

		const idx = Number(el.closest('.face[data-index]').dataset.index);

		switch (el.dataset.action) {
			case 'delete':
				this._deleteFace(event, idx);
				break;
			case 'shift':
				this._shiftFace(event, idx, el.dataset.direction);
				break;
		}
	}

	async _onSave(event) {
		event.preventDefault();

		this.form.disabled = true;
		this.form.classList.add('closing');

		// Cleanup
		this.config.faces.forEach(face => {
			if (!Number.isFinite(face.w)) delete face.w;
			if (!Number.isFinite(face.h)) delete face.h;
			if (!Number.isFinite(face.s)) delete face.s;
		});

		const doc = saveOnActor ? this.token.actor : this.token;
		if (this.config.faces.length == 0)
			await doc.unsetFlag('world', 'polyface');
		else
			await doc.setFlag('world', 'polyface', deepClone(this.config));

		this.close();
	}

	async _onReset(event) {
		event.preventDefault();

		this.form.disabled = true;
		this.form.classList.add('closing');

		const doc = saveOnActor ? this.token.actor : this.token;
		await doc.unsetFlag('world', 'polyface');
		this.close();
	}

	/**
		 * @param {JQuery<HTMLElement>} jq
		 */
	activateListeners(jq) {
		super.activateListeners(jq);

		this.form.querySelectorAll('.controls').forEach(el => {
			el.addEventListener('click', this._onClickControl.bind(this));
		});

		this.form.querySelector('footer .buttons button[data-action="save"]')
			.addEventListener('click', this._onSave.bind(this));

		this.form.querySelector('footer .buttons  button[data-action="delete"]')
			?.addEventListener('click', this._onReset.bind(this));
	}

	_renderInner(data) {
		const content = Handlebars.compile(faceConfigTemplate)(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
		const div = document.createElement('div');
		div.innerHTML = content;
		this.form = div.querySelector('form');
		return $(div.childNodes); // Foundry likes jquery
	}

	_updateObject(event, formData) {
		formData = expandObject(formData);

		this.config.faces = [];

		if (formData.faces) {
			this.config.faces = Object.values(formData.faces);
		}

		if (formData.newface) {
			// Add face
			this.config.faces.push({ img: formData.newface });
		}

		this.render();
	}
}

class FaceSelectDialog extends Dialog {
	constructor(data, opts) {
		const token = data.token;
		delete data.token;
		super(data, opts);
		this.token = token;
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		const [html] = jq;

		html.querySelectorAll('img').forEach(el => {
			el.addEventListener('click', ev => {
				assumeFace(this.token, Number(ev.target.dataset.index));
				this.close();
			});
		});
	}
}

/**
 * @param {TokenDocument} token
 */
function openConfig(token) {
	console.log('Configuring Faces');
	new FaceConfig(token).render(true);
}

/**
 * @param {TokenDocument} token
 * @param {number} face - Face index
 * @returns {Promise}
 */
async function assumeFace(token, face) {
	const { faces: altFaces } = getConfig(token);
	const originalFace = getOriginalFace(token);
	const faces = [originalFace, ...altFaces];
	const newFace = faces[face];
	if (!newFace) throw new Error(`Index ${face} not found for faces`);

	const updateData = {
		texture: {
			src: newFace.img,
			scaleX: newFace.s ?? originalFace.s,
			scaleY: newFace.s ?? originalFace.s
		},
		width: newFace.w ?? originalFace.w,
		height: newFace.h ?? originalFace.h,
		flags: { world: { polyface: { current: face } } }
	};

	console.log('Setting face:', face, newFace);

	return token.update(updateData);
}

/**
 * @param {TokenDocument} token
 */
function faceSelect(token) {
	console.log('Select a face');
	const { faces: altFaces } = getConfig(token);
	const faces = [getOriginalFace(token), ...altFaces];

	const data = {
		faces,
		current: getCurrent(token),
	};

	const content = Handlebars.compile(faceSelectTemplate)(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	new FaceSelectDialog({
		title: 'Select New Face',
		content,
		token,
		buttons: {},
	},
	{
		jQuery: false,
		rejectClose: false,
		classes: [...Dialog.defaultOptions.classes, 'face-select'],
		height: 'auto',
		width: 'auto',
	}).render(true);
}

/**
 * @param {TokenDocument} token
 */
function cycleFace(token) {
	const { faces } = getConfig(token);
	const current = getCurrent(token);
	const newTexIdx = (current + 1) % (faces.length + 1);
	return assumeFace(token, newTexIdx);
}

// Triage User Interaction
if (game.canvas.tokens.controlled.length !== 1) return void ui.notifications.warn('Select the token you wish to control.');

const tokenDoc = token.document;

if (saveOnActor && !tokenDoc?.actor) throw new Error('Token has no actor');

const { faces } = getConfig(tokenDoc);

// Reset face
if (faces.length && event.shiftKey && event.ctrlKey) return assumeFace(tokenDoc, 0);
// Open config
else if (!faces.length || event.shiftKey) return openConfig(tokenDoc);
// Open face selector
else if (alwaysOpenSelector || event.ctrlKey) return faceSelect(tokenDoc);
// Cycle face
else cycleFace(tokenDoc);
