/**
 * Missing Modules
 *
 * Displays a dialog with list of modules that have been enabled but are not installed.
 */

const modules = game.settings.get('core', 'moduleConfiguration');

const template = `
<div class='flexcol'>
<ul>
{{#if modules.length}}
{{#each modules}}
<li style='font-style:monospace;'>{{id}}</li>
{{/each}}
{{else}}
<p class='hint'>No missing modules.</p>
{{/if}}
</ul>
</div>
`;

const templateData = {
	modules: [],
}

Object.entries(modules)
	.filter(([key, state]) => state && !game.modules.get(key))
	.forEach(([key, _]) => templateData.modules.push(key));

const content = Handlebars.compile(template)(templateData);

Dialog.prompt({
	content,
	title: 'Missing Modules',
	buttons: {},
});
