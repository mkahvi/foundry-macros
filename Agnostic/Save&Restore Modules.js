/**
 * Save & Restore Modules
 *
 * Save list of active modules into an exported JSON that can be drag&dropped into the dialog for easy import in same or other worlds.
 *
 * Compatibility:
 * - Foundry v10 & v11
 */

/* --- Configuration --- */

// Export file name .json is appended
const exportFileName = `module-configuration-${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()}-${game.world.id}`;

/* --- Translations --- */

const i18n = {
	title: {
		initial: 'Import or Export Enabled Module Configuration',
		reload: 'Reload required',
		confirm: 'Enable Modules'
	},
	text: {
		reload: '<h3>Reload now?</h3><p>Please inform other players to reload also.',
	},
	tooltip: {
		enabled: 'Already enabled',
		notfound: 'Not installed'
	},
	instructions: {
		initial: 'Drop exported module configuration JSON here to import it.',
	}
}

/* --- Actual Code --- */

const getModuleCfg = () => game.settings.get('core', 'moduleConfiguration');
const setModuleCfg = async (cfg) => game.settings.set('core', 'moduleConfiguration', cfg);

const importModules = (event) => {
	const data = event.target.result;
	const modules = JSON.parse(data);

	Object.entries(modules).forEach(([moduleId, enable]) => {
		const mod = game.modules.get(moduleId);
		modules[moduleId] = {
			id: moduleId,
			enable: mod ? enable || !!mod?.active : false,
			active: mod?.active ?? false,
			found: !!mod,
			notfound: !mod,
			get disabled() { return this.active || this.notfound; },
			get tooltip() {
				if (this.active) return i18n.tooltip.enabled;
				if (this.notfound) return i18n.tooltip.notfound;
				return null;
			},
			label: mod?.title ?? `[Not Found: ${moduleId}]`
		};
	});

	/* eslint-disable no-irregular-whitespace */
	const content = `<form style='align-items: center; display: grid; grid-template-columns: 24px 1fr;'>
	<span style='background-color:rgba(0,0,0,0.05); padding:2px 0.3rem;'> </span>
	<label style='background-color:rgba(0,0,0,0.05);padding:2px 0.5rem;font-weight:bold;'>Label</label>
{{#each modules}}
<div style='display:contents;'>
	<input id='mangotango-module-enable-{{id}}' class='toggle' style='margin:2px 0.3rem;padding:0;width:16px;' type='checkbox' name='{{id}}' {{checked enable}} {{#if disabled}}disabled data-tooltip='{{tooltip}}'{{/if}}>
	<label for='mangotango-module-enable-{{id}}' class='label' style='padding:2px 0.5rem;' data-tooltip='{{id}}'>{{label}}</label>
</div>
{{/each}}
	</form><hr>`;

	new Dialog({
		title: i18n.title.confirm,
		content: Handlebars.compile(content)({ modules, i18n }, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		buttons: {
			import: {
				label: i18n.title.confirm,
				callback: (html) => {
					const formDataEx = new FormDataExtended(html.querySelector('form'));
					const formData = game.release.generation >= 10 ? formDataEx.object : formDataEx.toObject();

					const newCfg = { ...getModuleCfg() };
					let reloadNeeded = false;
					Object.entries(formData).forEach(([key, value]) => {
						if (value && newCfg[key] !== value) {
							reloadNeeded = true;
							newCfg[key] = true;
						}
					});

					console.log('Enabling Modules:\n', Object.entries(formData).filter(([key, enable]) => enable).map(([key, _]) => key));

					if (!reloadNeeded) {
						ui.notifications.info('No modules enabled, no reload needed.');
						return;
					}

					console.log('Saving configuration:', newCfg);
					setModuleCfg(newCfg)
						.then(_ => {
							if (game.release.generation >= 10)
								SettingsConfig.reloadConfirm({ world: true });
							else
								Dialog.prompt({
									title: i18n.title.reload,
									content: i18n.text.reload + '<hr>',
									callback: () => window.location.reload()
								})
						});
				}
			}
		},
		default: 'import'
	}, {
		jQuery: false,
		width: 460,
	}).render(true)
}

class ImportDialog extends Dialog {
	constructor() {
		const data = {
			content: null,
			buttons: {
				export: {
					label: 'Export',
					callback: () => {
						console.log('Exporting enabled module configuration...');
						const modules = getModuleCfg();
						Object.keys(modules).forEach(id => {
							if (!modules[id]) delete modules[id];
						});
						saveDataToFile(JSON.stringify(modules, null, 2), 'application/json', `${exportFileName.trim()}.json`);
					}
				}
			},
			default: 'export',
		};

		data.dragDrop = [{ dragSelector: null, dropSelector: null }];

		super(data);
	}

	static get defaultOptions() {
		return {
			...super.defaultOptions,
			title: i18n.title.initial,
			dragDrop: [{ dragSelector: null, dropSelector: '.dialog-content' }],
			jQuery: false,
		}
	}

	getData() {
		const context = super.getData();
		context.content = `
		<div style="border: 1px solid #777; border-radius: 0.5rem;padding: 0.5rem; background-color: rgba(0,0,0,0.05);">
		${i18n.instructions.initial}
		</div>
		<hr>
		`;
		return context;
	}

	_canDragDrop() { return true; }

	/**
	 * @param {DragEvent} event
	 */
	async _onDrop(event) {
		for (const f of event.dataTransfer.files) {
			if (f.type !== 'application/json') continue;
			const reader = new FileReader();
			reader.onload = importModules;
			reader.readAsText(f);
		}
		this.close();
	}

	static open = () => new ImportDialog().render(true);
}

ImportDialog.open();
