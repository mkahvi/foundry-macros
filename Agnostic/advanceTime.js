/**
 * Advances World Time.
 *
 * Also optionally:
 * - Advances combat time (only if forward progressing time), if combat time is configured in Foundry. Enabled by default.
 * - Expires effects on combatants. Disabled by default, requires system specific configuration below.
 *
 * Compatibility:
 * - Foundry v10
 */

// User configuration
const SETTINGS = {
	unit: 'minute', // 'second', 'minute', 'hour' ... needs to match key in TIME
	quantity: 1,
	commonChoices: {
		8: 'Basic rest hours',
		11: 'Advanced rest hours',
		30: '×½',
		60: '×1',
		180: '×3',
		300: '×5',
		600: '×10'
	},
	printCard: true, // Print simple chat card about passed time
	progressCombat: true, // Equivalent number of rounds will be passed in combat (unless it ends combat)
	progressOnlyActiveCombat: true, // If false, all combats are progressed
	expireCombatants: true, // Explicitly calls for effects to end on combatants if progressCombat is enabled
	combatTimeRounding: Math.floor, // Function to use for figuring out how many rounds to progress in combat (recommended variations: Math.floor, Math.ceil, Math.round)
};

// This needs to be adjusted for expireCombatants setting to work
const expireCombatantEffects = (actor) => {
	// actor.expireActiveEffects(); // PF1
};

const EXPIRE_COMBATANTS_CONFIGURED = false; // set to true once the above function is configured

// Translations
const LANG = {
	title: 'Advance Time',
	seconds: 'Seconds',
	minutes: 'Minutes',
	hours: 'Hours',
	advanced: '{quantity} {units}', // Time advanced
	combat: '{rounds} rounds',
	progressCombats: 'Progress Combats',
	expireCombatants: 'Expire Combatants',
	cardSpeaker: "Time"
};

// Time units
const TIME = {
	second: {
		label: LANG.seconds,
		scale: 1,
	},
	minute: {
		label: LANG.minutes,
		scale: 60,
	},
	hour: {
		label: LANG.hours,
		scale: 60 * 60,
	}
};

const DATA = {
	listId: 'advance-time-dialog-common-options',
};

const template = `
<form autocomplete='off'>
	<div class="form-group" style="gap:3px;">
		<input type="number" data-dtype="Number" step="1" name="quantity" value="{{quantity}}" list="{{listId}}" required>
		<div class="form-fields">
			<select name="scale" required>
			{{#select timeScaleDefault}}
			{{#each timeScale}}
				<option value='{{@key}}'>{{label}}</option>
			{{/each}}
			{{/select}}
			</select>
		</div>
	</div>
	{{#if canProgressCombat}}
	<div class="form-group" style="gap:3px;">
		<label class='flexrow' style='align-items:center;'>
			<input type='checkbox' name='combat' data-dtype='Boolean' {{checked combat}}>
			{{LANG.progressCombats}}
			<span class='combatRoundDisplay'></span>
		</label>
	</div>
	{{/if}}
	{{#if canExpireCombatants}}
	<div class="form-group" style="gap:3px;">
		<label class='flexrow' style='align-items:center;'>
			<input type='checkbox' name='combatants' data-dtype='Boolean' {{checked combatants}}>
			{{LANG.expireCombatants}}
		</label>
	</div>
	{{/if}}
	<datalist id='{{listId}}'>
	{{#each commonChoices}}
		<option value='{{@key}}'>{{this}}</options>
	{{/each}}
	</datalist>
	<hr>
</form>
`;

/** --- Actual Script --- */

class AdvanceTimeDialog extends Dialog {
	static open(quantity = 60, unit = 'second') {
		if (!game.user.isGM) return void ui.notifications.warn('Only GM can advance time!');

		const haveCombatsToProgress = SETTINGS.progressOnlyActiveCombat ? !!game.combat : game.combats.size > 0;

		const templateData = {
			quantity,
			listId: DATA.listId,
			commonChoices: SETTINGS.commonChoices,
			timeScale: TIME,
			timeScaleDefault: SETTINGS.unit,
			LANG,
			combat: SETTINGS.progressCombat,
			combatants: SETTINGS.expireCombatants,
			canExpireCombatants: EXPIRE_COMBATANTS_CONFIGURED,
			canProgressCombat: CONFIG.time.roundTime > 0 && haveCombatsToProgress,
		};

		const content = Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

		const buttons = {
			advance: {
				label: LANG.title,
				icon: '<i class="fas fa-hourglass-half"></i>',
				callback: html => {
					const data = expandObject(new FormDataExtended(html.querySelector('form')).object);
					data.seconds = data.quantity * TIME[data.scale].scale;
					return data;
				},
			},
		};

		/**
	 * @param {HTMLElement} html
	 */
		const onRender = (html) => {
			const rd = html.querySelector('.combatRoundDisplay'),
				combat = html.querySelector('input[name="combat"]'),
				combatants = html.querySelector('input[name="combatants"]');

			if (!rd) return;

			const qi = html.querySelector('input[name="quantity"]'),
				si = html.querySelector('select[name="scale"]');

			let q = parseInt(qi.value),
				u = si.value;

			const updateRounds = () => {
				const seconds = q * TIME[u].scale,
					combatToggle = seconds > 0;
				if (combatToggle) {
					const rounding = SETTINGS.combatTimeRounding,
						rounds = rounding(seconds / CONFIG.time.roundTime);
					rd.textContent = ` (${LANG.combat.replace('{rounds}', signNum(rounds))})`;
				}
				else {
					rd.textContent = '';
				}
				if (combat) combat.disabled = !combatToggle;
				if (combatants) combatants.disabled = !combatToggle;
			};

			qi.addEventListener('change', ev => {
				q = ev.target.valueAsNumber;
				updateRounds();
			});

			si.addEventListener('change', ev => {
				u = ev.target.valueAsNumber;
				updateRounds();
			});

			updateRounds();
		};

		return AdvanceTimeDialog.wait(
			{
				title: LANG.title,
				content,
				buttons,
				render: onRender,
				default: 'advance',
				close: _ => null,
			},
			{
				width: 360,
				jQuery: false,
			}
		);
	}
}

const signNum = (v) => Number.isFinite(v) ? v < 0 ? `${v}` : `+${v}` : v;

/**
 * @param {Combat} combat 
 * @param {number} rounds 
 * @param {object} rv 
 */
const updateCombat = async (combat, rounds, rv) => {
	if (!combat) return;
	await combat.update({ round: combat.round + rounds });

	if (!rv.combatants) return;

	for (const combatant of combat.combatants) {
		const actor = combatant.actor;
		if (!actor) continue;
		expireCombatantEffects(actor);
	}
};

const rv = await AdvanceTimeDialog.open(SETTINGS.quantity, SETTINGS.unit);

if (rv && rv.seconds != 0) {
	const scale = TIME[rv.scale];
	rv.units = scale?.label ?? '???';
	const labe = scale?.label ?? '???';

	const msg = LANG.advanced.replace(/{(\w+)}/g, (_, v) => signNum(rv[v]));
	ui.notifications.info(msg);

	await game.time.advance(rv.seconds);

	if (SETTINGS.printCard) {
		ChatMessage.create({
			content: `<h3 style="border:none;margin:0;padding:0;">${msg}</h3>`,
			speaker: { alias: LANG.cardSpeaker }
		});
	}

	if (!rv.combat) return;

	const rounding = SETTINGS.combatTimeRounding;
	const rounds = rounding(rv.seconds / CONFIG.time.roundTime);

	if (!(rounds > 0)) return;

	if (rv.combat) ui.notifications.info(LANG.combat.replace('{rounds}', signNum(rounds)));

	const combats = SETTINGS.progressOnlyActiveCombat ? [game.combat] : game.combats;
	for (const combat of game.combats)
		updateCombat(combat, rounds, rv);
}
