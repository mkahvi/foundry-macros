const srect = canvas.scene.dimensions.sceneRect;

const isOutside = (w) => {
	const [x0, y0, x1, y1] = w.c;
	return !srect.contains(x0, y0) || !srect.contains(x1, y1);
}

const outsideWalls = scene.walls.filter(isOutside);

scene.deleteEmbeddedDocuments('Wall', outsideWalls.map(w => w.id));
