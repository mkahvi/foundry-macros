// Finds all scenes in which the actor of the selected tokens are in.

// Compatibility: Untested, should be compatible with both 0.7 and 0.8.

{
	const actors = canvas.tokens.controlled
		.filter((o) => o.actor) // has actor
		.map(o => o.actor); // map to actor

	const actorIDs = actors
		.reduce((arr, o) => [...arr, o._id], []);

	if (actorIDs.length === 0) {
		console.log('No valid actors selected.');
		return;
	}

	if (game.scenes.entities.length === 0) {
		console.log('No valid scenes found.');
		return;
	}

	const inScenes = [];
	for (var scene of game.scenes) {
		const matches = scene.data.tokens.filter(o => actorIDs.includes(o.actorId));
		if (matches.length > 0) inScenes.push(scene);
	}

	if (inScenes.length > 0) {
		console.log('Selected actors (' + actors.map((o) => o.name).join(', ') + ') found in scenes:');
		inScenes.forEach((scene) => console.log(' - ' + scene.name));
	}
	else {
		console.log('Selected actors not found in scenes. How odd.');
	}
}
