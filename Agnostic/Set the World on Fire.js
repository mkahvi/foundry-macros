const img = 'icons/magic/fire/flame-burning-campfire-orange.webp';
const dist = canvas.scene.grid.distance;
const dim = dist * 8; // 8 times scene grid size (e.g. 40 ft with 5ft or 12 m with 1.5m)
const bright = dist * 4; // 4 times scene grid size
const color = '#e47049';
const animation = { type: 'flame', speed: 5, intensity: 2 };
const scale = 0.3;

// Get pointer position and shift it so token center is at mouse position
let { x, y } = canvas.mousePosition;
const halfGrid = canvas.grid.size / 2;
x -= halfGrid;
y -= halfGrid;

// Create fire
TokenDocument.implementation.create({
	x,
	y,
	texture: { src: img, scaleX: scale, scaleY: scale },
	alpha: 0.5,
	light: { bright, dim, color, animation, coloration: AdaptiveIlluminationShader.SHADER_TECHNIQUES.LOW_ABSORPTION.id, luminosity: 1 },
	name: 'Flame!',
	disposition: CONST.TOKEN_DISPOSITIONS.SECRET,
	displayName: CONST.TOKEN_DISPLAY_MODES.NONE,
	displayBars: CONST.TOKEN_DISPLAY_MODES.NONE,
	sight: { enabled: false },
	prependAdjective: false,
},
{
	parent: canvas.scene
});
