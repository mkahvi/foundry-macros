/**
 * Create New Combatant
 *
 * Creates simple combatant not tied to any actor or token.
 *
 * Useful for tracking entries in combat. Or things that don't represent an actor.
 */

function addCombatant(html, event) {
	const formData = new FormDataExtended(html.querySelector('form')).object;
	if (!formData.name || !Number.isFinite(formData.initiative)) return void ui.notifications.error('Need name and initiative!');
	Combatant.create(formData, { parent: game.combats.viewed });
}

await Dialog.wait({
	content: `<form autocomplete='off'>
	<label>
		Name:
		<input type='text' name='name' placeholder='Name' required>
	</label>
	<label>
		Initiative:
		<input type='number' name='initiative' placeholder='Initiative' step='0.1' required>
	</label>
	</form>
	<hr>`,
	buttons: {
		add: {
			label: 'Add',
			callback: addCombatant,
		}
	}
},
{
	jQuery: false
});
