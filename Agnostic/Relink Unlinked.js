/**
 * Relink Unlinked
 *
 * This copies data from unlinked token back to source actor and links them, and finally cleans out the overrides on the token.
 *
 * The purpose of this is in case you mistakenly have used unlinked token and want to keep using said actor.
 *
 * FOR SAFETY: Copypaste the token so you have backup (select token, ctrl+c it, and ctrl+v nearby).
 * 
 * Compatibility:
 * - Foundry v11
 */

if (!(game.release.generation >= 11)) return ui.notifications.error("You need at least Foundry v11 to use this macro.");

/**
 * @typedef {Token} token
 * @instance
 */

if (!token) return ui.notifications.error('No token selected'); // No token selected

const tokenDoc = token.document;
if (tokenDoc.actorLink) return ui.notifications.error("Token already linked");
if (!tokenDoc.actor) return ui.notifications.error("Token has no actor");

const actorData = tokenDoc.actor.toObject(); // Synthetic actor in its entirety

// Get source actor
const sourceActor = tokenDoc.baseActor;
if (!sourceActor) return ui.notifications.error('Source actor missing'); // Shouldn't happen

const rv = await Dialog.confirm({
	title: 'Relink Actor Data',
	content: `<p>Overwriting actor<br><em>${sourceActor.name}</em> <small>[${sourceActor.id}]</small><br>
	... with data from token.</p>
	<p style="color:darkred;">THIS CAN NOT BE UNDONE!</p>
	<p>Are you sure you wish to proceed?</p>`,
});

if (rv !== true) return; // Cancelled

// Update source actor with data from token
actorData.prototypeToken.actorLink = true;
await sourceActor.update(actorData, { diff: false, recursive: false });

// Link Token
await tokenDoc.update({ actorLink: true });

// Zero actor delta
await tokenDoc.delta.restore();
