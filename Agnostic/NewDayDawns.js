/**
 * Simple script to print "nice" new day message after querying which one it is.
 */


// Find and increment previous dawning
let initial = game.messages.contents.findLast(cm => cm.flags?.world?.dawning)?.flags?.world?.dawning;
if (initial >= 0) initial += 1;
else initial = "";

// Query for new day
const day = await Dialog.wait({
	content: `<input autofocus style='text-align:center;' type='number' name='day' placeholder='Day number' step='1' min='1' value='${initial}' required><hr>`,
	buttons: {
		msg: {
			label: "Post",
			callback: (html) => html.querySelector('[name="day"]').valueAsNumber,
		}
	},
	default: 'msg',
	close: () => NaN,
},
	{
		jQuery: false,
		width: 180,
		height: 'auto',
	});

if (!Number.isFinite(day)) return;

ChatMessage.create({
	content: `<h3 style='display:flex;align-items:center;gap:0.3em;margin:0;border:none;'><img src='icons/svg/sun.svg' height=32 width=32 style='flex:0 32px;border:none;filter:invert(1);'> Day <b>${day}</b> Dawns</h3>`,
	speaker: {
		alias: "Time",
	},
	flags: {
    world: {
      dawning: day
    }
  }
});
