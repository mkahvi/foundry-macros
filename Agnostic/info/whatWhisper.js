/**
 * Displays who a message was whispered to if any after you click a message.
 */

function whozat(cm) {
	const whispers = cm.whisper.map(id => game.users.get(id).name);
	if (whispers.length)
		ui.notifications.info(`Whispered to ${whispers.join(', ')}`);
	else
		ui.notifications.info('Not a whisper');
}

let mid;
function whamsg(event) {
	ui.notifications.remove(mid);
	const el = event.target;
	const cmEl = el.closest('.chat-message[data-message-id]');
	const msg = game.messages.get(cmEl?.dataset.messageId);
	if (msg) whozat(msg);
	else ui.notifications.warn('Not a chat message');
}

// Keeps catching the macro execution click without this delay
setTimeout(() => {
	mid = ui.notifications.info('Click a chat message pls');
	document.body.addEventListener('click', whamsg, { once: true, passive: true });
}, 50);
