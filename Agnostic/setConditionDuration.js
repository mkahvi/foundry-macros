/**
 * Set Condition Duration
 *
 * Alters enabled condition to have specified duration.
 *
 * Compatibility (tested):
 * - Foundry 11.313
 */

const options = {
	convertToSeconds: false, // Convert rounds to seconds
};

const i18n = {
	seconds: 'Seconds',
	rounds: 'Rounds',
	infinity: 'Infinity',
	editTitle: 'Edit Effect: {name}',
	apply: 'Apply',
	select: 'Select',
	active: 'Active',
	selectTitle: 'Select Condition from {name}',
	conditionLabel: 'Condition(s)',
	updated: 'Active Effect "{name}" Updated',
	noActor: 'No actor selected',
	roundsInline: 'rnds',
	secondsInline: 'secs',
};

// Disable conversion if impossible
if (CONFIG.time.roundTime == 0)
	options.convertToSeconds = false;

const knownEffects = CONFIG.statusEffects.reduce((all, s) => {
	all[s.id] = game.i18n.localize(s.label);
	return all;
}, {});

/**
 * @param {ActiveEffect} ae
 */
const queryEffectSetup = async (ae) => {
	const conditionIds = Array.from(ae.statuses);

	const condLabels = conditionIds.map(cid => knownEffects[cid] || cid);

	const template = `
	<form autocomplete='off'>
		<p>{{labels.conditionLabel}}: {{conditions}}</p>
		<div class='form-group'>
			<label>{{labels.seconds}}</label>
			<div class='form-fields'>
				<input type='number' name='duration.seconds' placeholder='{{labels.infinity}}' min='0' step='1' value='{{effect.duration.seconds}}'>
			</div>
		</div>
		<div class='form-group'>
			<label>{{labels.rounds}}</label>
			<div class='form-fields'>
				<input type='number' name='duration.rounds' placeholder='{{labels.infinity}}' min='0' step='1' value='{{effect.duration.rounds}}'>
			</div>
		</div>
		<div class='form-group'>
			<label></label>
			<div class='form-fields'>
				<label style='display:flex;flex:1' class='checkbox'>
					<input name='disabled' type='checkbox' {{checked (not effect.disabled)}}>
					{{labels.active}}
				</label>
			</div>
		</div>
	</form>
	<hr>
`;

	const templateData = {
		labels: i18n,
		effect: ae,
		conditions: condLabels.join(', '),
	};

	const content = Handlebars.compile(template)(templateData);

	return Dialog.wait({
		content,
		title: i18n.editTitle.replace('{name}', ae.name),
		buttons: {
			apply: {
				label: i18n.apply,
				callback: (html) => {
					const data = new FormDataExtended(html.querySelector('form')).object;
					data.disabled = !data.disabled;
					return data;
				}
			}
		},
		render: (html) => {
			if (options.convertToSeconds) {
				const seconds = html.querySelector('input[name="duration.seconds"]');
				const rounds = html.querySelector('input[name="duration.rounds"]');
				const updateSeconds = (ev) => {
					const newRounds = parseFloat(ev.target.value) * CONFIG.time.roundTime;
					seconds.placeholder = Number.isNaN(newRounds) ? i18n.infinity : newRounds;
				}
				rounds.addEventListener('change', updateSeconds, { passive: true });
				rounds.addEventListener('input', updateSeconds, { passive: true });
			}
		},
		default: 'apply',
		close: () => null,
	},
	{
		classes: [...Dialog.defaultOptions.classes, 'condition-duration-editor-edit-dialog'],

		jQuery: false,
		rejectClose: false,
	});
};

const selectCondition = async (actor) => {
	const template = `
	<form autocomplete='off'>
	<style>
	.condition-duration-editor-select-dialog .condition-item.disabled {
		opacity: 0.7;
	}
	.condition-duration-editor-select-dialog .condition-item label {
		flex: 1;
		display: inline-flex;
		gap: 3px;
	}
	.condition-duration-editor-select-dialog .dialog-content ul {
		list-style:none;
		padding:0;
	}
	.condition-duration-editor-select-dialog .dialog-content ul li input {
		flex:0;
		top:unset;
	}
	.condition-duration-editor-select-dialog .dialog-content ul li span.label {
		flex: 1;
	}
	.condition-duration-editor-select-dialog .dialog-content ul li span.duration {
		flex:0;
		white-space:nowrap;
	}
	</style>
	<ul>
	{{#each effects}}
		<li class='condition-item flexrow {{#if active}}active{{else}}disabled{{/if}}' {{#unless active}}data-tooltip="Disabled"{{/unless}}>
			<label>
				<input type='radio' name='id' value='{{id}}'>
				<span class='label'>{{label}}</span>
				<span class='duration'>[{{#if duration.seconds}}{{duration.seconds}}{{else}}–{{/if}} {{@root.labels.secondsInline}}]</span>
				<span class='duration'>[{{#if duration.rounds}}{{duration.rounds}}{{else}}–{{/if}} {{@root.labels.roundsInline}}]</span>
			</label>
		</li>
	{{/each}}
	</ul>
	{{#unless effects.length}}
	<p class='info'>No conditions found.</p>
	{{/unless}}
	</form>
	<hr>
`;

	const effects = actor.effects.map(ae => {
		const statuses = Array.from(ae.statuses);
		const labels = statuses.map(s => knownEffects[s] || s).join(', ');
		const label = ae.name === labels ? ae.name : `${ae.name} (${labels})`;
		return { id: ae.id, statuses, duration: ae.duration, label, active: !ae.disabled };
	}).filter(aed => aed.statuses.length > 0);

	const templateData = {
		effects,
		labels: i18n,
	};

	const content = Handlebars.compile(template)(templateData);

	const result = await Dialog.wait({
		content,
		title: i18n.selectTitle.replace('{name}', actor.name),
		buttons: {
			select: {
				label: i18n.select,
				callback: (html) => new FormDataExtended(html.querySelector('form')).object
			}
		},
		default: 'select',
		close: () => null,
	},
	{
		classes: [...Dialog.defaultOptions.classes, 'condition-duration-editor-select-dialog'],
		jQuery: false,
		rejectClose: false,
	});

	return actor.effects.get(result?.id);
};

async function handleActor(actor) {
	if (!actor) return ui.notifications.warn(i18n.noActor);

	const effect = await selectCondition(actor);
	if (!effect) return void console.log('No condition selected');

	let updateData = await queryEffectSetup(effect);
	if (!updateData) return void console.log('No duration set');
	updateData = foundry.utils.expandObject(updateData);

	if (options.convertToSeconds && updateData.duration.seconds == null) {
		updateData.duration.seconds = updateData.duration.rounds * CONFIG.time.roundTime;
		updateData.duration.rounds = null;
	}

	effect.update(updateData)
		.then(_ => ui.notifications.info(i18n.updated.replace('{name}', effect.label)));
}

handleActor(token?.actor);
