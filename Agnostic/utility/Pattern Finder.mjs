/**
 * Pattern Finder
 * by MKAhvi (discord: manaflower)
 * License: MIT
 * URL: https://gitlab.com/mkahvi/foundry-macros/-/blob/master/Agnostic/utility/Pattern%20Finder.mjs
 *
 * This is simplistic brute force pattern finder that attempts to solve a progression table.
 *
 * Originally meant for Pathfinder 1e, but does not depend on it.
 *
 * USAGE:
 * 1) Fill in the "Expected" line with your progression table.
 * 2) Observe the proof below and if it is witin expectations, copy the output formula if so.
 *
 * INTERFACE:
 * - Expected:   Progression table.
 * - Reference:  If provieded, `@level` in output formula is replaced with this, has no impact on anything else. Can be ignored.
 * - Increments: Visualization of how the progression increments, displaying the distance between numbers.
 *               Likely equal to the divisor used.
 * - Notes:      Debug notes on what happened to find the formula. Developer use only, really.
 *               - Sets is number of distinct increment lengths found.
 *               - Passes is how many different permutations were tried (usually 2-3).
 *               - Div Case # is pure debug information on which division route the code went through and when.
 *               - Partial equidistancy indicates it attempted to consolidate inequal increment distances.
 *               - Full equidistancy indicates no consolidation was needed.
 *               - Ignored start indicates initial value was put aside for separate consideration as special case.
 *
 * TODO:
 * - Consider recording and comparing against known patterns (e.g. anything that expects 3/4 progression).
 * - Merge ignore start determination so the extra forced pass can be eliminated.
 * - Permutations: don't just stack things on top, permutate.
 *
 * Compatibility:
 * - Foundry v10
 *
 * Revision: 2 (2023-07-10)
 */

// ----- CONFIGURATION -----

const options = {
	incremental: true, // Disabling this is not supported for now.
	paintOnFocus: true, // Paint input box contents on focus
	resetButton: false, // Display reset button
	copyToClipboard: true, // If true, clicking output formula will copy it to clipboard directly

	// Developer options
	dev: {
		verbose: false, // Verbose output
		prefills: false, // Enable prefilled test case access (meant for development of this script only)
		allResults: false, // Allow showing all results.
		sortBySuccess: false, // If false, results are in order of discovery. If true, they're sorted by correctness.
	},
}

const lang = {
	reference: 'Reference', // variable used to represent progression
	output: 'Output', // Output formula
	increments: 'Increments',
	expected: 'Expected', // User input value for what's expected at a level
	proof: 'Proof', // Proof and validation of the formula at each level.
	notes: 'Notes',

	pass: 'Pass',

	genericClassLevel: 'Generic Class Level',
	genericItemLevel: 'Generic Item Level',

	options: 'Options',
	incremental: 'Incremental', // UI toggle for options.incremental
	allResults: 'All results', // UI toggle for options.allResults

	constraints: 'Constraints',
	min: 'Min',
	max: 'Max',

	reset: 'Reset',

	incrementalMinError: 'Can not set value below previous value with incremental.',
};

// Test cases
const prefills = {
	sneakAttack: {
		label: 'Sneak Attack',
		values: [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10],
	},
	arcaneStrike: {
		label: 'Arcane Strike',
		values: [1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5],
	},
	archeologistsLuck: {
		label: 'Archeologist\'s Luck',
		values: [1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4]
	},
	monkIUSDice: {
		label: 'Monk Die Count',
		values: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2]
	},
	monkIUSSize: {
		// Fails on so many levels
		label: 'Monk Die Size',
		values: [6, 6, 6, 8, 8, 8, 8, 10, 10, 10, 10, 6, 6, 6, 6, 8, 8, 8, 8, 10],
	},
	monkAC: {
		label: 'Monk AC Bonus',
		values: [0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4]
	},
	monkFast: {
		label: 'Monk Fast Movement',
		values: [0, 0, 10, 10, 10, 20, 20, 20, 30, 30, 30, 40, 40, 40, 50, 50, 50, 60, 60, 60],
	},
	bombNonCrit: {
		label: 'Bomb Non-Crit Bonus',
		values: [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9]
	},
	bestowHopeDR: {
		label: 'Bestow Hope (DR)',
		values: [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7],
	},
	bravery: {
		label: 'Bravery/Nimble',
		values: [0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5],
	},
	powerAttackD: {
		label: 'Power Attack DMG',
		values: [2, 2, 2, 4, 4, 4, 4, 6, 6, 6, 6, 8, 8, 8, 8, 10, 10, 10, 10, 12],
	},
	powerAttackA: {
		// Fails building sane increment set
		label: 'Power Attack ATK',
		values: [-1, -1, -1, -2, -2, -2, -2, -3, -3, -3, -3, -4, -4, -4, -4, -5, -5, -5, -5, -6],
	},
	scorchingRay: {
		label: 'Scorching Ray',
		values: [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
	},
	iteratives: {
		label: 'Iteratives',
		values: [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4],
	},
	bloodlineSpells: {
		label: 'Sorcerer Bloodline Spells',
		values: [0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9],
	},
	animCompHD: {
		label: 'Animal Companion HD',
		values: [2, 3, 3, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 12, 12, 13, 14, 15, 15, 16]
	},
	soulDrinkerCL: {
		label: 'Soul Drinker CL',
		// https://www.aonprd.com/PrestigeClassesDisplay.aspx?ItemName=Souldrinker
		values: [0, 1, 2, 3, 4, 4, 5, 6, 7, 8],
	}
};

// ----- LOGIC ---

const signNum = (n) => n < 0 ? `${n}` : `+${n}`;

class Increment {
	amount = 0;

	distance = 0;
	offset = 0;

	/**
	 * @param {number} amount
	 * @param {number} distance
	 * @param {number} offset
	 */
	constructor(amount, distance, offset) {
		this.amount = amount;
		this.distance = distance;
		this.offset = offset;
	}

	get incrementSpan() {
		return Math.max(1, (this.distance || 1) + this.offset);
	}

	get increment() {
		return this.distance - this.offset;
	}

	clone() {
		return new Increment(this.amount, this.distance, this.offset);
	}
}

class Progression {
	/** @type {number} */
	level;

	/** @type {number} */
	proof = 1;

	/** @type {number} */
	expect = 1;

	/** @type {boolean} */
	isRepeat = false;

	/** @type {boolean} */
	isRepeatProof = false;

	/** @type {boolean} */
	isDown = false;

	constructor(level) {
		this.level = level;
	}

	get isMatch() {
		return this.expect === this.proof;
	}

	clone() {
		const ld = new Progression(this.level);
		return Object.assign(ld, this);
	}
}

const template = `
	<form autocomplete='off'>
		<style>
		.app.pattern-finder form {
			display: flex;
			flex-flow: column nowrap;
			gap: 0.3rem;
		}
		.app.pattern-finder form .instructions,
		.app.pattern-finder form .constraints {
			gap: 3px;
		}
		.app.pattern-finder form .prefills {
			flex: 0;
		}
		.app.pattern-finder form .instructions .option {
			display: inline-flex;
			flex-flow: row nowrap;
		}
		.app.pattern-finder form .instructions .flexrow {
			padding: 0 0.3em;
		}
		.app.pattern-finder form .instructions h4 {
			flex: 0;
			margin: 0;
			background-color: rgba(0, 0, 0, 0.2);
			border-radius: 5px 5px 0 0;
			padding: 0 0.3em;
			font-weight: bold;
		}
		.app.pattern-finder form .instructions label {
			display: flex;
			gap: 0.3em;
			white-space: nowrap;
		}
		.app.pattern-finder form .instructions input[type='number'] {
			width: 2em;
			height: 1.5em;
		}
		.app.pattern-finder form .instructions > .flexcol {
			height: 100%;
		}
		.app.pattern-finder form .pattern {
			margin: 0;
		}
		.app.pattern-finder form .pattern td {
			padding: 1px;
		}
		.app.pattern-finder form .pattern input {
			width: 2em;
			border: unset;
			text-align: center;
			border: 1px solid;
			border-color: var(--color-border-light-1);
		}
		.app.pattern-finder form .pattern .expected input {
			border-color: darkorange;
			box-shadow: 0 0 3px orange;
		}
		.app.pattern-finder form .pattern .expect .row-label {
			font-weight: bold;
		}
		.app.pattern-finder form .pattern input.repeat {
			color: gray;
		}
		.app.pattern-finder form .pattern input.change {
			font-weight: bold;
		}
		.app.pattern-finder form .pattern input.match {
			background-color: hsl(120deg 25% 65% / 58%);
			font-weight: bold;
		}
		.app.pattern-finder form .pattern input.mismatch {
			background-color: hsl(16deg 100% 50% / 21%);
		}
		.app.pattern-finder form .pattern input.wide {
			text-align: left;
			width: 100%;
		}
		.app.pattern-finder form .pattern td:first-child {
			padding: 0 5px;
		}
		.app.pattern-finder form .pattern .increments td[data-index] {
			text-align: right;
			padding: 0 0.7em;
		}
		.app.pattern-finder form .pattern .increments td[data-index].short {
			padding: 0;
		}
		.app.pattern-finder form .pattern .increments td[data-index].short span {
			margin: 0 0.5em;
		}
		.app.pattern-finder form .pattern .increments .increment {
			font-size: var(--font-size-11);
			vertical-align: top;
			display: block;
			border-top: 1px solid black;
		}
		.app.pattern-finder form .pattern .increments .increment.offset {
			font-weight: bold;
			color: maroon;
		}
		.app.pattern-finder form .pattern .proof input {
			height: 1.5em;
		}
		.app.pattern-finder form .flexrow { align-items: center; gap: 0.3em; }
		.app.pattern-finder form .flexrow label {
			flex: 0;
			display: inline-flex;
			align-items: center;
		}
		.app.pattern-finder form .tag-list {
			margin: 3px;
			padding: 0;
			list-style: none;
			display: flex;
			flex-flow: row wrap;
			gap: 1px 3px;
		}
		.app.pattern-finder form .tag-list .tag {
			font-size: var(--font-size-11);
			background-color: rgba(100, 50, 50, 0.15);
			border-radius: 3px;
			padding: 1px 3px;
			border-right: 1px solid black;
			border-bottom: 1px solid black;
			white-space: nowrap;
		}
		.app.pattern-finder form .output {
			font-weight: bold;
		}
		.app.pattern-finder form .output input {
			padding: 0 0.5em;
			font-size: var(--font-size-16);
			font-weight: bold;
		}
		.app.pattern-finder form .result-header {
			background-color: rgba(50, 50, 50, 0.2);
		}
		.app.pattern-finder form .result-header.best-result {
			background-color: rgba(50, 200, 100, 0.2);
		}
		.app.pattern-finder form .result-header.best-result.false {
			background-color: rgba(200, 50, 100, 0.2);
		}
		</style>

		<div class='flexrow'>
			<label>{{@root.lang.reference}}:</label>
			{{#if (and actor (not reference))}}
			<select name='classReference'>
				<option>@level</option>
				<option value="@class.TAG.level">[{{lang.genericClassLevel}}]</option>
				<option value="@item.level">[{{lang.genericItemLevel}}]</option>
				{{selectOptions references selected=class localize=true}}
			</select>
			{{else}}
			<input type='text' name='reference' placeholder='@level' value='{{reference}}'>
			{{/if}}
			{{#if options.dev.prefills}}
			<select name='prefill' class='prefills'>
				{{selectOptions prefills selected=prefill blank="n/a"}}
			</select>
			{{/if}}
		</div>

		<div class='instructions flexrow'>
			<div class='options flexcol'>
				<h4>{{@root.lang.options}}</h4>
				<div class='flexrow'>
					<div class='option'>
						<label>
							<input type='checkbox' name='options.incremental' {{checked options.incremental}} disabled readonly>
							{{@root.lang.incremental}}
						</label>
					</div>
					{{#if options.dev.showResultToggle}}
					<div class='option'>
						<label>
							<input type='checkbox' name='options.dev.allResults' {{checked options.dev.allResults}}>
							{{@root.lang.allResults}}
						</label>
					</div>
					{{/if}}
				</div>
			</div>
			<div class='constraints flexcol'>
				<h4>{{@root.lang.constraints}}</h4>
				<div class='flexrow'>
					<label class='min'>
						{{@root.lang.min}}
						<input type='number' name='constraints.min' value='{{constraints.min}}' placeholder='-∞' step='1'>
					</label>
					<label class='max'>
						{{@root.lang.max}}
						<input type='number' name='constraints.max' value='{{constraints.max}}' placeholder='+∞' step='1'>
					</label>
				</div>
			</div>
		</div>

		<table class='pattern'>
			<thead>
				<tr>
					<th></th>
					{{#each levels}}
					<th class='level'>{{level}}</th>
					{{/each}}
				</tr>
			</thead>

			<tbody>
				<tr class='expect'>
					<td class='row-label'>{{@root.lang.expected}}</td>
					{{#each levels}}
					<td class='expected'><input type='number' class='expect{{#if isDown}} down-trend{{/if}}{{#if isRepeat}} repeat{{else}} change{{/if}}' step='1' {{#if @root.constraints.max}}max='{{@root.constraints.max}}'{{/if}} {{#if @root.constraints.min}}min='{{@root.constraints.min}}'{{/if}} name='levels.{{level}}.expect' value='{{expect}}' data-index='{{@index}}'></td>
					{{/each}}
				</tr>

				{{#each results as |result index|}}
				<tr>
					<th colspan='21' class='result-header{{#if best}} best-result{{/if}}{{#if (ne correct 20)}} false{{/if}}'>
						{{@root.lang.pass}} #{{pass}} – Score: {{correct}} / 20 (<b>{{pct}} %</b>)
					</th>
				</tr>

				<tr class='increments'>
					<td>{{@root.lang.increments}}</td>
					{{#each increments}}
					<td colspan='{{incrementSpan}}' class='{{#unless (gt distance 1)}}short{{/unless}}' data-index='{{@index}}' data-amount='{{amount}}' data-distance='{{distance}}'>
						<span class='increment {{#if offset}}offset{{/if}}' {{#if offset}}data-tooltip='Offset: {{numberFormat offset sign=true}}'{{/if}}>{{numberFormat increment sign=true}}</span>
					</td>
					{{/each}}
					{{#if filler}}
					<td colspan='{{filler}}' class='filler'></td>
					{{/if}}
				</tr>

				<tr class='output'>
					<td>{{@root.lang.output}}</td>
					<td colspan='20'>
						<input class='wide' type='text' name='output' value='{{formula}}' placeholder='@level' data-index='{{@index}}' readonly>
					</td>
				</tr>

				<tr class='proof'>
					<td class='row-label'>{{@root.lang.proof}}</td>
					{{#each levels}}
					<td class='level'>
						<input type='number' class='proof{{#if isMatch}} match{{else}} mismatch{{/if}}{{#if isRepeatProof}} repeat{{/if}}' step='1' name='levels.{{level}}.proof' value='{{proof}}' readonly>
					</td>
					{{/each}}
				</tr>

				<tr class='increments'>
					<td>{{@root.lang.increments}}</td>
					{{#each proof.increments}}
					<td colspan='{{incrementSpan}}' class='{{#unless (gt distance 1)}}short{{/unless}}' data-index='{{@index}}' data-amount='{{amount}}' data-distance='{{distance}}'>
						<span class='increment {{#if offset}}offset{{/if}}' {{#if offset}}data-tooltip='Offset: {{numberFormat offset sign=true}}'{{/if}}>{{numberFormat increment sign=true}}</span>
					</td>
					{{/each}}
					{{#if proof.filler}}
					<td colspan='{{proof.filler}}' class='filler'></td>
					{{/if}}
				</tr>


				<tr class='tags'>
					<td>{{@root.lang.notes}}</td>
					<td colspan='20'>
						<ul class='tag-list'>
						{{#each tags}}
							<li class='tag'>{{this}}</li>
						{{/each}}
						</ul>
					</td>
				<tr>

				{{/each}}
			</tbody>
		</table>

		{{#if options.resetButton}}
		<div class='buttons'>
			<button type='button' class='reset'>{{@root.lang.reset}}</div>
		</div>
		{{/if}}
	</form>
	`;

class PatternFinder extends FormApplication {
	/** @type {Progression[]} */
	levels = [];
	/** @type {Increment[]} */

	constraints = {
		min: null,
		max: null,
	};

	tags = [];

	constructor(object, _opts) {
		if (object instanceof TokenDocument) object = object.actor;
		super(object, _opts);

		this._template = Handlebars.compile(template);

		this._options = deepClone(options);
		if (options.dev.allResults) this._options.dev.showResultToggle = true;

		this.levels = Array.fromRange(20).map(n => new Progression(n + 1));
	}

	get token() {
		return this.object;
	}

	get actor() {
		return this.token?.actor;
	}

	get title() {
		return 'Pattern Finder';
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'pattern-finder'],
			width: 'auto',
			height: 'auto',
			submitOnChange: true,
			closeOnSubmit: false,
			submitOnClose: false,
		};
	}

	async getData() {
		const references = {};

		let actor = this.object ?? canvas.tokens.controlled[0]?.actor;
		if (!actor?.isOwner) actor = null;
		actor ??= game.user.character;

		// The following is PF1 specific
		if (game.system.id === 'pf1') {
			if (actor) {
				const rollData = this.actor.getRollData();
				for (const [classId, classData] of Object.entries(rollData.classes)) {
					references[`@classes.${classId}.level`] = classData.name;
				}
			}
		}

		this._analyzeProgression();

		const results = [];
		let result;
		const levels = this.levels.map(l => l.clone());

		// console.log('Progression:', this.levels.map(l => l.expect));

		if (levels.some(l => l.expect !== 1)) {
			// console.log('EVALUATING!', levels.map(l => l.expect));
			// Two sets to compare, ignore start causes drastically different results, so compare the two
			const result0set = this.evaluateSet({ levels, ignoreStart: false });
			const result0 = result0set.at(-1);
			const result1set = this.evaluateSet({ levels, ignoreStart: true, pass: result0.pass });
			const result1 = result1set.at(-1);

			// Select best result
			result = result0;
			if (result0.correct == 20 && result1.correct == 20) {
				if (result1.complexity < result0.complexity)
					result = result1;
			}
			else {
				if (result1.correct > result0.correct)
					result = result1;
				else if (result1.correct == result0.correct && result1.complexity < result0.complexity)
					result = result1;
			}

			if (!this._options.dev.allResults) {
				const passes = result0.pass + result1.pass;
				result.tags.push(`Passes: ${passes}`);
			}

			results.push(...result0set, ...result1set);
		}
		else {
			const result0set = this.evaluateSet({ levels, ignoreStart: false });
			result = result0set.at(0);
			results.push(...result0set);
		}

		let totalTime = 0;
		results.forEach((r, i) => {
			r.pass = i + 1; // Record true pass, instead of pass within a set.
			r.filler = 20 - r.increments.reduce((v, i) => v + i.incrementSpan, 0);
			r.pct = Math.floor(r.correct / 20 * 100);
			r.best = r === result;
			r.proof.filler = 20 - r.proof.increments.reduce((v, i) => v + i.incrementSpan, 0);

			totalTime += r.ms;
		});

		if (options.dev.sortBySuccess) {
			results.sort((a, b) => {
				if (a.correct === b.correct) {
					return b.complexity - a.complexity;
				}
				else {
					return b.correct - a.correct;
				}
			});
		}

		// console.log('Final Result:', deepClone(result));

		return {
			actor,
			levels: this.levels,
			results: this._options.dev.allResults ? results : [result],
			lang,
			references,
			reference: this.reference,
			constraints: this.constraints,
			options: this._options,
			prefill: this.prefill,
			prefills: Object.entries(prefills).reduce((all, [key, info]) => {
				all[key] = info.label;
				return all;
			}, {}),
			time: {
				total: totalTime,
			}
		}
	}

	/**
		 * Super simplistic method for determining complexity.
		 * @param {string} formula
		 * @returns {number}
		 */
	assessComplexity(formula, sets) {
		const termCount = Roll.parse(formula).length;
		const openingParens = formula.length - formula.replaceAll('(', '').length;
		const mathOps = formula.length - formula.replaceAll(/[-/*+^]/g, '').length;
		return termCount + openingParens + mathOps + sets.length;
	}

	evaluateSet(evalOpts = {}) {
		const results = [];

		/** @type {Progression[]} */
		const levels = evalOpts.levels;
		const ignoreStart = evalOpts.ignoreStart;
		evalOpts.pass ??= 1;

		let retry = false;
		let result;
		do {
			const s0 = performance.now();
			evalOpts.levels = levels.map(l => l.clone());
			evalOpts.ignoreStart = ignoreStart;
			result = this.evaluate(evalOpts);
			retry = result.suggestions.retry === true;

			result.complexity = this.assessComplexity(result.formula, result.sets);
			result.ms = Math.floor(performance.now() - s0);
			results.push(result);

			result.pass += 1;

			if (result.correct === 20) break;
			if (!retry) break;

			evalOpts = result.suggestions;
			// console.log('Suggestions:', evalOpts);
			delete evalOpts.retry;
			evalOpts.pass = result.pass;

			if (evalOpts.pass >= 10) {
				console.error('Too many re-evaluation passes', evalOpts);
				break;
			}
		} while (retry);

		return results;
	}

	_analyzeProgression() {
		let last = NaN;
		this.levels.forEach((level, index) => {
			level.isRepeat = level.expect == last;
			last = level.expect;
		});
	}

	_generateProofSets({ levels, offset = 0, ignoreStart } = {}) {
		/** @type {Increment[]} */
		const increments = [];

		let distanceOffset = offset;
		let prevOffset = -1, last = NaN;
		levels.forEach((level, index) => {
			level.isRepeatProof = level.proof == last;

			if (!level.isRepeatProof) {
				const diff = level.proof - (last || 0);
				const distance = index - prevOffset;
				increments.push(new Increment(diff, distance, distanceOffset));
				prevOffset = index;
			}

			distanceOffset = 0;

			last = level.proof;
		});

		const initialValue = increments[0]?.amount;

		if (increments.length > 1 && ignoreStart) {
			const old = increments.shift();
			increments[0].distance += 1 + offset;
			increments[0].offset = old.offset;
		}

		return { increments, initialValue };
	}

	_generateIncrementSets({ levels, offset = 0, ignoreStart } = {}) {
		/** @type {Increment[]} */
		const increments = [];
		let distanceOffset = offset;
		let prevOffset = -1, last = NaN;
		levels.forEach((level, index) => {
			level.isRepeat = level.expect == last;
			level.isDown = level.expect < last;

			if (!level.isRepeat) {
				const diff = level.expect - (last || 0);
				const distance = index - prevOffset;
				increments.push(new Increment(diff, distance, distanceOffset));
				prevOffset = index;
			}

			distanceOffset = 0;

			last = level.expect;
		});

		const initialValue = increments[0]?.amount;

		if (increments.length > 1 && ignoreStart) {
			// console.log('Merging start level to next increment');
			const old = increments.shift();
			increments[0].distance += 1 + offset;
			increments[0].offset = old.offset;
		}

		// console.log('Steps:', this.levels.filter(l => !l.isRepeat).map(l => ({ level: l.level, value: l.expect })));
		// console.log('Increments:', deepClone(increments));

		// TODO: Consider wiggling level 1 value, it may or may not be used for minimum value declaration
		// TODO: Second increment may or may not have the first increment considered for distance

		/*
			if (increments.length == 2) {
				const stage2 = increments.at(-1);
				console.log({ stage2 });
			}
			*/

		// Evaluate increments for additional patterns
		const incrementSets = new Map();
		for (const incr of increments) {
			let dist = incr.increment;

			// Correct adjusted first distance
			if (dist < 1) dist = Math.abs(dist) - offset;

			const value = incrementSets.get(dist) ?? 0;
			incrementSets.set(dist, value + 1);
		}
		const sets = Array.from(incrementSets.entries()).sort(([_a, a1], [_b, b1]) => b1 - a1);
		// console.log('Increment Sets:', sets);

		return { increments, sets, initialValue };
	}

	/**
		 * Try to figure out a formula.
		 *
		 * WARNING: This is bad code, avoid using it as example of anything.
		 */
	evaluate({ levels, offset = 0, adjust = 0, bonus = 0, min = false, max = false, ignoreStart = false, pass = 1 } = {}) {
		if (options.dev.verbose) console.log('>>> PASS', pass, 'EVALUATION');

		const tags = [], parts = [];

		if (ignoreStart) tags.push('Ignored Start');

		const suggestions = {};

		if (bonus !== 0) {
			parts.push(`${bonus}`);
			tags.push(`Bonus: ${signNum(bonus)}`);
			suggestions.bonus = bonus;
		}
		if (offset !== 0) {
			tags.push(`Start Offset: ${signNum(offset)}`);
			suggestions.offset = offset;
		}

		suggestions.min = min;
		suggestions.max = max;
		suggestions.offset = offset;
		suggestions.bonus = bonus;

		let suggested = false;
		const setSuggestion = (scope, value) => {
			if (suggested) return; // Allow only one suggestion per pass to avoid messing things by adjusting too many things at the same time
			if (options.dev.verbose) console.log('=== SUGGESTION:', scope, '=', value);
			if (suggestions[scope] === value) return;
			suggested = true;
			suggestions[scope] = value;
			suggestions.retry = true;
		}

		let reference = '@level';
		const regenReference = (_offset) => {
			reference = _offset === 0 ? '@level' : `(@level + ${_offset})`;
			if (options.dev.verbose) console.log('Reference regenerated as:', reference);
		};

		regenReference(offset);

		const { increments, sets, initialValue } = this._generateIncrementSets({ levels, offset, bonus, ignoreStart })

		tags.push(`Initial: ${signNum(initialValue)}`);

		if (increments.length == 1) {
			const increment = increments[0];
			const dist = increment.increment;
			const amount = increment.amount;
			if (dist == 1) {
				tags.push('Base');
				parts.push(`${amount}`);
			}
			else {
				tags.push('Div Case 1');
				let part = `floor(${reference} / ${dist})`;
				if (amount !== 1) part += ` * ${amount}`;
				parts.push(part);
			}
		}
		else if (increments.length > 1) {
			const fullEquidistant = new Set(increments.map(i => i.increment)).size === 1;
			if (fullEquidistant) {
				// console.log('[---] [---] [---] Equidistant (Full):', fullEquidistant);
				tags.push('Full Equidistancy');
				const increment = increments[0];
				const dist = increment.increment;
				const amount = increment.amount;

				let part;
				if (dist === 1) {
					part = `${reference}`;
				}
				else if (offset == -1) {
					tags.push('Div Case 2a');
					part = `ceil(@level / ${dist})`;
				}
				else {
					tags.push('Div Case 2b')
					part = `floor(${reference} / ${dist})`;
				}

				if (amount !== 1) part += ` * ${amount}`;
				parts.push(part);
			}
			else {
				const increments1 = increments.map(i => i.clone());
				const offincr = increments1.shift();
				const partialEquidistant = new Set(increments1.map(i => i.increment)).size === 1;
				// console.log(' ---  [---] [---] Equidistant (Partial):', partialEquidistant);
				// console.log('Partial Equidistant:', partialEquidistant);
				if (partialEquidistant)
					tags.push('Partial Equidistancy');
					// TODO: What do?

				const increment = increments1[0];
				const dist = increment.increment;
				const amount = increment.amount;

				const loff = dist - offincr.increment;

				// console.log('---- PARTIAL:', offincr.clone());
				// console.log('----- ???', increment.clone());
				// console.log('---- OFFSET:', loff);

				if (loff !== 0)
					regenReference(loff);

				// console.log({ loff, dist });

				let part;
				if (dist === 1) {
					part = `${reference}`;
				}
				else if (dist == 2 && loff == 1) { // Sneak Attack and similar, converts: floor((Lvl + Off) / Dist) into ceil(Lvl / Dist)
					tags.push('Div Case 3a: floor((L+1)/2)')
					part = `ceil(@level / ${dist})`;
				}
				else {
					tags.push('Div Case 3b'); // Unhandled
					part = `floor(${reference} / ${dist})`;
				}

				if (amount !== 1) part += ` * ${amount}`;

				parts.push(part);
			}
		}
		else {
			// console.log('WHAT IS THIS?', increments);
		}

		// console.log('SETS:', sets);
		tags.push(`Sets: ${sets.length}`);

		if (sets.length > 1) {
			// Attempt to consolidate two sets into one
			if (sets.length === 2) {
				const biggest = Math.max(sets[0][0], sets[1][0]),
					smallest = Math.min(sets[0][0], sets[1][0]),
					diff = smallest - biggest;

				if (increments[0].distance === 1) {
					if (diff !== 0) {
						setSuggestion('offset', diff);
						tags.push('Set Consolidation');
					}
				}
			}
		}

		const rollData = { level: 0 };

		let formula = parts.length ? parts.join(' + ') : '@level';

		const lowest = levels.reduce((last, info) => Math.min(info.expect, last), Number.POSITIVE_INFINITY),
			highest = levels.reduce((last, info) => Math.max(info.expect, last), Number.NEGATIVE_INFINITY);

		let proof;

		let correct = 0, lastV = NaN;
		const resolveFormula = () => {
			if (max && min)
				formula = `clamped(${formula}, ${lowest}, ${highest})`;
			else if (max)
				formula = `min(${highest}, ${formula})`;
			else if (min)
				formula = `max(${lowest}, ${formula})`;

			correct = 0;
			levels.forEach(info => {
				rollData.level = info.level;
				try {
					const roll = new Roll(formula, rollData).evaluate({ async: false });
					const total = roll.total;
					info.proof = total;
					info.isRepeatProof = lastV === total;
					lastV = total;
				}
				catch (err) {
					console.error({ formula, rollData }, err);
					throw err;
				}

				if (info.isMatch) correct += 1;
			});

			proof = this._generateProofSets({ levels, offset, ignoreStart });
		}

		resolveFormula();

		const lowestV = levels.reduce((last, info) => Math.min(info.proof, last), Number.POSITIVE_INFINITY),
			highestV = levels.reduce((last, info) => Math.max(info.proof, last), Number.NEGATIVE_INFINITY);

		let rapidRetry = false;
		if (lowestV < lowest) {
			const diff0 = lowest - lowestV,
				diff1 = highest - highestV;
				// console.log({ diff0, diff1 });
			if (diff0 === diff1) {
				// Add bonus to pump up numbers
				// console.log('LOWEST OF THE LOW', lowestV, '<', lowest, 'ADDING TO BONUS:', suggestions.bonus, '+=', diff0);
				setSuggestion('bonus', suggestions.bonus + diff0);
			}
			else if (!min && bonus === 0) {
				// console.log('LOWEST OF THE LOW', lowestV, '<', lowest);
				min = true;
				rapidRetry = true;
			}
			else {
				if (sets.length === 1) {
					if (options.dev.verbose) console.log('What Do? (Case 4)');
				}
			}
		}
		if (highestV > highest) {
			if (!max && bonus === 0) {
				// console.log('HIGHEST OF THE HIGH', highestV, '>', highest);
				max = true;
				rapidRetry = true;
			}
		}

		if (rapidRetry) resolveFormula();

		// Check for formula offset

		if (min) tags.push(`Min: ${lowest}`);
		if (max) tags.push(`Max: ${highest}`);

		this.output = formula;
		if (correct !== 20 && ignoreStart) {
			const diff = levels[0].expect - levels[0].proof;
			if (diff !== 0 && levels.every(info => info.expect - info.proof === diff))
				setSuggestion('bonus', diff);
		}

		// Finalize the findings

		// Disable retry if suggestions are same as inputs
		if (offset === suggestions.offset && bonus === suggestions.bonus && min === suggestions.min && max === suggestions.max)
			suggestions.retry = false;

		// if (correct === 20) console.log('!!! Solution found:', { pass });

		const result = {
			correct,
			offset,
			ignoreStart,
			suggestions,
			pass,
			increments,
			proof,
			sets,
			levels,
			formula,
			tags,
			initialValue
		};

		// Correct formula with user provided reference
		if (this.reference) result.formula = result.formula.replace('@level', this.reference);

		// Remove unnecessarily complicated expressions
		result.formula = result.formula.replaceAll(' + -', ' - ');

		// console.log('Result:', deepClone(result));
		// console.log('Formula:', result.formula);
		// console.log('Proof:', result.levels.map(l => l.proof));

		return result;
	}

	_renderInner(templateData) {
		const content = this._template(templateData, {
			allowProtoMethodsByDefault: true,
			allowProtoPropertiesByDefault: true
		});
		const jq = $(content);
		this.form = jq[0];
		return jq;
	}

	/**
		 * @param {JQuery} jq
		 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelectorAll('.pattern input.expect').forEach(input => {
			if (options.paintOnFocus)
				input.addEventListener('focus', ev => ev.target.select(), { passive: true });

			if (this._options.incremental)
				input.addEventListener('change', this._onExpectedChange.bind(this));
		});

		const output = html.querySelector('input[name="output"]');

		if (options.paintOnFocus)
			output.addEventListener('focus', ev => ev.target.select(), { passive: true });

		if (options.copyToClipboard) {
			output.addEventListener('click', async ev => {
				const text = ev.target.value;
				await game.clipboard.copyPlainText(text);
				ui.notifications.info(`Copied "${text}" to clipboard`);
			}, { passive: true });
		}

		if (this._options.resetButton) {
			html.querySelector('button.reset').addEventListener('click', ev => {
				ev.preventDefault();

				if (this.levels.some(info => info.expect !== 1)) {
					this.levels.forEach(info => info.expect = 1);
					this.prefill = null;
					this.render();
				}
			});
		}
	}

	_onExpectedChange(ev) {
		ev.preventDefault();
		ev.stopImmediatePropagation();

		const el = ev.target;
		const value = el.valueAsNumber;
		const index = parseInt(el.dataset.index);

		this.setIncremental(value, index);
	}

	setIncremental(value, index) {
		value ||= 0;
		// Prevent setting lower value than earlier.
		const prev = this.levels[index - 1]?.expect || 0;
		if (value < prev) {
			ui.notifications.warn(lang.incrementalMinError);
			value = prev;
		}

		if (index < 20) {
			const oldValue = this.levels[index].expect;
			for (let i = index; i < this.levels.length; i++) {
				if (this.levels[i].expect === oldValue)
					this.levels[i].expect = value;
			}
		}

		this.render();
	}

	_fillPreset(preset) {
		const array = prefills[preset]?.values;
		if (array.length > 20) throw new Error(`Preset "${preset}" has invalid number of entries: ${array.length}`);

		this.levels.forEach((v, i) => v.expect = array[i]);

		// Fill in values for shorter than 20 levels of data
		if (array.length === 20) return;

		const count = 20 - array.length;
		const max = array.at(-1);
		const postfill = Array.fromRange(count, max);
		postfill.forEach((v, i) => this.levels[array.length + i].expect = max);
	}

	_updateObject(event, formData) {
		formData = expandObject(formData);

		if (this.prefill === formData.prefill) this.prefill = null;
		else this.prefill = formData.prefill;
		if (this.prefill) {
			this._fillPreset(this.prefill);
			return void this.render();
		}

		Object.assign(this._options, formData.options);

		this.reference = formData.reference || formData.classReference;

		formData.levels = Object.values(formData.levels);
		formData.levels.forEach((info, index) => this.levels[index].expect = info.expect || 0);

		const min = this.constraints.min = formData.constraints.min;
		const max = this.constraints.max = formData.constraints.max;
		this.levels.forEach(info => {
			if (Number.isFinite(min) && info.expect < min) info.expect = min;
			if (Number.isFinite(max) && info.expect > max) info.expect = max;
		});

		this.render();
	}
}

new PatternFinder(token).render(true, { focus: true });
