/**
 * Clear pack indexes
 *
 * Workaround for Foundry bug:
 * https://github.com/foundryvtt/foundryvtt/issues/9984
 */

game.packs.forEach(pack => pack.index.clear());
