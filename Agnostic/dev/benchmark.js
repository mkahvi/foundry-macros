async function bench(cb, n, sets = 1) {
	async function iter(cb, i) {
		const t0 = performance.now();
		await cb(i)
		const t1 = performance.now();
		return t1 - t0;
	}
	let min = Infinity,
		max = -Infinity;
	const setResults = [];
	for (let z = 0; z < sets; z++) {
		let set = 0;
		for (let i = 0; i < n; i++) {
			const t = await iter(cb);
			if (t < min) min = t;
			if (t > max) max = t;
			set += t;
		}
		setResults.push(set);
	}

	setResults.sort();

	const setmax = setResults.at(-1),
		setmin = setResults[0],
		setDiff = setmax - setmin;
	const normalize = n => Math.floor(n * 1_000) / 1_000;
	const diff = normalize(max - min);
	const totalTime = normalize(setResults.reduce((total, value) => total + value, 0));
	const diffPerSet = normalize(setDiff / n);
	const timePerCall = normalize(totalTime / n);
	console.log('Timings | Total:', totalTime, 'ms | Each:', timePerCall, 'ms | Set Error Margin:', diffPerSet, 'ms | Error Margin:', diff, 'ms');
	// return [totalTime, timePerCall, diffPerSet];
}
