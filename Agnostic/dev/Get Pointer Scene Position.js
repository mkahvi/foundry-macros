/**
 * Get Pointer/Mouse Scene Position
 *
 * Gives you the scene coordinates where your mouse is pointing as notification, console log text, and in clipboard.
 *
 * Holding shift snaps the position to grid (you may need to add shift+# to Foundry's control config for macros).
 */

const snapPrecision = 1; // interval parameter to getSnappedPosition()
const alwaysSnap = false;

let { x, y } = canvas.mousePosition;

if (alwaysSnap || event.shiftKey) {
	const { x: x1, y: y1 } = canvas.grid.getSnappedPosition(x, y, snapPrecision);
	x = x1;
	y = y1;
}

const msg = `X ${x}, Y ${y}`;
ui.notifications.info(msg, { console: false });
console.log(msg);

game.clipboard.copyPlainText(`${x},${y}`);
