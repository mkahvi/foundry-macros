const packs = ['pf1.spells'];

const testSheets = async (pack) => {
	console.log('Fetching documents from', pack);
	const items = await game.packs.get(pack).getDocuments()
	console.log('Rendering', items.length, 'sheet(s)');
	for (const item of items) {
		try {
			await item.sheet._render(true);
			item.sheet.close();
		}
		catch (err) {
			console.log(item.name, item.id, item);
			throw err;
		}
	}
	console.log('Done with', pack);
}

console.log('Testing sheets');
for (const p of packs)
	await testSheets(p);
console.log('All done');
