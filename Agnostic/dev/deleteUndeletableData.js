/**
 * Delete Undeletable Data
 *
 * Removes data from actors that is not deletable by normal updates.
 * That is: any keys that start with -= which should be impossible.
 *
 * This should not be needed by regular users. Only by devs doing something stupid with their updates.
 *
 * Should be harmless to run, barring game system weirdness.
 *
 * Compatibility:
 * - Foundry v10
 */

const dryRun = true; // Do not actually update anything

let fixCounter = 0, docsIgnored = 0;
const generateFix = (doc) => {
	let fixes = 0;

	const data = doc.toObject();

	const delveData = (_data, path) => {
		if (_data === undefined || _data === null) return;
		if (['number', 'string'].includes(typeof _data)) return;
		if (Array.isArray(_data))
			delveArray(_data, path);
		else if (_data instanceof Object)
			delveObject(_data, path);
	}

	const delveArray = (_data, path) => {
		_data.forEach((value, i) => delveData(value, path.concat(`${i}`)));
	}

	const delveObject = (_data, path) => {
		for (const [key, value] of Object.entries(_data)) {
			if (/^-=/.test(key)) {
				fixes++;
				console.log('Deleting:', path.join('.') + `.${key}`);
				delete _data[key];
			}
			else delveData(value, path.concat(key));
		}
	}

	delveObject(data, []);
	if (fixes == 0)
		docsIgnored++;
	else {
		fixCounter++;
		console.log(doc.name, { pack: doc.pack }, { fixes });
		return data;
	}
};

const fixDocument = async (doc) => {
	const update = generateFix(doc);
	if (update && !dryRun) await doc.update(update, { recursive: false, diff: false });
}

const fixScene = async (scene) => {
	for (const token of scene.tokens) {
		if (!token.actor) continue;
		await fixDocument(token.actor);
	}
};

const fixPack = async (pack) => {
	const wasLocked = pack.locked;
	const isActor = pack.metadata.type === 'Actor',
		isItem = pack.metadata.type === 'Item';
	if (!isActor && !isItem) return;

	const fixPackDocument = async (indexData) => {
		const doc = await pack.getDocument(indexData._id);
		const update = generateFix(doc);
		if (update && !dryRun) {
			if (wasLocked && pack.locked) await pack.configure({ locked: false });
			await doc.update(update, { recursive: false, diff: false });
		}
	};

	console.log('>>> Checking:', pack.collection);

	for (const indexData of pack.index) {
		await fixPackDocument(indexData);
	}

	if (wasLocked && !pack.locked) await pack.configure({ locked: true });
};

console.log('+++ Fixing Actors...');
const actorPromises = [], itemPromises = [];
game.actors.forEach(actor => actorPromises.push(fixDocument(actor)));
await Promise.allSettled(actorPromises);

console.log('+++ Fixing Scenes & Unlinked Tokens...');
for (const scene of game.scenes)
	await fixScene(scene);

console.log('+++ Fixing Items Directory...');
game.items.forEach(item => itemPromises.push(fixDocument(item)));
await Promise.allSettled(itemPromises);

console.log('+++ Fixing Compendiums...');
for (const pack of game.packs)
	await fixPack(pack);

console.log('All fixes done,', fixCounter, 'documents(s) adjusted;', docsIgnored, 'ignored');
