// Find all tokens with no valid actor linked

const scenes = {};

class BadTokenDialog extends Dialog {
	activateListeners(jq) {
		super.activateListeners(jq);

		jq.find('li[data-token-id]').on('click', (event) => {
			const el = event.target.closest('li[data-token-id]');
			const scene = game.scenes.get(el.dataset.sceneId);
			const token = scene?.tokens.get(el.dataset.tokenId);
			if (token && scene) {
				scene.view().then(() => {
					const tokenO = token.object;
					const { x, y } = tokenO;
					canvas.animatePan({ x, y, scale: 2 });
					tokenO.control({ releaseOthers: true });
				});
			}
			else {
				ui.notifications.error('Selected token (or their scene) not found');
			}
		});
	}
}

const template = `
<div class='flexcol'>
	<p style='font-size:8pt;'>Click on the tokens to swap scene and focus them.
	<hr>

	{{#each scenes}}
	<h3 class='flexrow'><scene>{{scene.name}}</scene> <id style='flex:0;'>[{{scene.id}}]</id></h3>
	<ul class='flexcol'>
		{{#each tokens}}
		<li class='flexrow' data-scene-id='{{../scene.id}}' data-token-id='{{id}}'><token>{{name}}</token> <id style='flex:0;'>[{{id}}]</id></li>
		{{/each}}
	</ul>
	{{/each}}
	<hr>
</div>
`;

if (game.user.isGM) {
	let found = false;
	game.scenes.forEach(scene => {
		const tokens = scene.tokens
			.reduce((at, t) => {
				if (!t.actor) at.push(t);
				return at;
			}, []);

		if (tokens.length) {
			found = true;
			scenes[scene.id] = { scene, tokens };
		}
	}, []);

	if (!found) {
		ui.notifications.info('No bad tokens found');
	}
	else {
		const compiled = Handlebars.compile(template);
		new BadTokenDialog(
			{
				content: compiled({ scenes }, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
				buttons: {
					dismiss: {
						label: 'OK'
					}
				},
				default: 'dismiss'
			},
			{
				width: 520,
			}
		).render(true);
	}
}
