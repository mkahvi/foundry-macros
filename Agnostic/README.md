## System Agnostic

These are expected to work with all game systems.

... unless they or the modules used override base functionality too much, or go out of their way to mangle core functionality.
