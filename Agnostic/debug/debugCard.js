// Add @Macro[debugCard]{Debug} into chat card's text to produce appropriate button for this.
// Output is console only

// Compatibility: All versions until game.messages or chat card format changes

const messageId = event.target.closest('.message')?.dataset.messageId;
const message = game.messages.get(messageId)

console.debug('DEBUG | CHAT MESSAGE: ', message);
