/**
 * Basic scatter determination.
 * Default unit is equal to grid cell. If unit is specified, that is used instead.
 * Cell distances are converted to units as per scene configuration.
 *
 * Holding shift skips the dialog.
 *
 * Direction math that has range of 1 to 8 will be mapped to cardinal directions.
 *
 * Example macro button usage:
 *   - @Macro[scatter]{3d4} // random cardinal direction
 *   - @Macro[scatter]{1d4 in 1d8} // random cardinal direction 1–4 grid spaces (5 to 20 ft) away
 *   - @Macro[scatter]{5 ft in 3d8} // does not result in cardinal direction, 5 feet away
 *   - @Macro[scatter]{10d6 ft} // random cardinal direction 10 to 60 feet away
 *   - @Macro[scatter]{5 anywhere} // rolls 360, anywhere is special word here for 360 degrees
 *
 * TODO / NOT IMPLEMENTED:
 *   - [[1d4]]@Macro[scatter]{<- in ->}[[1d8]] // NOT IMPLEMENTED
 *   - @Macro[scatter]{1d4 south west} // literal direction
 *   - @Macro[scatter]{3d8 feet} // units
 *   - Deny directions: e.g. 1,4,6-8
 *
 * Hacking/Customization:
 *   Variables: options, and dname
 *   Functions: createChatMessage, postMessageAction, preMessageFilter, directionTransform, rangeTransform, translateDirection, and parseLabel
 *
 * COMPATIBILITY: Should be fully version agnostic.
 */

const options = {
	defaultDirection: '1d8', // clockwise
	defaultRange: '1', // cells, not any specific units
	directionId: /\b(in|towards|toward|to)\b/ig, // splitting range from direction
	defaultUnit: '', //
	defaultCellSize: 5, // 5 feet, 1.5 m, whatever
	specialId: 'anywhere',
	rangeWords: /\b(ft|feet|m|meters|metres|distance|far|cell|cells|square|squares|away)\b/ig,
	dirWords: /\b(south|east|north|west)\b/ig,
	includeArrow: true, // include cardinal arrow
	ignoredWords: /\b(?:scatter|scatters)\b/ig,
};

const lang = {
	title: 'Scatter', // dialog title
	scatter: 'Scatter', // scatter button

	direction: 'Direction formula',
	range: 'Range formula',
	units: 'Units (defaults to cells)',

	scatters: 'Scatters {0} to {1}', // 0 = range, 1 = direction

	dismiss: 'Dismiss', // dismiss button
};

const viewedScene = game.scenes.viewed;

const enrichRoll = (roll, formula, label) => `<a class="inline-roll inline-result" title="${formula}" data-roll="${escape(JSON.stringify(roll))}"><i class="fas fa-dice-d20"></i> ${label}</a>`;

/**
 * Customization!
 * Called after chat message creation in case you desire to hook other things to this.
 */
function postMessageAction(speaker, direction, range, units) {
	// Do fancy stuff here
}

/**
 * Customization!
 * @returns true if we are to proceed, false if the action needs to be cancelled.
 */
function preMessageFilter(speaker, direction, range, units) {
	return true;
}

function distanceClamp(distance) {
	return Math.floor(distance * 10) / 10; // 1 decimal
}

const dname = [
	'North East',
	'East',
	'South East',
	'South',
	'South West',
	'West',
	'North West',
	'North',
];

const refreshRoll = (roll) => new Roll(roll.formula, roll.data);

function translateDirection(text) {
	for (let i = 0; i < dname.length; i++) {
		if (dname[i].toLowerCase() === text.toLowerCase()) return i;
	}
	return null;
}

function filterRangeText(text) {
	return text.replace(options.rangeWords, '');
}

function filterDirectionText(text) {
	return text.replace(options.dirWords, '');
}

function idPart(part) {
	return {
		direction: part.match(options.dirWords),
		range: part.match(options.rangeWords),
	};
}

function directionName(direction) {
	// console.log("Direction:", direction);
	const tr1 = refreshRoll(direction.roll), tr2 = refreshRoll(direction.roll);
	const min = tr1.evaluate({ minimize: true }), max = tr2.evaluate({ maximize: true });
	// console.log("directionName:", direction.special, min.total, min, max.total, max);
	if (direction.special || min.total !== 1 || max.total !== 8) {
		return `${direction.value}`;
	}
	else {
		const d = direction.value - 1; // 0 offset
		const arrow = '↗→↘↓↙←↖↑';
		return options.includeArrow ? `${dname[d]} ${arrow[d]}` : dname[d];
	}
}

function createChatMessage(speaker, direction, range, units) {
	const scUnits = viewedScene?.data.gridUnits ?? options.defaultUnit;
	if (!(units?.includes(scUnits) ?? true)) {
		console.warn('SCATTER | Unit mismatch:', scUnits, 'is not in', units);
	}

	if (!(units?.length > 0)) units = scUnits;

	const en_range = enrichRoll(range.roll, range.formula, `${distanceClamp(range.roll.total)} ${units}`),
		en_direction = enrichRoll(direction.roll, direction.formula, `${directionName(direction)}`);

	return lang.scatters.format(en_range, en_direction);
}

function parseLabel(label) {
	const directionSplit = label.split(options.directionId, 2); // in
	if (directionSplit.length > 1) {
		const rightIds = idPart(directionSplit[1]),
			leftIds = idPart(directionSplit[0]);

		const data = {
			range: filterRangeText(directionSplit[0]).trim(),
			units: leftIds.range,
			direction: directionSplit[1].trim(),
		};
		if (rightIds.direction) data.directionLabel = true;

		return data;
	}

	if (label.includes(options.specialId)) { // TODO: Make a special Id to function mapping instead
		// something something

		const df = label.replace(options.specialId, '').trim();
		const id = idPart(df);
		return {
			range: filterRangeText(df),
			units: id.range,
			direction: '1d360'
		};
	}

	const idd = idPart(label);
	if (idd.direction) {
		// console.log("Direction Words:", idd.direction);
	}
	else if (idd.range) {
		// console.log("Range Words:", idd.range);
		return {
			range: filterRangeText(label).trim(),
			units: idd.range,
			direction: options.defaultDirection
		};
	}

	try {
		new Roll(label).roll();
		return {
			range: label.trim(),
			direction: options.defaultDirection
		};
	}
	catch (err) {
		// NOP
	}

	console.warn('SCATTER | Couldn\'t parse instructions:', label);
	ui.notifications.warn('Couldn\'t parse scatter instructions: ' + label);
}

/**
 * Transforms a direction roll into socially acceptable from.
 */
function directionTransform(formula, rollData, units) {
	const roll = new Roll(formula, rollData).roll();
	return {
		formula: formula,
		units: units,
		roll: roll,
	};
}

/**
 * Transforms a range roll into socially acceptable form.
 */
function rangeTransform(formula, rollData, units) {
	formula = units?.length > 0 ? formula : `@cell * ${formula}`;
	const roll = new Roll(formula, rollData).roll();
	return {
		formula: formula,
		units: units,
		roll: roll,
	}
}

const row = () => $('<div/>').addClass('flexrow');
const label = (text) => $('<label/>').text(text);
const input = (id, defaultValue, placeholder = '') => $('<input data-dtype="String" type="text" />')
	.attr({ value: defaultValue, name: id })
	.attr({ placeholder });

async function chatCard(speaker, direction, range, units) {
	if (!preMessageFilter(speaker, direction, range, units)) return;
	console.group('SCATTER');
	console.log('Direction:', direction);
	console.log('Range:', range);
	console.log('Speaker:', speaker);
	console.log('Units:', units);
	console.groupEnd();
	const mode = game.settings.get('core', 'rollMode');
	await ChatMessage.create({
		content: '<div>' + createChatMessage(speaker, direction, range, units) + '</div>',
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
		rollMode: mode,
		whisper: ['gmroll', 'selfroll'].includes(mode) ? [...game.user.id, ...game.users.filter(u => u.isGM)] : [],
		speaker: ChatMessage.getSpeaker({ actor: speaker.actor }),
	});
	postMessageAction(speaker, direction, range, units);
}

function prepare(formula, rollData, transform, units) {
	formula = formula.replace(options.ignoredWords, '');
	const data = transform(formula, rollData, units);
	return {
		value: data.roll.total,
		formula: data.formula,
		roll: data.roll,
		units
	};
}

/**
 * @param {String} direction Formula to determine direction.
 * @param {String} range Distance formula for scene units.
 */
function scatter(data) {
	const cellDist = viewedScene?.data.gridDistance ?? options.defaultCellSize;
	chatCard(data.speaker, prepare(data.direction, {}, directionTransform), prepare(data.range, { cell: cellDist }, rangeTransform, data.units), data.units);
}

function generateDialog(data) {
	const html = $('<div/>').addClass('flexcol')
		.append(row().append(label(lang.direction), input('direction', data.direction ?? options.defaultDirection)))
		.append(row().append(label(lang.range), input('range', data.range ?? options.defaultRange)))
		.append(row().append(label(lang.units), input('units', data.units?.[0] ?? options.defaultUnit, game.scenes.active.data.gridUnits)))
		.append($('<hr/>'));

	new Dialog({
		title: lang.title,
		content: $('<div/>').append(html).html(),
		buttons: {
			scatter: {
				label: lang.scatter,
				icon: '<i class="far fa-compass"></i>',
				callback: html => scatter({ speaker, direction: html.find('input[name="direction"]').val(), range: html.find('input[name="range"]').val(), units: html.find('input[name="units"]').val() }),
			},
			dismiss: {
				label: lang.dismiss,
				icon: '<i class="fas fa-power-off"></i>',
			}
		},
		default: 'scatter',
	}).render(true);
}

const decideDialog = (data) => event.shiftKey ? scatter(data) : generateDialog(data);

function handleChatMessage(cm) {
	const ml = event.target.closest('button,a')?.textContent.trim(); // macro label
	const data = parseLabel(ml);
	if (data) {
		decideDialog({
			speaker: { actor: game.actors.get(cm.data.speaker) ?? token?.actor ?? actor ?? game.user.character },
			range: data?.range,
			direction: data?.direction,
			units: data.units,
		});
	}
}

const cm = game.messages.get(event.target.closest('.message')?.dataset.messageId);
if (cm) {
	handleChatMessage(cm);
}
else {
	decideDialog({
		speaker: { actor: token?.actor ?? actor ?? game.user.character },
		range: options.defaultRange,
		direction: options.defaultDirection,
	});
}
