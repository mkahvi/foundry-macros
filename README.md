# foundry-macros

Impotent flailing.

Actual macros are found in appropriate subfolders, the root folder is only for development environment stuff.

Mana#4176 in [Foundry Discord](https://discord.gg/foundryvtt)

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)
