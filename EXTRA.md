# Just extra stuff

This is not really related to this repository, just... extra things. Wasting space.

## Modules

Here's some modules I'd like to recommend for everyone to be at least aware of.

- [Bar Brawl](https://foundryvtt.com/packages/barbrawl/) – more token resource bars
- [Chat Message Accessibility Indicators](https://foundryvtt.com/packages/chat-indicators/) – clarity on message types
- [Custom CSS](https://foundryvtt.com/packages/custom-css/)
- [Cycle Token Stack](https://foundryvtt.com/packages/cycle-token-stack/)
- [DF Settings Clarity](https://foundryvtt.com/packages/df-settings-clarity/) – clarifies if settings are GM-only or client-side
- [Dice So Nice](https://foundryvtt.com/packages/dice-so-nice/) – very nice 3D dice
- [Health Monitor](https://foundryvtt.com/packages/Health-Monitor/) – logs health changes
- [Perfect Vision](https://foundryvtt.com/packages/perfect-vision/) – great way to have darkvision
- [Permission Viewer](https://foundryvtt.com/packages/permission_viewer/) – clarifies who has permissions to what
- [Token Mold](https://foundryvtt.com/packages/token-mold/) – token management
- [Token Tooltip Alt](https://foundryvtt.com/packages/token-tooltip-alt/) – more token numbers
- [Universal Battlemap Importer](https://foundryvtt.com/packages/dd-import/) – direct importing of Dungeondraft and other universal VTT format maps
