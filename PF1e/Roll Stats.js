/**
 * Roll Stats for PF1
 *
 * Compatibility: Foundry v10 - v11
 * 
 * Notes:
 * - Hold shift when executing the macro to prompt for the time period constraints.
 * 
 * Flaws:
 * - Fortune/Misfortune rolls are not accounted for as anything special, all active d20s are taken as if valid.
 * - Inline rolls are not accounted for.
 */

// Default configuration
const rollStatsCfg = {
	skipPrompt: true, // Skip prompt in case you feel this config here is good for all cases
	maxIdle: 120, // Assume end of rolls when this many minutes have passed between messages.
	maxDays: 3, // Don't look back more than this many days no matter what.
	minStreak: 2, // Minimum streak length to report. Values under 2 are ignored.
	ignoreTakeX: true, // Ignore take X rolls. Otherwise they're treated as normal rolls.
};

/* global RollPF */

class RollStats {
	/** @type {Number[]} */
	rolls = [];

	size = 20;

	get nat1s() { return this.rolls.filter(r => r === 1).length; }
	get nat20s() { return this.rolls.filter(r => r === 20).length; }

	get best() { return Math.max(...this.rolls); }
	get worst() { return Math.min(...this.rolls); }

	get spread() {
		const base = {};
		for (let i = 1; i < this.size + 1; i++) base[i] = 0;
		return this.rolls.reduce((split, cur) => {
			split[cur]++;
			return split;
		}, base);
	}

	get mode() {
		const entries = Object.entries(this.spread);
		const [mode, _count] = entries.sort(([_a, a], [_b, b]) => b - a)[0];
		return parseInt(mode);
	}

	get modeCount() {
		const entries = Object.entries(this.spread);
		const [_mode, count] = entries.sort(([_a, a], [_b, b]) => b - a)[0];
		return count;
	}

	get average() { return this.rolls.length ? Math.roundDecimals(this.rolls.reduce((prev, cur) => cur + prev) / this.rolls.length, 2) : NaN; }

	/**
	 * @param {Number} rolls
	 */
	add(...rolls) {
		// console.log('Adding:', ...rolls);
		this.rolls.push(...rolls);
	}

	finalize() {
		this.rolls.reverse();
	}

	constructor(size = 20) {
		this.size = size;
	}
}

class UserStats {
	#id;
	user;

	streaks = [];
	get longestStreak() { return this.streaks[0]; }
	_lastStreak = { num: 0, count: 0 };

	// Specific rolls
	specific = {
		100: new RollStats(100), 20: new RollStats(20), 12: new RollStats(12), 10: new RollStats(10), 8: new RollStats(8), 6: new RollStats(6), 4: new RollStats(4), 3: new RollStats(3), 2: new RollStats(2)
	};

	// All d20 rolls
	get checks() { return this.specific[20]; }

	saves = new RollStats();
	skills = new RollStats();
	attacks = new RollStats();
	attacksAll = new RollStats();
	ability = new RollStats();
	attacksConfirms = new RollStats();
	unidentified = new RollStats();

	constructor(userId) {
		this.#id = userId;
		this.user = game.users.get(userId);
	}

	get name() {
		return this.user.name;
	}

	add(rolldata) {
		// console.log('Adding:', rolldata);
		for (const [_size, rolls] of Object.entries(rolldata)) {
			const size = parseInt(_size);
			if (this.specific[size] === undefined) {
				console.warn('Unsupported die size:', size);
				continue;
			}
			this.specific[size].add(...rolls);
			if (size === 20) {
				for (const r of rolls) {
					if (this._lastStreak.num === r) this._lastStreak.count++;
					else {
						// Push and reset on streak end
						if (this._lastStreak.count > 1) this.streaks.push(this._lastStreak);
						this._lastStreak = { num: r, count: 1 };
					}
				}
			}
		}
	}

	finalize() {
		// Push lingering streaks in
		if (this._lastStreak.count > 1) this.streaks.push(this._lastStreak);
		delete this._lastStreak;

		if (rollStatsCfg.minStreak > 1) this.streaks = this.streaks.filter(s => s.count >= rollStatsCfg.minStreak);

		// Sort streaks
		this.streaks.sort((a, b) => {
			const diff = b.count - a.count;
			if (diff != 0) return diff;
			return a.num - b.num;
		});

		// Create compressed streak display with repeat streaks
		this.superStreaks = [];
		for (let streak of this.streaks) {
			const last = this.superStreaks.at(-1);
			if (last) {
				if (last.count === streak.count && last.num === streak.num) {
					last.repeat ??= 1;
					last.repeat += 1;
					continue;
				}
			}
			this.superStreaks.push(streak);
		}
		
		// Restore order in time
		Object.values(this).forEach(p => p instanceof RollStats ? p.finalize() : null);
		Object.values(this.specific).forEach(p => p.finalize());
	}
}

// Unpack active dice from a roll
function getDice(roll) {
	if (!roll) return {};
	if (roll.options.staticRoll && rollStatsCfg.ignoreTakeX) return {};

	return roll?.dice.reduce((dtotal, dt) => {
		const rs = dt.results.reduce((total, r) => { if (r.active) total.push(r.result); return total; }, []);
		if (rs.length) {
			dtotal[dt.faces] ??= [];
			dtotal[dt.faces].push(...rs);
		}
		return dtotal;
	}, {});
}

function scanChatMessages() {
	console.log("ROLL STATS | Scanning messages...");

	// get messages and reverse them so they're oldest first
	/** @type {ChatMessage[]} */
	const allMessages = [...game.messages.contents].sort((a, b) => (b.timestamp - a.timestamp));

	/** @type {Record<string,UserStats>} */
	const users = {};
	const now = Date.now(); // use last message timestamp
	let lastStamp = allMessages[0].timestamp;
	let lastMsg = allMessages[allMessages.length - 1];
	let includedMessages = 0;
	for (const cm of allMessages) {
		if (!cm.isContentVisible) continue; // Skip hidden messages
		// console.log('Processing:', cm);

		const roll = cm.rolls?.[0];

		// Init user
		const userId = cm.user.id;
		if (!userId) {
			console.warn('- Bad user ID:', cm);
			continue;
		}
		users[userId] ??= new UserStats(userId);
		/** @type {UserStats} */
		const user = users[userId];

		// CHECK TIMESTAMPS
		const stamp = cm.timestamp;
		const nowDelta = now - stamp;
		if (nowDelta > rollStatsCfg.maxDays * 86_400_000) {
			console.log('- Too old message', Math.roundDecimals(nowDelta / 86_400_000, 2), 'days', { msgId: cm.id });
			break;
		}
		const lastMsgDelta = lastStamp - stamp;
		if (lastMsgDelta > rollStatsCfg.maxIdle * 60_000) {
			console.log('- Idle time exceeded', Math.roundDecimals(lastMsgDelta / 60_000, 2), 'minutes', { msgId: cm.id });
			break;
		}
		lastStamp = stamp;

		// Unpack die rolls into an array
		const rolls = getDice(roll);
		// console.log('Rolls:', rolls);

		// CHECK ITEM DATA
		const metadata = cm.getFlag('pf1', 'metadata');
		const itemId = metadata?.item;
		const item = itemId ? cm.itemSource : null;
		// SUBJECT
		const subject = cm.getFlag('pf1', 'subject');

		if (subject) {
			// console.log('~ SUBJECT:', subject);
			// console.log('~ Rolls:', rolls);
			if (subject.save) {
				switch (subject.save) {
					case 'ref':
					case 'fort':
					case 'will': {
						if (rolls[20]?.length > 0) {
							user.saves.add(...rolls[20]);
						}
						break;
					}
				}
			}
			else if (subject.core) {
				// bab, init, cl, concentration
				if (rolls[20]) user.unidentified.add(...rolls[20]);
			}
			else if (subject.skill) {
				// const skillID = subject.skill;
				user.skills.add(...rolls[20]);
			}
			else if (subject.ability) {
				user.ability.add(...rolls[20]);
			}
			else {
				// Damage, healing, and level-up
				//console.log("- Unidentified:", subject, cm);
			}
			user.add(rolls);
		}
		else if (item) {
			const attacks = metadata.rolls?.attacks;
			if (attacks) {
				for (const atk of Object.values(attacks)) {
					// TODO: DAMAGE ROLLS

					if (atk.attack) {
						const atkRoll = RollPF.fromData(atk.attack);
						const atkRolls = getDice(atkRoll);
						user.attacks.add(...atkRolls[20] ?? []);
						user.attacksAll.add(...atkRolls[20] ?? []);
						user.add(atkRolls);
					}
					const critRoll = atk.critConfirm ? RollPF.fromData(atk.critConfirm) : null;
					if (critRoll) {
						const critRolls = getDice(critRoll);
						user.attacksConfirms.add(...critRolls[20] ?? []);
						user.attacksAll.add(...critRolls[20] ?? []);
						user.add(critRolls);
					}
				// atk.damage
				// atk.critDamage
				}
			}
		}
		else {
			if (rolls) {
				if (rolls[20]) user.unidentified.add(...rolls[20]);
				user.add(rolls);
			}
		}
		lastMsg = cm;
		includedMessages++;
	}

	for (const user of Object.values(users))
		user.finalize();

	const stats = {
		oldest: lastMsg?.id,
		newest: allMessages[0]?.id,
		messages: {
			included: includedMessages,
			total: allMessages.length
		}
	}

	console.log('ROLL STATS | Done scanning!');
	return { users, stats };
}

function generateStatDialog(users, stats) {
	const hbs = `
<style>
.app.dialog .roll-stats-macro.content {
	gap: 3px;
	display: grid;
	grid-template-rows: repeat(17, auto);
	grid-auto-flow: column;


	& .column {
		display: contents;

		& .even-row {
			background-color: rgba(0,0,0,0.05);
	 	}

		&.user {
	 		background-color:rgba(0,0,0,0.02);
		}

		& :is(.no-rolls,.no-streaks) {
			opacity: 0.4;
		}

		& .checks {
			border-bottom: 1px solid #c3acac;
		}

		& .roll.average {
			&.good {
				color: seagreen;
			}
			&.poor {
				color: maroon;
			}
		}

		& .streaks {
			& .streak {
				gap: 0.3em;

				& .result {
					flex: 0 2em;
					text-align: right;
				}

				& :is(.repeat,.count) {
					flex: 0 1em;
				}

				& .no-rolls {
					color: gray;
				}
			}
		}

		& .spread {
			& .grid {
				display: grid;
				grid-template-rows: repeat(20, auto);

				& .counts {
					gap: 0.3em;
					
					& .die-size {
						flex: 0 2em;
						text-align: right;
					}
				}

				& .no-stats {
					color: gray;
				}
			}
		}

		& .roll-subject {
			& > div {
				gap: 0.3em;

				& > label {
					flex:0 4em;
				}

				& > span {
					flex:1 4em;
					white-space: nowrap;
				}
			}
		}

		& h3 {
			border-bottom: 1px solid #c3acac;
		}
	}
}
</style>
<div class='config'>
	<label>Max days: <b>{{config.maxDays}}</b>.</label>
	<label>Max idle between messages: <b>{{config.maxIdle}}</b>.</label>
	<br>
	<label>Observed time period: <b>{{period}}</b> {{periodUnits}}.</label>
	<label>Observed messages: <b>{{messages.included}}/{{messages.total}}</b>.</label>
	<br>
	<label>Oldest message: <b>{{lastTimestamp}}</b>.</label>
</div>
<div class="roll-stats-macro content">
	<div class="column labels">
		<h2>User:</h2>
		<h3><b>d20</b></h3>
		<label class='even-row'>Average:</label>
		<label>Mode:</label>
		<label class='even-row'>Nat 20s:</label>
		<label>Nat 1s:</label>
		<label class='even-row'>Best – Worst:</label>
		<label>Longest streak:</label>
		<label class='even-row'>Streaks:</label>
		<div>Spread:</div>

		<h3><b>By Subject</b></h3>
		<label class='even-row'>Attacks:</label>
		<label>CritConfirms:</label>
		<label class='even-row'>Skills:</label>
		<label>Saves:</label>
		<label class='even-row'>Ability Checks:</label>
		<label data-tooltip="BAB, Initiative, CL and Concentration checks">Other:</label>
	</div>

	{{#each users as |user id|}}
	<div class='column user'>
		<h2>{{user.name}}</h2>
		<h3 class="checks">{{checks.rolls.length}}</h3>
		<label class='even-row roll average {{#if (gt checks.average 12)}}good{{else if (lt checks.average 8)}}poor{{/if}}'>{{checks.average}}</label>
		<label class='odd-row roll mode'>{{#if (gt checks.modeCount 1)}}<b>{{checks.mode}}</b> ×{{checks.modeCount}} [{{rollStats-macro-percentile checks.modeCount checks.rolls.length}}%]{{else}}n/a{{/if}}</label>
		<label class='even-row roll nat20s'>{{checks.nat20s}} [{{rollStats-macro-percentile checks.nat20s checks.rolls.length}}%]</label>
		<label class='odd-row roll nat1s'>{{checks.nat1s}} [{{rollStats-macro-percentile checks.nat1s checks.rolls.length}}%]</label>
		<label class='even-row roll range'>{{checks.best}} – {{checks.worst}}</label>
		<label class='odd-row roll streak longest {{#if (eq checks.rolls.length 0)}}no-streaks{{/if}}'>{{#if longestStreak}}<b>{{longestStreak.num}}</b> × {{longestStreak.count}}{{else}}n/a{{/if}}</label>
		<details class='roll streaks {{#if (eq streaks.length 0)}}no-streaks{{/if}}'>
			<summary class='even-row'><b>{{streaks.length}}</b> streak(s)</summary>
			<div>
			{{#each superStreaks}}
				<div class='flexrow streak {{#if (rollStats-macro-every-nth @index 2)}}odd-row{{else}}even-row{{/if}} {{#if (eq count 0)}}no-rolls{{/if}}'>
					<label class='result'><b>{{num}}</b></label>
					×<label class='count'>{{count}}</label>
					{{#if (gt repeat 1)}}
					×<label class='repeat'>{{repeat}}</label>
					{{/if}}
				</div>
			{{/each}}
			</div>
		</details>
		<details class='spread {{#if (eq checks.rolls.length 0)}}no-rolls{{/if}}'>
			<div class='grid'>
			{{#each checks.spread as |count size|}}
				<div class='flexrow counts {{#if (rollStats-macro-every-nth @index 2)}}even-row{{else}}odd-row{{/if}} {{#if (eq count 0)}}no-stats{{/if}}'>
					<label class='die-size'><b>{{size}}</b></label> ×<label class='count'>{{count}}</label>
				</div>
			{{/each}}
			</div>
		</details>

		<label></label>
		<details class='roll-subject {{#if (eq attacks.rolls.length 0)}}no-rolls{{/if}}'>
			<summary class='even-row'><b>{{attacks.rolls.length}}</b> checks </summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{attacks.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{attacks.mode}}</b> ×{{attacks.modeCount}} [{{rollStats-macro-percentile attacks.modeCount attacks.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{attacks.nat20s}} [{{rollStats-macro-percentile attacks.nat20s attacks.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{attacks.nat1s}} [{{rollStats-macro-percentile attacks.nat1s attacks.rolls.length}}%]</span>
			</div>
		</details>
		<details class='roll-subject {{#if (eq attacksConfirms.rolls.length 0)}}no-rolls{{/if}}'>
			<summary><b>{{attacksConfirms.rolls.length}}</b> checks </summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{attacksConfirms.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{attacksConfirms.mode}}</b> ×{{attacksConfirms.modeCount}} [{{rollStats-macro-percentile attacksConfirms.modeCount attacksConfirms.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{attacksConfirms.nat20s}} [{{rollStats-macro-percentile attacksConfirms.nat20s attacksConfirms.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{attacksConfirms.nat1s}} [{{rollStats-macro-percentile attacksConfirms.nat1s attacksConfirms.rolls.length}}%]</span>
			</div>
		</details>
		<details class='roll-subject {{#if (eq skills.rolls.length 0)}}no-rolls{{/if}}'>
			<summary class='even-row'><b>{{skills.rolls.length}}</b> checks</summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{skills.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{skills.mode}}</b> ×{{skills.modeCount}} [{{rollStats-macro-percentile skills.modeCount skills.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{skills.nat20s}} [{{rollStats-macro-percentile skills.nat20s skills.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{skills.nat1s}} [{{rollStats-macro-percentile skills.nat1s skills.rolls.length}}%]</span>
			</div>
		</details>
		<details class='roll-subject {{#if (eq saves.rolls.length 0)}}no-rolls{{/if}}'>
			<summary><b>{{saves.rolls.length}}</b> checks</summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{saves.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{saves.mode}}</b> ×{{saves.modeCount}} [{{rollStats-macro-percentile saves.modeCount saves.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{saves.nat20s}} [{{rollStats-macro-percentile saves.nat20s saves.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{saves.nat1s}} [{{rollStats-macro-percentile saves.nat1s saves.rolls.length}}%]</span>
			</div>
		</details>
		<details class='roll-subject {{#if (eq ability.rolls.length 0)}}no-rolls{{/if}}'>
			<summary class='even-row'><b>{{ability.rolls.length}}</b> checks</summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{ability.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{ability.mode}}</b> ×{{ability.modeCount}} [{{rollStats-macro-percentile ability.modeCount ability.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{ability.nat20s}} [{{rollStats-macro-percentile ability.nat20s ability.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{ability.nat1s}} [{{rollStats-macro-percentile ability.nat1s ability.rolls.length}}%]</span>
			</div>
		</details>
		<details class='roll-subject {{#if (eq unidentified.rolls.length 0)}}no-rolls{{/if}}'>
			<summary><b>{{unidentified.rolls.length}}</b> checks</summary>
			<div class='flexrow'>
				<label>Average:</label> <span>{{unidentified.average}}</span>
			</div>
			<div class='flexrow'>
				<label>Mode:</label> <span><b>{{unidentified.mode}}</b> ×{{unidentified.modeCount}} [{{rollStats-macro-percentile unidentified.modeCount unidentified.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 20s:</label> <span>{{unidentified.nat20s}} [{{rollStats-macro-percentile unidentified.nat20s unidentified.rolls.length}}%]</span>
			</div>
			<div class='flexrow'>
				<label>Nat 1s:</label> <span>{{unidentified.nat1s}} [{{rollStats-macro-percentile unidentified.nat1s unidentified.rolls.length}}%]</span>
			</div>
		</details>
	</div>
	{{/each}}
</div>
<hr>
`;

	function everyNth(index, nth) {
		return index % nth === 0;
	}

	function percentile(num, total) {
		return Math.roundDecimals(num / total * 100, 1);
	}

	Handlebars.registerHelper('rollStats-macro-every-nth', everyNth);
	Handlebars.registerHelper('rollStats-macro-percentile', percentile);
	let content;
	try {
		const msgNewest = game.messages.get(stats.newest);
		const msgOldest = game.messages.get(stats.oldest);

		const template = Handlebars.compile(hbs);
		const lastDate = new Date(msgOldest.timestamp);
		const today = new Date(Date.now()).toLocaleDateString() === lastDate.toLocaleDateString();


		let period = (msgNewest.timestamp - msgOldest.timestamp) / 1_000;
		let periodUnits = 'seconds';
		if (period >= 86_400) {
			period /= 86_400;
			periodUnits = 'days';
		}
		else if (period >= 3_600) {
			period /= 3_600;
			periodUnits = 'hours';
		}
		else if (period >= 300) {
			period /= 60;
			periodUnits = 'minutes';
		}

		period = Math.roundDecimals(period, 2);
		const templatedata = {
			users,
			period,
			periodUnits,
			messages: stats.messages,
			config: rollStatsCfg,
			lastTimestamp: (today ? '' : lastDate.toLocaleDateString() + ' ') + lastDate.toLocaleTimeString(),
		};
		content = template(templatedata, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	}
	finally {
		Handlebars.unregisterHelper('rollStats-macro-every-nth');
		Handlebars.unregisterHelper('rollStats-macro-percentile');
	}

	class StatDialog extends Dialog {
		/**
		 * @param {JQuery} jq
		 */
		activateListeners(jq) {
			super.activateListeners(jq);
			const html = jq[0].parentElement;
			const app = html.closest('.app.dialog');
			app.style.minHeight = '21rem';
			app.style.minWidth = '28rem';
			const buttons = html?.querySelector('.dialog-buttons');
			buttons.style.height = 'min-content';
			buttons?.querySelectorAll('button').forEach(el => el.style.height = 'min-content');
		}
	}

	new StatDialog({
		title: 'Roll Stats',
		content,
		buttons: {
			dismiss: {
				label: 'OK'
			}
		},
		default: 'dismiss'
	}, { resizable: true }).render(true);
}

async function getNewConfiguration() {
	const hbs = `
<form autocomplete='off'>
	<div class='form-group'>
		<label>Max idle minutes:</label> <input name='maxIdle' type='number' data-dtype='Number' step='1' min='5' value='{{maxIdle}}'>
		<p class='hint'>If period between messages is longer than this, stop searching.</p>
	</div>
	<div class='form-group'>
		<label>Max backlog days:</label> <input name='maxDays' type='number' data-dtype='Number' step='1' min='1' value='{{maxDays}}'>
		<p class='hint'>Don't look at messages older than this.</p>
	</div>
</form>
<hr>
`;

	const template = Handlebars.compile(hbs);
	const content = template(rollStatsCfg, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	return Dialog.prompt({
		title: 'Roll Stats Configuration',
		content,
		callback: html => {
			const form = html[0].querySelector('form');
			const fd = new FormDataExtended(form).object;
			Object.assign(rollStatsCfg, fd);
			return fd;
		},
		rejectClose: true,
	});
}

const isShift = game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.SHIFT);
if (!rollStatsCfg.skipPrompt && !isShift || rollStatsCfg.skipPrompt && isShift)
	await getNewConfiguration();

const { users, stats } = scanChatMessages();

console.log(users, stats);

generateStatDialog(users, stats);
