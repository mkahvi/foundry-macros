// Presents a dialog to apply Temp HP change to selected tokens.
// If invoked from a macro button, it tries to parse the button label for instructions.

//     Example: @Macro[Change Temp HP]{5}

// The label matching functions by searching for a number (only first instance is used in case there's multiple)...
// ... everything else is ignored.

// Additionally, thanks to Noon, the macro allows the label to contain reference to the immediately adjacent inline roll with...
//     '->' at the end of the label. This will override any internal number usage if any are present.
// This allows you to chain macros and inline rolls for more varied effects.
// The minus has no meaning, it is there simply to denote an arrow.

//     Example: @Macro[Change Temp HP]{Add ->}[[50]]

// BUGS: Unlinked tokens might not work correctly. Untested.

// Revision: 4 (2023-11-11)

// Compatibility:
// - Foundry VTT v11
// - Pathfinder v9.4

const lang = {
	change: 'Change',
	dismiss: 'Dismiss',
	actors: 'Chosen actors (×{count}):', // 0 = number of actors
	noValidActors: 'No valid actors selected!',
	changeResult: 'Temp HP changed by...',
	actualChange: '{value} temp HP to...',
};

const opts = {
	printChatCard: true,
	defaultChange: 0,
	siblingMarker: '->',
}

const signNum = (num) => num < 0 ? num : `+${num}`;

const tokens = canvas.tokens.controlled;

if (tokens.length <= 0)
	return void ui.notifications.warn(lang.noValidActors);

function printChatCard(amount) {
	const template = `<div class='flexcol'>
		<label>{{change}}</label>
		<div class='targets'>
		{{#each tokens}}
			<i style='white-space:nowrap;'>{{name}}</i>{{#unless @last}}, {{/unless}}
		{{/each}}
		</div>
	</div>`;

	const templateData = {
		change: lang.actualChange.replace('{value}', signNum(amount)),
		tokens,
	};

	const msgData = {
		content: Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
	};

	ChatMessage.applyRollMode(msgData, game.settings.get('core', 'rollMode'));

	ChatMessage.create(msgData);
}

async function applyTempHP(value) {
	console.log('Applying', signNum(value), 'to', tokens.length, 'tokens.');
	if (!Number.isSafeInteger(value) || value == 0)
		return void ui.notifications.warn(`"${value}" value is not meaningful, ignoring.`);

	const actors = tokens.map(t => t.actor).filter(a => a != null);

	const updates = [];

	for (const actor of actors) {
		const hp = actor.system.attributes.hp;
		const oldTemp = hp.temp;
		const newTemp = oldTemp + value;
		console.log(actor.name, oldTemp, '->', newTemp);

		const updateData = { _id: actor.id, 'system.attributes.hp.temp': newTemp };

		updates.push(updateData);
	}

	await Actor.implementation.updateDocuments(updates);

	console.log('Applied', signNum(value), 'temp HP to', actors.length, 'actors.');

	if (opts.printChatCard) printChatCard(value);
}

async function showDialog(value = 0) {
	console.log('Rendering temp HP dialog with', value, 'value.');

	const template = `
	<div class='flexcol' style='gap:0.5rem'>
		<div class='flexcol'>
			<label>{{labels.actors}}</label>
			<label style='font-weight:bold;'>{{selection}}</label>
		</div>
		<div class='flexrow'>
			<label>{{labels.change}}</label>
			<input name='change' type='number' step='1' value='{{value}}'>
		</div>
		<hr>
	</div>
	`;

	const templateData = {
		labels: {
			actors: lang.actors.replace('{count}', tokens.length),
			change: lang.change,
		},
		selection: tokens.map(t => t.actor && t.actor.name !== t.name ?
			`${t.name} (${t.actor.name})` : t.name
		).join(', '),
		value: value,
	};

	return Dialog.wait({
		title: 'Change Temp HP',
		content: Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		buttons: {
			harm: {
				label: lang.change,
				icon: '<i class="fas fa-meteor"></i>',
				callback: html => applyTempHP(html.querySelector('input[name=change]').valueAsNumber),
			},
			dismiss: {
				label: lang.dismiss,
				icon: '<i class="fas fa-power-off"></i>',
			},
		},
		default: 'harm',
	},
	{
		jQuery: false,
		width: 360,
	});
}

const label = event.target.closest('button,a')?.textContent.trim(); // macro label

let change = opts.defaultChange;

if (label?.length > 0) {
	let numSource = label;
	// if (label.startsWith('<-'))
	//	numSource = event.target.previousElementSibling?.textContent.trim();
	// else
	if (label.endsWith(opts.siblingMarker))
		numSource = event.target.nextElementSibling?.textContent.trim() ?? numSource;

	const num = numSource.match(/[+-]?\d+/);
	change = num?.length > 0 ? parseInt(num[0], 10) || opts.defaultChange : opts.defaultChange;
}

if (event.shiftKey) return applyTempHP(change);

showDialog(change);
