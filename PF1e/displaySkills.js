/**
 * Display Skills
 *
 * Displays skills of all selected tokens.
 */

// Array of actor IDs to use instead of selection
let preConfiguredActors = [];
// Convert actor IDs to actors
preConfiguredActors = preConfiguredActors.map(aid => game.actors.get(aid));

// TODO: Sort skills alphabetically
/* global pf1 */
const skills = Object.entries(pf1.config.skills)
	.reduce((all, [sklId, label]) => {
		all[sklId] = { id: sklId, label, best: -Infinity, worst: Infinity };
		return all;
	}, {});

const skillKeys = Object.values(skills).map(s => s.id);

// TODO: Sort actors alphabetically
const actors = (preConfiguredActors.length ? preConfiguredActors : game.canvas.tokens.controlled
	.map(t => t.document.actor))
	.filter(a => !!a)
	.map(actor => {
		const data = {
			name: actor.name,
			id: actor.id,
			img: actor.img,
			actor: actor,
			skills: {}
		}

		for (const sklId of skillKeys) {
			const info = actor.getSkillInfo(sklId);
			data.skills[sklId] = { mod: info.mod };
		}

		return data;
	});

if (actors.length > 1) {
	for (const sklId of skillKeys) {
		const skl = skills[sklId];
		for (const actor of actors) {
			const mod = actor.skills[sklId].mod;
			if (skl.best < mod) skl.best = mod;
			if (skl.worst > mod) skl.worst = mod;
		}
	}
}

const template = `
	<form autocomplete='false'>
		<style>
		.dialog.actor-skill-dialog ul {
			list-style: none;
			margin: 0;
			padding: 0;

			display: grid;
			grid-template-columns: min-content repeat(${actors.length}, 6em);
		}
		.dialog.actor-skill-dialog h4 {
			margin: 0;
			padding: 0 3px;
		}
		.dialog.actor-skill-dialog ul li {
			display: contents;
		}
		.dialog.actor-skill-dialog ul li .label {
			white-space: nowrap;
		}
		.dialog.actor-skill-dialog ul li:hover > * {
			background-color: gold;
		}
		.dialog.actor-skill-dialog ul li.header > * {
			background-color: rgba(0,0,0,0.1);
		}
		.dialog.actor-skill-dialog ul li .name,
		.dialog.actor-skill-dialog ul li .value {
			text-align: center;
		}
		.dialog.actor-skill-dialog ul li .value.best {
			font-weight: bold;
		}
		.dialog.actor-skill-dialog ul li .value.worst {
			opacity: 0.4;
		}
		.dialog.actor-skill-dialog ul li .name.active {
			font-weight: bold;
			text-shadow: 0 0 3px gold;
		}
		</style>
		<ul class='skill-list'>
			<li class='header'>
				<h4 class'header'>Skill</h4>
				{{#each actors}}
				<h4 data-actor-id="{{id}}" class='name'>{{name}}</h4>
				{{/each}}
			</li>
			{{#each skills}}
			<li class='skill'>
				<h4 class='label'>{{label}}</h4>
				{{#each @root.actors}}
				{{#with (lookup skills ../id)}}
				<span data-actor-id="{{../id}}" class='value {{#if (eq mod ../../best)}}best{{/if}} {{#if (eq mod ../../worst)}}worst{{/if}}'>{{numberFormat mod sign=true}}</span>
				{{/with}}
				{{/each}}
			</li>
			{{/each}}
		</ul>
	</form>
	<hr>
	`;

const templateData = {
	skills,
	actors,
};

const content = Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

new Dialog({
	title: 'Skills',
	content,
	buttons: {
		ok: {
			label: 'OIC'
		}
	},
	render: (/** @type {Element} */ html) => {
		const header = html.querySelector('li.header');
		for (const el of html.querySelectorAll('.value')) {
			el.addEventListener('mouseleave', () => {
				header.querySelectorAll('.active')
					.forEach(sel => sel.classList.remove('active'));
			}, { passive: true });

			el.addEventListener('mouseenter', (ev) => {
				const aid = ev.target.dataset.actorId;
				header.querySelectorAll('.name').forEach(sel => {
					if (sel.dataset.actorId === aid)
						sel.classList.add('active');
				});
			}, { passive: true });
		}
	}
},
{
	classes: [...Dialog.defaultOptions.classes, 'actor-skill-dialog'],
	jQuery: false,
	rejectClose: false,
	width: 'auto',
})
	.render(true);
