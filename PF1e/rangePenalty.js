// Opens a dialog for calculating range penalty.

// TODO:
// - Calculate range to target from currently selected token.
// - Dropdown menu for selected actor's ranged attacks
// - Account for elevation
// - Support metric

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

const opts = {
	sectorAdjust: true, // Calculates distances based on closest corner or edge rather than center of square
	avoidDialog: true, // displays notification only if sufficient data is provided. Shift invoking the macro toggles this on the fly.
	penalty: -2, // per increment penalty
};

const lang = {
	title: 'Range Penalty',
	dismiss: 'Dismiss',
};

// TODO: Call for all units.
function normalizeDistance(num, units) {
	return num;
}

const normAngle = (ray) => {
	const a = ray.angle % (2 * Math.PI);
	return a < 0 ? a + 2 * Math.PI : a;
};

function calculateIncrement(rangeIncrement, totalDistance) {
	const d = totalDistance, r = rangeIncrement;
	let p = Math.floor(d / r);
	// console.log('_calcPenalty', d, '/', r, '=', p);
	// console.log('actual(', d / r, '), modulo(', d % r, ')');
	if (d % r === 0) p--; // correction for edge cases

	return Math.max(0, p) + 1;
}

const calculatePenalty = (increment) => (increment - 1) * opts.penalty;

class RangeDialog extends Dialog {
	rangeIncrement = undefined;
	distanceToTarget = undefined;

	constructor(data, options, rangeIncrement, distanceToTarget) {
		super(data, options);
		this.data.title = lang.title;
		this.data.buttons = {
			dismiss: {
				label: lang.dismiss,
				icon: '<i class="fas fa-power-off"></i>',
			},
		};
		this.data.default = 'dismiss';

		this.distanceToTarget = distanceToTarget;
		this.rangeIncrement = rangeIncrement;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			template: 'templates/hud/dialog.html',
			classes: ['dialog'],
			width: 'auto',
			jQuery: true,
		});
	}

	static open(rangeIncrement = 0, distanceToTarget = 0) {
		const html = $('<div/>').addClass('flexcol');

		const max2Decimals = (n) => Math.floor(n * 100) / 100;
		rangeIncrement = max2Decimals(rangeIncrement);
		distanceToTarget = max2Decimals(distanceToTarget);

		const increment = calculateIncrement(rangeIncrement, distanceToTarget);
		const penalty = calculatePenalty(increment);

		const row = () => $('<div/>').addClass('flexrow');
		const label = (text) => $('<label/>').text(text);
		const input = (name) => $('<input type="number" />').attr({ name: name });

		html.append(
			row().append(label('Range increment')).append(input('range').attr({ value: rangeIncrement })),
			row().append(label('Distance to target')).append(input('distance').attr({ value: distanceToTarget })),
			$('<hr/>'),
			row().append(label('Increment')).append(input('increment').attr({ value: increment })),
			row().append(label('Penalty')).append(input('penalty').attr({ value: penalty })),
			$('<hr/>')
		);

		new RangeDialog({ content: html.html() }, {}, rangeIncrement, distanceToTarget).render(true);
	}

	range = undefined;
	distance = undefined;
	penalty = undefined;
	increment = undefined;

	_changeRange(jq) {
		this.rangeIncrement = parseInt(jq.target.value, 10);
		this._calcPenalty();
	}

	_changeDistance(jq) {
		this.distanceToTarget = parseInt(jq.target.value, 10);
		this._calcPenalty();
	}

	_calcPenalty() {
		const increment = calculateIncrement(this.rangeIncrement, this.distanceToTarget);
		this.increment.val(increment);
		this.penalty.val(calculatePenalty(increment));
	}

	activateListeners(html) {
		super.activateListeners(html);

		this.range = html.find('input[name="range"]');
		this.distance = html.find('input[name="distance"]');
		this.increment = html.find('input[name="increment"]');
		this.penalty = html.find('input[name="penalty"]');

		this.range.on('change', this._changeRange.bind(this));
		this.distance.on('change', this._changeDistance.bind(this));
		this.increment.prop('readonly', true);
		this.penalty.prop('readonly', true);

		this._calcPenalty();
	}
}

/** HELPER FUNCTIONS */

const rad2deg = (rad) => rad * (180 / Math.PI);

function getRay(pointA, pointB, scene) {
	return {
		/* global Ray */
		ray: new Ray(pointA, pointB),
		get cellDistance() { // cells
			return this.ray.distance / scene.data.grid;
		},
		get unitDistance() { // feet, meters, whatever
			return this.cellDistance * scene.data.gridDistance;
		},
		get normAngle() {
			return normAngle(this.ray);
		}
	}
}

function debugRay(rd, label) {
	console.group(label);
	console.log('Distance:', Number(rd.unitDistance.toFixed(2)), 'units', Number(rd.cellDistance.toFixed(2)), 'cells');
	console.groupCollapsed('Data');
	console.log('Origin:', rd.ray.A);
	console.log('Target:', rd.ray.B);
	console.log('Object:', rd.ray);
	console.groupEnd();
	console.groupEnd();
}

function basicDebug(source, target, item) {
	console.groupCollapsed('Basic Info');
	try {
		if (item) console.log('Item:', item.name, item);
		console.log('Target:', target?.name ?? 'Unknown', `[${target.actor?._id}]`);
		console.log('Source:', source.name, `[${source.actor._id}]`);
	}
	finally {
		console.groupEnd();
	}
}

// console.log('Sector.base:', angleDeg / sectorAngle);
// Sector: 45 degrees
// 0 = R, 1 = BR, 2 = B, 3 = BL, 4 = L, 5 = TL, 6 = T, 7 = TR
const sectorMap = {
	0: { x: 0.5, y: 0, label: 'MR' }, // middle right
	1: { x: 0.5, y: 0.5, label: 'BR' }, // bottom right,
	2: { x: 0, y: 0.5, label: 'BM' }, // bottom middle,
	3: { x: -0.5, y: 0.5, label: 'BL' }, // bottom left,
	4: { x: -0.5, y: 0, label: 'ML' }, // middle left,
	5: { x: -0.5, y: -0.5, label: 'TL' }, // top left,
	6: { x: 0, y: -0.5, label: 'TM' }, // top middle,
	7: { x: 0.5, y: -0.5, label: 'TR' }, // top right,
};

function sectorName(sector) {
	const l = { M: 'Middle', T: 'Top', B: 'Bottom', L: 'Left', 'R': 'Right' };
	const a = '→↘↓↙←↖↑↗';
	const s = sectorMap[sector].label;
	return `${l[s[0]]} ${l[s[1]]} ${a[sector]}`;
}

// new Token(tokenData) loses tokenData's x/y coordinates
function sectorAdjust(sector, token, origin) {
	const adjust = sectorMap[sector];
	// console.log(sector, adjust, token, origin);
	return {
		x: adjust.x * token.w + origin.x,
		y: adjust.y * token.h + origin.y
	};
}

/**
 * Get 45 degree sector number, 0 to 7 clockwise, starting from right.
 *
 * @param {Number} radian Radian to convert into sector
 * @returns {Number} Sector
 */
function getSector(radian) {
	const clampDeg = (d) => d >= 360 ? d - 360 : d;
	const clampRad = (r) => r >= Math.PI * 2 ? r - Math.PI * 2 : r;
	const wrapSector = (s) => s >= 8 ? 0 : s;
	const angleDeg = clampDeg(rad2deg(radian)),
		sectorAngle = 360 / 8, // 8 sectors
		// sectorMargin = sectorAngle / 2; //
		sector = wrapSector(Math.round(angleDeg / sectorAngle));
	console.log('rad:', Number(radian.toFixed(3)), '-> deg:', Number(angleDeg.toFixed(2)), '= sector:', sector, `[${sectorName(sector)}]`);
	return sector;
}

// Corner
// 1 = BR, 2 = BL, 3 = TL, 4 = TR
function getCorner(radian) {
	const angleDeg = rad2deg(radian),
		cornerAngle = 360 / 4, // 4 corners
		cornerMargin = cornerAngle / 2, //
		corner = Math.round((angleDeg + cornerMargin) / cornerAngle);
	// console.log('Corner:', corner);
	return corner;
}

/** ACTUAL FUNCTIONALITY */

// Attached to chat message
function handleCard(cm) {
	console.log('Call Source: Chat Message');
	const targets = Array.from(cm.user.targets).filter(t => t.visible);
	const target = targets[0]; // first target only. TODO: Make a dropdown selection?

	// TODO: Is target visible? To avoid spoiling hidden things being moved around.

	const tokenId = cm.data.speaker.token,
		scene = game.scenes.get(cm.data.speaker.scene),
		tokenData = scene.data.tokens.find(t => t._id === tokenId),
		token = new Token(tokenData, scene); // fills .w and .h

	const item = cm.itemSource,
		itemUnits = item.data.data.range.units,
		itemRange = parseInt(item.data.data.range.value, 10);

	console.log('Range increment:', itemRange, 'units');
	if (scene.data.gridUnits !== itemUnits)
		console.warn('Unit mismatch:', scene.data.gridUnits, 'in scene,', item.data.data.range.units, 'in attack');

	// NO TARGET
	if (!target) {
		console.warn('NO TARGET');
		return { rd: undefined, range: itemRange, sources: [], targets: [], attacks: [] };
	}

	basicDebug(target, token, item);

	const origin = { x: tokenData.x, y: tokenData.y };
	const destination = { x: target.position.x, y: target.position.y };

	const rd = getRay(origin, destination, scene);
	debugRay(rd, 'Basic Ray');

	if (!opts.sectorAdjust) return { rd: rd, range: itemRange, sources: [], targets: [], attacks: [] };

	const originSector = getSector(rd.normAngle),
		newOrigin = sectorAdjust(originSector, token, origin);
	// console.log('Origin:', origin, '- Adjusted:', newOrigin);
	const targetSector = getSector(rd.normAngle + Math.PI), // reverse sector
		newDest = sectorAdjust(targetSector, target, destination);
	// console.log('Destination:', destination, '- Adjusted:', newDest);

	console.log('Connecting sectors: [', sectorName(originSector), '] to [', sectorName(targetSector), ']');

	const nrd = getRay(newOrigin, newDest, scene);
	debugRay(nrd, 'Adjusted Ray');

	const increment = calculateIncrement(itemRange, nrd.unitDistance);

	console.log('Increment:', increment);
	console.log('Penalty:', calculatePenalty(increment));

	return { rd: nrd, range: itemRange, sources: [], targets: [], attacks: [] };
}

function handleOther() {
	console.log('Call source: Generic');

	const sources = canvas.tokens.controlled;
	const source = sources[0];

	const targets = Array.from(game.user.targets).filter(t => t.visible);
	const target = targets[0];

	const scene = game.scenes.viewed;

	if (source && target) {
		basicDebug(source, target);
		const rd = getRay({ x: source.x, y: source.y }, { x: target.x, y: target.y }, scene);

		return { rd: rd, range: 0, sources: sources, targets: targets, attacks: [] };
	}

	return { rd: undefined, range: 0, sources: sources, targets: targets, attacks: [] };
}

console.group('Range Penalty');
const cm = game.messages.get(event.target.closest('.message')?.dataset.messageId);
const d = cm ? handleCard(cm) : handleOther();
const distanceToTarget = d.rd?.unitDistance ?? 0;
const rangeIncrement = d.range;
console.groupEnd();

const increment = calculateIncrement(rangeIncrement, distanceToTarget);
if (!isNaN(increment)) {
	const penalty = calculatePenalty(increment);
	ui.notifications.info(`Range penalty: ${penalty}, at distance of ${distanceToTarget.toFixed(2)} units, at increment ${increment}.`);
	console.log('Range penalty:', penalty, 'at distance of', Number(distanceToTarget.toFixed(2)), 'units, at increment', increment);
}

RangeDialog.open(rangeIncrement, distanceToTarget);
