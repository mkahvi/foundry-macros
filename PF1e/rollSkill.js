// Usage: @Macro[rollSkill]{Skill Name}

// Example usage: Cornugon Smash attack context note for free intimidate check.
//                @Macro[rollSkill]{Intimidate}

// Limitation #1: Custom skills and subskills not supported.

// Compatibility:
// - Foundry VTT 0.7.x – v9
// - Pathfinder 0.77.24 – 0.80.24

const opt = {
	// ownerChentric
	// true = speaker > selected > configured  – great for very owner centric selection
	// false = selected > configured > speaker – great for flexibility on who rolls (for requesting rolls, better for most circumstances)
	favorSpeaker: false, // Favor speaker (character that printed the card) for rolling. If False, favors configuration/selection.

	skipDialog: false // skip skill prompt dialog
}

// Return first non-empty list
const firstNonEmpty = (...lists) => {
	for (const l of lists) {
		const l0 = l.filter(i => actor?.isOwner);
		if (l0.length) return l0;
	}
	return [];
}

const skillName = event.target.closest('a')?.textContent.trim(); // macro label

// TODO: Find skills from actors instead, adding support for custom skills.
// TODO: Check for similar-ish matches? Probably horrible idea with things like Chinese.
const [skillId, _] = Object.entries(CONFIG.PF1.skills)
	.find(([_skId, skName]) => skName.localeCompare(skillName, undefined, { ignorePunctuation: true, sensitivity: 'base', usage: 'search' }) === 0);

// get attached chat message if any
const chatMessage = game.messages.get(event.target.closest('.message[data-message-id]')?.dataset.messageId);

// Get actor from speaker
const getSpeaker = (cm) => ChatMessage.getSpeakerActor(cm?.data.speaker);

// Find suitable actors (ugh)
const actors = opt.favorSpeaker
	? // speaker > selected > configured – great for very owner centric selection
	firstNonEmpty(
		[getSpeaker(chatMessage)],
		canvas.tokens.controlled.map(t => t.actor),
		[game.user.character])
	: // selected > configured > speaker – great for flexibility on who rolls
	firstNonEmpty(
		canvas.tokens.controlled.map(t => t.actor),
		[game.user.character],
		[getSpeaker(chatMessage)]);

// if ((skill?.length || 0) < 3) // make sure the label is at least somewhat intelligible
//	return;

if (!actors.length) {
	const msg = 'No actor selected';
	console.warn(msg);
	return ui.notifications.error(msg);
}

if (!skillId) {
	const msg = `Bad skill name: ${skillName}`;
	console.warn(msg);
	return ui.notifications.error(msg);
}

if (skillId) actors.forEach(a => a.rollSkill(skillId, { skipDialog: opt.skipDialog || event.shiftKey }));
