/**
 * Rest Selected
 *
 * Rests selected tokens and provides dialog for advancing time by appropriate amount.
 */

const defaultTime = 8; // Hours
const useGroupSizeForDefault = true; // Get default time from group size if possible

// For resting times shorter than here, either there's periods without watches
// ... or the characters don't get full rest.
const groupTime = {
	1: 8, // No watches
	2: 16,
	3: 12,
	4: 10.666,
	5: 10,
	6: 9.6,
	7: 9.333,
	8: 9,
};

const actors = canvas.tokens.controlled.map(t => t.actor).filter(a => !!a);

const actualDefaultTime = useGroupSizeForDefault ? groupTime[actors.length] ?? groupTime[8] : defaultTime;

const performRest = async (data) => {
	const promises = [];
	for (const actor of actors) {
		try {
			const p = actor.performRest(data);
			promises.push(p);
		}
		catch (err) {
			ui.notifications.error(`Error resting "${actor.name}"`, { console: false });
			console.error(actor.name, 'couldn\'t rest\n', err, actor);
		}
	}

	await Promise.allSettled(promises);
	ui.notifications.info('All actors rested.');
};

class RestConfigDialog extends FormApplication {
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: ['pf1', 'actor-mass-rest'],
			template: 'systems/pf1/templates/apps/actor-rest.hbs',
			width: 500,
			closeOnSubmit: true
		};
	}

	get title() {
		return game.i18n.localize('PF1.Rest');
	}

	_updateObject(event, formData) {
		this.options = formData;
		performRest(formData).finally(() => this.resolve(this.options));
	}

	close(...args) {
		super.close(...args);
		if (!this.options) this.resolve(null);
	}

}

async function getNumber() {
	return Dialog.wait({
		title: 'Progress Time',
		content: `<p>Progress time by how many hours?</p><input type='number' name='time' min='0' value='${actualDefaultTime}'>`,
		buttons: {
			progress: {
				label: 'Pass Time',
				callback: (html) => html.querySelector('input').valueAsNumber
			}
		},
		close: () => NaN,
	},
	{
		jQuery: false,
		rejectClose: false,
	});
}

if (actors.length) {
	const rv = await new Promise((resolve) => {
		const rd = new RestConfigDialog();
		rd.resolve = resolve;
		rd.render(true, { focus: true });
	});
	if (!rv) return;
	const number = await getNumber();
	if (number > 0) game.time.advance(number * 3_600);
}
else {
	ui.notifications.warn('No actors selected.');
}
