/**
 * Coin Roller
 *
 * Rolls currencies for simplistic loot generation or just randomizing monetary values.
 *
 * Compatibility:
 * - Foundry v10.291
 * - Pathfinder 1e 0.82.5
 */

// Translations
const lang = {
	title: 'Coin Roller',
	button: 'Roll!',
	formula: 'PF1.Formula',
};

const currency = {
	pp: {
		label: 'PF1.CurrencyPlatinumP',
		image: 'icons/commodities/currency/coins-assorted-mix-platinum.webp'
	},
	gp: {
		label: 'PF1.CurrencyGoldP',
		image: 'icons/commodities/currency/coins-plain-stack-gold-yellow.webp',
	},
	sp: {
		label: 'PF1.CurrencySilverP',
		image: 'icons/commodities/currency/coins-shield-sword-stack-silver.webp',
	},
	cp: {
		label: 'PF1.CurrencyCopperP',
		image: 'icons/commodities/currency/coins-wheat-stack-copper.webp',
	}
};

// Pre-translate
Object.entries(lang).forEach(([key, value]) => lang[key] = game.i18n.localize(value));
Object.values(currency).forEach(c => c.label = game.i18n.localize(c.label));

const imgSize = 48;

const fakeInlineRoll = (roll) => {
	const escapedjson = escape(JSON.stringify(roll.toJSON()));
	return `<a class="inline-roll inline-result" data-tooltip="${roll.formula}"
	style='font-size:var(--font-size-18);font-weight:bold;border:unset;padding:unset;'
	data-roll="${escapedjson}">${roll.total}</a>`;
}

/**
 * @param {Object} fd Form data
 */
const createCard = (fd) => {
	// Build chat message contents
	const output = [];
	output.push('<div class="money-box" style=\'display:flex;flex-flow:row wrap;gap:3px;align-items:center;justify-content: center;\'>');
	['pp', 'gp', 'sp', 'cp'].forEach(c => {
		const formula = fd[c];
		if (!formula) return;

		const roll = RollPF.safeRoll(formula);
		const ir = fakeInlineRoll(roll);

		output.push(
			`<div style='display:flex;flex-flow:row nowrap;align-items:center;gap:3px;border:1px solid var(--color-border-light-2);padding:1px;border-radius:3px;' class='currency-${c}' data-tooltip='${currency[c].label}'>
			<img style='flex:1 ${imgSize}px;' width=${imgSize} height=${imgSize} src='${currency[c].image}'>
			<span style='flex:1;min-width:1.5em;flex-basis:fit-content;text-align:center;'>`,
			ir,
			'</span></div>'
		);
	});
	output.push('</div>');

	// Prepare message
	const chatData = {
		content: output.join('\n').replace('\t', ''),
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
	};
	ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
	// Woosh
	ChatMessage.create(chatData);
}

// Dialog contents
const content = `<form autocomplete='false' style='display:grid;grid-template-columns:2fr 3fr;grid-template-rows:min-content;gap:0.3rem;align-items:center;'>
	<label>${currency.pp.label}</label>
	<input type='text' data-dtype='String' placeholder='${lang.formula}' name='pp'>

	<label>${currency.gp.label}</label>
	<input type='text' data-dtype='String' placeholder='${lang.formula}' name='gp' autofocus>

	<label>${currency.sp.label}</label>
	<input type='text' data-dtype='String' placeholder='${lang.formula}' name='sp'>

	<label>${currency.cp.label}</label>
	<input type='text' data-dtype='String' placeholder='${lang.formula}' name='cp'>
</form><hr>`;

new Dialog({
	content,
	title: lang.title,
	buttons: {
		roll: {
			label: lang.button,
			callback: (html) => createCard(new FormDataExtended(html.querySelector('form')).object),
		}
	},
	default: 'roll',
},
{
	jQuery: false
})
	.render(true);
