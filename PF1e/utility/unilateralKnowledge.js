/**
 * Unilateral Knowledge
 * 
 * Rolls all knowledge skills, using the same base roll for all.
 * 
 * Bug? This may not work correctly on non-english.
 */

const alpha = false; // Sort alphabetically instead of highest to lowest
const commonroll = true; // Use shared roll for all. If false, all skills are rolled separately.

const kns = ["kar", "kdu", "ken", "kge", "khi", "klo", "kna", "kno", "kpl", "kre"];
const rollData = actor.getRollData();
const skills = kns.map(sklId => actor.getSkillInfo(sklId, { rollData }));

// Fix skill names
skills.forEach(skl => skl.name = /\((?<name>\w+)\)/.exec(skl.name)?.groups.name);

// Base roll
const roll = commonroll ? await new Roll("1d20").evaluate({ async: true }) : null;

// Roll all
for (let skl of skills) {
	skl.msg = new ChatMessage(await actor.rollSkill(skl.id, { dice: roll?.total, rollData, chatMessage: false, skipDialog: true }));
	skl.roll = skl.msg.rolls[0];
	skl.total = skl.roll.total;
	skl.anchor = skl.roll.toAnchor().outerHTML;
}

skills.sort((a, b) => b.roll.total - a.roll.total);

const trained = [];
const untrained = [];
skills.forEach(skl => {
	if (skl.rank > 0) trained.push(skl);
	else untrained.push(skl);
});

if (alpha) {
	trained.sort((a, b) => a.name.localeCompare(b.name, game.i18n.lang));
	untrained.sort((a, b) => a.name.localeCompare(b.name, game.i18n.lang));
}

let content = `
{{#if commonroll}}
<p><label>Base roll:</label> {{{anchor}}}</p>
{{/if}}
<h3>Knowledge</h3>
{{#if trained.length}}
<p>
{{#each trained}}
<span data-skill-id="{{id}}" style="white-space:nowrap;">
	<label>{{name}}</label> {{{anchor}}}{{#if (not @last)}}, {{/if}}
</span>
{{/each}}
</p>
{{else}}
<p>No trained skills.</p>
{{/if}}
{{#if untrained.length}}
<h3>Untrained</h3>
<p>
{{#each untrained}}
<span data-skill-id="{{id}}" style="white-space:nowrap;">
	<label>{{name}}</label> {{{anchor}}}†{{#if (not @last)}}, {{/if}}
</span>
{{/each}}
</p>
{{/if}}
`;

const templateData = {
	commonroll,
	roll,
	anchor: roll?.toAnchor().outerHTML,
	skills,
	trained,
	untrained,
};

content = Handlebars.compile(content)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

ChatMessage.create({ content, speaker: ChatMessage.getSpeaker({ actor, token }) }, { rollMode: game.settings.get("core", "rollMode") });
