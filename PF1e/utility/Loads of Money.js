const actors = canvas.tokens.controlled.map(t => t.actor).filter(a => !!a);

if (actors.length == 0) {
	return void ui.notifications.warn("No actors selected!");
}

const recipients = actors.map(a => a.name).join(", ");

const grants = await Dialog.wait({
	title: 'How much money?',
	content: `
	<form autocomplete="off">
	    <label><b>Recipients</b>: ${recipients}</label>
		<hr>
		<label class="flexrow" style="align-items:center;">
			<input type="checkbox" name="weightless">
			Weightless?
		</label>
		<hr>
		<div class="flexrow" style="gap:0.3em;">
			<label class="flexrow" style="align-items:center;gap:0.3em;">
				PP
				<input type="number" min="0" step="1" name="pp" placeholder="0" style="text-align:center;">
			</label>
			<label class="flexrow" style="align-items:center;gap:0.3em;">
				GP
				<input type="number" min="0" step="1" name="gp" placeholder="0" style="text-align:center;">
			</label>
			<label class="flexrow" style="align-items:center;gap:0.3em;">
				SP
				<input type="number" min="0" step="1" name="sp" placeholder="0" style="text-align:center;">
			</label>
			<label class="flexrow" style="align-items:center;gap:0.3em;">
				CP
				<input type="number" min="0" step="1" name="cp" placeholder="0" style="text-align:center;">
			</label>
		</div>
		<hr>
	</form>
	`,
	buttons: {
		launder: {
			label: 'Throw Money',
			callback: ([html]) => new FormDataExtended(html.querySelector("form")).object,
		}
	},
	close: () => null,
},
{
	rejectClose: false
});

if (!grants) return;

function initWealth(w) {
	w.cp ??= 0;
	w.sp ??= 0;
	w.gp ??= 0;
	w.pp ??= 0;
}

initWealth(grants);

const { weightless } = grants;

for (const actor of actors) {
	const currency = { ...((weightless ? actor.system.altCurrency : actor.system.currency) ?? {}) };
	initWealth(currency);

	currency.cp += grants.cp;
	currency.sp += grants.sp;
	currency.gp += grants.gp;
	currency.pp += grants.pp;

	let path = weightless ? "system.altCurrency" : "system.currency";
	actor.update({ [path]: currency });
}
