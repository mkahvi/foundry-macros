/**
 * Condition toggle dialog
 * 
 * Compatibiliy:
 * - Foundry VTT v11
 * - Pathfinder 1e v10
 */

const style = `
<style>
.app.dialog {
	& .condition-search {
		margin-bottom: 0.5em;
	}
	& .condition-selector label {
		flex:0 64px;
		align-items:center;
		&.unmatch {
			opacity: 0.2;
		}
		& img {
			opacity: 0.5;
			pointer-events: none;
		}
	}
	& .condition-selector label.active {
		color: darkblue;
		& img {
			opacity: 0.8;
		}
	}
	& .condition-selector label:hover img {
		opacity: 1;
	}
	& .condition-selector input {
		display: none;
	}
	& .condition-selector h5 {
		margin:0;
		padding:0;
		pointer-events: none;
	}
}
</style>
`;

const search = `
<div class='condition-search'>
	<input type='search' placeholder='Search...'>
</div>
`;

const gotOne = canvas.tokens.controlled?.length === 1;

const conditions = [...pf1.registry.conditions]
	.sort((a, b) => a.name.localeCompare(b.name))
	.map(c => {
		const css = (gotOne && actor.hasCondition(c.id)) ? "active" : "";
		return `
		<label class="flexcol ${css}" data-condition-id="${c.id}">
			<input type="radio" name="condition" value="${c.id}">
			<img width="64" height="64" src="${c.texture}">
			<h5>${c.name}</h5>
		</label>`;
	});

function doSearch(ev) {
	const value = ev.target.value;
	const re = new RegExp(value, "i");
	const html = ev.target.closest(".dialog-content");
	html.querySelectorAll(".condition-selector [data-condition-id]").forEach(el => {
		if (!value) return el.classList.remove('unmatch');
		const { conditionId } = el.dataset;
		const c = pf1.registry.conditions.get(conditionId);
		const match = re.test(c.name);
		el.classList.toggle("unmatch", !match);
	});
}

function toggleCondition(conditionId) {
	if (!conditionId) throw new Error("Shnickles");

	canvas.tokens.controlled
		.map(t => t.actor)
		.filter(a => a?.isOwner)
		.forEach(a => a.toggleCondition(conditionId));
}

const app = new Dialog({
	content: style + search + "<div class='flexrow condition-selector' style='gap:3px;'>" + conditions.join("") + "<div>",
	title: "Select Condition",
	buttons: {},
	render: (html) => {
		html.querySelector("input[type='search']").addEventListener("input", doSearch);
		html.querySelectorAll("[data-condition-id]").forEach(el => el.addEventListener("click", ev => {
			ev.preventDefault();
			app.close();
			toggleCondition(ev.target.dataset.conditionId);
		}));
	}
},
{
	jQuery: false,
	rejectClose: false,
	width: 420,
});

app.render(true);
