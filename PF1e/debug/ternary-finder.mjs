/**
 * Finds ternary formula usage
 */

// Descriptions might get matched
const IGNORE_DESCRIPTIONS = true;

const results = [];

function findFromActors(actors) {
	for (const actor of actors) {
		if (!actor.isOwner) continue;
		const found = {};
		for (const item of actor.items) {
			const testFlat = (obj, ppath) => {
				const flat = foundry.utils.flattenObject(obj);
				for (let [path, value] of Object.entries(flat)) {
					if (ppath) path = [ppath, path].join('.');
					if (Array.isArray(value)) testFlat(value, path);
					if (typeof value !== 'string') continue;
					if (IGNORE_DESCRIPTIONS && path.includes('description')) continue;
					if (/\?[^.!\]]+:/.test(value)) {
						found[item.id] ??= [];
						found[item.id].push({ path, value });
					}
				}
			};
			testFlat(item.toObject());
		}
		if (!foundry.utils.isEmpty(found)) {
			const data = { actor, items: [] };
			console.log(`\n${actor.name} [${actor.id}]`);
			for (const [itemId, problems] of Object.entries(found)) {
				data.items.push({
					item: actor.items.get(itemId),
					problems,
				});
			}
			data.items.sort((a, b) => a.item.name.localeCompare(b.item.name));
			results.push(data);
		}
	}
}

// Base actors
findFromActors(game.actors);

// Unlinked actors on current scene
// findFromActors(canvas.scene?.tokens.filter(t => !t.isLinked).map(t => t.actor).filter(a => a));

results.sort((a, b) => a.actor.name.localeCompare(b.actor.name));

if (results.length === 0) {
	ui.notifications.info("No results found");
	return;
}

const template = `
<div class="scrollable">
<style>
.application.dialog.ternary-finder {
  & code {
		font-size: var(--font-size-12);
	}

  & h3 {
	  margin: 0;

		& code {
			display: inline;
			font-size: var(--font-size-12);
			vertical-align: middle;
		}
	}
	& .dialog-content {
		& > div {
			display: flex;
			flex-flow: column;
		}
		& .problems {
			display: flex;
			flex-flow: column;
			gap: 0.3rem;
			& li {
				display: flex;
				flex-flow: row wrap;
				gap: 0.3rem;
				code {
					flex: 1;
				}
			}
		}
	}
}
</style>
{{#each results}}
  <h3>{{actor.link}} <code>{{actor.uuid}}</code></h3>
	<ul>
	  {{#each items}}
		<li>
	  	<label>{{item.link}}<label>
			<ul class="problems">
			  {{#each problems}}
				  <li>
					  <code>{{path}}</code>
					  <code>{{value}}</code>
					</li>
				{{/each}}
			</ul>
		</li>
		{{/each}}
	</ul>
{{/each}}
</div>
`;

const content = Handlebars.compile(template)({ results }, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

// Build dialog for results
foundry.applications.api.DialogV2.prompt({
	window: { title: "Ternary Finding Results" },
	content: await TextEditor.enrichHTML(content),
	classes: ["ternary-finder"],
	render: (event, html) => html.querySelector("form").classList.add("scrollable"), // Hack for v12 bug
});
