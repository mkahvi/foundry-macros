## Pathfinder 1e debug

Macros specifically meant for mining information about PF1e.

These are not expected to work with any other game system without major overhauls.
