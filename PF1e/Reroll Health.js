/**
 * Rerolls the class given health for selected tokens.
 *
 * Only unlinked NPC actors are touched.
 */

// Configuration

// How many levels to maximize
const MAXIMIZED = 1;

// Script proper starts here

const actors = canvas.tokens.controlled
	.filter(t => !t.document.isLinked) // get only unlinked tokens
	.map(t => t.actor).filter(a => !!a); // Make sure we have only actors

const SORT = {
	base: 1000,
	racial: 2000,
	npc: 3000,
};

for (const actor of actors) {
	if (actor.type !== 'npc') continue; // Ignore non-NPC actors
	// Get classes
	const classes = actor.itemTypes.class
		.filter(c => ['base', 'npc', 'racial'].includes(c.subType) && c.hitDice > 0);
	// Sort
	classes.sort((a, b) => SORT[a.subType] - SORT[b.subType]);

	const updates = [];

	let total = 0;
	let max = 0;
	for (const cls of classes) {
		let hp = 0;
		const hd = cls.system.hd || 8;
		let levels = 0;
		let clsmax = 0;
		while (max < MAXIMIZED) {
			hp += hd;
			max++;
			clsmax++;
			levels++;
			if (levels >= cls.hitDice) break;
		}
		const remaining = cls.hitDice - levels;
		const formula = `${remaining}d${hd}`;
		const roll = await RollPF.safeRoll(formula);
		hp += roll.total;
		const dice = roll.dice[0].results.map(r => r.result);
		const maxed = Array.fromRange(clsmax).fill(hd);
		maxed.forEach(hd => hp += hd);
		dice.unshift(...maxed);
		total += hp;
		console.log(`${actor.token.name} rolled ${formula} for ${cls.name} and got`, hp, `[${dice}] (${clsmax} maxed)`);
		updates.push({ _id: cls.id, system: { hp } });
	}

	ui.notifications.info(`${actor.token.name} now has ${total} HP from classes.`);
	await actor.updateEmbeddedDocuments('Item', updates);
}
