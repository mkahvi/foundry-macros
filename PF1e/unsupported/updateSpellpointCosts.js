/*
 * Update all selected actors (or configured actor if none are selected) with the defined spell point formula.
 *
 * COMPATIBILITY:
 *  - Does not work with PF1 0.81.0 and newer
 */

/* Configuration */

const replacementFormula = '3 + @sl';

/* Actual code below */

/* global actor, canvas  */

const controlled = canvas.tokens.controlled.map(t => t?.actor).filter(a => a != undefined);
const actors = controlled.length ? controlled : actor ? [actor] : [];

console.log('Found', actors.length, 'actors...');
for (const actor of actors) {
	const updates = [];

	// Go over spells
	for (const spell of actor.itemTypes.spell) {
		const book = spell.data.data.spellbook;

		const spellbook = actor.data.data.spells[book];
		if (!spellbook) {
			// No associated spellbook, unlikely, but can happen with alternative character sheets
			console.warn(`Failed to find book "${book}" in actor ${actor.name} [${actor.id}] for spell ${spell.name} [${spell.id}]`);
			continue;
		}

		// Test if spellbook uses spellpoints
		const useSP = spellbook.spellPoints.useSystem;
		if (!useSP) continue;

		const cost = spell.data.data.spellPoints.cost;
		if (replacementFormula !== cost)
			updates.push({ _id: spell.id, 'data.spellPoints.cost': replacementFormula });
	}

	// Batch update actor but only if necessary
	if (updates.length) {
		console.log('Updating actor:', actor.name, actor.id, '- update count:', updates.length);
		await actor.updateEmbeddedDocuments('Item', updates);
	}
}
