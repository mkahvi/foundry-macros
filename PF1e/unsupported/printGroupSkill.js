/**
 * Prints skill modifier for all selected actors. The skill is chosen in a dialog.
 */

const signNum = (val) => val >= 0 ? `+${val}` : `${val}`;

function figureOutSkills(actors, skill) {
	const label = CONFIG.PF1.skills[skill];
	const content = ['<div class="flexcol">'];
	content.push(`<h3>${label}</h3>`);
	const skilledActors = actors.map(a => { return { actor: a, skill: a.data.data.skills[skill].mod }; }).sort((a, b) => b.skill - a.skill);
	for (const a of skilledActors) {
		const roll = TextEditor.enrichHTML(`[[1d20+${a.skill}]]`);
		content.push(`<div class="flexrow" style="border-bottom: 1px dotted gray;"><label style="flex: 1;">${a.actor.name}</label><label style="flex: 0 2.5em;">${roll}</label></div>`);
	}
	content.push('</div>');

	ChatMessage.create({
		content: content.join(''),
		rollMode: 'gmroll',
		whisper: game.users.filter(u => u.isGM),
	});
}

function selectSkill() {
	const html = ['<div class="flexcol">'];
	html.push('<select name="skill">');
	html.push('<option name="" value=""></option>');
	for (const [key, label] of Object.entries(CONFIG.PF1.skills))
		html.push(`<option name="${key}" value="${key}">${label}</option>`);
	html.push('</select>');
	html.push('<hr/>');
	html.push('</div>');

	return new Promise((resolve) => new Dialog(
		{
			title: 'Select Skill',
			content: html.join(''),
			buttons: {
				select: {
					label: 'Select',
					callback: jq => resolve(jq.find('select[name="skill"]').val())
				},
			},
			close: _ => resolve(null),
			default: 'select',
		},
		{
			width: 460,
		}
	).render(true)
	);
}

async function startQuery() {
	const actors = canvas.tokens.controlled.map(t => t.actor);
	if (actors.length == 0) return ui.notifications.warn('Please select one or more actors.');

	const skill = await selectSkill();
	if (!(skill?.length > 0)) ui.notifications.warn('No valid skill selected.');
	else figureOutSkills(actors, skill);
}

startQuery();
