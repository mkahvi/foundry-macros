/**
 * Toggles selected token's darkvision as bright vision.
 * By default it extracts darkvision range from token actor's senses and tries to use that.
 *   ... but this can be overridden in the options.
 *
 * Additional use case:
 *   - Update Token & Prototype Token, but don't toggle it off...
 *       - Configuration: noToggleOff = true, syncPrototype = true
 *
 * COMPATIBILITY:
 * - Foundry VTT 0.7.x
 * - Pathfinder 1e 0.77.24
 *
 * FVTT 0.8.x compatibility unknown
 */

const options = {
	sense: 'darkvision', // localize or alter what sense is searched for as desired.
	// senseOverride: 'Darkvision 5', // override anything the actor might have.
	fallBack: '', // Fallback option for senses if missing.
	noWarnings: false, // Suppress warnings
	noToggleOff: false, // If true, adds the vision data but doesn't toggle it off
	syncPrototype: false, // Keep prototype vision state synced with token
};

const tokens = canvas.tokens.controlled; // which tokens to try

const visionKey = 'brightSight'; // alternatively dimSight

const toggleDarkvision = async (token) => {
	const actor = token.actor;

	if (token == null || actor == null) {
		if (!options.noWarnings) ui.notifications.warn('No valid token selected.');
		return;
	}

	let senseSource = options.senseOverride ?? actor.data.data.traits.senses;
	if (senseSource?.length == 0) senseSource = options.fallBack;

	const isSameIsh = (a, b) => a.localeCompare(b, undefined, { sensitivity: 'accent', usage: 'search' }) === 0;
	const matchingSenses = senseSource.split(';')
		.filter(s => isSameIsh(options.sense.toUpperCase(), s.substr(0, s.match(/\d+/)?.index ?? 0).trim().toUpperCase()));

	if (matchingSenses.length === 0) {
		if (!options.noWarnings) ui.notifications.warn(`No appropriate sense found on ${token.name}.`);
		return;
	}

	const sparts = matchingSenses[0].match(/\d+/);
	const range = sparts ? sparts[0] : undefined;
	if (range > 0) {
		const curRange = token.data[visionKey];
		const newRange = curRange === 0 || options.noToggleOff ? range : 0;

		const vData = {};
		vData[`${visionKey}`] = newRange;

		if (newRange !== curRange)
			await token.update(vData);

		const curPRange = actor.data.token[visionKey];

		if (options.updatePrototype && curPRange !== newRange) {
			const pvData = {};
			pvData[`token.${visionKey}`] = newRange;
			await actor.update(pvData);
			console.log(`ACTOR ${actor.name} [${actor.id}] PROTOTYPE SIGHT UPDATED TO: ${newRange}`);
		}
	}
	else {
		if (!options.noWarnings) ui.notifications.warn(`Invalid sense range '${range}' on ${token.name}.`);
	}
};

const ttokens = tokens.filter(t => t.isOwner);

if (ttokens.length > 0) {
	for (const token of ttokens) {
		await toggleDarkvision(token);
	}
}
else {
	if (!options.noWarnings) ui.notifications.warn('No valid tokens selected.');
}
