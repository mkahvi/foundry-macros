/**
 * Arithmancy
 *
 * Produces Arithmancy spellcraft DC chat card.
 * This was made out of curiosity. Please don't actually use Arithmancy?
 * Ref: https://aonprd.com/FeatDisplay.aspx?ItemName=Arithmancy
 * 
 * Usage:
 * - Add @Macro[Arithmancy] to your spell's description, notes, or similar so it'll be included in the chat card.
 */

const lang = {
	label: 'Arithmancy for {spell} [Lv {level}]', // 0 = spell name, 1 = spell level
	spellcraftDC: 'Spellcraft DC <b>{dc}</b>', // 0 = total DC
	//  or for example:
	// spellcraftDC: '@Macro[rollSkill]{Spellcraft} DC <b>{0}</b>',
	spellcraftBreakdown: ' = 10 + {level} [Level] + {root} [DR]',
};

const opts = {
	failsafe: 1000, // Limit on iterations
};

/* eslint-disable */
const letterValues = {
	a: 1, j: 1, s: 1,
	b: 2, k: 2, t: 2,
	c: 3, l: 3, u: 3,
	d: 4, m: 4, v: 4,
	e: 5, n: 5, w: 5,
	f: 6, o: 6, x: 6,
	g: 7, p: 7, y: 7,
	h: 8, q: 8, z: 8,
	i: 9, r: 9
};
/* eslint-enable */

function arithmancy(name, level) {
	// Step 1: Convert letters to numbers
	const nums = [...name.toLowerCase()].filter(c => c in letterValues).map(c => letterValues[c]);

	// Step 2: Sum the numbers
	let dr = nums.reduce((a, b) => a + b, 0);

	// Step 3: Sum digits until only 1 digit remains
	let failsafe = 0;
	while (dr > 9) {
		if (failsafe++ > opts.failsafe) throw 'Failsafe limit exceeded reached'; // should never be reached if the math is correct
		dr = [...dr.toString()].reduce((rv, ch) => rv + parseInt(ch, 10), 0); // Digital root
	}

	return {
		DC: 10 + level + dr,
		base: 10,
		level: level,
		name: name,
		numbers: nums,
		DR: dr,
	};
}
const cM = game.messages.get(event.target.closest('.message')?.dataset.messageId);
if (!cM)  return void ui.notifications.warn('Arithmancy needs to be part of a chat message.');

const item = cM.itemSource,
	armcy = arithmancy(item.name, item.data.data.level);

console.log('Arithmancy', armcy);

await ChatMessage.create({
	flavor: lang.label.replace("{spell}", armcy.name).replace("{level}", armcy.level),
	content: '<div>' + lang.spellcraftDC.replace("{dc}", armcy.DC) + lang.spellcraftBreakdown.replace("{level}", armcy.level).replace("{root}", armcy.DR) + '</div>',
	speaker: ChatMessage.getSpeaker({ actor: item.actor }),
});
