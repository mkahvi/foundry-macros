/**
 * Cycles compendium contents looking for matching documents.
 * 
 * This is for mostly manual adjusting of contents, such as editing descriptions or other things that can not be properly automated.
 * 
 * This is very slow to avoid Foundry's compendium cache expiry issues (you have about 5 minutes to ).
 */

/**
 * Wait for user interaction to move on. Set to false if you're not rendering sheets for editing.
 */
const interactive = true;

/**
 * Report progress every 2.5 seconds
 */
const progressTimer = 2_500;

/**
 * Prefetch all documents.
 * Can speed things up, but if you take more than 5 minutes to do the edits, things start behaving badly.
 */
const prefetch = false;

const packs = game.packs.filter(p => p.metadata.packageName === 'pf1');

/**
 * Test index entry for matching
 *
 * @param {object} entry 
 * @param {CompendiumCollection} pack
 * @returns {boolean}
 */
function matchEntry(entry, pack) {
	// Return true if this is valid index entry
	// This allows speeding up scanning if index can be used to determine correct documents already
	// For example, match only spells 
	if (pack.metadata.type !== "Item"); // Match item documents only
	if (entry.type !== "spell") return false; // Match only spells
	return true;
}

/**
 * Test actual document for matching
 *
 * @param {Document} doc 
 * @returns {boolean}
 */
function matchDoc(doc) {
	// return true if this is the document you want to edit
	return false;
}

/**
 * 
 */
function finalizeDoc(doc) {
	// Do actions here that you need done after item is finished with
  doc.sheet.close();
}

async function processDoc(doc) {
	console.log("!", doc.pack, doc.name);
	// Do actions here needed to process the document
  doc.sheet.render(true);
}

let t0 = performance.now();

const totalEntries = packs.reduce((total, pack) => total + pack.index.size, 0);
console.log("? Estimated total entries:", totalEntries);

let i=0;
for (const pack of packs) {
  if (pack.documentName !== 'Item') continue;

  if (!pack.indexed) await pack.getIndex();
  const wasLocked = pack.locked;
  if (wasLocked) await pack.configure({ locked: false });

	console.log("> Processing", pack.index.size, "documents from", pack.collection);
	
  try {
    for (const entry of pack.index) {
			i++;
			const t1 = performance.now();
			if (((t1 - t0) >= progressTimer)) {
				console.log("? Scanned over", i, "out of", totalEntries, "entries");
				t0 = t1;
			}
			if (!matchEntry(entry, pack)) continue;
      const doc = await fromUuid(entry.uuid);
      if (!matchDoc(doc)) continue;

      await processDoc(doc);

			// Wait for user to confirm to go to the next item
			if (interactive) {
				const rv = await Dialog.prompt({ title: "Continue?", callback: () => true, content: "<h3 class='text-center'>Open next item?</h3>", rejectClose: false, options: { top: 20 } });
				finalizeDoc(doc);
				if (rv !== true) return console.log("--- Processing cancelled");
			}
			else
				finalizeDoc(doc);
    }
  }
  catch (err) {
      throw err; // rethrow, here just for the finally clause
  }
  finally {
    if (wasLocked) await pack.configure({ locked: true });
  }
}

console.log('+++ ALL',i,'ITEMS DONE');
