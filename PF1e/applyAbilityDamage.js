// Presents a dialog to apply ability damage to selected tokens.
// If invoked from a macro button, it tries to parse the button label for instructions.

//     Examples:
//       @Macro[applyAbilityDamage]{5 Wisdom damage}
//       @Macro[applyAbilityDamage]{-3 Wisdom healing}

// Hold shift to skip the confirmation dialog.

// The label matching functions by searching for a number (only first instance is used in case there's multiple)...
// ... and fairly fuzzy method of matching the ability score name (either full or the three letter key, e.g. charisma and cha work)
// ... everything else is ignored.

// Additionally, thanks to Noon, the macro allows the label to contain reference to the immediately adjacent inline roll with...
//     '->' at the end of the label. This will override any internal number usage if any are present.
// This allows you to chain inline rolls for more varied effects.
// The minus has no meaning, it is there simply to denote an arrow.

//     Examples:
//       @Macro[applyAbilityDamage]{3 Wisdom damage}
//       @Macro[applyAbilityDamage]{Wisdom damage ->} [[max(1, floor(@attributes.hd.total / 3))]]
//       @Macro[applyAbilityDamage]{Wisdom healing ->} [[-3]]

// Revision: 5 (2024-01-21)

// Compatibility:
// - Foundry VTT v10 – v11
// - Pathfinder v0.82.5 – v9.6

const lang = {
	doHarm: 'Harm',
	dismiss: 'Dismiss',
	noValidActors: 'No valid actors selected!',
	abilityDamage: 'Dealt <b>{value}</b> {type} damage to...',
};

const opts = {
	printChatCard: true,
	defaultAbility: 'str',
	defaultDamage: 0,
	siblingMarker: '->',
	skipDialog: false, // If true, inverts shift key usage
}

// localeCompare options
const lcOpts = { ignorePunctuation: true, usage: 'search', sensitivity: 'base' };

// Chat card template
const cardTemplate = `
<div class='flexcol'>
	<label>Dealt <b>{{value}}</b> {{type}} damage to...</label>
	<div>
	{{#each tokens}}
	<span clasS='target' style='font-style:italic;'>{{name}}</span>{{#unless @last}}, {{/unless}}
	{{/each}}
	</div>
</div>
`;

function printChatCard(tokens, type, amount) {
	const templateData = {
		type: CONFIG.PF1.abilities[type],
		value: amount,
		tokens,
	}

	const content = Handlebars.compile(cardTemplate)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	const msgData = {
		content,
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
	};

	ChatMessage.applyRollMode(msgData, game.settings.get('core', 'rollMode'));
	ChatMessage.create(msgData);
}

const applyDamage = async (tokens, type, damage) => {
	damage = parseInt(damage, 10);
	console.log('Applying', damage, type, 'to', tokens.length, 'tokens.');
	if (!damage) {
		ui.notifications.warn(`${damage} ability damage is not meaningful, ignoring.`);
		return; // cancel
	}

	for (const token of tokens) {
		const actor = token.actor;
		if (!actor) continue;

		const oldDmg = actor.system.abilities[type].damage;
		const newDmg = oldDmg + damage;
		console.log(token.name, type, oldDmg, '->', newDmg);

		await actor.update({
			system: {
				abilities: {
					[type]: {
						damage: Math.max(0, newDmg),
					}
				}
			}
		});
	}

	console.log('Applied', damage, CONFIG.PF1.abilities[type], 'damage to', actors.length, 'actors.');

	if (opts.printChatCard) printChatCard(tokens, type, damage);
}

const dialogTemplate = `
<form autocomplete='off' class='flexcol' style='gap:3px;margin-bottom:0.7em;'>
	<label>Chosen actors (×{{actorCount}}):</label>
	<div>
	{{#each tokens}}
		<span class='target' style='font-weight:bold;' data-tooltip='{{#if (ne name actor.name)}}{{actor.name}}<br>{{/if}}' data-tooltip-direction='UP'>{{name}}</span>{{#unless @last}}, {{/unless}}
	{{/each}}
	</div>
	<div class='form-group flexrow'>
		<label>Ability score:</label>
		<div class='form-fields'>
			<select name='ability' style='width:100%;'>
			{{selectOptions abilities selected=ability}}
			</select>
		</div>
	</div>
	<div class='form-group flexrow'>
		<label>Damage:</label>
		<div class='form-fields'>
			<input type='number' name='damage' step='1' min='-100' max='100' value='{{value}}'>
		</div>
	</div>
</form>
`;

const showDialog = (tokens, type, damage = 0) => {
	console.log('Rendering ability damage dialog with', damage, CONFIG.PF1.abilities[type], 'damage.');

	const templateData = {
		actorCount: tokens.length,
		tokens,
		abilities: CONFIG.PF1.abilities,
		ability: type,
		value: damage,
	}

	const content = Handlebars.compile(dialogTemplate)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	new Dialog(
		{
			title: 'Apply Ability Damage',
			content,
			buttons: {
				harm: {
					label: lang.doHarm,
					icon: '<i class="fas fa-meteor"></i>',
					/** @param {Element} html */
					callback: html => {
						const form = new FormDataExtended(html.querySelector('form')).object;
						applyDamage(tokens, form.ability, form.damage);
					},
				},
				dismiss: {
					label: lang.dismiss,
					icon: '<i class="fas fa-power-off"></i>',
				},
			},
			default: 'harm'
		},
		{
			width: 360,
			jQuery: false,
		}
	).render(true, { focus: true });
}

function findAbility(text) {
	const words = text.match(/\w+/ig);

	const match = (a, b) => a.localeCompare(b, undefined, lcOpts) === 0;

	const testAbilities = (w) => {
		for (const [key, name] of Object.entries(CONFIG.PF1.abilities))
			if (match(w, name)) return key;

		for (const key of Object.keys(CONFIG.PF1.abilities))
			if (match(w, key)) return key;

		return undefined;
	}

	for (const w of words) {
		const abl = testAbilities(w);
		if (abl) return abl;
	}

	return undefined;
}

function prepareDialog() {
	// const chatMessage = game.messages.get(event.target.closest('.message')?.dataset.messageId);
	const label = event.target.closest('button,a')?.textContent.trim(); // macro label
	const tokens = canvas.tokens.controlled.filter(t => t.actor);

	if (tokens.length <= 0)
		return void ui.notifications.warn(lang.noValidActors);

	let damage = opts.defaultDamage,
		abl = opts.defaultAbility;

	if (label?.length > 0) {
		abl = findAbility(label) ?? opts.defaultAbility;

		let numSource = label;
		// if (label.startsWith('<-'))
		//	numSource = event.target.previousElementSibling?.textContent.trim();
		// else
		if (label.endsWith(opts.siblingMarker))
			numSource = event.target.nextElementSibling?.textContent.trim();

		const num = /(?<value>-?\d+)/.exec(numSource);
		damage = num ? parseInt(num.groups.value) : null;
		damage ??= opts.defaultDamage;
	}

	if (opts.skipDialog && !event.shiftKey || !opts.skipDialog && event.shiftKey)
		applyDamage(tokens, abl, damage);
	else
		showDialog(tokens, abl, damage);
}

prepareDialog();
