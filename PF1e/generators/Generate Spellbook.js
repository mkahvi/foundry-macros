/**
 * Generate spellbook item from the actor's spells.
 *
 * The macro prompts for which spellbook to generate the data from.
 *
 * Compatibility:
 * - Foundry v11
 * - PF1 ?? (tested on v10.4)
 *
 * TODO:
 * - Search for spells from items directory and compendiums that aren't found via core.sourceId.
 */

const CFG = {
	autoOpen: true, // Automatically open the spellbook after creation.
	adjustValue: true, // Adjust value based on theoretical value of spells
	onlyPrepared: true, // Only allow selecting prepared spellbooks
	approxPages: true, // Approximate page counts for unidentified descriptions. If false, exact number of pages is given. If true, the number is approximated.
};

// Determine spell value. Same as scroll cost without material or foci and minimum CL.
const spellValue = (spell, table) => {
	if (!CFG.adjustValue) return 0;
	if (!table) return 0;

	const level = spell.system.level ?? 1;
	const minCL = 1 + table.findIndex(row => row[level] >= 0);
	return 12.5 * spell.system.level * minCL;
}

// How many pages a spell takes (1 per spell level; cantrips take 1 per spell, too).
const pageCount = (spell) => Math.max(1, spell.system.level);

const approximatePageCount = (pages) => {
	if (!CFG.approxPages) return pages;
	// Exact number of pages when there's 20 or less
	if (pages <= 20) return pages;
	// 5 page increments with 3 page error margin added (skewing down) when there's not too many pages
	if (pages <= 60) return Math.ceil(pages / 5) * 5 + Math.round(Math.random() * 6) - 4;
	// Otherwise 10 page increments with 5-ish page error margin (skewing down)
	return Math.ceil(pages / 10) * 10 + Math.round(Math.random() * 12) - 8
}

// Base item
const generateTemplate = ({ pages = 0 } = {}) => new Item.implementation({
	name: 'Spellbook',
	type: 'loot',
	img: 'icons/sundries/books/book-embossed-gold-red.webp',
	system: {
		subType: 'gear',
		price: 15, // Severely incorrect for anything but empty book
		size: 'med',
		identified: false,
		hp: {
			value: 10,
			max: 10,
		},
		weight: { value: 3 },
		hardness: 0,
		equipped: false,
		carried: true,
		unidentified: {
			name: 'Strange Book',
			price: 15
		},
		description: {
			value: '', // Actual description here
			unidentified: `<p>A book filled with strange writings, symbology, diagrams and illustrations in difficult to parse dialect.</p><p>The book has about ${pages} pages filled.</p>`, // Unidentified description
		},
		/*
		aura: {
			school: 'tra',
		}
		*/
	}
}).toObject();

const books = {};
const rawbooks = actor.system.attributes.spells.spellbooks;
Object.entries(rawbooks).forEach(([bookId, bookData]) => {
	if (!bookData.inUse) return;
	if (CFG.onlyPrepared && !["prepared", "hybrid"].includes(bookData.spellPreparationMode)) return;
	books[bookId] = bookData;
});

if (foundry.utils.isEmpty(books)) return void ui.notifications.warn("No valid spellbooks found.");

const spellsByBook = {};
actor.itemTypes.spell
	.forEach(spell => {
		const sd = spell.system;
		if (sd.spellbook) {
			spellsByBook[sd.spellbook] ??= [];
			spellsByBook[sd.spellbook].push(spell);
		}
	});

const bookSelectionHbs = `
<form autocomplete='off'>
	<h3>Spellbook</h3>
	{{#each books as |book id|}}
		<label>
			<input type='radio' name='book' value='{{id}}' {{checked @first}}>
			{{#if label}}{{label}}{{else}}{{name}}{{/if}}
		</label>
	{{/each}}
</form><hr>`;

const bookId = await Dialog.prompt({
	title: `Select spellbook from ${actor.name}`,
	label: 'Generate Spellbook',
	content: Handlebars.compile(bookSelectionHbs)({ books }),
	callback: (html) => {
		const rfd = new FormDataExtended(html.querySelector('form'));
		const fd = game.release.generation >= 10 ? rfd.object : rfd.toObject();
		return fd.book;
	},
	rejectClose: false,
	options: {
		jQuery: false,
	}
});

if (!bookId) return;

const simplifySpell = (spell) => {
	const { name, img } = spell;
	const sd = spell.system;
	const { level } = sd;
	const source = spell.getFlag('core', 'sourceId');
	if (!source) {
		// TODO: Locate same-named spell from compendiums or items directory
		console.warn({ name, level, item: spell }, 'Could not find source ID');
	}

	return {
		name: spell.name,
		img: spell.img,
		level: sd.level,
		source,
		uuid: source,
	}
};

const bookData = books[bookId];

const progressionTable = pf1.config.casterProgression.castsPerDay[bookData.spellPreparationMode]?.[bookData.casterType];
if (!progressionTable) console.warn("Could not find caster progression type:", { type: bookData.spellPreparationMode, progression: bookData.casterType });

let totalSpellValue = 0;
let totalPageCount = 0;

const spells = spellsByBook[bookId];
const spellsByLevel = {};
spells.forEach(s => {
	const { level } = s.system;
	spellsByLevel[level] ??= [];
	spellsByLevel[level].push(simplifySpell(s));
	totalSpellValue += spellValue(s, progressionTable);
	totalPageCount += pageCount(s);
});

const maxLevel = Math.max(...Object.keys(spellsByLevel));
const levels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].filter(l => l <= maxLevel).filter(l => spellsByLevel[l]?.length > 0);

const levelLabels = {
	0: '0th-level',
	1: '1st-level',
	2: '2nd-level',
	3: '3rd-level',
	4: '4th-level',
	5: '5th-level',
	6: '6th-level',
	7: '7th-level',
	8: '8th-level',
	9: '9th-level',
};

const hbsTemplate = `
<p>Total pages filled: {{pages}}</p>
{{#each levels as |level|}}
	<h2>{{lookup ../levelLabels level}}</h2>
	{{#with (lookup @root.spellsByLevel level) as |spells|}}
		{{#if spells}}
		<ul>
			{{#each spells}}
			{{#if uuid}}
			<li>@UUID[{{uuid}}]{ {{~name~}} }</li>
			{{else}}
			<li>{{~name~}}</li>
			{{/if}}
			{{/each}}
		</ul>
		{{else}}
		<label>No spells<label>
		{{/if}}
	{{/with}}
{{/each}}`;

const approxPages = approximatePageCount(totalPageCount);

const book = generateTemplate({ pages: approxPages });

const bookLabel = bookData.label || bookData.name || bookId;
book.name += ` (${bookLabel})`;
const templateData = { spells, levelLabels, levels, spellsByLevel, pages: totalPageCount };
book.system.description.value = Handlebars.compile(hbsTemplate)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
book.sort = actor.itemTypes.loot.reduce((last, cur) => Math.max(last, cur.sort), 0) + CONST.SORT_INTEGER_DENSITY;
book.system.price += totalSpellValue;

console.log('Adding new spellbook', book);
const [item] = await actor.createEmbeddedDocuments('Item', book);
if (item) {
	if (CFG.autoOpen) item.sheet.render(true);

	// Test if the item needs migration
	// Don't do actual migration here to signal the need for this script to be updated
	const migration = await pf1.migrations.migrateItemData(item.toObject());
	delete migration["flags.pf1.migration"]; // Ignore migrated version
	if (!foundry.utils.isNewerVersion(game.system.version, "10.4")) delete migration["system.equipped"]; // 10.4 and older have bad migration for this
	if(!foundry.utils.isEmpty(migration)) {
		ui.notifications.warn("Generated spellbook needs migration!");
		console.log({ migration });
	}
}
console.log("Spellbook creation done!");
