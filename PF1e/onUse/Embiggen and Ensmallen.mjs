/**
 * Embiggen and Ensmallen
 *
 * Alters size when toggled.
 *
 * Intended Trigger: On Toggle
 *
 * Compatibility:
 * - Foundry VTT (10.291)
 * - Pathfinder 1e (0.82.5)
 */

// Configuration
const targetSize = 'lg'; // Explicit target size. Ignored if sizeAdjust is set to non-zero.
const sizeAdjust = 0; // How many steps to adjust size by. Overrides targetSize.

// Get current state
const defaultSize = 'med'; // Fallback if no size info is found
const normalSize = actor.race?.system.size || defaultSize; // Natural size as defined by race
const currentSize = actor.system.traits?.size || defaultSize; // Current actual size

// Sanity checks
if (CONFIG.PF1.sizeChart[targetSize] === undefined) throw new Error(`Defined size is invalid: "${targetSize}"`);
if (CONFIG.PF1.sizeChart[currentSize] === undefined) throw new Error(`Current size is invalid: "${currentSize}"`);
if (!Number.isFinite(sizeAdjust)) throw new Error(`Defined size adjust is invalid: ${sizeAdjust}`);

let newSize;
// Toggling on, apply new size
if (state) {
	if (sizeAdjust !== 0) {
		const steps = Object.keys(CONFIG.PF1.sizeChart);
		const current = steps.indexOf(currentSize);
		const adjusted = Math.clamped(current + sizeAdjust, 0, steps.length - 1);
		newSize = steps[adjusted];
	}
	else if (currentSize !== targetSize) {
		newSize = targetSize;
	}
}
// Toggling off, return to normal size
else {
	if (currentSize !== normalSize) newSize = normalSize;
}

// Actual update, if needed
if (newSize) actor.update({ 'system.traits.size': newSize });
