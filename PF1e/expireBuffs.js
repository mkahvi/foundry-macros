/**
 * Requires PF1e 0.79.7 or later
 */

// Get selected tokens
const actors = canvas.tokens.controlled
	.map(t => t.actor) // Map tokens to actors
	.filter(t => t?.isOwner); // Only owned and valid, can't modify others anyway

if (actors.length) {
	for (const actor of actors) {
		await actor.expireActiveEffects();
	}
}
else {
	// Toggle for configured character instead
	await game.user.character?.expireActiveEffects();
}
