/**
 * Set cantrip slot usage to 0
 * ... in case migration does not catch them.
 *
 * Compatibility:
 * - Foundry v10
 * - Pathfinder 1e 0.82.3
 */

const cantripFormula = '0';

let fixCounter = 0;
const generateSpellFix = (spell) => {
	if (spell.system.level !== 0) return;
	if (spell.system.uses.autoDeductChargesCost !== cantripFormula) {
		fixCounter += 1;
		return {
			_id: spell.id,
			system: {
				uses: {
					autoDeductChargesCost: cantripFormula,
				}
			}
		}
	}
}

const allPromises = [];

const fixActor = async (actor) => {
	const spells = actor.items.filter(i => i.type === 'spell');
	const updates = [];
	for (const spell of spells) {
		const update = generateSpellFix(spell);
		if (update) updates.push(update);
	}

	if (updates.length) {
		return actor.updateEmbeddedDocuments('Item', updates);
	}
}

const fixScene = async (scene) => {
	for (const token of scene.tokens) {
		if (!token.actor) return;
		await fixActor(token.actor);
	}
}

const fixPack = async (pack) => {
	if (pack.metadata.type !== 'Item') return;
	if (pack.locked) return;

	const updates = [];
	const promises = [];

	const fixPackSpell = async (indexData) => {
		const spell = await pack.getDocument(indexData._id);
		const update = generateSpellFix(spell);
		if (update) updates.push(update);
	}

	pack.index.forEach(indexData => {
		if (indexData.type !== 'spell') return;
		promises.push(fixPackSpell(indexData));
	});

	await Promise.all(promises);

	if (updates.length) {
		return Item.updateDocuments(updates, { pack: pack.collection });
	}
}

game.actors.forEach(actor => allPromises.push(fixActor(actor)));
game.scenes.forEach(scene => allPromises.push(fixScene(scene)));
game.packs.forEach(pack => allPromises.push(fixPack(pack)));

console.log('Waiting for fixing to finish...');
Promise.allSettled(allPromises);
console.log('All fixes done,', fixCounter, 'spell instance(s) adjusted');
