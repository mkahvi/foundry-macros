// Fixes SBC imports with hitdice (and anything calculated from it, e.g. BAB) stuck at 1

game.actors
	.filter(actor => actor.type === 'npc' && actor.items.find(i => i.type === 'buff' && i.name === 'sbc | Conversion Buff'))
	.forEach(actor => actor.items.filter(i => i.type === 'class').forEach(cls => cls.update({ 'data.-=hitDice': null })));
