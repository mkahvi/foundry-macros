/**
 * Fixes invalid learned at data in spells.
 * 
 * Keywords:
 * - learned-at
 * - _migrateItemLearnedAt
 * - TypeError: Cannot read properties of null (reading 'split')
 */

async function fixLearnedAt(actor) {
	if (!actor) return;

	for (const spell of actor.itemTypes.spell) {
		const updateData = {};
		const learnedAt = spell.toObject().system.learnedAt ?? {};
		for (const [category, entries] of Object.entries(learnedAt)) {
			if (!Array.isArray(entries)) continue;
			if (entries.length === 0) continue;
			// Filter invalid entries
			const newEntries = entries.filter(([cls, lvl]) => !!cls && typeof cls === 'string');
			if (newEntries.length === entries.length) continue;
			updateData[`system.learnedAt.${category}`] = newEntries;
		}
		if (foundry.utils.isEmpty(updateData)) continue;
		await spell.update(updateData);
		console.log('~ Fixed:', spell.name, spell);
	}
}

const mid0 = ui.notifications.info('Fixing source actors');
for (const a of game.actors) {
	await fixLearnedAt(a);
}

const mid1 = ui.notifications.info('Fixing unlinked actors');
for (const s of game.scenes) {
	for (const t of s.tokens) {
		if (t.isLinked) continue; // Skip already fixed
		await fixLearnedAt(t?.actor);
	}
}
ui.notifications.remove(mid0);
ui.notifications.remove(mid1);
ui.notifications.info('Done fixing');
