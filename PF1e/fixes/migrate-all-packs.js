/**
 * Migrate all packs regardless if they're locked or not.
 *
 * Revision: 9 (2023-06-20)
 *
 * Compatibility:
 * - Foundry v10 (.291)
 * - Pathfinder 1e 0.82.5
 */

// --- Configuration ---

const activelyRemoveNotifications = true; // Can eat notifications not related to migration

// Ignore packs that come with the system, these should be pre-migrated.
// Regular users should not need to disable this. System and module devs may want to.
const ignoreSystemPacks = true;

// Ignore packs that come with modules, these may be outdated.
// Since modules may not migrate their own packs, or you may be using outdated packs, keeping this disabled is for the better.
// Enable if you've already migrated the packs.
const ignoreModulePacks = false;

// Ignore packs defined in the world itself, likely outdated.
// Enable if you've already migrated the packs for this system version.
const ignoreWorldPacks = false;

// Keep record of migrated packs
// If enabled, keeps record which system version a pack was last migrated for.
// If the flag value matches current version, they're ignored on subsequent runs of this macro.
// System and module devs should probably disable this.
const keepMigrationRecord = true;

// --- Logic ---

const macro = this;

/** @type {CompendiumCollection[]} */
const allPacks = Array.from(game.packs);
const packs = allPacks.filter(pack => {
	if (keepMigrationRecord && macro.getFlag('world', pack.collection) === game.system.version)
		return false;

	const type = pack.metadata.packageType;
	return !(ignoreWorldPacks && type === 'world'
		|| ignoreSystemPacks && type === 'system'
		|| ignoreModulePacks && type === 'module');
});

const eatNotifications = () => {
	if (activelyRemoveNotifications) {
		ui.notifications.queue = [];
		while (ui.notifications.active.length)
			ui.notifications.active.pop()?.remove();
	}
}

const content = `
	<form class='flexcol'>
		<h2>Pack Migration</h2>
		<p class='hint'>${packs.length} packs found, ${allPacks.length - packs.length} ignored.</p>
		<div class='form-group'>
			<label style='line-height:unset;'>Progress</label>
			<div class='form-fields progress flexrow' style='text-align:right;justify-content: flex-end;'>
				<span class='done' style='flex:0 3em;'>0</span>
				<span class='sep' style='flex:0 2em;'>/</span>
				<span class='total' style='flex:0 3em;'>${packs.length}</span>
			</div>
		</div>
		<progress max='${packs.length}' value='0' style='flex: 0 1em;width:100%;height:1em;transition:all 200ms;'>0%</progress>
		<div class='current-pack' style='transition:all 500ms;'>
			<h3 style='margin: 5px 0 0 0;'>Current:</h3>
			<div class='form-group' style='align-items:center;'>
				<label style='line-height:unset;'>Pack:</label>
				<div class='form-fields' style='text-align:right;'>
					<span class='current' style='user-select:text;'>...</span>
				</div>
			</div>
			<div class='form-group'>
				<label style='line-height:unset;'>Entries:</label>
				<div class='form-fields' style='text-align:right;'>
					<span class='entries'>...</span>
				</div>
			</div>
		</div>
		<hr>
	</form>
	`;

class MigrationDialog extends Dialog {}

/* global pf1 */

let cancelled = false, finished = false, currentMigration;

const handlePack = async (pack) => {
	if (cancelled) return;

	const wasLocked = pack.locked;
	if (wasLocked) await pack.configure({ locked: false });

	let p;

	try {
		const systemMigratable = ['Actor', 'Item', 'Scene'].includes(pack.metadata.type);
		if (systemMigratable) {
			// Migrate
			p = pf1.migrations.migrateCompendium(pack);
		}
		else {
			// Perform Foundry's own basic migration (PF1 migration runs this for others)
			p = pack.migrate();
		}

		eatNotifications();
		await p;
		eatNotifications();
	}
	catch (_) {
		/* Ignore */
	}

	await p;
	eatNotifications();

	if (keepMigrationRecord)
		await macro.setFlag('world', pack.collection, game.system.version);

	if (wasLocked) await pack.configure({ locked: true });
}

const cancelMigration = async (html) => {
	cancelled = true;
	if (!finished) {
		const button = html.querySelector('button');
		button.disabled = true;
		button.textContent = 'Cancelling...';
		console.log('CANCELLING MIGRATIONS');
		await currentMigration;
		console.log('CANCELLATION COMPLETE');
	}
}

/**
	 * @param {Element} html
	 */
const dialogRender = async (html) => {
	const progress = html.querySelector('progress');
	const done = html.querySelector('.done');
	const pack = html.querySelector('.current-pack');
	const current = pack.querySelector('.current');
	const entries = pack.querySelector('.entries');

	console.log(`Migrating ${packs.length} compendiums.`);

	try {
		let i = 1;
		for (const pack of packs) {
			if (cancelled) return;
			pf1.migrations.__packMigrationInProgress = pack.collection;
			entries.textContent = `${pack.index.size}`;
			console.log(`Migrating (${i}/${packs.length}): ${pack.collection}: ${pack.title} [${pack.index.size} entries]`);
			current.textContent = pack.collection;
			currentMigration = handlePack(pack);

			await currentMigration;

			eatNotifications();

			done.textContent = `${i}`;
			progress.value = i;
			progress.textContent = `${Math.floor(i / packs.length * 100)}`;
			i++;
		}

		html.nextElementSibling.querySelector('button').textContent = 'OK';
	}
	finally {
		delete pf1.migrations.__packMigrationInProgress;
	}

	eatNotifications();

	finished = true;
	pack.style.cssText += 'visibility:hidden;opacity:0;';

	console.log('All Migrations complete!');
};

if (pf1.migrations.isMigrating) throw new Error('World migration in progress');
if (pf1.migrations.__packMigrationInProgress) throw new Error('Previous instance of the macro is already migrating content');

const dialogOptions = {
	content,
	render: dialogRender,
	buttons: {
		cancel: {
			label: 'Cancel',
			callback: cancelMigration,
		}
	},
};

const appOptions = { jQuery: false, width: 600, height: 'auto' };

new MigrationDialog(dialogOptions, appOptions)
	.render(true, { focus: true });
