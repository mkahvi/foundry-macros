/**
 * Iterates over all actors and finds spells that have bad spell level.
 *
 * Clicking on the spell in the generated dialog opens the spell sheet for fixing them.
 * Usually simply opening the sheet is enough to fix them.
 * 
 * Compatibility:
 * - Foundry v10
 */

// Data harvesting

function filterActorSpells(actor) {
	const badSpells = [];
	if (actor.items.length === 0) return badSpells;
	const spells = actor.items.filter(i => i.type === 'spell');
	for (const spell of spells) {
		const l = spell.system.level;
		if (typeof l === 'string' || Math.clamped(l, 0, 9) !== l) {
			badSpells.push(spell);
		}
	}
	return badSpells;
}

function filterActors(actors) {
	const rv = [];
	for (const actor of actors) {
		const spells = filterActorSpells(actor);
		if (spells.length) {
			const rd = {
				spells,
				actor
			};
			rv.push(rd);
		}
	}
	return rv;
}

function filterTokens(tokens) {
	const rv = [];
	for (const token of tokens) {
		const spells = filterActorSpells(token.actor);
		if (spells.length) {
			const rd = {
				spells,
				actor: token.actor,
				token,
			};
			rv.push(rd);
		}
	}
	return rv;
}

function getUnlinkedActors() {
	const rv = [];
	if (game.user.isGM) {
		// Iterate through unlinked tokens
		for (const scene of game.scenes) {
			const tokens = scene.tokens
				.filter(t => !t.isLinked && t.actor); // get valid actors
			const rs = filterTokens(tokens);
			if (rs.length) rv.push(...rs);
		}
	}
	return rv;
}

// UI generation

const template = `
{{#each categories}}
<h2>{{title}}</h2>
{{#each actors}}
<h3 class="click-to-open-sheet" data-actor-id="{{id}}" data-uuid="{{uuid}}" style="cursor:pointer;"
	data-tooltip="Click to open actor sheet." data-tooltip-direction="UP">{{name}}</h3>
{{#each spells}}
<div class="flexrow click-to-open-sheet" data-actor-id="{{../id}}" data-item-id="{{id}}"
	data-uuid="{{uuid}}"
	data-tooltip="Click to open item sheet.<br>Spell level is configured in details tab." data-tooltip-direction="UP"
	style="align-items:center;gap:3px;cursor:pointer;">
	<img src="{{img}}" style="flex:0 24px;width:24px;height:24px;">
	<h4 class="name" style="margin:0;padding:0;">{{name}}</h4>
	<label class="level" style="flex:0;">{{level}}</label>
</div>
{{/each}}
{{#unless @last}}<hr>{{/unless}}
{{/each}}
{{/each}}
<hr>
`;

class BadSpellDialog extends Dialog {
	_openSheet(event) {
		const _reallyOpenSheet = (sheet) => sheet.render(true, {focus:true});

		const itemEl = event.target.closest('[data-uuid]');

		const { tokenId, sceneId, actorId, itemId, uuid } = itemEl.dataset;
		let item, actor;
		if (uuid) {
			const doc = fromUuidSync(uuid);
			if (doc instanceof TokenDocument) actor = doc.actor;
			else if (doc instanceof Actor) actor = doc;
			else if (doc instanceof Item) item = doc;
		}
		else {
			const scene = game.scenes.get(sceneId),
				token = scene?.tokens.get(tokenId);
			
			actor = token?.actor ?? game.actors.get(actorId);
			item = actor.items.get(itemId);
		}
	
		if (item) _reallyOpenSheet(item.sheet);
		else if (actor) _reallyOpenSheet(actor.sheet);
		else {
			ui.notifications.warn('Could not find item!', {console:false});
			console.warn('NOT FOUND:', { itemId, tokenId, actorId, sceneId });
		}
	}

	/**
	 * @param {JQuery} jq 
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];
		html.querySelectorAll('.click-to-open-sheet')
			.forEach(el => el.addEventListener('click', this._openSheet.bind(this)));
	}

	static open(linked, unlinked) {
		let context = {
			categories: [],
		};

		const createList = (actors, title) => {
			const category = {actors:[], title};

			for (const rd of actors) {
				rd.name = rd.actor.name;
				rd.id = rd.actor.id;
				rd.uuid = rd.actor.uuid;
				rd.spells = rd.spells.map(spell => ({ spell, name:spell.name, img:spell.img, id:spell.id, level: spell.system.level, uuid: spell.uuid }));
				category.actors.push(rd);
			}
			context.categories.push(category);
		}

		if (linked.length) createList(linked, 'Linked');
		if (unlinked.length) createList(unlinked, 'Unlinked');

		const content = Handlebars.compile(template)(context, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

		return new BadSpellDialog(
			{
				title: 'Bad spells',
				content,
				buttons: {
					dismiss: {
						label: 'Dismiss',
						icon: '<i class="fas fa-power-off"></i>',
					},
				},
				default: 'dismiss'
			},
			{
				jQuery: false,
				width: 360,
			}
		).render(true);
	}
}

// Start processing

const results_real = filterActors(game.actors.filter(a => a.isOwner));
const results_unlinked = getUnlinkedActors();
const allActors = [...results_real, ...results_unlinked];

if (allActors.length)
	BadSpellDialog.open(results_real, results_unlinked);
else
	ui.notifications.info('No bad spells found.');
