// Migrates all items on selected actors
// Including container contents

const actors = canvas.tokens?.controlled
	.map(t => t.actor).filter(a => !!a);

for (const actor of actors) {
	let updateCount = 0;
	for (const item of actor.allItems) {
		const update = game.pf1.migrations.migrateItemData(item.toObject());
		if (!foundry.utils.isObjectEmpty(update)) {
			await item.update(update);
			updateCount++;
		}
	}

	if (updateCount > 0) {
		ui.notifications.info(`Updated ${updateCount} items in ${actor.name} [${actor.id}]`);
		console.log(`Updated %c${updateCount}%c items in %c${actor.name}%c [%c${actor.id}%c]`,
			'color:mediumpurple', 'color:unset', 'color:mediumseagreen', 'color:unset', 'color:goldenrod', 'color:unset');
	}
}
