// Fixes actors with missing subtargets in conditionals
// THIS IS NOT TESTED PROPERLY. MAKE BACKUPS!

let ac = 0;
const p = [];
function fixActor(actor, isToken = false) {
	ac++;
	const itemUpdates = [];
	for (const item of actor.items) {
		const conditionals = deepClone(item.data.data.conditionals ?? []);
		let updated = false;
		for (const cond of conditionals) {
			for (const mod of cond.modifiers ?? []) {
				if (mod.subTarget === undefined) {
					updated = true;
					mod.subTarget = '';
				}
			}
		}

		if (!updated) continue;

		itemUpdates.push({
			_id: item.id,
			'data.conditionals': conditionals,
		});
	}

	if (itemUpdates.length) {
		console.log('Fixing conditionals for', itemUpdates.length, 'items on', actor.name, actor.id, '');
		p.push(actor.updateEmbeddedDocuments('Item', itemUpdates));
	}
}

ui.notifications?.info('Checking actors for bad conditionals.');

// Fix source actors
game.actors.forEach(a => fixActor(a, false));

// Fix unlinked token actors
game.scenes.forEach(scene => scene.tokens
	.filter(t => !t.isLinked && t.actor != null && (t.data.actorData?.items ?? []).length > 0)
	.forEach(t => fixActor(t.actor, true)));

if (p.length)
	Promise.allSettled(p).then(_ => ui.notifications?.info(`${p.length} actors fixed. Please re-run migration.`));
else
	ui.notifications?.info(`No actors needing fixing found (${ac} checked).`);
