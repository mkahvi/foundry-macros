// Simple way to migrate individual actors.

const actorSelectionHbs = `
<form autocomplete='off' onsubmit='return false'>
	<label>Actor:</label>
	<select name='actor'>
	{{#each actors}}
		<option value='{{id}}'>{{name}}</option>
	{{/each}}
	</select>
</form><p>You may drag&drop an actor into this dialog, too.</p><hr>`;

const actorSelectionTemplate = Handlebars.compile(actorSelectionHbs);

const actors = game.actors.filter(a => a.isOwner && a.type !== 'basic');
// TODO: v10 sort by last modified for "relevancy"?

const actorId = await Dialog.prompt({
	title: 'Select actor',
	label: 'Migrate',
	content: actorSelectionTemplate({ actors }, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
	callback: (html) => new FormDataExtended(html.querySelector('form')).toObject().actor,
	render: (html) => {
		html.addEventListener('drop', (ev) => {
			ev.preventDefault();

			let data;
			try {
				data = JSON.parse(event.dataTransfer.getData('text/plain'));
			}
			catch (err) {
				console.error(err);
				return false;
			}

			if (data.type !== 'Actor') return;
			if (data.pack) return; // Ignore actors from packs
			const actorId = data.id;
			const actor = game.actors.get(actorId);
			if (actor) {
				const selector = html.querySelector('select[name]');
				selector.value = actorId;
				selector.closest('.window-content').querySelector('button').click();
			}
		});
	},
	options: {
		jQuery: false,
		width: 'auto',
	}
});

const actor = game.actors.get(actorId);
if (actor) {
	if (!actor.isOwner) return ui.notifications.warn("Received actor to which you don't have edit rights.");
	if (actor.type == 'basic') return ui.notifications.warn('Migrating basic actor type is not supported.');

	ui.notifications.info(`Migrating ${actor.name} [${actor.id}]`);
	const updateData = game.pf1.migrations.migrateActorData(actor.toObject());
	delete updateData._id;
	if (!isObjectEmpty(updateData)) {
		console.log('Migrating actor:', actor.name, actor.id, { updateData });
		actor.update(updateData).then(_ => {
			ui.notifications.info('Migration complete!');
			console.log('Migration complete!');
		});
	}
	else {
		ui.notifications.info('No changes were made to the actor.');
		console.log('Migration produced no changes to the actor.');
	}
}
else {
	ui.notifications.warn('No valid actor selected');
}
