/**
 * Fix spells with invalid or missing spellbook.
 *
 * Compatibility:
 * - Foundry v10
 */

let fixedActors = 0, fixedSpells = 0;

// Book to push bad spells into.
const targetSpellbook = 'primary';

const fixActorSpells = async (actor) => {
	const books = actor.system.attributes.spells.spellbooks;
	const spells = actor.items.filter(i => i.type === 'spell');
	let fixed = false;
	for (const spell of spells) {
		const bookId = spell.system.spellbook;
		if (books[bookId] === undefined) {
			await spell.update({ system: { spellbook: targetSpellbook } });
			fixedSpells += 1;
			fixed = true;
			console.log(`Fixed "${spell.name}" on "${actor.token?.name ?? actor.name}"; was: "${bookId}"`);
		}
	}

	if (fixed) fixedActors += 1;
}

const fixActors = async (actors) => {
	for (const actor of actors)
		await fixActorSpells(actor);
}

const fixTokens = async (tokens) => {
	for (const token of tokens)
		await fixActorSpells(token.actor);
}

const fixUnlinkedActors = async () => {
	// Iterate through unlinked tokens
	for (const scene of game.scenes) {
		const tokens = scene.tokens
			.filter(t => !t.isLinked && t.actor && ['character', 'npc'].includes(t.actor.type) && t.actor.isOwner); // get valid actors
		await fixTokens(tokens);
	}
}

const actors = game.actors.filter(a => a.isOwner && ['character', 'npc'].includes(a.type));
console.log('Processing source actors');
await fixActors(actors);
console.log('Processing unlinked tokens');
await fixUnlinkedActors();
ui.notifications.info(`All spells with bad spellbooks fixed (fixed ${fixedSpells} spell[s] in ${fixedActors} actor[s])`);
