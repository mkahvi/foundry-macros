/**
 * Attempt to fix data broken by PF1 0.80.19 update.
 *
 * PLEASE NOTE: IT IS BETTER TO RESTORE DATA FROM BACKUP THAN RELYING ON THIS.
 * THIS WILL NOT PROVIDE 100% RECOVERY, AS SOME DATA IS LOST.
 *
 * You have this issue if you have any of the following errors:
 *  - TypeError: note.notes.filter is not a function
 *  - TypeError: data.item.data.data.changes.reduce is not a function
 *  - getProperty(...).concat is not a function
 *
 * This does not fix all scenes, only the viewed one.
 * If you swapped scenes when everything was breaking, it is up to you to run this for each scene.
 *
 * Explanation of the issue:
 * The problem stems from some data having become non-arrays when passed to update functions.
 * Which itself is required because Foundry doesn't support arrays well, so personal touch is needed.
 * The PF1 .19 update broke this custom workflow, resulting in data corruption.
 *
 * Final note:
 * This should be safe to run if you don't have the issue, as it should harmlessly pass over the data doing nothing to it.
 */

// These are supposed to be arrays, but 0.80.19 may have turned them into objects
const restoreArray = [
	'attackParts',
	'damage.parts',
	'damage.critParts',
	'damage.nonCritParts',
	'contextNotes',
	'scriptCalls',
	'attackNotes',
	'effectNotes',
	'links.charges',
	'links.children',
	'changes'
];

/**
 * @param {Item} item
 */
function fixItemData(item) {
	const itemData = item.data.data;

	const updateData = {};

	restoreArray.forEach(path => {
		const data = getProperty(itemData, path);
		// Attempt to turn into an array if they aren't already
		if (data !== undefined && !Array.isArray(data)) {
			const newData = Object.values(data);
			if (newData.length) {
				// Test for internal array, as needed by at least damage.parts/.critParts/.nonCritParts
				if (newData[0][0] !== undefined && !Array.isArray(newData[0])) {
					newData.forEach((dp, i) => {
						newData[i] = Object.values(newData[i]);
					});
				}
			}
			// console.log(path, '\n--- old:', data, '\n--- new:', newData);
			updateData[`data.${path}`] = newData;
		}
	});

	if (!isObjectEmpty(updateData)) {
		updateData._id = item.id;
		return updateData;
	}
}

/**
 * @param {Actor} actor
 */
async function fixActorData(actor, linked) {
	const itemUpdates = [];
	actor.items.forEach(item => {
		const updateData = fixItemData(item);
		if (updateData) itemUpdates.push(updateData);
	});

	if (itemUpdates.length) {
		console.log(actor.name, itemUpdates);
		if (linked) await actor.updateEmbeddedDocuments('Item', itemUpdates);
		else return itemUpdates;
	}
}

ui.notifications?.info('Attempting to fix main actors...');
for (const actor of game.actors.contents) {
	await fixActorData(actor, true);
}

ui.notifications?.info('Attempting to fix world items...');
for (const item of game.items) {
	const updateData = fixItemData(item);
	if (updateData) {
		console.log(item.name, updateData);
		await item.update(updateData);
	}
}

ui.notifications?.info('Attempting to fix unlinked tokens on current scene...');
for (const actor of game.scenes?.viewed?.tokens?.map(t => t.actor) ?? []) {
	if (actor?.token == null) continue; // Skip linked and tokens with no actor
	const itemUpdates = await fixActorData(actor, false);
	if (itemUpdates) {
		console.log(actor.name, itemUpdates);
		for (const updateData of itemUpdates) {
			const item = actor.items.get(updateData._id);
			delete updateData._id;
			await item.update(updateData);
		}
	}
}

ui.notifications?.info('All done.');
