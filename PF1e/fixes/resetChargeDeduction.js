/*
 * Resets spell charge usage formula
 *
 * COMPATIBILITY:
 *  - Requires PF1 0.81.0 or newer
 */

/* Configuration */
const replacementFormula = '1';

const C = {
	label: 'color:mediumseagreen',
	id: 'color:darkseagreen',
	number: 'color:mediumpurple',
	unset: 'color:unset'
};

/* Actual code below */

/* global actor, canvas  */

const actors = canvas.tokens.controlled.map(t => t?.actor).filter(a => !!a && a.type !== 'basic');

let updatedActors = 0, totalSpells = 0, totalActions = 0;

console.log('Found', actors.length, 'actors...');
for (const actor of actors) {
	const rollData = actor.getRollData();

	let updatedSpells = 0;
	let start = false;

	const actOnActor = () => {
		if (start) return;
		start = true;
		console.group(`Actor %c${actor.name}%c [%c${actor.id}%c]`,
			C.label, C.unset, C.id, C.unset);
	};

	// Go over spells
	for (const spell of actor.itemTypes.spell) {
		let updatedActions = 0;
		const spellData = spell.system;
		const book = spellData.spellbook;

		const spellbook = rollData.spells[book];
		if (!spellbook) {
			// No associated spellbook, unlikely, but can happen with alternative character sheets
			actOnActor();
			console.log(`⛔ Book %c${book}%c not found for spell %c${spell.name}%c [%c${spell.id}%c]`,
				C.label, C.unset, C.label, C.unset, C.id, C.unset);
			continue;
		}

		for (const action of spell.actions) {
			if (action.data.uses.autoDeductChargesCost !== replacementFormula) {
				await action.update({ 'uses.autoDeductChargesCost': replacementFormula });
				updatedActions++;
				totalActions++;
			}
		}

		if (updatedActions > 0) {
			updatedSpells++;
			totalSpells++;
			actOnActor();
			console.log(`Spell %c${spell.name}%c [%c${spell.id}%c] had %c${updatedActions}%c action(s) updated`,
				C.label, C.unset, C.id, C.unset, C.number, C.unset);
		}
	}

	if (updatedSpells > 0)
		updatedActors++;

	console.log('Total spells updated:', updatedSpells);
	console.groupEnd();
}

ui.notifications.info(`${updatedActors} actors updated for total ${totalSpells} spells and ${totalActions} actions.`);
