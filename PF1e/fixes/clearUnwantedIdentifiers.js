/**
 * Delete item identifiers (.system.tag) for items that don't need them.
 * 
 * Classes always need them, hence why they are preserved.
 * 
 * This can be run by any user.
 */

let cleared = 0, totalitems = 0;

async function cleanActor(actor) {
	for (const item of actor.items) {
		totalitems += 1;
		if (["class"].includes(item.type)) continue;
		const raw = item.toObject();
		if ((raw.type === "spell" || !raw.system.uses?.per) && raw.system.tag) {
			console.log(">", actor.name, "|", item.name, "| Clearing identifier:", { identifier: item.system.tag });
			cleared += 1;
			await item.update({ "system.-=tag": null });
		}
	}
}

console.log("Checking actors directory...");
const actors = game.actors.filter(a => a.isOwner);
for (let actor of actors) await cleanActor(actor);

// Clean unlinked actors as GM
console.log("Checking scenes (unlinked actors)...");
for (let scene of game.scenes) {
	const actors = scene.tokens.filter(t => !t.isLinked && t.actor && t.actor.isOwner).map(t => t.actor);
	for (let actor of actors) await cleanActor(actor);
}

ui.notifications.info(`${cleared} Excess identifiers cleared from ${totalitems} items!`);
