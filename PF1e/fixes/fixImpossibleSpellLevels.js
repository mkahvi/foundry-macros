// Fix impossible spells for selected token

const spells = actor?.items
	.filter(i => i.type === 'spell' && Math.clamped(i.data.data.level, 0, 9) !== i.data.data.level);

ui.notifications.info(`Found ${spells.length} bad spell(s).`);
if (spells.length) {
	for (const spell of spells)
		await spell.update({ 'data.level': 9 });
	ui.notifications.info('All bad spells shunted to level 9.');
}
