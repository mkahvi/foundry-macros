function clearActorEnhs(actor) {
	const items = [];
	for (const item of actor.items) {
		let update = false;
		const itemData = item.data.data;
		if (itemData.actions === undefined) continue;
		for (const action of itemData.actions) {
			if (action.enh.value !== null) {
				action.enh.value = null;
				update = true;
			}
			delete action.enh.override;
		}
		if (update) items.push({ _id: item.id, actions: itemData.actions });
	}

	return items;
}

function processActors(actors) {
	const actorUpdates = [];
	for (const a of actors) {
		if (!['character', 'npc'].includes(a.type)) continue;
		const items = clearActorEnhs(a);
		if (items.length) actorUpdates.push({ _id: a.id, items });
	}
	return actorUpdates;
}

const updates = processActors(game.actors);
if (updates.length) {
	ui.notifications.info(`${updates.length} actors being updated`);
	await Actor.updateDocuments(updates);
}
else {
	ui.notifications.info('No actors to update');
}
