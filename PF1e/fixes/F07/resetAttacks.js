// This resets Changes to attacks.

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

(async () => {
	let actors = canvas.tokens.controlled.map(o => o.actor).filter(o => o.isOwner);

	if (actors.length > 0) {
		const waiting = [];
		actors.forEach(async (a) => {
			console.log("RESETTING ATTACKS for " + a.name);
			waiting.push(a.update({
				'data.attributes.attack.general': 0,
				'data.attributes.attack.melee': 0,
				'data.attributes.attack.ranged': 0,
				'data.attributes.attack.shared': 0,
			}));
		});
		await Promise.all(waiting);

		ui.notifications.info("Actor attack reset complete!");
		console.log("ACTOR ATTACK RESET COMPLETE");
	}
	else {
		ui.notifications.warn("No valid actors found...");
		onsole.log("NO VALID ACTORS SELECTED");
	}
})();
