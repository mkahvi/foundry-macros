// Lite Sheet Fix macro
// Fixes a sheet converted from lite NPC sheet to not have broken HP & init values.
// https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/503

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

let actor = canvas.tokens.controlled[0]?.actor; // from selected token
//let actor = game.actors.getName("Character Name"); // alternative, find actor by name

if (actor) {
	(async () => {
		const cleanNum = (value) => isNaN(value) ? 0 : value;
		const hp = actor.data.data.attributes.hp;
		await actor.update({
			'data.attributes.hp.base': 0, // must be 0
			//'data.attributes.hp.value': 0, // can ignore being broken
			'data.attributes.hp.temp': cleanNum(parseInt(hp.temp)), // try to salvage
			'data.attributes.hp.nonlethal': cleanNum(parseInt(hp.nonlethal)), // try to salvage
			//'data.attributes.init.total': 0, // can ignore being broken
			'data.attributes.init.value': 0, // must be 0
		});
	})();
}
