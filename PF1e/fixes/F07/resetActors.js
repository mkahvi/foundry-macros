// Reset all actors

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

(async () => {
	let reset = 0, skipped = 0;

	ui.notifications.info("Resetting all actors...");
	console.log("RESETTING ALL ACTORS");

	for (var actor of game.actors) {
		if (!actor.isOwner) {
			skipped++;
			continue;
		}
		reset++;
		await actor.update({
			// AC
			'data.attributes.ac.normal.total': 10,
			'data.attributes.ac.normal.value': 0,
			'data.attributes.ac.flatFooted.total': 10,
			'data.attributes.ac.flatFooted.value': 0,
			'data.attributes.ac.touch.total': 10,
			'data.attributes.ac.touch.value': 0,
			// CMD
			'data.attributes.cmd.total': 10,
			'data.attributes.cmd.value': 0,
			'data.attributes.cmd.flatFootedTotal': 10,
			// Attacks
			'data.attributes.attack.general': 0,
			'data.attributes.attack.melee': 0,
			'data.attributes.attack.ranged': 0,
			'data.attributes.attack.shared': 0,
			// Damage
			'data.attributes.damage.general': 0,
			'data.attributes.damage.weapon': 0,
			'data.attributes.damage.spell': 0,
			// CMB
			'data.attributes.cmb.total': 0,
			'data.attributes.cmb.value': 0,
			// Init
			'data.attributes.init.total': 0,
			'data.attributes.init.value': 0,
			'data.attributes.init.bonus': 0,
		});
	}

	console.log("ACTOR RESET DONE");
	ui.notifications.info(`Actor reset complete; ${reset} actors reset, ${skipped} skipped.`);
})();
