// Lite Sheet Fix macro
// Fixes a sheet converted from lite NPC sheet to not have broken HP & init values.
// https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/503

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

const allBrokenActors = [];
for (var a of game.actors) {
	if (a?.isOwner && // get only things you have control over
		!(typeof a.sheet === "ActorSheetPFNPCLite") && // ignore those that still use lite sheet
		typeof (a.data.data.attributes.hp.base) === "string" || a.data.data.attributes.init.value > 0) // get only problem cases
		allBrokenActors.push(a);
}

const cleanNum = (value) => isNaN(value) ? 0 : value;

const asyncCalls = [];

const fixActor = (actor) => {
	if (actor) {
		const hp = actor.data.data.attributes.hp;
		asyncCalls.push(actor.update({
			'data.attributes.hp.base': 0, // must be 0
			//'data.attributes.hp.value': 0, // can ignore being broken
			'data.attributes.hp.temp': cleanNum(parseInt(hp.temp)), // try to salvage
			'data.attributes.hp.nonlethal': cleanNum(parseInt(hp.nonlethal)), // try to salvage
			//'data.attributes.init.total': 0, // can ignore being broken
			'data.attributes.init.value': 0, // must be 0
		}));
	}
}

if (allBrokenActors.length > 0) {
	(async () => {
		console.log(`FIX LITE SHEET | Processing ${allBrokenActors.length} actor(s)...`);

		allBrokenActors.forEach(a => fixActor(a));
		await Promise.all(asyncCalls);

		console.log("FIX LITE SHEET | Fixed actors: ",
			allBrokenActors.reduce((acc, a) => { acc[a._id] = a.name; return acc; }, {}));
	})();
} else {
	console.log("FIX LITE SHEET | Found nothing to fix. All good!");
}
