// Attempt to find damage formulas that have problematic elements.
//
// Mostly complains about @critMult usage and some use cases of sizeRoll's fourth parameter.

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

{
	const actors = game.actors.filter(a => a.isOwner);

	const lang = {
		title: 'Potentially Outdated Damage Formulas',
		dismiss: 'Dismiss',
		notfound: 'No issues found',
	};

	function hasCritMult(formula) {
		return /@critMult/.test(formula);
	}

	function assessFormula(formula, item) {
		const hasSizeRoll = /sizeRoll\(/.test(formula);
		return {
			critMult: hasCritMult(formula),
			sizeRoll: hasSizeRoll,
			veryBad: hasSizeRoll ? assessSizeRoll(formula, item) : false
		}
	}

	function assessSizeRoll(formula, item) {
		// The followig somewhat fails with some parenthesis usage.
		//const re = /sizeRoll\([^,]+,[^,]+,[^,]+,([\d\s\+\-\*\\]+(?!.?\b[a-z]))?\b\)/g;
		// RE with //g uses the RE as the matchin group.
		const reg = /sizeRoll\([^,]+,[^,]+,(?:[^,](?!\s+\b[^@][a-z]))+,\s*((?:@[a-zA-Z\.]+|[\d\s\+\-\*\\])+)\b\s*\)/g;
		const res = /sizeRoll\([^,]+,[^,]+,(?:[^,](?!\s+\b[^@][a-z]))+,\s*((?:@[a-zA-Z\.]+|[\d\s\+\-\*\\])+)\b\s*\)/;
		/**
		 * Test strings:
		 * sizeRoll(1, 8, @size) + min(5,1)
		 * sizeRoll(1, 8, @size , 5 )
		 * sizeRoll(1, 8, @size, @critMult) + 5
		 * sizeRoll(1, 8, (@size - 1), 5+2) + sizeRoll(1, 4, 4, 5) + 1
		 * sizeRoll(1, 8, @size) + min(@abilities.str.mod, 2)
		 */
		const m = formula.match(reg);
		if (m == null) {
			//console.log("- Ignoring:", formula);
			return false;
		}
		//console.log("~ Testing:", formula);
		let isBad = false;
		for (let i = 0; i < m.length; i++) {
			const sf = m[i];
			const m2 = sf.match(res);
			const param4 = RegExp.$1;
			if (m2 === null) {
				//console.log("--- Ignored:", sf);
				continue;
			}
			//console.log("~~~ Testing:", sf);

			const cm = hasCritMult(param4);
			// critMult here is pretty much universally bad
			if (cm) isBad = true;

			// assume any fourth parameter that is not pure number to be bad
			if (!(/(\d[^\)\+-@[a-zA-Z\.]+]+)/.test(param4.trim()))) isBad = true;
		}
		return isBad;
	}

	const problemActors = [];

	for (let actor of actors) {
		const items = actor.items.filter(i => i.hasDamage);
		if (items.length === 0) continue;

		const problemItems = [];

		//console.group("Actor:", actor.name, actor.id);
		for (let item of items) {
			const pConds = [];

			//console.group("Item:", item.name);
			item.data.data.conditionals?.forEach(cond => {
				for (let mod of cond.modifiers) {
					const rv = assessFormula(mod.formula, item);
					if (rv.critMult) pConds.push(mod.formula);
				}
			});

			function loop(parts) {
				return parts.filter(f => f.length && f[0].length).map(f => f[0]).filter(f => assessFormula(f, item).critMult);
			}

			const damage = item.data.data.damage,
				pParts = loop(damage.parts),
				pNonCrit = loop(damage.nonCritParts),
				pCrit = loop(damage.critParts);

			if (pConds.length || pParts.length || pNonCrit.length || pCrit.length)
				problemItems.push({
					item: item,
					conditional: pConds,
					normal: pParts,
					nonCrit: pNonCrit,
					critOnly: pCrit
				});
			//console.groupEnd();
		}
		//console.groupEnd();

		if (problemItems.length > 0)
			problemActors.push({ actor, items: problemItems });
	}

	const col = () => $('<div/>').addClass('flexcol')
	const label = (text) => $('<label/>').text(text);
	const h2 = (text) => $('<h2/>').text(text);
	const h3 = (text) => $('<h3/>').text(text).css({ 'margin-bottom': '0px' });
	const warning = () => $('<i/>').addClass('fas fa-exclamation-triangle');

	const dataKeys = (actorId, itemId) => {
		const o = {};
		if (actorId) o['data-actor-id'] = actorId;
		if (itemId) o['data-item-id'] = itemId;
		return o;
	}

	const sheetButton = (actorId, itemId) => $('<i/>').addClass('fas fa-edit').addClass('click-to-open-sheet').attr(dataKeys(actorId, itemId)).css({ 'margin-left': '3px' });

	const jq = col();

	if (problemActors.length > 0) {
		console.log("Reporting bad formulas...");
		problemActors.forEach(ad => {
			jq.append(
				h2(ad.actor.name).append(sheetButton(ad.actor.id)),
				label(`ID: ${ad.actor.id}`)
			);
			for (let id of ad.items) {
				jq.append(h3(id.item.name).css({ 'font-weight': 'bold' }).append(sheetButton(ad.actor.id, id.item.id)));

				function loop(list, title) {
					if (list.length <= 0) return;
					jq.append(
						label(title).css({ 'margin-left': '1em' }),
						col().append(list.map(fi => {
							const l = $('<div/>').css({ 'margin-left': '2em' })
							const r = assessFormula(fi, ad.actor);
							if (r.veryBad) l.append(warning().css({ 'margin-right': '3px' }));
							l.append(label(fi));
							return l;
						}))
					);
				}

				loop(id.conditional, "Conditionals");
				loop(id.normal, "Normal");
				loop(id.nonCrit, "Non-CritMult");
				loop(id.critOnly, "CritOnly");
			}
		});
	} else {
		jq.append(label(lang.notfound));
	}

	jq.append($('<hr/>'));

	function openSheet(sheet) {
		if (sheet.rendered) {
			if (sheet._minimized) sheet.maximize();
		}
		else sheet.render(true);
	}

	class FDialog extends Dialog {
		_openSheet(jq) {
			console.log(jq);
			const actorId = jq.target.dataset.actorId;
			const itemId = jq.target.dataset.itemId;
			const actor = game.actors.get(actorId);
			if (itemId) {
				const item = actor?.items.get(itemId);
				if (item) openSheet(item.sheet);
				else ui.notifications.warn(`Actor ${actorId} or Item ${itemId} not found!`);
			}
			else if (actorId) {
				if (actor) openSheet(actor.sheet);
				else ui.notifications.warn(`Actor ${actorId} not found!`);
			}
		}

		activateListeners(html) {
			super.activateListeners(html);
			html.find('.click-to-open-sheet').on('click', this._openSheet);
		}
	}

	new FDialog(
		{
			title: lang.title,
			content: $('<div/>').append(jq).html(),
			buttons: {
				dismiss: {
					label: lang.dismiss,
					icon: '<i class="fas fa-power-off"></i>',
				},
			},
			default: 'dismiss'
		},
		{
			width: 360,
		}
	).render(true);
}
