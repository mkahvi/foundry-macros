/**
 * Displays info dialog about various bonus sources and optionally prints a GM whisper chat message about it for selected tokens.
 *
 * Hold shift to skip dialog, or to show it if default is to skip it.
 *
 * TODO:
 * - Use actor.changes instead.  Current usage of actor.sourceDetails
 *
 * BUGS:
 * - Wound thresholds are not accounted for.
 */

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

// Need newer version of PF1e than this
const lastIncompatibleVersion = '0.77.21';

// Bugs:
//  - Wound Thresholds are not accounted for (attributes.woundThresholds.penalty)

// Options. Configure default behaviour with these as desired.
class EBOptions {
	static maxActors = 7; // make sure not too many actors are handled
	static skipDialog = false; // If true, no dialog is shown and chat message is generated directly.
	static printDefault = false; // if true, default dialog button is to print rather than dismiss
	static fullDebug = false; // displays most changes (some exceptions), not just those listed in sources. Output is uglier.
	static onlyErrors = false; // show only bits that have errors in them
	static reopenAfterReset = true; // Re-open dialog after reset. This will hopefully show what was fixed.
	static includeItemIds = true; // Include item IDs in the output

	static maxChatDataHeight = '320px'; // default is what the dialog has otherwise
};

const defenseDefault = 10; // Base defense value.

const unwantedItemTypes = ['class', 'spell'];

// Flip default behaviour if shift is held
// @ts-ignore
if (event.shiftKey) EBOptions.skipDialog = !EBOptions.skipDialog;

// This configures the default view.
// Format: { category: { label: datapath } }
// These are actor.sourceDetails things
const sources = {
	attack: {
		shared: 'data.attributes.attack.shared',
		general: 'data.attributes.attack.general',
		melee: 'data.attributes.attack.melee',
		ranged: 'data.attributes.attack.ranged',
		cmb: 'data.attributes.cmb.total',
	},
	damage: {
		general: 'data.attributes.damage.general',
		weapon: 'data.attributes.damage.weapon',
		spell: 'data.attributes.damage.spell',
	},
	defense: {
		'armor class': 'data.attributes.ac.normal.total', // 10,
		'ac, flat-footed': 'data.attributes.ac.flatFooted.total', // 10,
		'ac, touch': 'data.attributes.ac.touch.total', // 10,
		'combat maneuver defense': 'data.attributes.cmd.total', // 10,
		'cmd, flat-footed': 'data.attributes.cmd.flatFootedTotal', // 10,
	},
	'saving throws': {
		fortitude: 'data.attributes.savingThrows.fort.total',
		reflex: 'data.attributes.savingThrows.ref.total',
		will: 'data.attributes.savingThrows.will.total',
	},
	misc: {
		init: 'data.attributes.init.total',
	},
};

// Potential translation strings.
const lang = {
	title: 'Bonus Sources',
	dismiss: 'Dismiss',
	mode: 'Mode',
	reset: 'Reset',
	print: 'GM whisper',
	sources: 'Sources',
	notfound: 'No sources found.',
	totalBonus: 'Total',
	itemCount: 'Items',
	mismatch: 'Error; Numbers don\'t add up (total {0} ≠ {1} cached)',
	unsupported: 'Source type unsupported & wildly inaccurate.',
	onlyErrors: "Displaying only errors.",

	// following are always uppercase regardless how this is written
	notes: 'NOTES',
	other: 'OTHER',
	health: 'HEALTH',

	baseBonus: '+++ Base +++', // Nameless base modifier, usually 10 or unmodified ability score.
	unknownItem: '??? Unknown ???', // Nameless item

	sizeBonus: '+++ Size +++', // Nameless size bonus

	deferred: 'Deferred',
	strangeSource: 'Warning; Odd change source. Effects uncertain.', //
};

/** --- Functions --- */

// Merge all objects passed in as an array
/** @param {Object[]} objs */
const mergeAll = (...objs) => objs.reduce((o0, o1) => Object.assign(o0, o1), {});
/** @param {Number} num */
const signNum = (num) => `${(num >= 0 ? '+' : '') + num}`; // add + to positive numbers
const getChange = (actor, change) => game.pf1.utils.getChangeFlat.call(actor, change.subTarget, change.modifier);

const stackingTypes = CONFIG.PF1.stackingBonusModifiers;

const changeInstructions = {
	'data.attributes.attack.general': {
		sources: {
			key: 'attack',
			label: lang.deferred
		}
	},
	'data.attributes.ac.normal.total': {
		types: {
			equipment: {
				maxOnly: true,
				types: ["base"],
				perElem: "itemType",
			},
		}
	},
	'data.attributes.ac.touch.total': {
		types: {
			equipment: {
				maxOnly: true,
				types: ["base"],
				perElem: "itemType",
			}
		}
	},
	'data.attributes.ac.flatFooted.total': {
		types: {
			equipment: {
				maxOnly: true,
				types: ["base"],
				perElem: "itemType",
			}
		}
	},
	'data.attributes.acp.total': {
		inverted: true,
	},
	'data.attributes.maxDexBonus': {
		minOnly: true,
		inverted: true,
		invertValue: true,
	},
}

const debug = {
	items: false,
	names: false,
}

// Sources to be more loud about in logs
const debugSources = [
	/*
	'data.attributes.ac.flatFooted.total',
	'data.attributes.ac.touch.total',
	'data.attributes.acp.total',
	'data.attributes.maxDexBonus',
	'data.attributes.speed.land.total',
	'data.attributes.acp.total',
	*/
];

// Sources that currently are not supported
const unsupportedSources = [
	'data.attributes.acp.total', // difficult to handle rules via info accessible by macro
	'data.attributes.acp.armorBonus', // incomplete ?
	'data.attributes.acp.shieldBonus', // incomplete ?
	'data.attributes.speed.land.total', // text-only things
	'data.attributes.speed.climb.total', // text-only things
	'data.attributes.speed.swim.total', // text-only things
	'data.attributes.speed.burrow.total', // text-only things
	'data.attributes.speed.fly.total', // text-only things
	'data.attributes.vigor.max', // unfamiliar rules
	'data.attributes.wounds.max', // unfamiliar rules
	'data.traits.armorProf', // text only
	'data.traits.weaponProf', // text only
];

// Hide sources with these paths
const hideSources = [
	'data.attributes.vigor.max', // unfamiliar rules
	'data.attributes.wounds.max', // unfamiliar rules
	'data.traits.armorProf', // text only
	'data.traits.weaponProf', // text only
];

// Hide sources based on RegExp
const hideSourcesRE = [
	/\.checkMod$/,
	/^temp\./,
	/^data\.abilities\..*\.base$/,
	/\.cl\.total$/, // spellbooks, numbers don't match
];

const hideSourcesMatchAny = (key) => {
	for (let re of hideSourcesRE)
		if (re.test(key)) return true;
	return false;
}

const baseNeedsHarvest = {
	'data.abilities.str.total': true,
	'data.abilities.dex.total': true,
	'data.abilities.con.total': true,
	'data.abilities.int.total': true,
	'data.abilities.wis.total': true,
	'data.abilities.cha.total': true,
};

const baseStaticBonus = {
	'data.attributes.ac.normal.total': defenseDefault,
	'data.attributes.ac.touch.total': defenseDefault,
	'data.attributes.ac.flatFooted.total': defenseDefault,
	'data.attributes.cmd.total': defenseDefault,
	'data.attributes.cmd.flatFootedTotal': defenseDefault,
};

const changeExpansion = {
	'attack.attack': 'data.attributes.attack.~attack', // incorrect mapping
	'speed.landSpeed': 'data.attributes.speed.land.total',
	'speed.swimSpeed': 'data.attributes.speed.swim.total',
	'speed.climbSpeed': 'data.attributes.speed.climb.total',
	'speed.flySpeed': 'data.attributes.speed.fly.total',
	'speed.burrowSpeed': 'data.attributes.speed.burrow.total',
}

/**
 * Transform only numbers with callback.
 * @param {Number} number
 * @param {Function} callback
 */
const numberTransform = (number, callback) => Number.isFinite(number) ? callback(number) : number;
const noUndef = (x, cb) => x === undefined ? x : cb(x);

class Change {
	id;
	path;
	name;
	type;
	bonusType;
	value;
	formula;
	operator;
	positive = true;
	enabled = true;
	final = 0;
	hasDice = false;
	inverted = false;

	/**
	 * @type {ItemPF?}
	 */
	_item = null;
	/**
	 * @type {String?}
	 */
	itemId = null;
	itemType = '';
	itemSubtype = '';

	constructor(name, type, bonusType, value, formula, operator, path, id) {
		this.id = id;
		this.path = path;
		this.name = name;
		this.type = type;
		this.bonusType = bonusType ?? 'base';
		this.value = value;
		this.formula = formula;
		this.operator = operator;

		if (this.name == null && this.type === 'size')
			this.name = lang.sizeBonus;
	}

	set item(item) {
		this._item = item;
		this.itemId = item.id;
		this.itemType = item.data.data.equipmentType;
		this.itemSubType = item.data.data.equipmentSubtype;
	}
	get item() {
		return this._item;
	}

	calculate(rollData) {
		let tFinal = 0;

		if (this.formula?.length > 0) {
			try {
				const roll = new Roll(this.formula, rollData).roll();
				this.hasDice = roll?.dice.find(d => d.number) != null;
				tFinal += roll?.total ?? 0;
			}
			catch (err) {
				console.error("ERROR: ", this.name, this.formula);
			}
		}

		tFinal += Number.isFinite(this.value) ? this.value : 0;

		if (this.inverted) tFinal = -tFinal;
		this.final = tFinal;
		this.positive = tFinal >= 0;
		return tFinal;
	}

	static fromChange(ch, actor) {
		console.log("fromChange:", ch);
		return new Change(ch.parent?.name ?? null, ch.type, ch.modifier, ch.value, ch.formula, ch.operator, getChange(actor, ch), ch._id);
	}
}

const simplifyDetailed = (src, actor) => {
	if (src == null) return [];

	let changes = [];
	const iterate = (srcs, valueTransform, formulaTransform) => srcs.forEach(ch => {
		if (ch.name == null && debug.names) console.log("NAMELESS BONUS: ", ch);
		const key = getChange(actor, ch);
		//console.log("NAMELESS:", ch, key, src);
		changes.push(new Change(ch.name, ch.type, ch.modifier, noUndef(ch.value, valueTransform), ch.formula, ch.operator, key, ch._id));
	});

	iterate(src.positive, (x) => x, (x) => x);
	iterate(src.negative, (x) => -x, (x) => `-(${x})`);

	return changes;
};

const getSourceBonus = (actor, src) => getProperty(actor.data, src);
const getItems = (actor) => actor.items.filter(item => !unwantedItemTypes.includes(item.type));

/**
 * @param {ActorPF} actor
 * @param {Change[]} trueSources
 * @param {ItemPF[]} items
 */
const enrichWithItems = (actor, trueSources, items) => {
	for (let src of trueSources) {
		if (src.name?.length === 0 || src.item !== null) continue;

		if (src.type === 'equipment') {
			let matchingItems = items.filter(i => i.name === src.name);
			if (matchingItems.length > 0) {
				src.item = matchingItems[0];
				src.itemId = src.item.id;

				if (matchingItems.length > 1)
					console.warn(`Multiple matching items for "${src.name}" found: `, matchingItems);
				else if (debug.items)
					console.log(`+++ Source "${src.name}" matched with item id:${src.item.id}`, src.itemType, src.itemSubType);
			}
			else if (debug.items)
				console.log(`--- No match for Source "${src.name} in items"`);
		}
		else {
			if (![undefined, 'BASE'].includes(src.type) && debug.items)
				console.log(`--- Source "${src.name}" has incorrect type for item matching: "${src.type}"`);
		}
	}
}

const gatherData = (actor) => {
	const data = {
		actor: actor,
		sources: {},
		osources: {},
	};

	// Process actor.changes contents, too.
	// TODO
	/*
	const allChanges = [];
	actor.changes.forEach(ch => {
		const key = getChange(actor, ch)
		if (key === undefined || key === null) return;
		allChanges.push(`${CONFIG.PF1.buffTargets[ch.subTarget]}::${ch.subTarget} – ${ch.operator} [${ch.modifier}] – ${ch.formula} = ${ch.value}`);
	});

	data.changes = duplicate(actor.changes);
	*/

	if (EBOptions.fullDebug) {
		const skey = "FULL DEBUG";
		data.osources[skey] = {};

		Object.keys(actor.sourceDetails)
			.filter(o => !hideSources.includes(o) && !hideSourcesMatchAny(o))
			.forEach((value) => data.osources[skey][value] = value);
	} else {
		data.osources = duplicate(sources);
	}

	let rollData = actor.getRollData();
	let items = getItems(actor);

	Object.keys(data.osources).forEach(cat => {
		data.sources[cat] = {};

		Object.keys(data.osources[cat]).forEach(key => {
			const subCat = data.osources[cat][key],
				undetailed = actor.sourceDetails[subCat],
				detailed = actor.sourceInfo[subCat],
				instr = changeInstructions[subCat];

			if (debugSources.includes(subCat)) console.warn("Processing:", subCat);

			// Build sources
			let trueSources = [];

			let base = 0;
			base += baseStaticBonus[subCat] ?? 0;
			if (baseNeedsHarvest[subCat] ?? false) {
				if (undetailed[0].name === 'Base')
					base += undetailed[0].value;
				else
					console.warn(`Couldn't find Base value for ${subCat} but it must have it`);
			}
			if (base !== 0) trueSources.push(new Change(lang.baseBonus, 'BASE', 'base', base, null, 'add', null, null));

			trueSources = [...trueSources, ...simplifyDetailed(detailed, actor)];

			// Enrich with item data
			enrichWithItems(actor, trueSources, items);

			// Process
			let foundTypes = [], foundActions = [];

			const filterMinMax = (src, max) => {
				if (src.length === 0) return;
				src.forEach(t => t.enabled = false);
				src.sort((a, b) => max ? b.value - a.value : a.value - b.value);
				src[0].enabled = true;
			};

			trueSources.forEach(k => {
				if (instr?.inverted) k.inverted = true;
				k.calculate(rollData);

				if (!foundTypes.includes(k.type)) foundTypes.push(k.type);
				if (!foundActions.includes(k.operator)) foundActions.push(k.operator);
			});

			let extra = [];

			// Apply instructions
			if (instr) {
				if (instr.sources) {
					actor.sources?.entries.filter(x => x.subTarget === instr.sources.key).forEach(x => {
						extra.push(Change.fromChange(x, actor));
					});
				}

				if (instr.maxOnly || instr.minOnly) {
					let maxOnly = instr.maxOnly ?? false;
					if (instr.inverted) maxOnly = !maxOnly;
					filterMinMax(trueSources, instr.maxOnly ?? maxOnly);
				}

				if (instr.types) {
					// Type specific filtering
					Object.keys(instr.types).forEach(k => {
						let t = instr.types[k];
						if (t.maxOnly || t.minOnly) {
							let s = trueSources.filter(ch => ch.type === k && (t.types ? t.types.includes(ch.bonusType) : true));
							if (t.perElem) {
								s.forEach((ch, i) => {
									const it = ch[t.perElem];
									if (s.findIndex(cht => cht[t.perElem] === it) === i)
										filterMinMax(s.filter(chs => chs[t.perElem] === it), t.maxOnly);
								});
							}
							else
								filterMinMax(s, t.maxOnly);
						}
					});
				}
			}

			let total = 0;
			trueSources.forEach(ch => {
				if (!ch.enabled) return;

				switch (ch.operator) {
					case 'add':
						total += ch.final;
						if (ch.final === 0) ch.enabled = false; // Disable 0 modifiers
						break;
					case 'set':
						total = ch.final;
						break;
					default:
						console.warn("Unrecognized operator: ", ch.key, ch.operator);
						break;
				}
			});

			// Final modifications
			if (instr) {
				//if (instr.inverted) total = -total;
			}

			const fdata = {
				key: subCat,
				items: trueSources,
				count: trueSources.length,
				cached: getSourceBonus(actor, subCat),
				actual: total,
			};
			data.sources[cat][key] = fdata;

			if (fdata.actual !== fdata.cached)
				console.log("trueSources:", trueSources);

			//if (cat === 'defense') data.sources[cat][key].cached += defenseDefault;
		});
	});

	return data;
}

const resetActor = async (actor, qdata = new QData()) => {
	ui.notifications.info(`Resetting actor: ${actor.name} [${actor.id}]`);

	const dataReset = {
		// AC
		'data.attributes.ac.normal.total': defenseDefault,
		'data.attributes.ac.normal.value': 0,
		'data.attributes.ac.flatFooted.total': defenseDefault,
		'data.attributes.ac.flatFooted.value': 0,
		'data.attributes.ac.touch.total': defenseDefault,
		'data.attributes.ac.touch.value': 0,
		// CMD
		'data.attributes.cmd.total': defenseDefault,
		'data.attributes.cmd.value': 0,
		'data.attributes.cmd.flatFootedTotal': defenseDefault,
		// Attacks
		'data.attributes.attack.general': 0,
		'data.attributes.attack.melee': 0,
		'data.attributes.attack.ranged': 0,
		'data.attributes.attack.shared': 0,
		// Damage
		'data.attributes.damage.general': 0,
		'data.attributes.damage.weapon': 0,
		'data.attributes.damage.spell': 0,
		// CMB
		'data.attributes.cmb.total': 0,
		'data.attributes.cmb.value': 0,
		// Init
		'data.attributes.init.total': 0,
		'data.attributes.init.value': 0,
		'data.attributes.init.bonus': 0,
		// ACP
		'data.attributes.acp.armorBonus': 0,
		'data.attributes.acp.shieldBonus': 0,
		// MaxDex
		'data.attributes.mDex.armorBonus': 0,
		'data.attributes.mDex.shieldBonus': 0,
		// Ability Score penalties and checmods
		'data.abilities.str.checkMod': 0,
		'data.abilities.str.penalty': 0,
		'data.abilities.dex.checkMod': 0,
		'data.abilities.dex.penalty': 0,
		'data.abilities.con.checkMod': 0,
		'data.abilities.con.penalty': 0,
		'data.abilities.int.checkMod': 0,
		'data.abilities.int.penalty': 0,
		'data.abilities.wis.checkMod': 0,
		'data.abilities.wis.penalty': 0,
		'data.abilities.cha.checkMod': 0,
		'data.abilities.cha.penalty': 0,
	};

	// Remove unnecessary updates
	for (let [k, v] of Object.entries(dataReset)) {
		const vo = getProperty(actor.data, k);
		if (vo === v) delete dataReset[k];
	}

	await actor.update(dataReset);

	ui.notifications.info(`Actor reset complete for ${actor.name} [${actor.id}]`);

	if (EBOptions.reopenAfterReset)
		printDialog(actor, true, null, qdata)
}

// Styling
const disabledStyle = { opacity: 0.6, 'font-size': '80%' }, // Style applied to changes that are not actually doing anything
	baseStyle = { 'font-style': 'italic' }, // Special style for Base values
	leftLabelStyle = { flex: '10 1 8em' },
	rightLabelStyle = { flex: '1 5 4em', 'text-align': 'right' },
	centerStyle = { 'text-align': 'center' },
	biggerLabel = { 'font-size': '120%' },
	monoStyle = { 'font-family': 'monospace' },
	itemIdStyle = { flex: '0 0 6em', 'font-size': '80%', 'text-align': 'right' },
	rightFloaterStyle = { float: 'right' },
	removeMarginStyle = { margin: 'unset' },
	bottomMarginStyle = { 'margin-bottom': '0.6em' },
	smallLeftMarginStyle = { 'margin-left': '0.6em' },
	bigLeftMarginStyle = { 'margin-left': '1.6em' },
	shadedBackgroundStyle = { background: 'rgba(0, 0, 0, 0.12)' },
	darkerBackgroundStyle = { background: 'rgba(0, 0, 0, 0.2)' },
	dottedLineStyle = { 'border-style': 'none none dotted none', 'border-width': '1px', },
	vertPaddingStyle = { 'padding-top': '1px', 'padding-bottom': '2px' },
	heavyStyle = { 'font-weight': 'bold' },
	hideStyle = { display: 'none' },
	showStyle = { display: 'block' },
	overflowBox = { overflow: 'scroll', 'overflow-y': 'auto', flex: '1 1 600px', 'max-height': '540px' },
	//overflowBox = { 'height': '600px', 'max-height': '600px' },
	bigLabel = { 'font-size': '110%' },
	clipboardParent = { 'white-space': 'nowrap' },
	clipboardStyle = { opacity: '0.6', 'margin-left': '4px' };

// Simpler elements for jquery
const div = '<div />', label = '<label />', h3 = '<h3 />', span = '<span />', pre = '<pre />', hr = '<hr />', p = '<p />', input = '<input />';

const errorGroupClass = 'bonus-error';

function changeMode(actor, qdata) {
	EBOptions.fullDebug = !EBOptions.fullDebug;
	printDialog(actor, true, constructHTML(actor, qdata), qdata);
}

function printToChat(html) {
	if (EBOptions.maxChatDataHeight != null)
		// Rewrite the max-height to make it fit a chat card better
		html = $(html).find('.overflowContent').css({ 'max-height': EBOptions.maxChatDataHeight }).html();

	return ChatMessage.create({
		content: html,
		type: CONST.CHAT_MESSAGE_TYPES.WHISPER,
		whisper: game.users.entities.filter((u) => u.isGM).map((u) => u._id),
	});
}

class DebugDialog extends Dialog {
	constructor(data, _dOptions, actor, html, qdata) {
		super(data, _dOptions);
		this.data.title = lang.title;
		this.data.buttons = {
			dismiss: {
				label: lang.dismiss,
				icon: '<i class="fas fa-power-off"></i>',
			},
			reset: {
				label: lang.reset,
				icon: '<i class="fas fa-sync-alt"></i>',
				callback: _ => resetActor(actor, qdata),
			},
			mode: {
				label: lang.mode,
				icon: EBOptions.fullDebug ? '<i class="fas fa-compress-alt"></i>' : '<i class="fas fa-expand-alt"></i>',
				callback: _ => changeMode(actor, qdata)
			},
			print: {
				label: lang.print,
				icon: '<i class="fas fa-print"></i>',
				callback: _ => printToChat(html),
			},
		};
		this.data.default = EBOptions.printDefault ? 'print' : 'dismiss';
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			template: 'templates/hud/dialog.html',
			classes: ['dialog'],
			minHeight: 660,
			maxHeight: 800,
			height: 600,
			minWidth: 480,
			maxWidth: 800,
			width: "auto",
			//scrollY: ['.overflowContent'],
			//closeOnSubmit: true,
			jQuery: true,
			//resizable: true,
		});
	}

	async copyToClipboard(ev) {
		let data = ev.target.nodeName === 'I' ? ev.target.parentNode.textContent : ev.target.textContent;
		await navigator.clipboard.writeText(data);
		ui.notifications.info(`"${data}" copied to clipboard.`);
	}

	activateListeners(html) {
		super.activateListeners(html);
		html.closest('.window-app').css({ 'min-height': this.options.minHeight, 'min-width': this.options.minWidth });
		html.closest('.window-content').css({ overflow: 'unset' });
		html.closest('.dialog-content').css({ overflow: 'none' });
		html.closest('.dialog-buttons').css({ height: '3em', flex: '0 10 2em' });

		html.find('.copyable').on('click', this.copyToClipboard).css(clipboardParent)
			.append($('<i/>').addClass('fas fa-clipboard').css(clipboardStyle));
	}
}

const constructHTML = (actor, qdata) => {
	// TODO: Print appropriate warnings about sourceless buffs.
	const data = gatherData(actor);
	qdata.data = data;

	// Header
	//$('<form/>')
	const content = $('<div/>').addClass('flexcol')
		.append($('<div/>').addClass('flexrow').css(bottomMarginStyle)
			.append($('<h2/>').css(mergeAll(leftLabelStyle, removeMarginStyle, heavyStyle)).text(actor.name))
			.append($('<label/>').css(mergeAll(itemIdStyle, rightFloaterStyle, monoStyle)).text(actor.id).addClass('copyable')));

	const maybeUpperCase = (label, uppercase) => uppercase ? label.toUpperCase() : label;

	const srcBox = (label, bonus = null, upperCase = false) =>
		$('<div/>').addClass('flexcol').css(bottomMarginStyle)
			.append($('<div/>').addClass('flexrow').css(shadedBackgroundStyle)
				.append($('<h3/>').css(mergeAll(leftLabelStyle, removeMarginStyle, bigLabel, heavyStyle)).text(maybeUpperCase(label, upperCase)).addClass(EBOptions.fullDebug ? 'copyable' : ''))
				.append($('<label/>').css(rightLabelStyle).text(bonus != null ? `${lang.totalBonus} = ${signNum(bonus)}` : ''))
			);

	const srcBoxItem = ({ name = null, value = null, note = null, noteIcon = null, id = null, formula = null, hasDice = false }) => {
		const item = $('<div/>').addClass('flexcol').css(mergeAll(dottedLineStyle, vertPaddingStyle));
		if (name) {
			const namediv = $('<div/>').addClass('flexrow');
			namediv.append($('<label/>').css(mergeAll(leftLabelStyle, smallLeftMarginStyle)).text(name));
			if (id !== null) namediv.append($('<label/>').css(mergeAll(itemIdStyle, monoStyle)).addClass('copyable').text(id));
			const num = $('<label/>').css(rightLabelStyle);
			if (hasDice) num.append($('<i/>').addClass('fas fa-dice'));
			num.append(document.createTextNode(signNum(value)));
			if (formula) num.attr({ 'title': formula });
			namediv.append(num);
			item.append(namediv);
		}
		if (note) {
			const notediv = $('<div/>').css(bigLeftMarginStyle);
			if (noteIcon) notediv.append($('<i/>').addClass(noteIcon).html("&nbsp;"));
			notediv.append($('<label/>').text(note));
			item.append(notediv);
		}
		return item;
	}

	const srcCat = (cat) => {
		const rv = $('<div/>').addClass('flexcol');
		if (cat)
			rv.append($('<h3/>').text(cat).css(mergeAll(removeMarginStyle, darkerBackgroundStyle, heavyStyle, centerStyle, biggerLabel)));
		return rv;
	}

	const processSubKeys = (sources, category) => {
		let supErrors = 0;

		const catLayout = srcCat(category?.toUpperCase());

		Object.keys(sources).forEach(key => {
			let subErrors = 0;
			const src = sources[key];
			const srcLayout = srcBox(key, src.cached, !EBOptions.fullDebug);
			if (src.count > 0) {
				src.items.forEach((item) => {
					//item.bonusType
					//item.type
					let el = srcBoxItem({
						name: item.name || lang.unknownItem,
						value: item.text ?? item.final,
						id: EBOptions.includeItemIds ? item.itemId : null,
						formula: item.formula, hasDice: item.hasDice
					});
					if (!item.enabled) el.css(disabledStyle);
					if (item.type === 'BASE') el.css(baseStyle);
					srcLayout.append(el);
				});
			}
			else
				srcLayout.append(srcBoxItem({ note: lang.notfound, noteIcon: 'fas fa-battery-empty' })); // Empty

			if (src.count > 0 && unsupportedSources.includes(src.key))
				srcLayout.append(srcBoxItem({ note: lang.unsupported, noteIcon: 'fas fa-info-circle' }));

			if (src.cached !== src.actual) console.warn(`${src.key} = ${src.actual} != ${src.cached}`);

			if (src.actual !== src.cached) {
				srcLayout.append(srcBoxItem({ note: lang.mismatch.format(src.actual, src.cached), noteIcon: 'fas fa-exclamation-triangle' }));
				subErrors++;
			}

			if (subErrors > 0) {
				srcLayout.addClass(errorGroupClass);
				//if (opts.onlyErrors) srcLayout.css('display:none');
			}
			else {
				if (EBOptions.onlyErrors) srcLayout.css(hideStyle);
			}
			catLayout.append(srcLayout);
			supErrors += subErrors;
		});

		if (supErrors > 0)
			catLayout.addClass(errorGroupClass);
		else if (EBOptions.onlyErrors)
			catLayout.css(hideStyle);

		return catLayout;
	};

	let errors = 0;

	const contentBox = $('<div />').addClass('flexcol').addClass('overflowContent').css(overflowBox);

	if (EBOptions.onlyErrors) {
		const noteBox = srcBox(lang.notes, null, true);
		noteBox.append(srcBoxItem({ note: lang.onlyErrors, noteIcon: 'fas fa-wrench' }));
		contentBox.append(noteBox);
	}

	const contentBoxItems = [];
	const wrapProcess = (ssources, category) => {
		if (qdata.errors[category] === undefined) qdata.errors[category] = { error: false };

		const catLayout = processSubKeys(ssources, category);
		const hadErrors = qdata.errors[category]?.error ?? false;
		let haveErrors = catLayout.hasClass(errorGroupClass);
		if (haveErrors) errors++;
		qdata.errors[category].error = haveErrors;
		if (hadErrors && !haveErrors)
			catLayout.append(srcBoxItem({ note: 'Previous errors have been fixed.', noteIcon: 'fas fa-notes-medical' }));
		contentBoxItems.push(catLayout);
	}

	Object.keys(data.sources).forEach(cat => wrapProcess(data.sources[cat], cat));

	// Display something at least if no errors are there and error-only output is enabled
	if (errors === 0 && EBOptions.onlyErrors)
		contentBox.append(
			srcBox("info", null, true)
				.append(srcBoxItem({ note: 'No errors recognized.' })));

	contentBox.append(contentBoxItems);

	// HP
	const hp = actor.data.data.attributes.hp;
	// TODO: push to lang.~
	// TODO maybe add HP sources here?
	if (hp.value > hp.max)
		contentBox.append(
			srcBox(lang.health, null, true)
				.append(srcBoxItem({
					name: "Hit Points", value: hp.value,
					note: 'Hit point value over maximum (current {0} > {1} maximum)'.format(hp.value, hp.max),
					noteIcon: 'fas fa-exclamation-triangle'
				})));

	// Ability score
	if (!EBOptions.onlyErrors) {
		const ablm = actor.data.data.abilities,
			str = ablm.str.mod, dex = ablm.dex.mod, con = ablm.con.mod, int = ablm.int.mod, wis = ablm.wis.mod, cha = ablm.cha.mod;

		const ablBox = srcBox(lang.other, null, true);
		// TODO: push to lang.~
		ablBox.append(srcBoxItem({
			name: "Ability Score Modifier", value: "n/a",
			note: `Str[${signNum(str)}], Dex[${signNum(dex)}], Con[${signNum(con)}], Int[${signNum(int)}], Wis[${signNum(wis)}], Cha[${signNum(cha)}]`
		}));
		contentBox.append(ablBox);
	}

	/*
	const defAtk = actor.sources.entries.filter(x => x.subTarget === 'attack');
	const defDmg = actor.sources.entries.filter(x => x.subTarget === 'damage');
	if (defAtk.length || defDmg.length) {
		const xatkBox = srcBox(lang.deferred, null, true);
		xatks.forEach(ch => {
			const key = getChange(actor, ch);
			const ci = new Change(ch.parent ? `${ch.parent.name} [${ch.parent.type}]` : lang.unknownItem, 'ITEM', ch.modidifer, 0, ch.formula, ch.operator, key, ch._id);
			ci.calculate();
			xatkBox.append(srcBoxItem(ci.name, ch.value, lang.strangeSource, 'fas fa-exclamation-triangle', ch.parent?._id, ch.formula, ci.hasDice));
		});

		contentBox.append(xatkBox);
	}
	*/

	content.append(contentBox);

	return $('<div />').append(content).html();
}

class QData {
	data = {};
	errors = {};
}

const printDialog = (actor, showDialog = true, html = null, qdata = new QData()) => {
	if (!html) html = constructHTML(actor, qdata);

	if (showDialog)
		new DebugDialog({ content: html }, null, actor, html, qdata).render(true);
	else
		printToChat(html);
}

// Figure out actors involved
let actors = canvas.tokens.controlled.map(o => o.actor).filter(o => o.isOwner);
if (actors.length === 0 && game.user.character !== null) actors = [game.user.character]; // try to find something at least

// Remove actors if too many have been selected
if (actors.length > EBOptions.maxActors) actors.splice(EBOptions.maxActors, actors.length - EBOptions.maxActors);

// TODO: Add confirmation if there's more than 3 actors selected.

if (isNewerVersion(game.system.data.version, lastIncompatibleVersion)) {
	if (actors.length > 0)
		//actors.forEach(a => printAttackModifiers(a));
		actors.forEach(a => printDialog(a, !EBOptions.skipDialog));
	else {
		ui.notifications.warn("No valid actors selected for attack bonus enumeration");
	}
}
else {
	ui.notifications.warn("This script requires newer version of PF1e");
}
