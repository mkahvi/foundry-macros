# Pathfinder 1e fixes

Macros specifically meant for fixing problems with PF1e.

These _should_ be harmless to run even if you aren't needing to fix anything, but I make no guarantees.

These are not expected to work with any other game system without major overhauls.
