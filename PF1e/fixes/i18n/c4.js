// 4th pass language conversion
// removes keys missing from source language

const src = 'en';

let filename;

class ImportDialog extends Dialog {
  constructor() {
    const data = {
      content: null,
      buttons: {},
    };
    data.dragDrop = [{ dragSelector: null, dropSelector: null }];
    super(data);
  }

  static get defaultOptions() {
    return {
      ...super.defaultOptions,
      title: "Language transformation – 1st pass",
      dragDrop: [{ dragSelector: null, dropSelector: '.dialog-content' }],
      jQuery: false,
    }
  }

  getData() {
    const context = super.getData();
    context.content = `
      <div style="border: 1px solid #777; border-radius: 0.5rem;padding: 0.5rem; background-color: rgba(0,0,0,0.05);">
        Drop language JSON here.
      </div>`;
    return context;
  }

  _canDragDrop() { return true; }

  async _onDrop(event) {
    for (const f of event.dataTransfer.files) {
      if (f.type !== 'application/json') continue;
      filename = f.name;
      const reader = new FileReader();
      reader.onload = convertLang;
      reader.readAsText(f);
    }
    this.close();
  }

  static open = () => new ImportDialog().render(true);
}

const fu = foundry.utils;

const fetchLang = async (lang) => {
  const data = await fu.fetchJsonWithTimeout(`systems/pf1/lang/${lang}.json`);
  return fu.flattenObject(data);
};

const srcx = fu.expandObject(await fetchLang(src));
const srcf = fu.flattenObject(src);

function convertLang(event) {
  const l = JSON.parse(event.target.result);

  const lang = filename.split(".").shift();

  const fl = fu.flattenObject(l);

  const deletions = {};
  for (let key of Object.keys(fl)) {
    if (!fu.hasProperty(srcx, key)) {
      const parts = key.split(".");
      parts.push(`-=${parts.pop()}`);
      deletions[parts.join(".")] = null;
    }
  }

  console.log(deletions);
  fu.mergeObject(l, deletions, {performDeletions:true});

  saveDataToFile(JSON.stringify(l, null, 2), 'application/json', filename);
}

ImportDialog.open();
