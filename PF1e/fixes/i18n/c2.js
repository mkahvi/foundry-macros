// 2nd pass language conversion
// expands language data

let filename;

class ImportDialog extends Dialog {
  constructor() {
    const data = {
      content: null,
      buttons: {},
    };
    data.dragDrop = [{ dragSelector: null, dropSelector: null }];
    super(data);
  }

  static get defaultOptions() {
    return {
      ...super.defaultOptions,
      title: "Language transformation – 1st pass",
      dragDrop: [{ dragSelector: null, dropSelector: '.dialog-content' }],
      jQuery: false,
    }
  }

  getData() {
    const context = super.getData();
    context.content = `
      <div style="border: 1px solid #777; border-radius: 0.5rem;padding: 0.5rem; background-color: rgba(0,0,0,0.05);">
        Drop language JSON here.
      </div>`;
    return context;
  }

  _canDragDrop() { return true; }

  async _onDrop(event) {
    for (const f of event.dataTransfer.files) {
      if (f.type !== 'application/json') continue;
      filename = f.name;
      const reader = new FileReader();
      reader.onload = convertLang;
      reader.readAsText(f);
    }
    this.close();
  }

  static open = () => new ImportDialog().render(true);
}

const fu = foundry.utils;

function convertLang(event) {
  const l = JSON.parse(event.target.result);

  const lang = filename.split(".").shift();

  const xl = fu.expandObject(l);

  saveDataToFile(JSON.stringify(xl, null, 2), 'application/json', filename);
}

ImportDialog.open();
