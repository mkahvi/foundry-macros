/**
 * Reset spell costs.
 *
 * This does not touch spellpoint costs!
 *
 * Compatibility:
 * - Foundry v10
 * - Pathfinder 1e 0.82.3–0.82.5
 */

const resetOnlySpecificActionFormula = true; // If false, all action formulas are reset
const specificBadActionFormula = '1 + @sl';
const unlockCompendiums = false; // Set to true to automatically unlock&relock compendiums

const getFormula = (spell) => {
	// Cantrips consume no slots.
	if (spell.system.level === 0)
		return '0';

	// Empty formula for everything else, to let things fall back to default behaviour
	return '';
}

let fixCounter = 0, spellsIgnored = 0;
const generateSpellFix = (spell) => {
	const formula = getFormula(spell);
	const currentFormula = spell.system.uses?.autoDeductChargesCost;
	const updateData = {
		_id: spell.id,
		system: { uses: {} }
	};
	let updated = false;
	if (currentFormula === undefined && !formula) {
		// NOP, already reset
	}
	else if (currentFormula !== formula || !formula) {
		if (!formula)
			updateData.system.uses['-=autoDeductChargesCost'] = null;
		else
			updateData.system.uses.autoDeductChargesCost = formula;

		updated = true;
	}

	// Reset actions, too.
	const actions = deepClone(spell._source.system.actions ?? []);
	if (actions) {
		let actionsUpdated = false;
		for (const action of actions) {
			const aformula = action.uses?.autoDeductChargesCost;
			if (!aformula) continue;
			if (resetOnlySpecificActionFormula && aformula !== specificBadActionFormula) continue;

			actionsUpdated = updated = true;
			delete action.uses.autoDeductChargesCost;
		}

		if (actionsUpdated) {
			updateData.system.actions = actions;
		}
	}

	if (updated) {
		fixCounter += 1;
		return updateData;
	}
	else {
		spellsIgnored += 1;
	}
}

const fixActor = (actor, immediate = true) => {
	const spells = actor.items.filter(i => i.type === 'spell');
	const updates = [];
	for (const spell of spells) {
		const update = generateSpellFix(spell);
		if (update) updates.push(update);
	}

	if (updates.length) {
		if (immediate)
			return actor.updateEmbeddedDocuments('Item', updates);
		else
			return { _id: actor.id, items: updates };
	}
}

const fixScene = async (scene) => {
	for (const token of scene.tokens) {
		if (!token.actor) continue;
		await fixActor(token.actor);
	}
}

const fixPack = async (pack) => {
	const wasLocked = pack.locked;
	if (wasLocked && !unlockCompendiums) return;
	const isActor = pack.metadata.type === 'Actor',
		isItem = pack.metadata.type === 'Item';
	if (!isActor && !isItem) return;

	const updates = [];
	const promises = [];

	const fixPackSpell = async (indexData) => {
		const spell = await pack.getDocument(indexData._id);
		const update = generateSpellFix(spell);
		if (update) updates.push(update);
	}

	const fixPackActor = async (indexData) => {
		const actor = await pack.getDocument(indexData._id);
		const update = fixActor(actor, false);
		if (update) updates.push(update);
	}

	console.log('>>> Checking:', pack.collection);

	pack.index.forEach(indexData => {
		if (isItem) {
			if (indexData.type !== 'spell') return;
			promises.push(fixPackSpell(indexData));
		}
		else {
			promises.push(fixPackActor(indexData));
		}
	});

	await Promise.all(promises);

	if (updates.length) {
		if (wasLocked) await pack.configure({ locked: false });
		try {
			console.log('=> Updating:', pack.collection);
			if (isItem)
				await Item.updateDocuments(updates, { pack: pack.collection });
			else if (isActor)
				await Actor.updateDocuments(updates, { pack: pack.collection });
		}
		finally {
			if (wasLocked) await pack.configure({ locked: true });
		}
	}
}

console.log('+++ Fixing Actors...');
const actorPromises = [];
game.actors.forEach(actor => actorPromises.push(fixActor(actor)));
await Promise.allSettled(actorPromises);

console.log('+++ Fixing Scenes & Unlinked Tokens...');
for (const scene of game.scenes)
	await fixScene(scene);

console.log('+++ Fixing Compendiums...');
for (const pack of game.packs)
	await fixPack(pack);

console.log('All fixes done,', fixCounter, 'spell(s) adjusted;', spellsIgnored, 'spell(s) ignored');
