// Convert equipment size, weight and price

// Note, this converts only items of type weapon and equipment (which includes armor)
// Gear like bedrolls, tents, etc. have special rules for what conversion ratio they use and need to be handled manually.

// WARNING: This only alters size, weight and price.
// WARNING: This uses armor conversion for non-armor. Core rules do not give this for weapons except in the small to large range. And nothing for non-armor/non-weapons.
// WARNING: If the item has unidentified version, that remains untouched.
// WARNING: Unlinked tokens are not handled.

// How to determine if character is humanoid or not.
const determineHumanoid = (actor) => actor.race?.data.data.creatureType === 'humanoid' &&
	actor.data.data.attributes.quadruped === false &&
	actor.data.data.traits.stature === 'tall';

const Scaling = {
	// Fine
	fine: { weight: 0.1, cost: { humanoid: 0.5, nonhumanoid: 1 } },
	// Diminutive
	dim: { weight: 0.1, cost: { humanoid: 0.5, nonhumanoid: 1 } },
	// Tiny
	tiny: { weight: 0.1, cost: { humanoid: 0.5, nonhumanoid: 1 } },
	// Small
	sm: { weight: 0.5, cost: { humanoid: 1, nonhumanoid: 2 } },
	// Medium
	med: { weight: 1, cost: { humanoid: 1, nonhumanoid: 2 } },
	// Large
	lg: { weight: 2, cost: { humanoid: 2, nonhumanoid: 4 } },
	// Huge
	huge: { weight: 5, cost: { humanoid: 4, nonhumanoid: 8 } },
	// Gargantuan
	grg: { weight: 8, cost: { humanoid: 8, nonhumanoid: 16 } },
	// Colossal
	col: { weight: 12, cost: { humanoid: 16, nonhumanoid: 32 } },
};

const tokens = canvas.tokens.controlled;

if (tokens.length === 0) return ui.notifications.error('No tokens selected');

let updatedActors = 0;
for (let t of tokens) {
	const actor = t?.actor;
	if (!actor) {
		const msg = `"${token.name}" lacks an actor.`;
		console.warn(msg);
		ui.notifications.error(msg);
		continue;
	}

	if (actor.isToken) {
		const msg = `"${token.name}" is unlinked; ignoring.`;
		console.warn(msg);
		ui.notifications.error(msg);
		continue;
	}

	const actorSize = actor.data.data.traits.size,
		isHumanoid = determineHumanoid(actor);

	const itemsToUpdate = actor.items.filter(i => ['weapon', 'equipment'].includes(i.type));

	const updates = [];
	for (let item of itemsToUpdate) {
		const itemData = item.data.data;

		const isMwk = itemData.masterwork,
			enhancement = itemData.enh ?? itemData.armor?.enh ?? 0,
			size = itemData.size,
			price = itemData.price,
			weight = itemData.weight;

		if (size !== actorSize) {
			const newWeight = weight / Scaling[size].weight * Scaling[actorSize].weight;

			const update = {
				_id: item.id,
				"data.size": actorSize,
			};

			if (weight !== newWeight) update["data.weight"] = newWeight;

			// Don't touch items marked with mwk or enhancement since they likely have altered cost that is not affected by size
			if (!isMwk && !(enhancement > 0)) {
				const originalCost = isHumanoid ? Scaling[size].cost.humanoid : Scaling[size].cost.nonhumanoid,
					targetCost = isHumanoid ? Scaling[actorSize].cost.humanoid : Scaling[actorSize].cost.nonhumanoid;

				const newPrice = price / originalCost * targetCost;
				if (price !== newPrice) update["data.price"] = newPrice;
			}

			//console.log(item.name);
			//console.log(update);
			//console.log({size, weight, price});
			updates.push(update);
		}
	}

	if (updates.length) {
		console.log(actor.name, "has", updates.length, "items needing update");
		await actor.updateEmbeddedDocuments("Item", updates);
		updatedActors++;
	}
}

console.log(updatedActors, "actor(s) updated");
