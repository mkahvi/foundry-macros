/**
 * Usage: Select tokens & trigger macro
 */

const DefaultDamage = 0;

async function applyDamage(damage, tokens) {
	if (damage == 0 || !Number.isFinite(damage)) {
		console.warn(`"${damage}" is bad damage value`);
		return;
	}

	console.log('Applying damage:', damage, tokens);
	for (const t of tokens) {
		if (!t.actor) continue;
		const hp = t.actor.data.data.attributes.hp;
		const nl = hp.nonlethal;
		let newNL = Math.max(0, nl + damage);
		if (newNL == nl) continue;
		let overflow = 0;
		let current = hp.value;
		if (newNL > hp.max) {
			overflow = newNL - hp.max;
			newNL = hp.max;
			if (overflow != 0) current -= overflow;
		}
		const updateData = { _id: t.actor.id };
		if (nl != newNL) {
			updateData['data.attributes.hp.nonlethal'] = newNL;
		}
		if (hp.value != current) {
			updateData['data.attributes.hp.value'] = current;
		}
		if (nl != newNL || hp.value != current) {
			console.log(`Updating: ${t.name}`, updateData);
			await t.actor.update(updateData);
		}
	}
}

function renderDialog() {
	const tokens = canvas.tokens.controlled;
	if (tokens.length == 0) return;

	const html = $('<form class="flexcol">')
		.append(
			$('<h3 style="align-self: center;">').text('Target(s):'),
			$('<div class="flexrow" style="justify-content: center;white-space: break-spaces;">').html(tokens.map(t => `<span style="flex:0;white-space: nowrap;" title="Token ID: ${t.id}\nActor:\n - Name: ${t.actor.name}\n - ID: ${t.actor.id}">${t.name}</span>`).join(', ')),
			$('<hr>'),
			$('<h3 style="align-self: center;">').text('NL damage:'),
			$(`<input style="flex: 0;text-align: center;font-size:16pt;" type="number" data-dtype="Number" name="damage" value="${DefaultDamage}" step="1">`),
			$('<hr>')
		);

	new Dialog(
		{
			title: 'Deal nonlethal damage',
			content: html.get(0).outerHTML,
			buttons: {
				apply: {
					label: 'Apply Damage',
					callback: jq => {
						const fd = new FormDataExtended(jq.find('form').get(0)).toObject();
						console.log(fd);
						applyDamage(fd.damage, tokens);
					}
				},
			},
			default: 'apply'
		},
		{
			width: 360,
		}
	).render(true);
}

renderDialog();
