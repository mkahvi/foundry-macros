// De-activate selected buff types of selected tokens.

// Compatibility:
// - Foundry VTT 0.7.x
// - Pathfinder 0.77.24
//
// FVTT 0.8.x compatibility unknown

// Configuration
const clearTypes = {
	temp: true,
	misc: true,
	item: false,
	perm: false,
};

// Collect data
const types = Object.entries(clearTypes).filter((x) => x[1]).map((x) => x[0]);

const actors = canvas.tokens.controlled.map(o => o.actor);

// Update actor
const updateActorBuffs = function (actor) {
	const removed = [];
	actor.itemTypes.buff.filter(i => i.data.data.active && types.includes(i.data.data.buffType))
		.forEach(i => {
			i.update({ 'data.active': false }, { updateChanges: false });
			removed.push(i.name);
		});

	// actor.update({}); // trigger update changes now; unnecessary for some reason?

	if (removed.length > 0)
		console.log(`${actor.name} disabled buffs: ${removed.join(', ')}`);
	else
		console.log(`${actor.name} had no relevant buffs.`)
}

// Process
actors.forEach(updateActorBuffs);
