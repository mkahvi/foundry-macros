/**
 * Spell Preparatation Status.
 *
 * Provivides info on selected tokens on their spell preparation
 *
 * Compatibility:
 * - Foundry v10
 * - PF1 0.82.x
 */

// Get actors from selected tokens, but only if they have valid actors and not of 'basic' type
// Modify this line if you want it to get different actors, or to have specific ones always, or any other variation.
const actors = canvas.tokens.controlled.filter(t => t.actor && t.actor.type !== 'basic').map(t => t.actor);

class SimpleBook {
	id;
	levels = {};
	raw;

	constructor(bookId, bookData) {
		this.id = bookId;
		this.raw = bookData;

		Object.entries(bookData.spells)
			.forEach(([levelId, levelData]) => {
				const level = Number(levelId.replace(/^spell/, ''));
				// Do something with those
				if (levelData.max === null) return;
				this.levels[level] = {
					max: levelData.preparation.max,
					unused: levelData.preparation.unused,
					get prepared() { return this.max - this.unused; },
					get ready() { return this.unused == 0; },
					get overPrep() { return this.prepared > this.max; },
					get underPrep() { return this.unused > 0; }
				};
			});

		// Fill in levels that didn't get info
		Array(this.maxLevel + 1).map((v, i) => i).forEach(l => this.levels[i] ??= { ready: true });
	}

	get incompletePrep() {
		return Object.values(this.levels).some(level => level.unused !== 0);
	}

	get maxLevel() {
		return Math.max(...Object.keys(this.levels).map(l => Number(l)));
	}
}

const problemCasters = [];

actors.forEach(actor => {
	const actorData = game.release.generation >= 10 ? actor.system : actor.data.data,
		spellbooks = actorData.attributes.spells.spellbooks;

	const caster = {
		actor,
		get isCaster() { return Object.keys(this.books).length > 0; },
		books: {},
		get anyIncompletePrep() {
			return Object.values(this.books).some(book => book.incompletePrep);
		}
	};

	Object.entries(spellbooks).forEach(([bookId, bookData]) => {
		console.log({ inUse: bookData.inUse, prepared: bookData.spellPreparationMode != 'spontaneous' });
		if (!bookData.inUse) return;
		if (bookData.spellPreparationMode == 'spontaneous') return;
		caster.books[bookId] = new SimpleBook(bookId, bookData);
	});

	console.log({ name: actor.name, isCaster: caster.isCaster, casterData: caster });
	if (!caster.isCaster) return;

	problemCasters.push(caster);
});

if (problemCasters.length == 0)
	return ui.notifications.info('All chosen actors have preparation complete without issues.');

const templateSrc = `<div class='flexcol'>
{{#each casters}}
<h2>{{actor.name}}</h2>
{{#each books}}
<h3 style='font-weight:bold;'>{{raw.label}}</h3>
<div class='flexrow' style='gap:1px;'>
{{#each levels as |d level|}}
<div class='flexcol' style='gap:1px;'>
<label style='background:rgba(0,0,0,0.1);text-align:center;font-weight:bold;line-height:1.5;'>{{level}}</label>
<div class='flexrow' style='text-align:center;line-height:1.5;background-color:{{#if ready}}rgba(0,255,0,0.15);{{else}}rgba(255,0,0,0.05);{{/if}}{{#if overPrep}}background-color:rgba(255,0,0,0.15);{{/if}}'><span>{{prepared}}</span><span style='flex:0;'>/</span><span>{{max}}</span></div>
</div>
{{/each}}
</div>
{{/each}}
{{#unless @last}}<hr>{{/unless}}
{{/each}}
<hr>
</div>`;

const template = Handlebars.compile(templateSrc);

const templateData = {
	casters: problemCasters
};

const content = template(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

Dialog.prompt({
	title: 'Spellbook Preparation',
	label: 'Print to Chat',
	content,
	callback: () => {
		ChatMessage.create({
			content: content.replace(/<hr>\n<\/div>$/, "</div>"),
		});
	},
	rejectClose: false,
	options: {
		jQuery: false,
	}
});
