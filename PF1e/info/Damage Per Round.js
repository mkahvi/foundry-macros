/**
 * Damage Per Round
 *
 * Estimates damage potential.
 *
 * Usage:
 * 1) Select token
 * 2) Run macro
 *
 * Notes:
 * - Damage Reduction is not accounted for.
 * - Conditional modifier effects on criticals are not accounted for
 * - Conditional modifiers for size is not accounted for
 * - Non-multiplying and critical only damage is not accounted for
 * - Bonus dice are multiplied with crits even though they shouldn't
 */

async function generateDPRHTML(actor, data) {
	const content = `
<form autocomplete='off'>
	<style>
	.app.pf1.damage-per-round {
		& ul.attacks {
			display: grid;
			width: 100%;
			grid-template-columns: 3fr 1fr 4em 1fr 4em;

			margin: 0;
			padding: 0;
			gap: 0;

			& > li {
				display: contents;

				&.header > * {
					font-weight: bold;
					background-color: var(--pf1-bg-dark-1);
					white-space: nowrap;
				}

				& > * {
					padding: 2px 5px;

					border-right: 1px solid black;
					&:last-child {
						border-right: none;
					}
				}

				& :is(.attack-roll, .critical-hit) {
					display: flex;
					flex-flow: row nowrap;
					white-space: nowrap;
					gap: 0.5rem;
				}
			}
		}

		& .damage {
			& > .details {
				flex: 1 100%;
			}
		}

		& h3 {
			font-weight: bold;
			margin: 0.5rem 0;
		}
		& h4 {
			margin: 0;
		}
	}
	</style>

	<div class='form-group flexrow'>
		<label>{{labels.type}}</label>
		<select name='itemId'>
		{{#each items}}
			<optgroup label='{{label}}'>
				{{selectOptions choices selected=@root.itemId}}
			</optgroup>
		{{/each}}
		</select>

		<label>{{localize 'PF1.Action'}}</label>
		<select name='actionId'>
		{{selectOptions choices.actions selected=@root.actionId}}
		</select>
	</div>

	<div class='form-group'>
		<label>Buffs</label>
		<div class='form-fields'>
			<label>Attack</label>
			<input name='attackBonus' type='number' value='{{attackBonus}}' placeholder='0' min='-20' step='1' max='20'>
			<label>Damage</label>
			<input name='damageBonus' type='text' value='{{damageBonus}}' placeholder='0'>
		</div>
	</div>
	<div class='form-group'>
		<label>Critical</label>
		<div class='form-fields'>
			<label>Confirm</label>
			<input name='critConfirmBonus' type='number' value='{{critConfirmBonus}}' placeholder='0' min='0' step='1' max='20'>
			<label>Mult.</label>
			<input name='critMultBonus' type='number' value='{{critMultBonus}}' placeholder='0' min='0' step='1' max='6'>
			<label class='checkbox'>
				<input name='opt.keen' type='checkbox' {{checked opt.keen}}>
				Keen
			</label>
		</div>
	</div>

	<div class='form-group stacked'>
		<label class='checkbox'>
			<input type='checkbox' name='opt.powerAttack' {{checked opt.powerAttack}}>
			Power Attack ({{numberFormat bonus.pa.damage sign=true}}/{{numberFormat bonus.pa.attack sign=true}})
		</label>
	</div>

	<hr>

	<div class='form-group'>
		<label>Target</label>
		<div class='form-fields'>
			<label>AC</label>
			<input name='ac' type='number' value='{{ac}}' placeholder='14' min='0' step='1' max='50'>
			<label>HP</label>
			<input name='hp' type='number' value='{{hp}}' placeholder='0' min='0' step='1'>
		</div>
	</div>

	<hr>

	<h3>Damage</h3>
	<div class='flexrow damage'>
		<div class='details'>
			<b>Formula</b>:
			<code class='formula' data-tooltip='{{damage.formula}}'>{{damage.simplified}}</code>
		</div>
		<div class='details'>
			<b>Range</b>:
			<span class='range'>
				<span class='min'>{{damage.min}}<span>
				–
				<span class='max'>{{damage.max}}<span>
				(<span class='average'>~{{damage.avg}}<span>)
			</span>
		</div>

		<h4>Critical</h4>
		<div class='details'>
			<b>Chance</b>: {{critical.chance}}%
			<b>Multiplier</b>: ×{{critical.multiplier}}
		</div>
	</div>

	<h3>Attacks</h3>
	<ul class='attacks'>
		<li class='header'>
			<span>Label</span>
			<span>Hit Chance</span>
			<span>DPR</span>
			<span>Critical Chance</span>
			<span>CDPR</span>
		</li>

		{{#each attacks}}
		<li class='attack'>
		<label>{{label}}</label>
		<div class='attack-roll'>
			<span class='bonus' data-tooltip='Attack Bonus'>{{numberFormat bonus sign=true}}</span>
			<span class='chance' data-tooltip='Hit Chance'>{{chance}}%</span>
			<span class='minroll' data-tooltip='Minimum Roll'><i class="fa-solid fa-dice-d20"></i> {{minRoll}}+ </span>
			<span class='buffcap' data-tooltip='Buff Capacity'><i class="fa-solid fa-pepper-hot"></i> {{numberFormat buffcap sign=true}}</span>
		</div>
		<span class='contrib' data-tooltip='Average Damage Contribution'><i class="fa-solid fa-heart-crack"></i> {{numberFormat dpr decimals=1}}</span>
		<div class='critical-hit'>
			<span class='chance' data-tooltip='Confirm Chance'>{{crit.confirmChance}}%</span>
			<span class='minroll' data-tooltip='Minimum Roll'><i class="fa-solid fa-dice-d20"></i> {{crit.minRoll}}+ </span>
		</div>
		<span class='contrib' data-tooltip='Average Damage Contribution'><i class="fa-solid fa-heart-crack"></i> {{numberFormat cdpr decimals=1}}</span>
		</li>
		{{/each}}
	</ul>

	<h3>Stats</h3>
	<div class='stats flexrow'>
		<div class='normal'>
			<h4>Normal</h4>
			<div><b>Damage Potential</b>: {{stats.damage.min}} – {{stats.damage.max}} (~{{stats.damage.avg}})</div>
			<div><b>Average DMG per Round</b>: {{numberFormat stats.damage.dpr decimals=1}}</div>
			<div><b>Rounds until Death</b>: ~{{#if stats.target.rtd}}{{stats.target.rtd}}{{else}}Infinite{{/if}}</div>
		</div>
		<div class='critical'>
			<h4>With Criticals</h4>
			<div><b>Damage Potential</b>: {{numberFormatAlt stats.damage.cmin decimals=1}} – {{numberFormatAlt stats.damage.cmax decimals=1}} (~{{numberFormatAlt stats.damage.cavg decimals=2}})</div>
			<div><b>Average DMG per Round</b>: {{numberFormat stats.damage.cdpr decimals=1}}</div>
			<div><b>Rounds until Death</b>: ~{{#if stats.target.crtd}}{{stats.target.crtd}}{{else}}Infinite{{/if}}</div>
		</div>
	</div>
</form>
	`;

	return Handlebars.compile(content)(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
}

class DPRDialog extends FormApplication {
	config = { ac: 14 };

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: ['pf1', 'damage-per-round'],
			width: 460,
			height: 'auto',
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
		};
	}

	get title() {
		return `Damage Per Round – ${this.actor.name}`;
	}

	/** @type {Actor} */
	get actor() {
		const actor = this.object;
		if (!(actor instanceof Actor)) throw new Error('No actor found!');
		if (actor.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)) return actor;
		return null; // Nope
	}

	async getData() {
		const items = this.getItems();

		// Pre-choose item if none are selected yet
		this.config.itemId ||= Object.values(items)[0]?.items[0]?.id; //

		const actions = this.getActions();
		this.config.actionId ||= actions[0].id;

		const item = this.chosenItem,
			action = this.chosenAction;

		this.rollData = action.getRollData();

		const bonus = {
			pa: this.getPowerAttack(),
		};

		const attacks = await action.getAttacks({ full: true, resolve: true, conditionals: true, bonuses: true, rollData: this.rollData });

		const damageBonuses = [];

		const ac = this.config.ac;
		let atkBonus = this.config.attackBonus || 0;

		const isPA = this.config.opt?.powerAttack ?? false;
		if (isPA) {
			atkBonus += bonus.pa?.attack ?? 0;
			damageBonuses.push({ formula: `${bonus.pa?.damage ?? 0}`, label: 'PA' });
		}

		// TODO: Point Blank, Manyshot, Rapid shot
		const damage = await this.getDamage(damageBonuses);
		const critical = this.getCritical();

		const stats = { damage: { min: 0, max: 0, avg: 0, dpr: 0, cmin: 0, cmax: 0, cavg: 0, cdpr: 0 }, target: { rtd: null, crtd: null } };

		for (const atk of attacks) {
			atk.bonus += atkBonus;
			atk.minRoll = Math.clamped(ac - atk.bonus, 2, 20); // Can't require worse than 2, can't exceed nat 20
			atk.chance = (21 - atk.minRoll) * 5;
			// if (atk.minRoll < 2) atk.minRoll += 1; // Nat 1 is always a miss
			atk.buffcap = Math.max(0, atk.minRoll - 2);

			atk.dpr = (atk.chance / 100) * damage.avg; // DPR contribution

			// There may be some flaws in this logic considering
			// if you need 19 to roll to hit a target, and your crit range is 19,
			// you'd be constantly critting if you confirm them.
			// But this may be offset simply that this is DPR across arbitrary period of time, not per single attack
			atk.crit ??= {};
			atk.crit.minRoll = Math.clamped(atk.minRoll - critical.confirm, 2, 20);
			atk.crit.confirmChance = (21 - atk.crit.minRoll) * 5; // Chance to confirm
			atk.crit.chance = (critical.chance / 100) * (atk.crit.confirmChance / 100); // Actual chance to confirm crit (attack + confirm)
			atk.crit.damageMult = atk.crit.chance * critical.multiplier; // Effect on DPR
			atk.crit.damageMultP = atk.crit.damageMult * 100;

			atk.cdpr = atk.dpr * (1 + atk.crit.damageMult);
			atk.crit.dpr = atk.cdpr - atk.dpr; // DPR increase from crits

			stats.damage.dpr += atk.dpr;
			stats.damage.cdpr += atk.cdpr;
			stats.damage.min += damage.min;
			stats.damage.cmin += damage.min;
			stats.damage.max += damage.max;
			stats.damage.cmax += damage.max * critical.multiplier;
		}

		stats.damage.avg = (stats.damage.min + stats.damage.max) / 2;
		stats.damage.cavg = (stats.damage.cmin + stats.damage.cmax) / 2;

		// TODO: Critical hits
		// TODO: Damage Reduction

		stats.target.rtd = Math.ceil((this.config.hp ?? 0) / stats.damage.dpr);
		stats.target.crtd = Math.ceil((this.config.hp ?? 0) / stats.damage.cdpr);

		return {
			actor: this.actor,
			item,
			action,
			items,
			actions,
			attacks,
			damage,
			critical,
			bonus,
			stats,
			choices: {
				actions: Object.fromEntries(actions.map(a => [a.id, a.name])),
			},
			labels: {
				type: this.getTypeLabel(this.chosenItem),
			},
			...this.config,
		};
	}

	get chosenItem() {
		return this.actor.items.get(this.config.itemId);
	}

	get chosenAction() {
		return this.chosenItem?.actions.get(this.config.actionId);
	}

	getTypeLabel(item) {
		return game.i18n.localize(CONFIG.Item.typeLabels[item?.type]);
	}

	getItems() {
		const items = this.actor.items
			.filter(i => ['attack', 'weapon'].includes(i.type)) // Attacks & Weapons only
			.filter(i => i.hasAttack && i.hasDamage); // Only if they have attack rolls and damage rolls

		const rv = {};
		for (const item of items) {
			rv[item.type] ??= { items: [], label: this.getTypeLabel(item) };
			rv[item.type].items.push(item);
		}

		for (const data of Object.values(rv)) {
			// TODO: Sort choices
			data.choices = Object.fromEntries(data.items.map(i => [i.id, i.name]));
		}

		// TODO: Sort categories

		return rv;
	}

	getActions() {
		const item = this.chosenItem;
		if (!item) return [];
		return [...item.actions];
	}

	async getDamage(bonus) {
		const action = this.chosenAction;

		const actionData = action.data; // Remove .data with PF1v11

		const parts = actionData.damage.parts.map(p => ({ formula: p.formula, label: 'Base' }));

		const abl = actionData.ability.damage;
		if (abl) {
			const mod = this.rollData.abilities[abl]?.mod ?? 0;
			const mult = actionData.ability.damageMult ?? 1;
			const label = pf1.config.abilitiesShort[abl];
			parts.push({ formula: `floor(${mod} * ${mult})`, label });
		}

		action.allDamageSources.forEach(p => parts.push({ label: p.flavor, formula: p.formula }));
		if (bonus?.length) parts.push(...bonus);

		if (this.config.damageBonus) {
			parts.push({ formula: this.config.damageBonus, label: 'Custom' });
		}

		for (const part of parts) {
			part.min = RollPF.safeRollSync(part.formula, this.rollData, undefined, undefined, { minimize: true }).total;
			part.max = RollPF.safeRollSync(part.formula, this.rollData, undefined, undefined, { maximize: true }).total;
		}

		const min = parts.reduce((total, p) => total + p.min, 0);
		const max = parts.reduce((total, p) => total + p.max, 0);
		const avg = (min + max) / 2;

		const formula = parts.map(p => `${p.formula}[${p.label}]`).join(' + ');

		const simplified = pf1.utils.formula.compress(pf1.utils.formula.simplify(formula, this.rollData))
			// Expand formula
			.replaceAll(/[-+]/g, m => ` ${m} `);

		return { parts, min, max, avg, formula, simplified };
	}

	getCritical() {
		const action = this.chosenAction;

		const actionData = action.data; // Remove .data with PF1v11

		const critConfirmBonus = RollPF.safeRollSync(actionData.critConfirmBonus || '0').total;

		const isKeen = this.config.opt?.keen ?? false;
		const critConfirmBonusManual = this.config.critConfirmBonus || 0;
		const critMultBonusManual = this.config.critMultBonus || 0;

		let critRange = action.critRange;
		if (isKeen) critRange -= (21 - critRange);

		const critChance = (21 - critRange) * 5;
		const critMult = (actionData.ability?.critMult ?? 2) + critMultBonusManual;

		return {
			confirm: critConfirmBonus + critConfirmBonusManual,
			range: critRange,
			multiplier: critMult,
			chance: critChance,
		};
	}

	getPowerAttack() {
		const action = this.chosenAction;
		if (!action) return;

		const scaling = 1 + Math.floor(this.rollData.bab / 4);

		const damage = this.rollData.action?.powerAttack?.damageBonus ?? 2;
		const bonus = Math.floor((scaling * damage) * action.getPowerAttackMult({ rollData: this.rollData }));

		return { damage: bonus, attack: -scaling };
	}

	async _renderInner(data) {
		const content = await generateDPRHTML(this.item, data);
		const html = $(content); // Turn to jQuery
		this.form = html[0]; // Regular HTML for ourselves
		return html; // Foundry wants jQ
	}

	_updateObject(event, formData) {
		const oldItemId = this.config.itemId;

		this.config = foundry.utils.expandObject(formData);

		// Clear action ID if item is changed
		if (this.config.itemId !== oldItemId) this.config.actionId = null;

		this.config.ac ??= 14; // Add AC if it's cleared

		this.render();
	}
}

new DPRDialog(actor).render(true);
