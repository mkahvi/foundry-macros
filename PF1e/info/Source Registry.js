/**
 * Display Source Registry contents
 */

const SETTINGS = {
	sortDate: true, // if false, uses alphabetical sorting instead
};

// Read macro params
if (scope.sortDate !== undefined) SETTINGS.sortDate = Boolean(scope.sortDate);

const sources = [...pf1.registry.sources]
	.map(src => ({ ...src, id: src.id, dateStamp: Date.parse(src.date) }))
	.sort((a, b) => a.dateStamp - b.dateStamp);

if (SETTINGS.sortDate) sources.sort((a, b) => a.dateStamp - b.dateStamp);
else sources.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base' }));

const template = `
<style>
.dialog.source-list-macro {
	& .window-content {
		overflow: hidden;
	}
	& .dialog-buttons {
		flex: 0;
	}
	& form {
		display: flex;
		flex-flow: row nowrap;
		gap: 3px;

		& label {
			display: flex;
			gap: 3px;
			flex-flow: row nowrap;
			white-space: nowrap;

			& input {
				top: unset;
			}
		}
	}
	& .dialog-content {
		display: flex;
		flex-flow: column nowrap;
		gap: 3px;
		overflow: hidden;
	}
	& :is(input, label) {
		flex: 0;
	}
	& ul {
		flex: 1;
		overflow: hidden scroll;
		list-style: none;
		margin: 0;
		padding: 0;
		display: grid;
		grid-template-columns: min-content min-content 1fr min-content min-content;
		grid-auto-rows: min-content;
		gap:3px;
	}
	& li {
		display: contents;

		&.hidden {
			& > * {
				display: none;
			}
		}
	}
  & li > :is(.date,.abbr,.book-id) {
		font-family: monospace;
		border: 1px solid #a59d90;
		padding: 0 3px;
		border-radius: 5px;
		white-space: nowrap;
		height: min-content;
		&:empty {
		  border: none;
		}
	}
	& li > label {
		font-weight: bold;
	}
	& li > * {
		user-select: all;
	}
}
</style>
<form class='sorting'>
	<label>
		<input type='radio' value='date' class='sort' {{checked sortDate}}>
		Sort by Date
	</label>
	<label>
		<input type='radio' value='name' class='sort' {{checked (not sortDate)}}>
		Sort by Name
	</label>
</form>
<input type='search' placeholder='Search filter...'>
<ul>
	{{#each sources}}
	<li>
		<span class='date'>{{date}}</span>
		<span class='abbr'>{{abbr}}</span>
		<label data-tooltip='Page count: {{pages}}'>{{name}}</label>
		{{#if url}}<a href="{{{url}}}" class='link' target='_blank' data-tooltip='{{url}}'><i class="fa-solid fa-link"></i></a>{{else}}<span></span>{{/if}}
		<span class='book-id'>{{id}}</span>
	</li>
	{{/each}}
</ul>
`;

const templateData = {
	sources,
	sortDate: SETTINGS.sortDate,
};

let queryCache = scope.query;

Dialog.wait({
	title: 'Source Registry',
	content: Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
	rejectClose: false,
	buttons: {},
	close: () => null,
	render: (html) => {
		const content = [...html.querySelector('ul').querySelectorAll('li')].map(el => ({
			element: el,
			name: el.querySelector('label').textContent.trim(),
			id: el.querySelector('.book-id').textContent.trim(),
			date: el.querySelector('.date').textContent.trim(),
		}));

		const filter = (query) => {
			queryCache = query;
			const q = new RegExp(query, 'ig');
			for (const { element, name, id, date } of content) {
				const matchName = q.test(name);
				const matchId = q.test(id);
				const matchDate = q.test(date);
				element.classList.toggle('hidden', !matchName && !matchId && !matchDate);
			}
		};

		const search = html.querySelector('input[type="search"]');
		search.addEventListener('input', ev => filter(ev.target.value), { passive: true });
		search.addEventListener('change', ev => filter(ev.target.value), { passive: true });

		// Hacky sorting by re-executing the macro with some options
		html.querySelectorAll('input.sort').forEach(el => el.addEventListener('click', ev => {
			ev.preventDefault();

			const sortDate = ev.target.value === 'date';
			if (SETTINGS.sortDate === sortDate) return;

			const appId = ev.target.closest('.app[data-appid]').dataset.appid;
			ui.windows[appId].close();

			this.execute({ sortDate, query: queryCache });
		}));

		if (queryCache) {
			search.value = queryCache;
			filter(queryCache);
		}
	},
},
{
	height: 720,
	width: 640,
	jQuery: false,
	rejectClose: false,
	resizable: true,
},
{
	classes: ['dialog', 'source-list-macro'],
});
