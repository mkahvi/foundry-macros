/**
 * Find items on actors
 * - Shows dialog to find an item by name
 * - Finds all Items on character that have the text in their name.
 * - Filters out attacks by default but nothing else
 * - Output results into a new dialog.
 *
 * Compatibility:
 * - Foundry VTT 11.307
 * - Pathfinder 1e v9.4
 *
 * TODO:
 * - Remove classes from .toAnchor() once Foundry fixes the function.
 */

class SearchOptions {
	// skip the following item types
	// Valid types are: attack, class, feat, spell, consumable, weapon, equipment, container, loot, ....

	unwantedTypes = ['attack'];

	caseInsensitive = true;
	regularExpression = false;

	containers = true;

	name = true;
	description = false;
	note = false;
}

/**
	 * Configure following per system. Default is set for Pathfinder 1e.
	 */
const searchTargets = {
	name: {
		paths: ['name'],
		label: 'Name',
		default: true,
	},
	description: {
		paths: ['system.description.value'],
		label: 'Description',
		default: false,
	},
	note: {
		paths: ['system.contextNotes'],
		label: 'Context Notes',
		default: false,
		handler: (search, data) => {
			for (const n of data)
				if (search.test(n.text)) return true;
		}
	}
};

const SearchLang = {
	// Search box
	findTitle: 'Find item on actors',
	findDescription: 'Find following item from actor(s):',
	exampleSearch: 'e.g. Bag of Holding',

	searchWhere: 'From',
	searchHow: 'Options',

	// Buttons
	findAll: 'Find from all',
	findSelected: 'Find from selected',

	newSearch: 'New Search',
	refineSearch: 'Refine Search',
	dismiss: 'Dismiss',

	caseInsensitive: 'Case insensitive',
	regularExpression: 'RegExp',

	itemName: 'Name',
	description: 'Description',
	contextNote: 'Note',

	// Results dialog
	results: 'Search results',
	noResults: 'No matching items found',
};

const findItem = function (search, actors, searchOptions) {
	if (!search) return ui.notifications.error('Can\'t search for nothing');
	searchOptions.search = search;
	const origSearch = search;
	if (searchOptions.caseInsensitive) search = search.toLowerCase(); // do it once

	// Escape regexp things
	const escapeRE = (s) => s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
	const REify = (s) => new RegExp(s, searchOptions.caseInsensitive ? 'i' : undefined);
	search = REify(searchOptions.regularExpression ? search : escapeRE(search));

	const isWanted = (i) => {
		if (searchOptions.unwantedTypes.includes(i.type)) return false;
		if (searchOptions.name && search.test(i.name)) return true;
		if (searchOptions.description && search.test(i.system.description?.value)) return true;
		if (searchOptions.note && i.system.contextNotes?.length)
			for (const n of i.system.contextNotes)
				if (search.test(n.text)) return true;
		return false;
	};

	const checkItems = (items, depth) => items.filter((i) => {
		if (i.type === 'container' && searchOptions.checkContainers &&
				checkItems(i.items, depth + 1).length > 0) return true;

		return isWanted(i);
	});

	const matchingActors = [];
	for (const actor of actors) {
		if (!actor.isOwner) continue;

		const depth = 0;
		const matchingItems = checkItems([...actor.items.values()], depth);

		if (matchingItems.length > 0)
			matchingActors.push({ actor, name: actor.name, id: actor.id, items: matchingItems });
	}

	const content = `
		<style>
		.app.dialog.find-item-results hr {
			padding: 2px;
		}
		.app.dialog.find-item-results .name {
			flex: 10 0 8em;
		}
		.app.dialog.find-item-results .name > a {
			display: inline-flex;
			gap: 3px;
			border: none;
			background: unset;
		}
		.app.dialog.find-item-results .actor .name {
			margin-bottom: 2px;
		}
		.app.dialog.find-item-results .header {
			background: rgba(0,0,0,0.1);
			padding: 3px;
		}
		.app.dialog.find-item-results .id {
			flex: 1 2 8em;
			margin: unset;		
		}
		.app.dialog.find-item-results .item-list .item {
			padding: 3px;
		}
		.app.dialog.find-item-results .item-list .item {
			border-style: none none dotted none;
			border-width: 1px;
			padding-top: 1px;
			padding-bottom: 2px;
		}
		.app.dialog.find-item-results .item-list .item .name {
			padding-left: var(--depth * 0.6em);
		}
		.app.dialog.find-item-results .item-list .item .quantity {
			flex: 0 8 28px;
		}
		.app.dialog.find-item-results .item-list .item .type {
			flex: 0 5 6em;
		}
		</style>

		{{#*inline "item"}}
		<div class='flexrow item'>
			<label class='name' style='--depth:{{depth}}'>{{{itemLink}}}</label>
			<label class='quantity'>{{quantity}}</label>
			<label class='type'>{{type}}</label>
			<pre class='id'>[{{id}}]</pre>
		</div>
		{{#if items.length}}
			{{> "item"}}
		{{/if}}
		{{/inline}}

		{{#*inline "item-list"}}
		{{#if items.length}}
			<div class='flexcol item-list'>
			{{#each items}}
				{{> "item"}}
			{{/each}}
			</div>
		{{/if}}
		{{/inline}}

		<form class='flexcol' style='max-height:760px;overflow:auto' autocomplete='off'>
		{{#if results.length}}
			{{#each results}}
				<div class='header flexrow actor'>
					<h3 class='name'>{{{actorLink}}}</h3>
					<pre class='id'>[{{id}}]</pre>
				</div>

				{{> "item-list"}}

				{{#unless @last}}<hr>{{/unless}}
			{{/each}}
		{{else}}
			<p>{{@root.lang.noResults}}</p>
		{{/if}}
		</form>
		<hr>
		`;

	const results = matchingActors.map(match => {
		const items = [];

		const buildTree = (stack, items, depth) => {
			for (const item of items) {
				const itemData = {
					item,
					name: item.name,
					id: item.id,
					type: item.type,
					itemLink: item.toAnchor({ classes: ['content-link'] }).outerHTML,
					items: [],
					depth,
					quantity: item.system.quantity !== undefined ? `×${item.system.quantity}` : '—',
				};

				stack.push(itemData);

				// Add container items
				if (searchOptions.checkContainers && item.type === 'container')
					buildTree(itemData.items, item.items, depth + 1);
			}
		}

		buildTree(items, match.items);

		const { actor } = match;

		return {
			actor: actor,
			name: actor.name,
			id: actor.id,
			actorLink: actor.toAnchor({ classes: ['content-link'] }).outerHTML,
			items,
		}
	});

	const templateData = {
		results,
		lang: SearchLang
	}

	new Dialog({
		title: `${SearchLang.results}: ${origSearch}`,
		content: Handlebars.compile(content)(templateData),
		buttons: {
			ok: {
				label: SearchLang.dismiss,
				icon: '<i class="fas fa-power-off"></i>',
			},
			refine: {
				label: SearchLang.refineSearch,
				icon: '<i class="fas fa-search"></i>',
				callback: () => generateQuery(origSearch, searchOptions),
			},
			new: {
				label: SearchLang.newSearch,
				icon: '<i class="fas fa-redo"></i>',
				callback: () => generateQuery('', new SearchOptions()),
			}
		},
		default: 'ok'
	},
	{
		jQuery: false,
		width: 620,
		resizable: true,
		classes: [...Dialog.defaultOptions.classes, 'find-item-results'],
	}
	).render(true);
};

/**
	 * @param {HTMLElement} html
	 */
const getItemName = (html) => html.querySelector('input[name="search"]').value;
/**
	 * @param {HTMLElement} html
	 * @param {object} options
	 */
const getOptions = (html, options) => {
	const form = html.querySelector('form');
	const formData = new FormDataExtended(form).object;
	Object.assign(options, formData);
	return options;
};
const getSelectedActors = () => canvas.tokens.controlled.map(o => o.actor);
const getAllActors = () => [...game.actors.values()];

const generateQuery = (value = '', searchOptions) => {
	const content = `
	<form class='flexcol' autocomplete='off'>
		<style>
		.find-item-macro-dialog form label {
			display: inline-flex; align-items: center; align-self: flex-end; flex: 0;
		}
		.find-item-macro-dialog form .flexcol {
			text-align: center;align-items: center;
		}
		.find-item-macro-dialog form .flexrow {
			gap: 0.5em;
		}
		.find-item-macro-dialog form h4 {
			flex: 0; width: 100%; font-weight: bold; border-bottom: 1px solid maroon;
		}
		</style>

		<p>{{labels.findDescription}}</p>
		<input type="text" name="search" placeholder="{{labels.exampleSearch}}" value="{{search}}" autofocus
			style="margin-bottom:8px;">
		<div class="flexrow">
			<div class="flexcol">
				<h4>{{labels.searchHow}}</h4>
				<label>
					{{labels.regularExpression}}
					<input type="checkbox" name="regularExpression" {{checked regularExpression}}>
				</label>
				<label>
					{{labels.caseInsensitive}}
					<input type="checkbox" name="caseInsensitive" {{checked caseInsensitive}}>
				</label>
			</div>
			<div class="flexcol">
				<h4>{{labels.searchWhere}}</h4>
				<label>
					{{labels.itemName}}
					<input type="checkbox" name="name" {{checked name}}>
				</label>
				<label>
					{{labels.description}}
					<input type="checkbox" name="description" {{checked description}}>
				</label>
				<label>
					{{labels.contextNote}}
					<input type="checkbox" name="note" {{checked note}}>
				</label>
			</div>
		</div>
		<hr>
	</form>
	`;

	const templateData = {
		...searchOptions,
		labels: SearchLang,
	};

	new Dialog({
		title: SearchLang.findTitle,
		content: Handlebars.compile(content)(templateData),
		buttons: {
			find: {
				label: SearchLang.findSelected,
				icon: '<i class="fas fa-user-friends"></i>',
				callback: html => findItem(getItemName(html), getSelectedActors(), getOptions(html, searchOptions)),
			},
			findAll: {
				label: SearchLang.findAll,
				icon: '<i class="fas fa-search"></i>',
				callback: html => findItem(getItemName(html), getAllActors(), getOptions(html, searchOptions)),
			},
		},
		default: 'find',
	},
	{
		jQuery: false,
		classes: [...Dialog.defaultOptions.classes, 'find-item-macro-dialog'],
	}).render(true);
};

const defaultSeach = new SearchOptions();

// Check settings
const types = game.system.template.Item.types;
const badTypes = defaultSeach.unwantedTypes.filter(t => !types.includes(t));
if (badTypes.length) console.warn('findItem: Unrecognized type(s): ' + badTypes.join(', '));

generateQuery('', defaultSeach);
