/**
 * Pearl of Power
 *
 * Usage:
 * - Paste this into a new script macro
 * - In pearl of power advanced tab, drag&drop the script macro in as on use script.
 * - Set dictionary flag with key "maxLevel" on the pearl of power item with value equal to maximum allowed level.
 * - Use the pearl of power as normal.
 * 
 * Compatibility:
 * - Foundry v11
 * - Pathfinder 1e v10
 */

const allowedBookTypes = ['prepared']; // possible choices: prepared, spontaneous, hybrid and prestige

const maxLevel = 6; // Used only if item reference with maxLevel dictionary flag is not found.

const addToChatcard = true;

const loc = (s) => game.i18n.localize(s);

const i18n = {
	title: 'Pearl of Power',
	cancel: loc('Cancel'),
	novalidbooks: 'No valid spellbooks',
	novalidspells: 'No valid spells',
	levels: {
		1: loc("PF1.SpellLevels.1"),
		2: loc("PF1.SpellLevels.2"),
		3: loc("PF1.SpellLevels.3"),
		4: loc("PF1.SpellLevels.4"),
		5: loc("PF1.SpellLevels.5"),
		6: loc("PF1.SpellLevels.6"),
		7: loc("PF1.SpellLevels.7"),
		8: loc("PF1.SpellLevels.8"),
		9: loc("PF1.SpellLevels.9"),
	},
	cardTitle: 'Spell Restored',
};

/* global shared */
const isScriptCall = typeof shared !== 'undefined';

function reject() {
	if (isScriptCall)
		shared.reject = true;
}

// Find valid spellbooks
const books = [];
for (const [bookId, bookData] of Object.entries(actor.system.attributes?.spells?.spellbooks ?? {})) {
	if (!bookData.inUse) continue;
	// By book type
	if (!allowedBookTypes.includes(bookData.spellPreparationMode)) continue;
	// Ignore spellpoint books
	if (!bookData.autoSpellLevelCalculation && bookData.spellPoints?.useSystem) continue;

	books.push({ id: bookId, label: bookData.label, spells: { 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [] } });
}

if (books.length === 0) {
	ui.notifications.error(i18n.novalidbooks);
	return reject();
}

const validBookIds = books.map(b => b.id);

/* global item */
let maxValidLevel = maxLevel ?? 6;
if (isScriptCall) {
	const _maxLevel = item.getItemDictionaryFlag('maxLevel');
	if (_maxLevel > 0)
		maxValidLevel = _maxLevel ?? maxValidLevel;
	else
		console.warn('Pearl of Power | No max level defined. Add "maxLevel" dictionary flag with appropriate value.');
}

// Find valid spells
const spells = actor.itemTypes.spell
	?.filter(s => validBookIds.includes(s.system.spellbook) &&
			// Can actually be restored
			s.system.preparation?.value < s.system.preparation?.max &&
			// Of reasonable level
			s.system.level <= maxValidLevel &&
			// Not cantrip
			s.system.level > 0);

if (spells.length === 0) {
	ui.notifications.error(i18n.novalidspells);
	return reject();
}

// Sort spells by level
for (const spell of spells) {
	const book = books.find(b => b.id === spell.system.spellbook);
	if (!('all' in book.spells)) {
		Object.defineProperty(book?.spells, 'all', { value: [] });
	}
	book.spells.all.push(spell);
	book.spells[spell.system.level].push(spell);
}

function empowerSpellCharges(spell) {
	//
	const maxValidLevel = item.getItemDictionaryFlag('maxLevel') ?? maxLevel ?? 6;
}

const template = `
	<div>
	<style>
	.app.pearl-of-power {
		& ul {
			list-style: none;
			margin: 0;
			padding: 0;
		}
		& :is(h2, h3, h4) {
			margin: 0;
			padding: 0;
		}
		& :is(h2, h3) {
			margin-bottom: 3px;
		}
		& h4 {
			flex: 1;
			border: none;
		}
		& .item.spell {
			display: flex;
			flex-flow: row nowrap;
			align-items: center;
			gap: 0.3em;

			& .preparation {
				padding: 0 3px;
			}

			& img {
				flex: 0 24px;
				width: 24px;
				height: 24px;
			}

			&:hover {
				background-color: rgba(0,0,0,0.1);
			}
		}
	}
	</style>
	<form autocomplete='off'>
	{{#each books}}
		{{#if spells.all.length}}
		<h2>{{label}}</h2>
		<ul class='spell-level-list'>
			{{#each spells as |levelspells level|}}
			{{#if levelspells.length}}
			<li class='spell-level'>
				<h3>{{lookup @root.i18n.levels level}}</h3>
				<ul class='spell-list'>
					{{#each levelspells}}
						<li class='spell item' data-item-id='{{id}}'>
							<img src='{{img}}'>
							<h4 class='name'>{{name}}</h4>
							<div class='preparation'>
								<span class='left'>{{system.preparation.value}}</span>
								<span class='delimiter'>/</span>
								<span class='max'>{{system.preparation.max}}</span>
							</div>
						</li>
					{{/each}}
				</ul>
			</li>
			{{/if}}
			{{/each}}
		</ul>
		{{/if}}
	{{/each}}
	</form>
	</div>
	`;

class PoPDialog extends Application {
	_choice = null;

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: i18n.title,
			classes: [...options.classes, 'pearl-of-power'],
		};
	}

	getData() {
		return {
			actor,
			books,
			i18n,
		};
	}

	_renderInner(data) {
		const content = Handlebars.compile(template)(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
		return $(content); // Foundry likes jquery
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.addEventListener('click', event => {
			let el = event.target;
			if (!el.classList.contains('item')) el = el.closest('.item');
			if (!el) return;

			event.preventDefault();

			const itemId = el?.dataset.itemId;
			const item = actor.items.get(itemId);
			if (item) {
				this._choice = item;
				this.close();
			}
		});
	}

	close(...args) {
		super.close(...args);
		this.resolve(this._choice);
	}

	static open(renderOpts = { focus: true }) {
		return new Promise(resolve => {
			const app = new PoPDialog();
			app.resolve = resolve;
			app.render(true, renderOpts);
		});
	}
}

/** @type {Item} */
const spell = await PoPDialog.open(); // eslint-disable-line

if (spell) {
	await spell.addCharges(1); // eslint-disable-line

	if (isScriptCall && addToChatcard) {
		// TODO: Hide from non-owners?
		Hooks.once('preCreateChatMessage', (msg, data) => {
			if (msg.itemSource !== item) return;
			const d = document.createElement('div');
			d.innerHTML = data.content;
			// Find end of description
			const c = d.querySelector('.card-content');
			if (!c) return;
			// Add restoration message
			const rd = document.createElement('div');
			rd.style.textAlign = 'center';
			rd.classList.add('gm-sensitive'); // Tell system to hide this from non-owner
			const h3 = document.createElement('h3');
			h3.innerText = i18n.cardTitle;
			rd.append(h3, spell.link);
			c.after(rd);
			// Commit
			msg.updateSource({ content: d.innerHTML });
		});
	}
}
else return reject();
