/**
 * Add Custom Skill
 * 
 * Adds custom skill to selected actors.
 */

// Configuration
const skillId = 'nuskill'; // Must be unique
const skillData = {
	name: 'New Skill', // User facing name
	ability: 'int', // Ability score
	rt: false, // requires training
	acp: false, // can suffer from ACP
	background: false, // is background skill?
	journal: null, // UUID to journal entry describing the skill
};

// Logic
if (skillId !== skillId.slugify()) throw new Error(`Skill ID "${skillId}" is not acceptable`);

const actors = canvas.tokens.controlled.map(t => t.actor).filter(a => a?.isOwner);

for (const actor of actors) {
	if (skillId in actor.system.skills) continue; // Already has it
	await actor.update({ system: { skills: { [skillId]: { ...skillData, rank: 0, custom: true } } } });
}
