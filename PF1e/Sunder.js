/**
 * Sunder
 * ... or other equipment smashing.
 *
 * Compatibility:
 * - PF1 0.82.2
 * - Foundry 10
 */

// i18n support
const lang = {
	title: 'Sunder: {name}', // {name} is replaced with token/actor name
	apply: 'Apply',
	value: 'Damage',
	name: 'Name',
	health: 'HP',
	hardness: 'Hardness',
	hint: 'Hardness is NOT automatically accounted for.',
	noDamage: 'No damage defined, sunder cancelled.',
	noItem: 'No item selected, sunder cancelled.',
	notFound: 'Defined item not found!',
};

const defaultDamage = ''; // Leave as empty string to not have default value

// PF1v10 makes the following unnecessary
const autoBreak = true; // Automatically apply broken condition when health goes to autoBreakThreshold
const autoBreakThreshold = 0.5; // 50%

const autoDestroy = true; // Automatically destroy item if HP reaches 0 or less, by reducing its quantity.
const autoDestroyRepairs = true; // Reset HP to max when reducing quantity, otherwise set to 0

// Order of items. Types must match return values of getType() function below.
const itemOrder = ['armor', 'shield', 'weapon', 'equipment', 'other'];

// Get target actor
const target = token.actor ?? game.user.character;

// Dialog template
const template = `
<form autocomplete='off' class='flexcol' style='gap:0.3rem;'>
<label class='flexrow'>
	<span class='label' style='flex:2;align-items:center;'>${lang.value}</span>
	<input type='number' name='s_damage' style='flex:3;' data-dtype='Number' value='${defaultDamage}' placeholder='0' step='1' required>
</label>
<p class='hint' style='align-self:center;'>${lang.hint}</p>
<ul class='item-list flexcol' style='list-style:none;margin:0;padding:0;gap:2px;'>
	<li class='flexrow' style='gap:0.3rem;font-weight:bold;background-color:hsl(0deg 0% 0% / 20%);line-height:1.5;'>
		<span class='select' style='flex:0 1.5rem;'> </span>
		<span class='name'>${lang.name}</span>
		<span class='health' style='flex:0 3rem;text-align:center;'>${lang.health}</span>
		<span class='hardness' style='flex:0 4rem;text-align:center;'>${lang.hardness}</span>
	</li>
	{{#each items}}
	<li class='item flexrow' data-item-id='{{id}}'>
		<label class='flexrow' style='gap:0.3rem;'>
			<input type='radio' name='s_itemId' value='{{id}}' style='flex:0 1.5rem;margin:unset;top:unset;' required>
			<span class='name' style='flex:1;overflow:hidden;text-overflow:ellipsis;'>{{name}}</span>
			<span class='health' style='flex:0 3rem;text-align:center;'>{{hp}}/{{hpMax}}</span>
			<span class='hardness' style='flex:0 4rem;text-align:center;'>{{hardness}}</span>
		</label>
	</li>
	{{/each}}
</ul>
<hr>
</form>
`;

// Get simplified type
const getType = (i) => {
	switch (i.type) {
		case 'weapon':
			return 'weapon';
		case 'equipment': {
			const stype = i.subType;
			if (['armor', 'shield'].includes(stype))
				return stype;
			return 'equipment';
		}
		default:
			return 'other';
	}
}

const sortByType = (a, b) => {
	const d = itemOrder.indexOf(a.type) - itemOrder.indexOf(b.type);
	if (d != 0) return d;
	// Fall back to .sort value
	return a.sort - b.sort;
}

const doSunder = (html) => {
	const form = html.querySelector('form');
	const formData = new FormDataExtended(form).object;
	const damage = formData.s_damage ?? 0,
		itemId = formData.s_itemId ?? null;

	if (!itemId) return void ui.notifications.error(lang.noItem);

	if (damage == 0) return void ui.notifications.error(lang.noDamage);

	const item = target.items.get(itemId);

	if (!item) return void ui.notifications.error(lang.notFound);

	const chp = item.system.hp.value,
		nhp = Math.min(chp - damage, item.system.hp.max ?? 1);

	const updateData = { system: {} };
	if (nhp <= 0 && autoDestroy) {
		// Item is destroyed
		updateData.system.quantity = item.system.quantity - 1;

		// Next item in stack is full health
		if (autoDestroyRepairs) updateData.system.hp = { value: item.system.hp.max };
		// Set HP anyway
		else updateData.system.hp = { value: nhp };
	}
	else {
		updateData.system.hp = { value: nhp };
	}

	const isBroken = item.system.broken ?? false;
	if (autoBreak) {
		const percentage = updateData.system.hp / item.system.max;
		if (percentage <= autoBreakThreshold && !isBroken) {
			updateData.system.broken = true;
		}
		else if (percentage > autoBreakThreshold && isBroken) {
			// Unbreak if HP increases above threshold
			updateData.system.broken = false;
		}
	}

	item.update(updateData);
}

if (target) {
	const items = target.items
		.filter(item => item.system.equipped && item.system.quantity > 0);

	const templateData = {
		items: items.map(item => ({
			id: item.id,
			name: item.name,
			type: getType(item),
			hp: item.system.hp.value,
			hpMax: item.system.hp.max,
			broken: item.system.hp.value / item.system.hp.max <= autoBreakThreshold,
			hardness: item.system.hardness ?? 0,
			sort: item.sort,
		}))
			.sort(sortByType),
	};

	const content = Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	new Dialog({
		title: lang.title.replace('{name}', target.token?.name ?? target.name),
		content,
		buttons: {
			sunder: {
				label: lang.apply,
				icon: '<i class="fas fa-meteor"></i>',
				callback: doSunder,
			},
		},
		default: 'sunder'
	},
	{
		jQuery: false,
		resizable: true,
	})
		.render(true);
}
