/**
 * Apply Spell Focus, Greater Spell Focus, and Domain status
 * ... to spells within actor's spellbook for spells of specific school.
 *
 * Compatibility:
 * - Foundry v10
 * - PF1 0.82.2
 */

const lang = {
	sf: 'Spell Focus',
	gsf: 'Greater Spell Focus',
};

/**
 * @param {Actor} actor
 * @param {Object} options
 * @param {String} book Book identifier
 * @param {String} school School identifier (evo, con, ...)
 * @param {Boolean} sf Spell Focus
 * @param {Boolean} gsf Greater Spell Focus
 * @param {Boolean} domain Domain spell
 */
function modifySpells(actor, { book = null, school = null, gsf = false, sf = false, domain = false } = {}) {
	const actorData = actor.system;
	const books = actorData.attributes.spells.spellbooks;

	// Minor sanity check
	if (!Object.keys(CONFIG.PF1.spellSchools).includes(school))
		return void ui.notifications?.warn(`Invalid school: ${school || 'undefined'}`);

	if (books[book] === undefined)
		return void ui.notifications?.warn(`Invalid book ID: ${book || 'undefined'}`);

	// Get spells with right school and are in right spellbook
	const spells = actor.items
		.filter(i => i.type === 'spell' && i.system.school === school && i.system.spellbook === book);

	if (spells.length === 0)
		return void ui.notifications?.warn(`No spells found for school: ${school}`);

	const updates = [];

	for (const spell of spells) {
		const spellData = spell.toObject().system;
		const update = { system: {} };
		const actions = spellData.actions ?? [];
		if ((sf || gsf) && actions.length > 0) {
			let actionsUpdated = false;

			for (const action of actions) {
				const save = action.save;
				if (!save.type) continue;

				const oldDc = action.save.dc.trim()
					.replace(/^0$/, ''); // Remove pointless 0

				const dc = [];
				if (sf && !save.dc.includes(`1[${lang.sf}]`))
					dc.push(`1[${lang.sf}]`);
				if (gsf && !save.dc.includes(`1[${lang.gsf}]`))
					dc.push(`1[${lang.gsf}]`);

				if (dc.length > 0) {
					if (oldDc.length > 0) dc.unshift(oldDc); // Add old DC back if it has something
					save.dc = dc.join(' + ');
					actionsUpdated = true;
				}
			}

			if (actionsUpdated)
				update.system.actions = actions;
		}

		if (domain && spellData.domain !== true)
			update.system.domain = true;

		if (!foundry.utils.isEmpty(update.system)) {
			update._id = spell.id;
			updates.push(update);
		}
	}

	return actor.updateEmbeddedDocuments('Item', updates)
		.then(_ => void ui.notifications?.info(`${updates.length} spell(s) updated`));
}

const dialogTemplate = `
<form autocomplete="off" class="flexcol">
	<div class="form-group">
		<label>Actor:</label>
		<span>{{actor.name}}<span>
	</div>

	<div class="form-group">
		<label>Book:</label>
		<select name="book" data-dtype="String">
		{{selectOptions books localize=true nameAttr='id' labelAttr='label'}}
		</select>
	</div>

	<div class="form-group">
		<label>School:</label>
		<select name="school" data-dtype="String">
		<option value=''></option>
		{{selectOptions schools}}
		</select>
	</div>

	{{! Spell Focus }}
	<div class="form-group">
		<input id="apply-spellfocus-sf" type="checkbox" data-dtype="Boolean" name="sf">
		<label for="apply-spellfocus-sf">+1 Spell Focus</label>
	</div>

	{{! Greater Spell Focus }}
	<div class="form-group">
		<input id="apply-spellfocus-gsf" type="checkbox" data-dtype="Boolean" name="gsf">
		<label for="apply-spellfocus-gsf">+1 Greater Spell Focus</label>
	</div>

	{{! Spell School }}
	<div class="form-group">
		<input id="apply-spellfocus-domain" type="checkbox" data-dtype="Boolean" name="domain">
		<label for="apply-spellfocus-domain">Domain/School</label>
	</div>

	<hr>
</form>
`;

function generateDialog(actor) {
	const actorData = actor.system,
		books = actorData.attributes.spells.spellbooks;

	const bookOptions = Object.entries(books).reduce((all, [bookId, book]) => {
		if (book.inUse) all.push({ id: bookId, label: game.i18n.localize(book.label) + ` [${bookId}]` });
		return all;
	}, []);

	const templateData = {
		actor,
		books: bookOptions,
		schools: CONFIG.PF1.spellSchools,
	};

	new Dialog({
		title: 'Spell Focus / Domain',
		content: Handlebars.compile(dialogTemplate)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		buttons: {
			apply: {
				label: 'Apply',
				// icon: '',
				callback: (html) => modifySpells(actor, new FormDataExtended(html.querySelector('form')).object)
			}
		},
		default: 'apply',
	}, {
		width: '22rem',
		jQuery: false,
	})
		.render(true);
}

if (actor) generateDialog(actor);
else ui.notifications?.warn('No valid actor found.');
