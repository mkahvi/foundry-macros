## Pathfinder 1e Rules

Macros specifically meant to implement certain rules for PF1e.

These are not expected to work with any other game system without major overhauls.
