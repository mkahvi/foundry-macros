/**
 * Making Craft Work, originally by Spes Magna Games
 * This implements the alternative crafting rules.
 *
 * Install: Script type macro.
 * Usage: Just use it as is, from quickbar or something.
 *
 * TODO: Drag & drop item in the dialog and inherit data for the craft check (problematic as items don't have appropriate data built-in)
 * TODO: Retry icon needs to be placed somewhere else.
 * TODO: Print actor selection dialog if no token is selected.
 * TODO: Print more info on what success/failure does exactly (time modifiers and such)
 *
 * Compatibility:
 * - Foundry VTT 11
 * - Pathfinder v9.6
 */

// -- Basic options
const opts = {
	workHours: false, // display work time in hours instead of days
	workHoursInDay: 8, // hours available for work. Only  used if workHours is enabled
	materialsInSilver: false, // display material costs in silver instead of gold
	defaultItem: { // deault item values
		value: 0, // GP
		adjustDC: 0, // DC modifier
		complexity: 2, // index number to complexity
		material: 0, // index number to materials
		tool: 1, // index number to tool
		masterwork: false,
	},

	showSkills: ['crf', 'pro', 'art', 'lor', 'spl'], // Shows these skills. Empty to show all. pf1.config.skills contains full list of skill keys.
	showSkillGroups: true, // Group subskills together
	showSubskillParent: true, // Allow rolling subskill parent (e.g. plain Craft)

	// NOT IMPLEMENTED
	// Formula for adjusting final DC
	// includes references to @tool, @skill, @material, @complexity, @masterwork, @value, @cost, @time,
	shifters: [
		{ target: 't', formula: '', enabled: false },
		{ target: 'dc', formula: '', enabled: false },
	],

	craftingCost: '@value / 3', // Roll formula compatible string for calculating craft cost. @value is available for referring to base cost.

	allowRetry: true, // If false, retry functionality is not hooked into chat messages
	waitMaxMS: 2000, // max wait time (in ms) to find the chat message
	waitIncrement: 200, // ms delay between searches
	debug: false, // If true, noisy console logging is enabled
}

/**
 * Hacking breadcrumbs
 *
 * Data:
 *   {@link opts} options, configure normally
 *   {@link lang} translations
 *   {@link complexity} Crafting complexity
 *   {@link materials} Crafting materials
 *   {@link tools} Crafting tools
 *   {@link successCheck} Checks and modifications to result for successes
 *   {@link failCheck} Checks and modifications to result for failures
 *
 * Objects:
 *   {@link Craftable} Class for defining the crafting item
 *   {@link CraftDialog} Custom dialog for crafting
 *   {@link Skill} Describes skill roll relevant data
 *   {@link Component} Describes generic craft component (material, complexity, tool).
 *
 *  {@link mwkComponent} = Component instance describing masterwork quality effects
 *  {@link baseComponent} = Component instance describing base modifiers.
 *
 * Functions:
 *   {@link craftChatCard} Creates the chat message
 *   {@link generateDialogHTML} Creates dialog HTML
 *   {@link showDialog} Show dialog with supplied info
 *   {@link tryDialog} = basic entry to dialog display
 *   {@link getSkillSelection} Responsible for constructing available skill set.
 *
 * Sections:
 *   Helpers = small functions to make things easier
 */

const materialUnit = opts.materialsInSilver ? 'sp' : 'gp';
const timeUnit = opts.workHours ? 'hours' : 'days';

// turn default item options into numbers if strings are given
// const stringMatch = (a, b) => a.localeCompare(b, undefined, { sensitivity: 'accent' }) === 0;
// const componentFind = (set, callback) => Object.values(set).find(callback);
// if (typeof opts.defaultItem.material === 'string')
// opts.defaultItem.material = Object.values(materials).find(m => stringMatch(m.name, opts.defaultItem.material))?.key ?? 0;

const lang = {
	unnamed: 'Undefined', // Name of item if not defined

	crafting: 'Crafting',
	craftingWith: 'Crafting with {actor}', // crafting with actor
	itemName: 'Item name',
	optional: 'Optional',
	tools: 'Tools',
	toolsHint: 'Tools provide competence modifier to a check.',
	complexity: 'Complexity',
	complexityDC: ' – DC modifier',
	complexityTime: ' – Base time ({tu})'.replace('{tu}', timeUnit),
	masterworked: 'Masterworked',
	masterworkDetailed: 'Masterwork ({dc} DC, {time} time)', // +X DC, +X% time
	material: 'Material',
	materialDC: ' – DC modifier',
	materialTime: ' – Time modifier',
	valueGP: 'Base value (gp)',
	craftCost: 'Materials ({mu})'.replace('{mu}', materialUnit),
	craftCostHint: 'Actual crafting cost.',
	adjustDC: 'DC adjust',
	adjustDCHint: 'DC adjustment as dictated by GM',
	finalDC: 'Final DC',
	finalTimeMod: 'Final time modifier',
	finalWorkTime: 'Work time ({tu})'.replace('{tu}', timeUnit),

	craftButton: 'Craft with selected actor',

	craftSkill: 'Craft skill',
	craftTimeBase: 'Base time ({tu})'.replace('{tu}', timeUnit),
	craftTimeMod: ' – Time modifier',
	craftCheck: 'Check result',
	outcome: 'Outcome',

	skillFormat: '{skill}',
	subSkillFormat: '{group}: {skill}',

	rollType: 'Roll type',

	lostMaterial: 'Lost material ({mu})'.replace('{mu}', materialUnit),

	unknownOutcome: 'Meh.',

	retry: 'Retry',
	yes: 'Yes',
	no: 'No',

	DC: 'DC',
	time: 'Time'
}

// UNUSED
const icon = {
	value: 'icons/commodities/currency/coins-plain-pouch-gold.webp',
	cost: 'icons/commodities/currency/coins-assorted-mix-silver.webp',
	complexity: 'icons/commodities/tech/blueprint.webp',
	material: 'icons/commodities/materials/glass-cube.webp',
	masterwork: 'icons/commodities/treasure/trinket-plane-gold.webp',
	dc: 'icons/tools/smithing/crucible.webp',
	time: 'icons/commodities/tech/watch.webp',
	gm: 'icons/commodities/biological/eye-blue-gold.webp',
}

const dialogWidth = 350, dialogHeight = 'auto';

class Component {
	/** @type {number} */
	index;
	/** @type {string} */
	label;
	/** @type {number} */
	modifier = 0;
	/** @type {number} */
	time = 0;
	/** @type {number} */
	timeMod = 0;
	/** @type {string} */
	description = '';
	/** @type {string} */
	examples = '';
	/** @type {boolean} */
	special = true;

	/** @type {boolean} */
	enabled = true;

	/**
	 * @param {number} index To holding array.
	 * @param {string} label Short label.
	 * @param {number} modifier DC modifier;
	 * @param {number} time Work time flat bonus.
	 * @param {number} timeMod Work time percentage modifier.
	 * @param {string} description Long description.
	 * @param {string} examples Relevant examples.
	 * @param {boolean} special Special component. If false, the compoonent may be hidden by parts of the code.
	 */
	constructor(index, label, modifier = 0, time = 0, timeMod = 0, description = '', examples = '', special = true) {
		this.index = index;
		this.label = label;
		this.modifier = modifier;
		this.time = time;
		this.timeMod = timeMod;
		this.description = description;
		this.examples = examples;
		this.special = special;
	}

	get timePct() {
		return this.time * 100;
	}

	get timeModPct() {
		return this.timeMod * 100;
	}

	get signedMod() {
		return signNum(this.modifier);
	}

	get timeLabel() {
		return maybeItsHours(this.time);
	}

	clone(data = {}) {
		const c = new this.constructor(this.index, this.label, this.modifier, this.time, this.timeMod, this.description, this.examples, this.special);
		return foundry.utils.mergeObject(c, data);
	}
}

// -- Basic components
const baseComponent = new Component(0, 'Base DC', 10); // 10 DC
const mwkComponent = new Component(0, 'Masterworked', 4, 0, 0.5); // +4 DC & +50% time

// Complexity options
const complexity = {
	0: new Component(0, 'Very simple', 0, 0.5, 0, 'These items are more or less all one piece or one material of simple shape with no moving parts.', 'Crowbar, quarterstaff', false),
	1: new Component(1, 'Simple', 2, 1, 0, 'A simple item is largely made of one material, but it requires a more specialized shape.', 'Many simple weapons, backpack, most common articles of clothing, simple traps such as pits.'),
	2: new Component(2, 'Moderate', 4, 2, 0, 'Moderate complexity items are characterized by diverse materials or different parts that must be integrated into a whole.', 'Most martial and exotic weapons, bows, all shields, locks, simple traps using simple mechanical triggers, acid.'),
	3: new Component(3, 'Complex', 8, 4, 0, 'Complex items have diverse materials, moving parts, different parts, and/or decorative bits.', 'Most types of armor, strength bows, crossbows, most vehicles (excluding large ocean-going vessels), alchemist’s fire, smokesticks, tingertwigs.'),
	4: new Component(4, 'Very complex', 10, 7, 0, 'These are the most complicated items. They require diverse materials, moving parts, different parts, decorated bits, and/or multiple functions or uses.', 'Ocean-going vessels, unusual armors (such as barding), antitoxins, tanglefoot bags, sunrods, thunderstones.'),
}

// Special materials add +50% time
const materials = {
	0: new Component(0, 'Normal', 0, 0, 0, '', '', false),
	1: new Component(1, 'Darkwood', 2, 0, 0.5),
	2: new Component(2, 'Cold Iron', 2, 0, 0.5),
	3: new Component(3, 'Alchemical Silver', 2, 0, 0.5),
	4: new Component(4, 'Mithral', 4, 0, 0.5),
	5: new Component(5, 'Dragonhide', 4, 0, 0.5),
	6: new Component(6, 'Adamantine', 6, 0, 0.5),
}

// Success & Failure checks

class Result {
	/**
	 * @param {number} index Key to holding container.
	 * @param {string} formula Roll() compatible formula to test if this result matches. Must return true/false.
	 * @param {string} timeEffect Roll() compatible formula to produce new work time.
	 * @param {string} lossEffect Roll() compatible formula to produce a material cost.
	 * @param {string} outcome Description of the result.
	 * @param {boolean} stop Stop processing after this result check.
	 */
	constructor(index, formula, timeEffect, lossEffect, outcome, stop) {
		this.index = index;
		this.formula = formula;
		this.timeEffect = timeEffect;
		this.lossEffect = lossEffect;
		this.outcome = outcome;
		this.stop = stop;
	}
}

const successCheck = {
	0: new Result(0, '@check >= @dc', '', '', 'Success!', false),
	1: new Result(1, '@check >= (@dc+5)', '@time / 2', '', 'It is exceptional!', false),
	2: new Result(2, '@check >= (@dc+10)', '@time / 2', '', 'Epic work!', false),
}

const failCheck = {
	0: new Result(0, '@check < @dc', '', '', 'Failure... time is lost.', false),
	1: new Result(1, '@check <= (@dc - 5)', '', '@cost / 2', 'Ruined! Part of the materials are lost.', false),
	2: new Result(2, '@check <= (@dc - 10)', '', '', 'Catastrophy! If you\'re working with alchemy, your lab explodes. You must replace your lab in addition to ruined materials. Additionally make DC 10 Reflex to avoid 1d6 fire damage.', false),
}

// ---------------------------------

/**
 * Circumstance bonus to check.
 * Probably should not tracked in the dialog.
 */
const tools = {
	0: new Component(0, 'Improvised', -2),
	1: new Component(1, 'Normal', 0, 0, 0, '', '', false),
	2: new Component(2, 'Masterwork', 2)
}

// Helpers
const getControlledActors = () => canvas.tokens.controlled.map(t => t.actor).filter(a => !!a).filter(t => t.isOwner);

const signNum = (num) => `${(num >= 0 ? '+' : '') + num}`; // add + to positive numbers
const cleanNum = (value) => isNaN(value) ? 0 : value;
const filterNum = (value) => cleanNum(parseFloat(value));
const enrichRoll = (roll, formula, label) => `<a class="inline-roll inline-dsn-hidden inline-result" title="${formula}" data-roll="${escape(JSON.stringify(roll))}"><i class="fas fa-dice-d20"></i> ${label}</a>`;
const fasIcon = (classData) => $('<i />').addClass(classData);
const maybeItsSilver = (gold) => opts.materialsInSilver ? gold * 10 : gold;
const maybeItsHours = (time) => opts.workHours ? time * opts.workHoursInDay : time;
const craftRollData = (item, roll) => ({ dc: item.final.modifier, check: roll.total, time: item.workTimeActual, cost: item.craftCost });

// ---------------------------------

// All the info to make an item
class Craftable {
	/** @type {string} - Item name */
	name = '';
	/** @type {number} - Purchase value, in GP */
	value = opts.defaultItem.value;
	/** @type {number} - Material cost to craft, in GP */
	craftCost = 0;
	rollType = 0;

	// Components
	/** @type {Component} */
	base = baseComponent.clone();
	/** @type {Component} */
	tool = tools[opts.defaultItem.tool].clone();
	/** @type {Skill|Component} */
	skill = new Component(0, 'No Skill', 0);
	/** @type {Skill[][]} */
	skills = [];
	/** @type {Component} */
	complexity = complexity[opts.defaultItem.complexity].clone();
	/** @type {Component} */
	material = materials[opts.defaultItem.material].clone();
	/** @type {Component|null} */
	masterwork = mwkComponent.clone({ enabled: opts.defaultItem.masterwork });
	/** @type {Component} */
	adjust = new Component(0, 'DC adjust', opts.defaultItem.adjustDC);

	finalTimeMod = 0;
	workTime = 0;

	final = new Component(0, 'Final', baseComponent.modifier, baseComponent.time, baseComponent.timeMod);

	// Craft result stuff
	loss = 0;
	workTimeActual = 0;

	constructor() {
		// Components
		this.skill.fullName = 'n/a';
	}

	get finalTimePct() {
		return Math.floor(this.finalTimeMod * 1_000) / 10;
	}

	get workTimeActualLabel() {
		return maybeItsHours(this.workTimeActual);
	}

	get lossLabel() {
		const loss = Math.floor(this.loss * 100) / 100; // round to precision of 2 decimals.
		return maybeItsSilver(loss);
	}

	updateFinal() {
		this.final.modifier = 0;
		this.final.time = 0;
		this.final.timeMod = 0;

		if (opts.debug) console.log('COMPONENTs: ', this.components);

		for (const c of this.components) {
			if (!c.enabled) continue;
			this.final.modifier += c.modifier;
			this.final.time += c.time;
			this.final.timeMod += c.timeMod;
		}

		this.workTime = (1 + this.final.timeMod) * this.final.time;

		this.rollNeed = this.final.modifier - (this.skill.modifier + this.tool.modifier);
		this.chance = Math.clamped(21 - this.rollNeed, 0, 20) * 5;

		if (opts.debug) console.log('FINAL: ', this.final, 'WorkTime: ', this.workTime);
	}

	/** @type {Component[]} */
	get components() {
		return [this.base, this.complexity, this.material, this.masterwork, this.adjust].filter(o => o.enabled);
	}

	updateCraftCost() {
		const mcost = Roll.defaultImplementation.safeRoll(opts.craftingCost, { value: this.value }).total ?? 0;

		this.craftCost = Math.floor(mcost * 100) / 100; // Round to nearest copper
	}

	get craftCostLabel() {
		return maybeItsSilver(this.craftCost);
	}
}

// ---------------------------------

class Skill {
	/** @type {number} */
	index;
	/** @type {string} */
	name;
	/** @type {string} */
	key;
	/** @type {number} */
	modifier;
	/** @type {string} */
	path;

	/**
	 * @param {number} index - Index offset
	 * @param {object} info - actor.getSkillInfo() result
	 * @param {string} key - Skill key, as accepted by actor.rollSkill()
	 * @param {string} path - Full data path to skill within ActorPF scope (system.skills.~).
	 * @param {object} parent - Parent skill
	 */
	constructor(index, info, key, path, parent) {
		this.index = index;
		this.name = info.name;
		this.key = key;
		this.modifier = info.mod;
		this.path = path;
		this.rank = info.rank;
		this.parent = parent;
	}

	get fullName() {
		if (this.parent)
			return lang.subSkillFormat.replace('{group}', this.parent.name).replace('{skill}', this.name).replace('{mod}', signNum(this.modifier));
		return lang.skillFormat.replace('{skill}', this.name).replace('{mod}', signNum(this.modifier));
	}

	get trained() {
		return this.rank > 0;
	}

	clone(data = {}) {
		const si = { name: this.name, rank: this.rank, mod: this.modifier };
		const s = new this.constructor(this.index, si, this.key, this.path, this.parent);
		return foundry.utils.mergeObject(s, data);
	}
}

/**
 * @param {Actor} actor
 * @returns {Skill[][]} - Array of Skill arrays
 */
function getSkillSelection(actor) {
	const skillSet = [];
	let idx = 0;

	const skills = actor.system.skills;

	const askills = opts.showSkills.length === 0 ? Object.keys(skills) : [...opts.showSkills];

	const getSubSkills = (subSkills, parentId, parent) => Object
		.keys(subSkills)
		.map(key => {
			const skillId = `${parentId}.subSkills.${key}`;
			const si = actor.getSkillInfo(skillId);

			return new Skill(idx++, si, `${parentId}.${key}`, `system.skills.${parentId}.subSkills.${key}`, parent);
		});

	askills.forEach(skillId => {
		if (!skills[skillId]) return;
		const si = actor.getSkillInfo(skillId);
		if ('subSkills' in skills[skillId]) {
			const parent = [];
			if (opts.showSubskillParent)
				parent.push(new Skill(idx++, si, skillId, `system.skills.${skillId}`));
			const subs = getSubSkills(skills[skillId].subSkills, skillId, si);
			const totalsubs = [...parent, ...subs];
			totalsubs._label = si.name;
			if (totalsubs.length > 0)
				skillSet.push(totalsubs);
		}
		else {
			skillSet.push([new Skill(idx++, si, skillId, `system.skills.${skillId}`)]);
		}
	});

	return skillSet;
}

// ---------------------------------

class RollType {
	/** @type {number} */
	index;
	/** @type {string} */
	die;
	/** @type {string} */
	label;
	/** @type {number} */
	forceResult;

	/**
	 * @param {number} index - Index offset
	 * @param {string} die - Die formula.
	 * @param {string} label - Display name.
	 * @param {number} forceResult - If greater than 0, the die roll is spoofed into this.
	 */
	constructor(index, die, label, forceResult = 0) {
		this.index = index;
		this.die = die;
		this.label = label;
		this.forceResult = forceResult;
	}

	get rollLabel() {
		if (this.forceResult > 0) return `${this.die}=${this.forceResult}`;
		return this.die;
	}
}

const rollTypes = {
	0: new RollType(0, '1d20', 'Normal'),
	1: new RollType(1, '1d20', 'Take 10', 10),
	2: new RollType(2, '1d20', 'Take 20', 20),
	3: new RollType(3, '2d20kh1', 'Fortune'), // a.k.a. Advantage
	4: new RollType(4, '2d20kl1', 'Misfortune'), // a.k.a. Disadvantage
}

// ---------------------------------

// Crafting; Generate chat message and roll

function retryCraft(jqe) {
	const actorId = jqe.target.dataset.actorId,
		actor = !jqe.shiftKey ? game.actors.get(actorId) : getControlledActors()[0],
		item = foundry.utils.mergeObject(new Craftable, JSON.parse(unescape(jqe.target.dataset.itemData)));

	if (actor && item)
		CraftDialog.open(actor, item);
	else {
		if (opts.debug) console.log('CRAFTING | Invalid retry data: ', actor, item);
		ui.notifications.warn('Invalid actor or item data received for crafting.');
	}
}

/**
 * @param {Actor} actor
 * @param {Craftable} item
 */
async function craftChatCard(actor, item) {
	item.name = item.name.trim() || lang.unnamed;

	if (opts.debug) console.log('CRAFTING | Generating chat card: ', actor, item);

	// TODO: rollSkill() should work nowadays, use it.
	// actor.rollSkill(item.skills[item.skill].skill, { noSound: true });
	//  I don't trust .mod being complete, but there's nothing better besides replicating actor.rollSkill

	const rollType = rollTypes[item.rollType];

	const roll = Roll.defaultImplementation.safeRoll('@die + @mod + @tool', { die: rollType.die, mod: item.skill.modifier, tool: item.tool.modifier });

	// Spoof rolls
	if (rollType.forceResult) {
		let btotal = 0;
		for (const term of roll.terms) {
			if (term instanceof Die) {
				for (const result of term.results) {
					if (!result.active) continue;
					btotal += rollType.forceResult - result.result;
					result.result = rollType.forceResult;
				}
			}
		}
		roll._total += btotal;
	}

	item.workTimeActual = item.workTime;
	item.loss = 0;
	item.outcome = [];

	let successes = 0,
		failures = 0;

	// Check success conditions
	for (const i of Object.keys(successCheck).sort()) {
		const check = successCheck[i];
		const rollData = craftRollData(item, roll);

		const successRoll = Roll.defaultImplementation.safeRoll(check.formula, rollData);
		const success = successRoll.total; // true/false
		if (success) {
			successes++;
			if (check.timeEffect?.length > 0) {
				rollData.time = item.workTimeActual; // make sure it is updated
				item.workTimeActual = Roll.defaultImplementation.safeRoll(check.timeEffect, rollData).total;
			}
			item.outcome.push(check.outcome);
			if (check.stop) break;
		}
	}

	// Check failure conditions
	for (const i of Object.keys(failCheck).sort()) {
		const check = failCheck[i];
		const rollData = craftRollData(item, roll);

		const failRoll = Roll.defaultImplementation.safeRoll(check.formula, rollData);
		const fail = failRoll.total;
		if (fail) {
			failures++;
			if (check.lossEffect?.length > 0) {
				const lossRoll = Roll.defaultImplementation.safeRoll(check.lossEffect, rollData);
				item.loss += lossRoll.total;
			}
			item.outcome.push(check.outcome);
			if (check.stop) break;
		}
	}

	if (item.outcome.length === 0) item.outcome.push(lang.unknownOutcome);

	// CONTENT

	const template = `
<div class="craft-chat-card" style="line-height: 1.5;">
	{{! Name }}
	<div class="flexrow name" style="background: rgba(0, 0, 0, 0.15);">
		<label>{{i18n.itemName}}</label>
		<label>
			{{item.name}}
			<i class="craft-retry-icon"></i>
		</label>
	</div>

	{{! Base Cost }}
	<div class="flexrow base-cost" {{#unless item.value}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.valueGP}}</label>
		<label>{{item.value}} <i class="fas fa-coins"></i></label>
	</div>

	{{! Crafting Cost }}
	<div class="flexrow craft-cost" {{#unless item.craftCost}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.craftCost}}</label>
		<label>{{item.craftCostLabel}} <i class="fas fa-coins"></i></label>
	</div>

	{{! Skill }}
	<div class="flexrow skill">
		<label>{{i18n.craftSkill}}</label>
		<label>{{item.skill.fullName}}</label>
	</div>

	{{! Tools }}
	<div class="flexrow tools" {{#unless item.tool.special}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.tools}}</label>
		<label>{{item.tool.label}}</label>
	</div>

	{{! Complexity }}
	<div class="flexrow complexity" {{#unless item.complexity.special}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.complexity}}</label>
		<label>{{item.complexity.label}}</label>
	</div>

	{{! Material }}
	<div class="flexrow material" {{#unless item.material.special}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.material}}</label>
		<label>{{item.material.label}}</label>
	</div>

	{{! Masterwork }}
	<div class="flexrow masterwork" {{#unless item.masterwork}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.masterworked}}</label>
		<label>
		{{#if item.masterwork.enabled}}
			{{i18n.yes}} <i class="far fa-check-circle"></i>
		{{else}}
			{{i18n.no}} <i class="far fa-times-circle"></i>
		{{/if}}
		</label>
	</div>

	{{! DC Adjust }}
	<div class="flexrow dc-adjust">
		<label>{{i18n.adjustDC}}</label>
		<label>{{item.adjust.modifier}}</label>
	</div>

	{{! Crafting Base Time }}
	<div class="flexrow craft-time">
		<label>{{i18n.craftTimeBase}}</label>
		<label>{{item.complexity.timeLabel}}</label>
	</div>

	{{! Final Time Modifier }}
	<div class="flexrow final-time-mode" {{#unless item.finalTimeMode}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.craftTimeMod}}</label>
		<label>+{{item.finalTimePct}}%</label>
	</div>

	{{! Roll Type }}
	<div class="flexrow roll-type" {{#unless item.rollType}}style="opacity:0.6"{{/unless}}>
		<label>{{i18n.rollType}}</label>
		<label>
			{{labels.rollType}}
			<i class="fas fa-dice-d20"></i>
		</label>
	</div>

	{{! Craft Check }}
	<div class="flexrow craft-check" style="font-weight: bold;">
		<label>{{i18n.craftCheck}}</label>
		<label>
			{{{enriched.check}}}
			vs <b>DC {{item.final.modifier}}</b>
			{{#if success}}
			<i class="fas fa-tools"></i>
			{{else}}
			<i class="far fa-trash-alt"></i>
			{{/if}}
		</label>
	</div>

	{{! Outcome }}
	<div class="flexrow outcome" style="background: rgba(0, 0, 0, 0.15); font-weight:bold;">
		<label>{{i18n.outcome}}</label>
	</div>
	<div class="flexcol outcomes" style="padding-left:1em;">
		{{#each item.outcome}}
		<div>{{this}}</div>
		{{/each}}
	</div>

	{{! Lost Materials }}
	{{#if item.loss}}
	<div class="flexrow lost-materials">
		<label>{{i18n.lostMaterial}}</label>
		<label>
			{{item.lossLabel}}
			<i class="fas fa-coins"></i>
		</label>
	</div>
	{{/if}}

	<div class="flexrow total-time">
		<label>{{i18n.finalWorkTime}}</label>
		<label>
			{{item.workTimeActualLabel}}
			<i class="far fa-clock"></i>
		</label>
	</div>
</div>
		`;

	const templateData = {
		i18n: lang,
		item,
		enriched: {
			check: enrichRoll(roll, roll.formula, roll.total),
		},
		labels: {
			rollType: rollType.label
		},
		success: successes > 0,
	};

	const content = Handlebars.compile(template)(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	// Retry icon
	// fasIcon('fas fa-sync-alt').addClass('craft-retry-icon').css(retryIcon).css(bigGlow).attr({ name: 'craft-retry-icon' }))

	// ---------------------------------------

	// Button not created here since it can't be reliably linked to ephemeral macro.

	const msgData = {
		flavor: lang.crafting,
		content,
		type: CONST.CHAT_MESSAGE_TYPES.ROLL,
		rolls: [roll],
		sound: CONFIG.sounds.dice,
		rollMode: game.settings.get('core', 'rollMode'),
		speaker: ChatMessage.getSpeaker({ actor }),
		flags: { world: { craftingMCW: true } }
	};

	let flukes = 0;
	let hookId;
	/**
	 *
	 * @param {ChatMessage} cm
	 * @param {JQuery<HTMLElement>} html
	 */
	function addRetryButton(cm, [html]) {
		const mcw = cm.getFlag('world', 'craftingMCW');
		flukes++;
		if (mcw || flukes > 25) Hooks.off('renderChatMessage', hookId);
		if (!mcw || flukes > 25) return;

		const retry = html.querySelector('.message-content i.craft-retry-icon');
		retry.classList.add('fas', 'fa-sync-alt');
		retry.dataset.actorId = actor.id;
		retry.dataset.itemData = escape(JSON.stringify(item));
		retry.addEventListener('click', retryCraft);
	}

	if (opts.allowRetry) hookId = Hooks.on('renderChatMessage', addRetryButton);

	const msg = await ChatMessage.implementation.create(msgData);

	if (opts.debug) console.log('CRAFTING | RESULT: ', actor, item, msg);

	if (hookId) setTimeout(() => Hooks.off('renderChatMessage', hookId), opts.waitMaxMS);
}

// Crafting dialog
class CraftDialog extends FormApplication {
	/** @type {Actor} */
	actor;
	/** @type {Craftable} */
	item;

	/**
	 * @param {object} object
	 * @param {Craftable} object.item
	 * @param {Actor} object.actor
	 * @param appOptions
	 */
	constructor(object, appOptions) {
		super(object, appOptions);

		this.actor = object.actor;
		this.item = object.item ?? new Craftable;

		this.item.skills = getSkillSelection(this.actor);
		this.item.skill = this.item.skills.flat().sort((a, b) => b.modifier - a.modifier)[0];

		this.item.updateCraftCost();
		this.item.updateFinal();
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: lang.craftingWith.replace('{actor}', actor.name),
			classes: [...options.classes, 'craft-mcw'],
			width: dialogWidth,
			height: dialogHeight,
			submitOnClose: false,
			closeOnSubmit: false,
			submitOnChange: true,
			resizable: true,
		}
	}

	static open(actor, item, options) {
		return new Promise(resolve => {
			const app = new this({ actor, item }, options);
			app.resolve = resolve;
			app.render(true, { focus: true });
		});
	}

	async getData() {
		const i18n = foundry.utils.deepClone(lang);

		const actor = this.actor,
			item = this.item;

		const labels = {
			masterwork: lang.masterworkDetailed.replace('{dc}', signNum(mwkComponent.modifier)).replace('{time}', `${signNum(mwkComponent.timeMod * 100)}%`),
		};

		const context = {
			i18n,
			labels,
			item,
			craftCost: maybeItsSilver(item.craftCost),
			skills: item.skills,
			rollTypes,
			tools,
			complexity,
			materials,
			baseComponent,
		};

		return context;
	}

	async _renderInner(data) {
		const content = await generateDialogHTML(this.item, data);
		const html = $(content);
		this.form = html[0];
		return html;
	}

	close(...args) {
		super.close(...args);
		this.resolve?.(null);
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		this.form.querySelector('button.print').addEventListener('click', this._onCraft.bind(this));
	}

	async _onCraft(event) {
		event.preventDefault();

		await this._onSubmit(event, { preventRender: true });

		craftChatCard(this.actor, this.item);

		this.close({ force: true });
	}

	_updateObject(event, formData) {
		const { item } = foundry.utils.expandObject(formData);

		const old = this.item;

		if (item.complexity.index !== old.complexity.index)
			old.complexity = complexity[item.complexity.index].clone();

		if (item.material.index !== old.material.index)
			old.material = materials[item.material.index].clone();

		if (item.tool.index !== old.tool.index)
			old.tool = tools[item.tool.index].clone();

		if (item.skill.index !== old.skill.index)
			old.skill = old.skills.flat().find(s => s.index === item.skill.index).clone();

		delete item.skill;
		delete item.tool;
		delete item.material;
		delete item.complexity;

		foundry.utils.mergeObject(this.item, item);

		old.updateCraftCost();
		old.updateFinal();

		this.render();
	}
}

// Construct UI

/**
 * @param {Craftable} item
 * @param {object} data
 * @returns
 */
async function generateDialogHTML(item, data) {
	if (opts.debug) console.log('CRAFTING | generateDialogHTML.item: ', item);

	const content = `
<form autocomplete='off'>
	<style>
	.app.craft-mcw form { display: flex; flex-flow: column nowrap; gap: 1px; }
	.app.craft-mcw form label { flex: 1 0.8 7em; }
	.app.craft-mcw form input { text-align: center; height: 22px; margin-top: 2px; text-shadow: rgba(250, 250, 255, 0.8) 0px 0px 1px; flex: 0.5 3 3em; }
	.app.craft-mcw form input[name="item.name"] { flex: 1 1 8em; }
	.app.craft-mcw form input:read-only { background: rgba(0, 0, 0, 0.1); color: var(--color-text-dark-primary); border: 1px solid var(--color-border-light-secondary); }
	.app.craft-mcw form .flexrow { align-items: center; }
	</style>
	<h3>Base Details</h3>
	<div class="flexrow">
		<label>{{i18n.itemName}}</label>
		<input type="text" name="item.name" placeholder="{{i18n.optional}}" value="{{item.name}}">
	</div>
	<div class="flexrow">
		<label>{{i18n.valueGP}}</label>
		<input type="number" name="item.value" value="{{item.value}}" min="0">
	</div>
	<div class="flexrow">
		<label>{{i18n.craftCost}}</label>
		<input type="number" data-name="craftCost" placeholder="0" value="{{craftCost}}" data-tooltip="{{i18n.craftCostHint}}" readonly>
	</div>
	<h3>Check</h3>
	<div class="flexrow">
		<label>{{i18n.craftSkill}}</label>
		<select name="item.skill.index" data-dtype="Number">
			{{#select item.skill.index}}
			{{#each skills as |skillSet id|}}
			{{#if (gt skillSet.length 1)}}
			<optgroup label="{{_label}}">
			{{/if}}
			{{#each skillSet}}
				<option value="{{index}}" data-skill="{{key}}">
					{{name}} [{{modifier}}]
				</option>
			{{/each}}
			{{#if (gt skillSet.length 1)}}
			</optgroup>
			{{/if}}
			{{/each}}
			{{/select}}
		</select>
	</div>
	<div class="flexrow">
		<label>{{i18n.rollType}}</label>
		<select name="item.rollType" data-dtype="Number">
			{{#select item.rollType}}
			{{#each rollTypes}}
			<option value="{{@key}}" data-roll-die="{{die}}" data-spoof="{{forceResult}}">
				{{label}} [{{rollLabel}}]
			</option>
			{{/each}}
			{{/select}}
		</select>
	</div>
	<div class="flexrow">
		<label>{{i18n.tools}}</label>
		<select name="item.tool.index" data-dtype="Number" data-tooltip="{{i18n.toolsHint}}">
			{{#select item.tool.index}}
			{{#each tools}}
			<option value="{{@key}}" data-craft-tool="{{@key}}" data-tool-modifier="{{modifier}}">
				{{label}} [{{modifier}}]
			</option>
			{{/each}}
			{{/select}}
		</select>
	</div>
	<h3>Project</h3>
	<div class="flexrow">
		<label>Complexity</label>
		<select name="item.complexity.index" data-dtype="Number">
			{{#select item.complexity.index}}
			{{#each complexity}}
			<option value="{{index}}" data-time="{{time}}" data-modifier="{{modifier}}">
				{{label}} [{{modifier}}]
			</option>
			{{/each}}
			{{/select}}
		</select>
	</div>
	<div class="flexrow">
		<label> – DC modifier</label>
		<input type="text" data-name="complexityDC" placeholder="0" value="{{item.complexity.modifier}}" readonly>
	</div>
	<div class="flexrow">
		<label> – Base time (days)</label>
		<input type="text" data-name="complexityTime" placeholder="0" value="{{item.complexity.time}}" readonly>
	</div>
	<div class="flexrow">
		<label>Material</label>
		<select name="item.material.index" data-dtype="Number">
			{{#select item.material.index}}
			{{#each materials}}
			<option value="{{@key}}" data-time="{{time}}" data-modifier="{{modifier}}">
				{{label}} [{{modifier}}]
			</option>
			{{/each}}
			{{/select}}
		</select>
	</div>
	<div class="flexrow">
		<label> – DC modifier</label>
		<input type="text" data-name="materialDC" placeholder="0" value="{{item.material.modifier}}" readonly>
	</div>
	<div class="flexrow">
		<label> – Time modifier</label>
		<input type="text" data-name="materialTime" placeholder="0" value="{{item.material.timeMod}}" readonly>
	</div>
	<div class="flexrow">
		<label>{{labels.masterwork}}</label>
		<input type="checkbox" name="item.masterwork.enabled" {{checked item.masterwork.enabled}}>
	</div>
	<div class="flexrow">
		<label>DC adjust</label>
		<input type="number" name="item.adjust.modifier" data-tooltip="DC adjustment as dictated by GM" value="{{item.adjust.modifier}}" placeholder="0">
	</div>
	<div class="flexrow bold">
		<label>Final DC</label>
		<input type="text" data-name="item.final.modifier" value="{{item.final.modifier}}" readonly>
	</div>
	<div class="flexrow">
		<label> – Chance</label>
		<input type="text" data-name="chance" placeholder="0" value="{{item.chance}}%" {{#if item.chance}}data-tooltip="Roll of {{item.rollNeed}}+"{{/if}} readonly>
	</div>
	<div class="flexrow">
		<label>Final time modifier</label>
		<input type="text" data-name="finalTimeMod" value="+{{item.final.timeModPct}}%" placeholder="+0%" readonly>
	</div>
	<div class="flexrow bold">
		<label>Work time (days)</label>
		<input type="number" data-name="workTime" value="{{item.workTime}}" readonly>
	</div>
	
	<hr>

	<div class="buttons">
		<button type="button" class="print" data-button="print">
			<i class="fas fa-hammer"></i>
			Craft with selected actor
		</button>
	</div>
</form>`;

	return Handlebars.compile(content)(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
}

function tryDialog() {
	const actors = getControlledActors();

	if (actors.length > 1)
		ui.notifications.warn('Too many tokens selected.');
	else if (actors.length == 0)
		ui.notifications.warn('No valid token selected');
	else
		CraftDialog.open(actors[0], new Craftable);
}

tryDialog();
