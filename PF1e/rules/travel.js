/**
 * Travel Calculator for planning extended trips, or just to see the travel time.
 *
 * TODO/PLANs:
 * - Decide if NOT hustling is better in case the removal of a break it causes gets you farther.
 *   - Forced march may be more useful
 * - Output to share with the rest of the players.
 * - Breaks during travel to heal damage (using available idle time) to mitigate damage.
 * - Late start on travel (lost available hours for single day).
 * - Travel interruption based on distance or time, including status of the party at the time of interruption.
 * - Last push option. Risk everything.
 *
 * NOTES:
 * - 24 hour clock is used.
 * - Default rest period of 11 hours assumes party size of 4 with reasonable watch cycling and full rest for all.
 *
 * Built-in camping times based on party size are based on info from PF2e: https://2e.aonprd.com/Rules.aspx?ID=534
 * The same logic works for PF1 so there was no reason to make something custom.
 *
 * COMPATIBILITY:
 * - Foundry VTT 0.7.x
 * - Pathfinder 0.77.24
 *
 * FVTT 0.8.x compatibility should be unaffected.
 * */

class TravelOptions {
	/**
	 * Delay healing based on these criteria to ensure maximal healing and as little time is spent on bad breaks.
	 */
	static healingThresholdFlat = 4; // minimum damage
	static healingThresholdPercentage = 0.15; // 15% damage at least

	static overlandUnit = 'mi'; // for display purposes only

	static defaultHustling = 3;
	static defaultForceMarch = 0;

	/**
	 * End rest at this hour of the day.
	 * 5 leads to +1h prep/setup
	 */
	static alignRestEnd = 5; // End rest at this hour of the day.
	/**
	 * Hide resting hours. Effectively same as setting resting to 0, except the healing still occurs.
	 */
	static displayRest = true;

	static displayRemainingDay = false; // Display remaining day info even if destination is reached

	static combineTravel = true; // combine consecutive travel segments // NOT IMPLEMENTED
	static combineTravelIdenticalOnly = true; // Combine only near-identical travel segments. // NOT IMPLEMENTED
	/**
	 * Combine camping segments (rest, prep, setup, idle,...).
	 */
	static combineCamping = true; // combine consecutive camping segments

	/**
	 * Reduce prep duration by setup duration.
	 */
	static prepMixedWithSetup = true; // NOT IMPLEMENTED

	/**
	 * Setup is divided on both ends of travel.
	 * Effectively means some characters handle setup while those that need prep do prep.
	 */
	static splitSetup = true; // split setup on both sides of travel

	/**
	 * Speed multiplier for hustled hours.
	 * Default: 2
	 */
	static hustleSpeed = 2;
	/**
	 * Environmental Damage.
	 * Default: True
	 *
	 * If True: travel damage is treated as environmental damage.
	 * 		This forces to use actual breaks during travel to mitigate the NL damage.
	 * If False: you can keep healing it despite accumulating more damage.
	 * 		This effectively grants free virtual break hour between every hour.
	 */
	static envDamage = true; // NOT IMPLEMENTED
	/**
	 * Hours in a day.
	 * Default: 24
	 */
	static dayHours = 24;
	/**
	 * Normal hours available for travel
	 * Default: 8
	 *
	 * Going past this number is force marching.
	 */
	static normalTravelHours = 8;
	/**
	 * Default travel speed.
	 */
	static normalDailyTravelSpeed = 24;

	static debug = false;
};

/**
 * Limits to not exceed for any reason.
 */
class SafetyOptions {
	static MaxTravel = 16;
	static MaxCamping = 20;

	/**
	 * Don't rest if damage is less than HD value.
	 */
	static NoRestUnderHD = true;
};

const signNum = (n) => n < 0 ? `${n}` : `+${n}`;

/**
 * @param {Number} hitDice
 * @param {Number} totalHitPoints
 * @param {Number} damageRemaining
 * @param {TravelInfo} travel
 * @returns {Boolean}
 */
const healOrNot = (hitDice, totalHitPoints, damageRemaining, travel) => {
	if (SafetyOptions.NoRestUnderHD && damageRemaining < hitDice) return false;
	if (damageRemaining < travel.acceptableDamage) return false;
	if (damageRemaining > TravelOptions.healingThresholdFlat) return true;
	if (damageRemaining > (totalHitPoints * TravelOptions.healingThresholdPercentage)) return true;
	return false;
}

class Time {
	days = 0;
	hours = 0;
	minutes = 0;
	seconds = 0;

	static get Day() { return 86400; };
	static get DayCs() { return 864000; };
	static get Hour() { return 3600; };
	static get HourCs() { return 36000; };
	static get Minute() { return 60; };
	static get MinuteCs() { return 600; };
	static get Cs() { return 10 };

	get totalCs() {
		return (this.days * Time.DayCs) +
			(this.hours * Time.HourCs) +
			(this.minutes * Time.MinuteCs) +
			(this.seconds * Time.Cs);
	}
	_totalCs = 0;

	constructor({ days = 0, hours = 0, minutes = 0 } = {}) {
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
	}
	get totalHours() { return this.hours + (this.minutes / 60) + (this.days * 24) + (this.seconds / 60 / 60); }
	get rounded() { return Math.ceil(this.totalHours); }
	toString() {
		if (this.days > 0)
			return `${this.days}:${clock(this.hours)}:${clock(this.minutes)}`;
		else
			return `${clock(this.hours)}:${clock(this.minutes)}`;
	}

	static fromCs(cs) {
		let t = new Time();
		let r = t._totalCs = cs;
		t.days = Math.floor(r / 864000);
		r = r % 864000;
		t.hours = Math.floor(r / 36000);
		r = r % 36000;
		t.minutes = Math.floor(r / 600);
		r = r % 600;
		t.seconds = Math.floor(r / 10);
		return t;
	}

	redistribute(hours = false) {
		Object.assign(this, Time.fromCs(this.totalCs));
		if (hours) {
			this.hours += this.days * 24;
			this.days = 0;
		}
	}

	/**
	 * @param {Time} time
	 */
	static copy(time) { return Object.assign(new Time(), duplicate(time)); }
}

class NightWatching {
	totalTime = 0;
	perWatch = 0;
	watchers = 0;

	constructor(watchers, total, perWatch) {
		this.watchers = watchers;
		this.totalTime = total;
		this.perWatch = perWatch;
	}
};

const nightWatches = {
	1: new NightWatching(1, new Time({ hours: 8 }), new Time({ hours: 8 })),
	2: new NightWatching(2, new Time({ hours: 16 }), new Time({ hours: 8 })),
	3: new NightWatching(3, new Time({ hours: 12 }), new Time({ hours: 4 })),
	4: new NightWatching(4, new Time({ hours: 10, minutes: 40 }), new Time({ hours: 2, minutes: 40 })),
	5: new NightWatching(5, new Time({ hours: 10 }), new Time({ hours: 2 })),
	6: new NightWatching(6, new Time({ hours: 9, minutes: 36 }), new Time({ hours: 1, minutes: 36 }))
	// More people don't affect minimum rest without causing fatigue.
};

const clampNum3 = (n) => Math.floor(n * 1000) / 1000;
const range = (r) => r > 0 ? (new Array(r)).fill().map((_, i) => i + 1) : [];
const clamp = (n, min, max) => Math.min(max, Math.max(min, n));
const clock = (n) => n < 100 ? ("00" + n).slice(-2) : n;

// > deals 1 point of nonlethal damage, and each additional hour deals twice the damage taken during the previous hour of hustling.
// 1, 2, 3, 4, 5,  6,  7,  8,   9,  10
const hustleDmg = [0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024];
const hustleHour = (h) => hustleDmg[h - 1];
const hustleCumulative = (h) => hustleDmg.slice(0, h).reduce((c, x) => x + c, 0);
const forceMarch = (hours) => {
	return { time: hours, formula: '(@fm) d6' };
};
const forceMarchCheckDC = (hour) => 10 + 2 * (hour - 1);

class TravelInfo {
	idle = 0;
	distance = 250;
	multiplier = 1;
	terrainMod = 1;
	/**
	 * Time available for any kind of traveling after all other activities are subtracted. Not counting forced march.
	 */
	availableTravelHours = 8; // base based on other info
	/**
	 * Total actual hours spent traveling. This includes normal travel hours and planned forced march.
	 */
	get dailyHoursPlan() { return this.availableTravelHours + this.forcedMarch; }
	/**
	 * Available travel hours + Hustle effect as planned.
	 */
	get dailyHoursEffectivePlan() { return this.dailyHoursPlan + this.hustle; } // hours traveling total, effective, including bonus from hustle

	forcedMarch = TravelOptions.defaultForceMarch; // hours forced marching
	forcedMarchActual = 0;
	hustle = TravelOptions.defaultHustling; // hours hustling
	breaks = 0;
	acceptableDamage = 0;
	minimumDamage = 0;
	dailyMaxDamage = null;
	dailyMinDamage = null;

	fullDays = 0;
	remainderFromFullDays = 0;
	remainder = 0;
	destinationReached = false;

	dailyDistance = TravelOptions.normalDailyTravelSpeed;
	hourlyDistance = 0;
	totalTravelTime = new Time();
	dailyNaturalHealing = 0;

	abandonedTravel = 0;

	/**
	 * @param {TravelInfo} travel
	 */
	static copy(travel) {
		const b = Object.assign(new TravelInfo(), duplicate(travel));
		b.totalTravelTime = Time.copy(b.totalTravelTime); // fix type
		return b;
	}

	sanitize() {
		//this.idle = Math.max(0, this.idle);
		this.distance = Math.max(0, this.distance);
		this.multiplier = Math.max(0, this.multiplier);
		this.hustle = Math.max(0, this.hustle);
		this.breaks = Math.max(0, this.breaks);
		this.forcedMarch = Math.max(0, this.forcedMarch);
	}
}

class CampingData {
	other = 0; // Any other time
	setup = 1; // Total time to set up and break down a camp.
	resting = 11; // Time spent resting, total, including watches.
	preparing = 0; // prepare spells and such
	get total() { return this.other + this.setup + this.resting + this.preparing; }

	/**
	 * @param {CampingData} camp
	 */
	static copy(camp) {
		return Object.assign(new CampingData(), duplicate(camp));
	}

	sanitize() {
		this.other = Math.max(0, this.other);
		this.setup = Math.max(0, this.setup);
		this.resting = Math.max(0, this.resting);
		this.preparing = Math.max(0, this.preparing);
	}
}

class PartyInfo {
	/**
	 * Party size
	 * Affects resting period.
	 */
	size = 4;
	/**
	 * Hit Points
	 * Capacity for damage before travel becomes impossible.
	 */
	hitPoints = 10;
	/**
	 * Hit Dice
	 * Natural healing.
	 */
	hitDice = 1;
	/**
	 * Fast Healing
	 */
	fastHeal = 0;
	/**
	 * Overland movement speed.
	 */
	speedOverland = 24;
	/**
	 * Actual overland movement speed with speed multiplier applied.
	 */
	speedOverlandActual = 24;
	/**
	 * Tactical movement speed. For easy conversion to overland speed.
	 */
	speedTactical = 30;

	/**
	 * @param {PartyInfo} party
	 */
	static copy(party) {
		return Object.assign(new PartyInfo(), duplicate(party));
	}

	sanitize() {
		this.hitDice = Math.max(1, this.hitDice);
		this.hitPoints = Math.max(1, this.hitPoints);
		this.speedTactical = Math.max(5, this.speedTactical);
		this.speedOverland = this.speedTactical * 0.8;
	}
}

// Activity enum
const Activity = { Travel: 'travel', Rest: 'rest', Break: 'break', Preparation: 'preparation', Setup: 'setup', Idle: 'idle', Other: 'other', Start: 'start', Destination: 'destination' };
const ActIcons = {
	hustle: 'fas fa-running',
	travel: 'fas fa-hiking',
	rest: 'fas fa-bed',
	break: 'fas fa-mug-hot', // 'fas fa-beer', 'fas fa-coffee'
	preparation: 'fas fa-book-reader',
	setup: 'fas fa-campground',
	idle: 'fas fa-battery-empty',
	other: 'fas fa-drumstick-bite',
	start: 'fas fa-route',
	destination: 'fas fa-flag-checkered', // 'fas fa-place-of-worship'
	force: 'fas fa-lungs',
	hurt: 'fas fa-band-aid',
	thinking: 'fas fa-sign',
	injury: 'fas fa-heart-broken',
};

const randomMapSymbols = [
	'fas fa-map-signs', //
	'fas fa-drafting-compass',
];

const randomPainSymbols = [
	'far fa-sad-cry',
	'far fa-sad-tear',
];

class TimeSegment {
	index = 0;
	/**
	 * Distance covered by this segmnet.
	 */
	distance = 0;
	distanceTotal = 0;
	/**
	 * Time spent on this segment.
	 */
	time = 1;
	timeTotal = 0;
	/**
	 * Activity for the time segment.
	 */
	activity = Activity.Rest;
	get isCamping() { return [Activity.Break, Activity.Rest, Activity.Setup, Activity.Idle, Activity.Other].includes(this.activity); }
	get isTravel() { return [Activity.Travel].includes(this.activity); }

	healthAdjust = 0;
	healthTotal = 0;

	hustle = false;
	hustleTotal = 0;
	forced = false;
	forcedTotal = 0;

	constructor(time = 1, distance = 0) {
		this.time = time;
		this.distance = distance;
	}

	heal(hp) {
		this.healthAdjust += hp;
		this.healthTotal = Math.min(0, this.healthTotal + hp);
	}

	/**
	 * @param {TimeSegment} sg
	 */
	static copy(sg) {
		return Object.assign(new TimeSegment(), duplicate(sg));
	}
}

class TravelLog {
	/**
	 * @type {TimeSegment[]}
	 */
	segments = [];
	/**
	 * @type {PartyInfo}
	 */
	party;
	/**
	 * @type {CampingData}
	 */
	camp;
	/**
	 * @type {TravelInfo}
	 */
	travel;

	/**
	 * Travel distance based on segment info.
	 */
	get travelDistance() {
		return this.segments.reduce((a, x) => a + x.distance, 0);
	}
	/**
	 * Total time based on segment info.
	 */
	get totalTime() {
		return this.segments.reduce((a, x) => a + x.time, 0);
	}
	/**
	 * Total time based on segment info up to reaching destination.
	 */
	get finalDayTime() {
		let time = 0;
		for (let i = 0; i < this.segments.length; i++) {
			time += this.segments[i].time;
			if (this.segments[i].activity === Activity.Destination) break;
		}
		return time;
	}

	/**
	 * Total health modifier based on segment info.
	 */
	get health() {
		return this.segments.reduce((a, x) => a + x.healthAdjust, 0);
	}
	/**
	 * All travel segments.
	 */
	get travelSegments() {
		return this.segments.filter(s => s.activity === Activity.Travel);
	}
	/**
	 * All break segments.
	 */
	get breakSegments() {
		return this.segments.filter(s => s.activity === Activity.Break);
	}
	/**
	 * All rest segments.
	 */
	get restSegments() {
		return this.segments.filter(s => s.activity === Activity.Rest);
	}

	get first() {
		return this.segments[0];
	}

	get last() {
		return this.segments[this.segments.length - 1];
	}

	constructor(partyInfo, campingData, travelInfo) {
		this.party = PartyInfo.copy(partyInfo);
		this.camp = CampingData.copy(campingData);
		this.travel = TravelInfo.copy(travelInfo);
		this.party.sanitize();
		this.camp.sanitize();
		this.travel.sanitize();
	}

	/**
	 * @param {Boolean} push Calculate as if doing final push, risking yourself to get as much as possible of the time available. // Not implemented
	 */
	calculate(push = false) {
		this.travel.forcedMarchActual = 0;

		this.travel.hourlyDistance = this.party.speedOverlandActual / TravelOptions.normalTravelHours;
		this.travel.idle = TravelOptions.dayHours - (this.camp.total + this.travel.dailyHoursPlan);

		let hustlingLeft = this.travel.hustle;
		let curHustleHours = 0;
		let nonFM = this.travel.availableTravelHours;
		let remainingDistance = this.travel.distance;
		this.travel.abandonedTravel = 0;
		let totalDamage = 0;

		this.travel.dailyNaturalHealing = 0;
		this.travel.breaks = 0;

		let breaksLeft = this.travel.breaks;

		const hDmg = hustleCumulative(this.travel.hustle);
		const fmDmg = forceMarch(this.travel.forcedMarch);
		const formula = `${hDmg}+${fmDmg.formula}`;
		const rollMax = new Roll(formula, { fm: this.travel.forcedMarch }).evaluate({ maximize: true });
		const rollMin = new Roll(formula, { fm: this.travel.forcedMarch }).evaluate({ minimize: true });
		this.travel.dailyMaxDamage = rollMax;
		this.travel.dailyMinDamage = rollMin;

		const breakStop = (hp) => {
			const sg = new TimeSegment(1);
			sg.activity = Activity.Break;
			sg.healthTotal = hp;
			sg.heal(this.party.hitDice);
			return sg;
		}

		/**
		 * @param {TimeSegment} sg
		 */
		const tryHeal = (sg) => {
			let damage = -totalDamage;
			while (this.travel.idle > 0 && healOrNot(this.party.hitDice, this.party.hitPoints, damage, this.travel)) {
				let upcomingDamage = 0;
				if (hustlingLeft > 0) {
					upcomingDamage = hustleHour(curHustleHours + 1);
				}
				if ((damage + upcomingDamage) < this.travel.acceptableDamage)
					break;

				const b = breakStop(-damage);
				totalDamage = b.healthTotal;

				this.travel.breaks++;
				this.travel.idle--;

				this.segments.push(b);
				this.travel.dailyNaturalHealing += b.healthAdjust;
				damage = Math.max(0, damage - b.healthAdjust);
			}
		};

		const fillTime = (r, activity, time = 1) => {
			range(r).forEach(_ => {
				const s = new TimeSegment(time);
				s.activity = activity;
				s.healthAdjust = this.party.hitDice;
				this.segments.push(s);
			});
		};

		fillTime(this.camp.resting, Activity.Rest);
		fillTime(this.camp.preparing, Activity.Preparation);
		fillTime(this.camp.setup, Activity.Setup);

		const travelStart = this.segments.length - 1;

		range(this.travel.dailyHoursPlan).forEach(i => {
			if (remainingDistance <= 0) {
				this.travel.abandonedTravel++;
				return;
			}

			const sg = new TimeSegment(1);
			sg.index = i;
			sg.hustle = (hustlingLeft-- > 0);
			sg.distance = this.travel.hourlyDistance * (sg.hustle ? TravelOptions.hustleSpeed : 1);
			remainingDistance -= sg.distance;
			sg.activity = Activity.Travel;
			sg.healthAdjust = Math.min(0, sg.hustle ? -hustleHour(++curHustleHours) : 0);
			totalDamage = Math.min(0, totalDamage + sg.healthAdjust); // Can't overheal
			sg.healthTotal = totalDamage;
			sg.forced = --nonFM < 0;
			if (sg.forced) this.travel.forcedMarchActual++;
			this.segments.push(sg);

			if (remainingDistance <= 0) {
				let speed = sg.distance;
				sg.distance += remainingDistance; // remove overtravel
				sg.time = sg.distance / speed;
				const end = new TimeSegment(0);
				end.activity = Activity.Destination;
				this.travel.destinationReached = true;
				this.segments.push(end);
			}
			else
				tryHeal(sg);
		});

		const travelEnd = this.segments.length - 1;


		if (TravelOptions.debug) {
			console.log("IgnoredTravel:", this.travel.abandonedTravel);
			console.log("IgnoredBreaks:", breaksLeft);
			console.log("OverTravel:", -remainingDistance);
			console.log("ForcedMarch:", this.travel.forcedMarchActual);
		}
		this.travel.remainder = Math.max(0, remainingDistance);

		if (this.travel.remainder > 0 || TravelOptions.displayRemainingDay) {
			fillTime(this.camp.other, Activity.Other);
			fillTime(this.travel.idle, Activity.Idle);
		}

		// Update HP recovery
		this.segments.filter(s => !s.isTravel).forEach(s => s.healthAdjust = this.party.hitDice * s.time);

		this.travel.dailyDistance = this.travelDistance;
		this.travel.fullDays = Math.floor(this.travel.distance / this.travelDistance);
		this.travel.remainderFromFullDays = this.travel.distance - (this.travel.fullDays * this.travelDistance);
	}

	/**
	 * Ensure totals are accurate.
	 */
	accumulate() {
		let hp = 0, dst = 0, tm = 0, f = 0, h = 0;
		for (let i = 0; i < this.segments.length; i++) {
			const s = this.segments[i];
			s.index = i;
			s.distanceTotal = (dst += s.distance);
			s.healthTotal = hp = Math.min(0, (hp + s.healthAdjust));
			s.timeTotal = (tm += s.time);
			if (s.forced) s.forcedTotal = ++f;
			if (s.hustle) s.hustleTotal = ++h;
		}
	}

	/**
	 * Transform travel log.
	 * Re-order, split, and combine events.
	 */
	transform() {
		this.accumulate();

		/**
		 * Splits activity around the delimiter activity.
		 * End defines what activity stops the search.
		 * If delimiter is undefined, it pushes the split to the end instead.
		 */
		const split = (activity, end, delimiter = [], splitTime = true) => {
			const desired = this.segments.filter(s => s.activity === activity);
			const items = desired.length;
			if (items <= 0) return; // nothing to do
			//const time = desired.reduce((t, c) => t + c.time, 0);
			let reloc = this.segments.length - 1;
			if (delimiter.length) {
				for (; reloc > 0; reloc--) {
					const sg = this.segments[reloc];
					if ([...delimiter, ...end].includes(sg.activity)) {
						reloc++; // back up. +1 to get next one after the breaking point, +1 for correcting the --
						break;
					}
				}
			}

			let splitCount = Math.ceil(items / 2);
			let splitWell = splitTime && items % 2 !== 0;
			if (TravelOptions.debug) console.log("Split:", activity, splitCount, splitWell, "Reloc to:", reloc);
			if (splitCount > 0 || splitWell) {
				if (TravelOptions.debug) console.group("Splitting:", activity);
				let splitIndex = -1;
				for (let i = 0; i < this.segments.length - 1; i++) {
					const sg = this.segments[i];

					if (TravelOptions.debug) console.log("Testing segment:", i, sg.activity);

					if (end.includes(sg.activity)) {
						if (TravelOptions.debug) console.log("Ending search at:", i);
						break;
					}
					if (splitCount <= 0) continue;
					if (sg.activity === activity) {
						const rpl = [];
						let adjustIndex = false;
						if (splitWell) {
							splitIndex = i;
							sg.time /= 2;
							rpl.push(TimeSegment.copy(sg));
							splitWell = false; // don't do this again
						}
						else
							adjustIndex = true;

						// HACK: Special case for not showing ending setup for the day you reach your destination on.
						if (!(sg.activity === Activity.Setup && this.travel.destinationReached)) {
							const cut = this.segments.splice(i, 1, ...rpl);
							if (delimiter.length)
								this.segments.splice(reloc, -1, ...cut);
							else
								this.segments.push(...cut);
						}

						if (adjustIndex) i--;
						--splitCount;
					}
				}
				if (TravelOptions.debug) console.groupEnd();
			}
			else {
				// split singular item
				if (TravelOptions.debug) console.log("SINGULAR");
				let nb = null;
				if (setup.length % 2 === 0) {
					const splitTime = setup[0].time /= 2;
					nb = TimeSegment.copy(setup[0]);
					nb.time = setup[0].time = splitTime;
					// insert at end of travel
				}
			}
		};

		if (this.camp.resting > 0)
			split(Activity.Rest, [Activity.Travel, Activity.Setup, Activity.Preparation]);

		const setup = this.segments.filter(s => s.activity === Activity.Setup);
		if (TravelOptions.splitSetup && setup.length > 0)
			split(Activity.Setup, [Activity.Travel], [Activity.Travel]);

		if (TravelOptions.combineCamping) {
			for (let i = 0; i < this.segments.length; i++) {
				const s = this.segments[i];
				if (!s.isCamping) continue;
				const ns = this.segments[i + 1];
				if (s.activity === ns?.activity) {
					s.healthAdjust += ns.healthAdjust;
					s.distance += ns.distance;
					s.time += ns.time;
					this.segments.splice(i + 1, 1);
					i--;
				}
			}
		}

		/**
		 * Remove remaining activity from a day.
		 */
		if (this.travel.destinationReached && !TravelOptions.displayRemainingDay) {
			for (let i = 0; i < this.segments.length - 1; i++) {
				const sg = this.segments[i];
				if (sg.activity === Activity.Destination) {
					this.segments.splice(i + 1);
					break;
				}
			}
		}

		this.accumulate(); // fix
	}

	/**
	 * @param {TravelLog} tl
	 */
	static copy(tl) {
		const day = new TravelLog(tl.party, tl.camp, tl.travel);
		day.segments = tl.segments.map(x => TimeSegment.copy(x));
		return day;
	}
}

// UI elements

const col = () => $('<div/>').addClass('flexcol');
const row = () => $('<div/>').addClass('flexrow').css({ 'vertical-align': 'middle', 'margin-bottom': '2px' });
const title = (text) => $('<h2/>').text(text).css({ 'margin-bottom': '5px', flex: '0', 'font-size': '1.3em' }).css(darkRowCss);
const subtitle = (text) => $('<h3/>').text(text).css({ flex: '0' });
const label = (text) => $('<label/>').text(text).css({ 'line-height': '1.5em' });
const slabel = (text) => $('<label/>').text(text).css({ 'line-height': '1.2em' });
const input = (value = '', type = 'text', id) => $('<input/>').attr({ type, value, name: id, maxlength: 6 }).addClass('travel-input').css({ flex: '0 0 5em', 'font-weight': 'bold', color: 'black', height: '1.5em' });
const tooltip = (j, text) => j.attr({ title: text });
const faIcon = (icon) => $('<i/>').addClass(icon).css({ flex: '0 0 1.5em', 'vertical-align': 'middle', 'line-height': '1.5em' }).html('&nbsp;');
const hr = () => $('<hr/>').css({ margin: '1px' });

const tsCss = { 'text-align': 'right', flex: '.2 .5 1.8em' },
	tsCss2 = { 'text-align': 'right', flex: '.2 .5 1.2em', 'margin-left': '3px' },
	primeInputCss = { 'background-color': 'rgba(143, 255, 160, 0.20)' },
	interestingDataCss = { 'background-color': 'rgba(255, 230, 50, 0.15)' },
	resultCss = { 'background-color': 'rgba(0, 200, 255, 0.25)' },
	readOnlyCss = { color: '#090909', 'background-color': 'rgba(255, 250, 210, 0.15)' },
	indentCss = { 'margin-left': '1em' },
	underlineCss = { 'border-bottom': '1px dashed rgba(0, 0, 0, 0.2)' },
	darkRowCss = { 'background-color': 'rgba(0, 0, 0, 0.07)' },
	darkerRowCss = { 'background-color': 'rgba(0, 0, 0, 0.15)' };

const logHeader = () => {
	return row().append(
		faIcon(''),
		slabel('Activity'),
		tooltip(slabel('HP Δ'), 'Hit point delta (Change).').css(tsCss),
		tooltip(slabel('HP Σ'), 'Hit point sigma (Cumulative).').css(tsCss),
		slabel('Dist.').css(tsCss),
		slabel('Time').css(tsCss2),
	)
		.css({ border: '1px dashed rgba(0,0,0,0.2)', 'margin-bottom': '2px', padding: '2px', 'max-height': '1.5em', height: '1.5em', 'line-height': '1.5em', 'font-weight': 'bold' })
		.css(darkerRowCss)
		.addClass('travel-log-item travel-log-header');
};

/**
 * @param {TimeSegment} s
 */
const logEntry = (s) => {
	const sr = [];
	const act = s.hustle ? 'hustle' : s.activity;
	sr.push(faIcon(ActIcons[act]));
	let lb = act.capitalize();
	if (s.forced) lb += ' [Forced]';
	sr.push(slabel(lb));
	const hp = [];
	if (s.forced) hp.push('-1d6')
	if (s.healthAdjust != 0) hp.push(signNum(s.healthAdjust));
	sr.push(slabel(hp.length ? `${hp.join('')}` : ' ').css(tsCss));
	sr.push(slabel(`${s.healthTotal}`).css(tsCss));

	const clampDist = (d) => Math.floor(d * 100) / 100;
	sr.push(slabel(s.distance != 0 ? `${signNum(clampDist(s.distance))}` : ' ').css(tsCss));
	const tt = Time.fromCs(s.time * Time.HourCs);
	sr.push(slabel(`${tt.toString()}`).css(tsCss2).css(s.time !== 1 ? { 'font-weight': 'bold' } : {}));
	return row().append(sr)
		.css({ border: '1px dashed rgba(0,0,0,0.2)', 'margin-bottom': '2px', padding: '2px', 'max-height': '1.5em', height: '1.5em', 'line-height': '1.5em' })
		.addClass(`travel-log-item travel-log-activity-${s.activity}`);
}

/**
 * @param {TravelLog} d
 */
const logFooter = (d, final) => {
	const sr = [];
	sr.push(faIcon('fas fa-equals').css({ 'font-weight': 'bold' }));
	sr.push(slabel('')); // empty for alignment
	sr.push(slabel('').css(tsCss)); // empty for alignment
	const hp = [];
	if (d.travel.forcedMarchActual > 0) hp.push(`-${d.travel.forcedMarchActual}d6`);
	hp.push(signNum(d.last.healthTotal));
	sr.push(slabel(hp.length ? `${hp.join('')}` : ' ').css(tsCss));
	sr.push(slabel(d.travelDistance != 0 ? `${d.travelDistance}` : ' ').css(tsCss));
	const tt = Time.fromCs((final ? d.finalDayTime : d.totalTime) * Time.HourCs);
	tt.redistribute(true);
	sr.push(slabel(tt.days > 0 ? `${clampNum3(tt.totalHours)}h` : tt.toString()).css(tsCss2));
	return row().append(sr)
		.css({ border: '1px dashed rgba(0,0,0,0.2)', 'margin-bottom': '2px', padding: '2px', height: '1.5em', 'max-height': '1.5em', 'font-weight': 'bold' })
		.css(darkerRowCss)
		.addClass('travel-log-item travel-log-footer');
};

// UI itself

class TravelDialog extends Dialog {
	travelTimeBase = TravelOptions.normalTravelHours;

	/**
	 * @type {PartyInfo}
	 */
	party = new PartyInfo();
	/**
	 * @type {TravelInfo}
	 */
	travel = new TravelInfo();
	/**
	 * @type {CampingData}
	 */
	camp = new CampingData();

	/**
	 * @type {TravelLog?}
	 */
	fullDay = null;
	/**
	 * @type {TravelLog?}
	 */
	finalDay = null;
	/**
	 * @type {TravelLog?}
	 */
	initialDay = null;

	jq = null;

	/**
	 * @param {PartyInfo} p
	 * @param {TravelInfo} t
	 * @param {CampingData} c
	 */
	constructor(_data, _options, p, t, c) {
		super(_data, _options);
		this.party = p;
		this.travel = t;
		this.camp = c;
		this.data.title = 'Travel Calculator';
		this.data.buttons = {
			/*
			travel: {
				label: 'Travel',
				icon: '<i class="fas fa-route"></i>',
				callback: () => { }
			},
			*/
			dismiss: {
				label: 'Dismiss',
				icon: '<i class="fas fa-power-off"></i>',
			},
		};
		//this.data.default = 'travel';

		c.resting = nightWatches[clamp(p.size, 1, 6)].totalTime.rounded ?? 11;
		if (TravelOptions.debug) console.log("Party Size:", p.size, "Clamped:", clamp(p.size, 2, 6));
		const h =
			row().append(
				// SETUP
				col().css({ flex: '0 0 280px' }).append(
					title('Party (weakest link)'),
					row().append(
						faIcon('fas fa-dice'),
						label('Hit Dice'),
						input(p.hitDice, 'number', 'hit-dice').attr({ min: 1, max: 40, step: 1 }),
					),
					row().append(
						faIcon('fas fa-heartbeat'),
						label('Hit Points'),
						input(p.hitPoints, 'number', 'hit-points').attr({ min: 1, max: 500, step: 1 }),
					),
					row().append(
						faIcon('fas fa-shoe-prints'),
						label('Tactical speed'),
						input(p.speedTactical, 'number', 'speed-tactical').attr({ min: 1, step: '.1' }),
					),
					row().append(
						faIcon('fas fa-sleigh'),
						label('Overland speed'),
						input(p.speedOverland, 'number', 'speed-overland').attr({ min: 1, step: '.1' }),
					),
					tooltip(row().append(
						faIcon('fas fa-mountain'),
						label('Speed multiplier').css(underlineCss),
						input(t.multiplier, 'number', 'speed-multiplier').attr({ step: '.01' }),
					), 'Speed multiplier that affects the entire journey.'),
					title('Journey'),
					row().append(
						faIcon('fas fa-route'),
						label('Distance'),
						input(t.distance, 'number', 'distance').attr({ min: 1, step: '.01' }).css(primeInputCss),
					),
					/*
					// NOT IMPLEMENTED
					tooltip(row().append(
						faIcon('far fa-tired'),
						label('Acceptable damage').css(underlineCss),
						input(Math.floor(p.hitPoints / 2), 'number', 'acceptable-damage').attr({ min: 0, max: 500, step: 1 }),
					), 'Maximum damage allowed to accumulate at any point.'),
					*/
					tooltip(row().append(
						faIcon(ActIcons.force),
						label('Force march').css(underlineCss),
						input(t.forcedMarch, 'number', 'force-march').attr({ min: 0, max: 22, step: 1 }),
					), 'Hours to force march per day over the normal daily travel hours.'),
					tooltip(row().append(
						faIcon(ActIcons.hustle),
						label('Hustle').css(underlineCss),
						input(t.hustle, 'number', 'hustle').attr({ min: 0, max: 12, step: 1 }),
					), 'Hours to hustle per day.'),
					tooltip(row().append(
						faIcon('fas fa-user-injured'),
						label('Daily damage').css(underlineCss),
						input('0', 'text', 'damage-daily').attr({ min: 0, step: 1 }).css(readOnlyCss).addClass('readonly')
					), 'Nonlethal damage accumulated over the entire day.'),
					row().append(
						label(' – Range').css(indentCss),
						input('0', 'text', 'damage-daily-range').css(readOnlyCss).addClass('readonly')
					),
					tooltip(row().append(
						faIcon(ActIcons.hurt),
						label('Acceptable damage').css(underlineCss),
						input('10', 'number', 'acceptable-damage').attr({ min: '0', step: '1' })
					), 'Amount of damage not attempted to heal during travel.'),
					tooltip(row().append(
						label('Daily healing'),
						input('0', 'text', 'daily-healing')
					), 'Semicolon separated list of available healing options.'),
					tooltip(row().append(
						faIcon(ActIcons.break),
						label('Breaks').css(underlineCss),
						input(t.breaks, 'number', 'daily-breaks').css(readOnlyCss, interestingDataCss).addClass('readonly')
					), 'Hours spent resting during travel, including watches and other activities.'),
					tooltip(row().append(
						faIcon('fas fa-hourglass-half'),
						label('Travel hours per day').css(underlineCss),
						input(t.dailyHoursPlan, 'number', 'daily-travel-time').attr({ min: 0, step: '.1' }).css(readOnlyCss).addClass('readonly')
					), 'Total time used per day for travel.'),
					tooltip(row().append(
						label(' – Effective:').css(indentCss).css(underlineCss),
						input(t.dailyHoursPlan, 'number', 'daily-travel-time-effective').attr({ min: 0, step: '.1' }).css(readOnlyCss).addClass('readonly')
					), 'Includes hustling.'),
					row().append(
						faIcon(ActIcons.travel),
						label('Travel distance per day'),
						input(t.dailyDistance, 'number', 'daily-distance').attr({ min: 0, step: '.001' }).css(readOnlyCss).addClass('readonly')
					),
					tooltip(row().append(
						faIcon('fas fa-hourglass-end'),
						label('Hours to destination'),
						input('0', 'text', 'hours-to-destination').css(readOnlyCss).addClass('readonly')
					), 'Total time to destination, mostly as hours. Format is HHH:MM.'),
					tooltip(row().append(
						faIcon('far fa-calendar-alt'),
						label('Days to destination'),
						input('0', 'text', 'time-to-destination').css(readOnlyCss).addClass('readonly').css(resultCss)
					), 'Total time to destination. Format is DD:HH:MM'),
					title('Camping'),
					tooltip(row().append(
						faIcon('fas fa-users'),
						label('Party size'),
						input(p.size, 'number', 'party-size').attr({ min: 1, max: 24, step: 1 })
					), 'Adjusting party size auto-calculates needed resting to be appropriate to allow full rest for all.'),
					tooltip(row().append(
						faIcon(ActIcons.rest),
						label('Resting').css(underlineCss),
						input(c.resting, 'number', 'resting').attr({ min: 0, max: 24, step: '.1' }).css(primeInputCss)
					), 'Total sleep, including any necessary watch time.'),
					tooltip(row().append(
						faIcon(ActIcons.preparation),
						label('Preparation').css(underlineCss),
						input(c.preparing, 'number', 'preparation').attr({ min: 0, max: 24, step: '.1' })
					), 'Preparing spells and such. Set to zero if this can overlap with setup time.'),
					tooltip(row().append(
						faIcon(ActIcons.setup),
						label('Setup').css(underlineCss),
						input(c.setup, 'number', 'setup').attr({ min: 0, max: 4, step: '.1' })
					), 'Time to set and break camp and similar activities.'),
					tooltip(row().append(
						faIcon(ActIcons.other),
						label('Other').css(underlineCss),
						input(c.other, 'number', 'other').attr({ min: 0, max: 8, step: '.1' })
					), 'Base necessities of life and other activities.'),
					row().append(
						faIcon('fas fa-cloud-sun'),
						label('Total'),
						input(c.total, 'number', 'total-camping').attr({ min: 0, step: '.1' }).css(readOnlyCss).addClass('readonly')
					),
					tooltip(row().append(
						faIcon(ActIcons.idle),
						label('Idle').css(underlineCss),
						input('0', 'number', 'idle').css(readOnlyCss, interestingDataCss).addClass('readonly')
					), 'Time not used by anything specific and thus available for extra force marching or other activities.'),
				),
				col().css({ flex: '0 1 7px' }),
				col().css({ flex: '1' }).addClass('travel-log')
					.append(
						title('Travel Log'),
						row().append(
							col().addClass('travel-log-initial'),
							col().css({ flex: '0 1 5px' }).addClass('travel-log-initial-separator'),
							col().addClass('travel-log-fullday'),
							col().css({ flex: '0 1 5px' }).addClass('travel-log-final-separator'),
							col().addClass('travel-log-final')
						)
					),
			);

		this.data.content = $('<div/>').append(h, hr()).html();
	}

	static open({ c = new CampingData(), t = new TravelInfo(), p = new PartyInfo() } = {}) {
		new TravelDialog({}, { width: 900 }, p, t, c).render(true);
	}

	_updateDialog() {
		if (!this.fullDay) return;

		// Party
		this.jq.find('input[name="hit-dice"]').val(this.party.hitDice);
		this.jq.find('input[name="hit-points"]').val(this.party.hitPoints);
		this.jq.find('input[name="speed-tactical"]').val(this.party.speedTactical);
		this.jq.find('input[name="speed-overland"]').val(this.party.speedOverland);
		this.jq.find('input[name="speed-multiplier"]').val(this.travel.multiplier);

		// Journey
		this.jq.find('input[name="distance"]').val(this.travel.distance);
		this.jq.find('input[name="acceptable-damage"]').val(this.travel.acceptableDamage);
		this.jq.find('input[name="force-march"]').val(this.travel.forcedMarch);
		this.jq.find('input[name="hustle"]').val(this.travel.hustle);

		this.jq.find('input[name="damage-daily"]').val(this.travel.dailyMinDamage?.formula ?? '');
		this.jq.find('input[name="damage-daily-range"]').val(`${this.travel.dailyMinDamage?.total ?? 0} - ${this.travel.dailyMaxDamage?.total ?? 0}`);

		this.jq.find('input[name="daily-breaks"]').val(this.travel.breaks);

		this.jq.find('input[name="daily-travel-time"]').val(this.travel.dailyHoursPlan);
		this.jq.find('input[name="daily-travel-time-effective"]').val(this.travel.dailyHoursEffectivePlan);
		this.jq.find('input[name="daily-distance"]').val(this.travel.dailyDistance);

		const bh = Time.copy(this.fullDay.travel.totalTravelTime);
		const hh = Time.copy(bh);
		bh.redistribute(false);
		hh.redistribute(true);

		this.jq.find('input[name="time-to-destination"]').val(bh.toString());
		this.jq.find('input[name="hours-to-destination"]').val(hh.toString());

		// Camping
		this.jq.find('input[name="party-size"]').val(this.party.size);
		this.jq.find('input[name="resting"]').val(this.camp.resting);
		this.jq.find('input[name="preparation"]').val(this.camp.preparing);
		this.jq.find('input[name="setup"]').val(this.camp.setup);
		this.jq.find('input[name="other"]').val(this.camp.other);
		this.jq.find('input[name="total-camping"]').val(this.camp.total);
		this.jq.find('input[name="idle"]').val(this.travel.idle);

		// Update warnings
		const idle = this.jq.find('input[name="idle"]');
		const idleWarn = idle.parent().find('.travel-idle-time-warning');
		if (this.travel.idle < 0 && idleWarn.length === 0) {
			idle.before(faIcon('fas fa-exclamation-triangle').addClass('travel-idle-time-warning'));
			idle.css(darkRowCss);
		}
		else if (this.travel.idle >= 0) {
			idleWarn?.remove();
			idle.css({ 'background-color': 'rgba(255, 250, 210, 0.15)' });
		}


	}

	_updateTravel() {
		const availableTimeForBaseTravel = TravelOptions.dayHours - this.camp.total;
		this.travel.availableTravelHours = Math.min(TravelOptions.normalTravelHours, availableTimeForBaseTravel);
		if (TravelOptions.debug) console.log("availableTimeForBaseTravel:", availableTimeForBaseTravel);

		this._generateSchedule();
		this._processSchedule();
		this._updateDialog()
	}

	// Build basic schedule
	_generateSchedule() {
		this.fullDay = new TravelLog(this.party, this.camp, this.travel);
		this.fullDay.calculate();
		this.fullDay.transform();
		this.party = this.fullDay.party;
		this.camp = this.fullDay.camp;
		this.travel = this.fullDay.travel;
		if (TravelOptions.debug) console.log("Remaining distance for final day:", this.travel.remainderFromFullDays);

		this.finalDay = new TravelLog(this.fullDay.party, this.fullDay.camp, this.fullDay.travel);
		this.finalDay.travel.distance = this.fullDay.travel.remainderFromFullDays;
		this.finalDay.calculate();
		this.finalDay.transform();
	}

	_processSchedule() {
		if (!this.fullDay) return;
		const day = this.fullDay;
		const final = this.finalDay;

		// Re-update output
		const p_hours = day.totalTime * day.travel.fullDays;
		const f_hours = final?.finalDayTime ?? 0;
		day.travel.totalTravelTime = Time.fromCs((p_hours + f_hours) * Time.HourCs);

		this._renderSchedule();
	}

	_renderSchedule() {
		if (!this.fullDay) return;
		const day = this.fullDay;
		const fday = this.finalDay;
		if (TravelOptions.debug) {
			console.log("FullDay:", day);
			console.log("FinalDay:", fday);
		}

		const renderDay = (d, jq, final) => {
			jq.append(logHeader())
			d.segments.forEach(s => {
				if (s.activity === Activity.Rest && !TravelOptions.displayRest) return;
				try {
					jq.append(logEntry(s));
				}
				catch (err) {
					console.warn(d, s);
					console.error(err);
				}
			});
			//jq.append($('<div/>').css({ flex: '10 2 0px' })); // fillter
			jq.append(logFooter(d, final))
		}

		// Initial day
		// NOT IMPLEMENTED
		const initCol = this.jq.find('.travel-log-initial');
		const initColSep = this.jq.find('.travel-log-initial-separator');
		const initial = col().css({ flex: '1' });
		initCol.empty().append(subtitle("Initial day"), initial);
		initCol.hide();
		initColSep.hide();

		// Normal day
		const fulldayCol = this.jq.find('.travel-log-fullday');
		const fullday = col().css({ flex: '1' });
		fulldayCol.empty().append(subtitle(`Full day of travel (×${day.travel.fullDays})`), fullday);
		renderDay(day, fullday);

		// Final day
		const finalCol = this.jq.find('.travel-log-final');
		const finalColSep = this.jq.find('.travel-log-final-separator');
		const final = col().css({ flex: '1' });
		finalCol.empty().append(subtitle("Final day"), final);
		if (fday?.travelDistance > 0) {
			renderDay(fday, final, true);
			finalCol.show();
			finalColSep.show();
		}
		else {
			finalCol.hide();
			finalColSep.hide();
		}

		function alternateRowColors(col) {
			col.find('div.travel-log-item').not('.travel-log-header').not('.travel-log-footer')
				.each(function (i) {
					if (i % 2 === 0)
						$(this).css(darkRowCss);
				});
		};

		alternateRowColors(initCol);
		alternateRowColors(fulldayCol);
		alternateRowColors(finalCol);
	}

	_updateDamage() {
		const hDmg = hustleCumulative(this.travel.hustle);
		const fmDmg = forceMarch(this.travel.forcedMarch);
		const formula = `${hDmg}+${fmDmg.formula}`;

		const rollMax = new Roll(formula, { fm: this.travel.forcedMarch }).evaluate({ maximize: true });
		const rollMin = new Roll(formula, { fm: this.travel.forcedMarch }).evaluate({ minimize: true });

		let damageRemaining = hDmg;
		this.travel.dailyNaturalHealing = 0;
		this.travel.breaks = 0;
		while (this.travel.idle > 0 && healOrNot(this.party.hitDice, this.party.hitPoints, damageRemaining, this.travel)) {
			this.travel.breaks++;
			this.travel.idle--;
			const adjust = Math.min(damageRemaining, this.party.hitDice);
			damageRemaining -= adjust;
			this.travel.dailyNaturalHealing += adjust;
		}

		const ddmg = this.jq.find('input[name="damage-daily"]');
		ddmg.val(rollMax.formula);
		this.jq.find('input[name="damage-daily-range"]').val(`${rollMin.total} - ${rollMax.total}`);

		const dmWarn = ddmg.parent().find('.travel-damage-warning');
		if (rollMin.total > this.travel.acceptableDamage && dmWarn.length === 0)
			ddmg.before(faIcon('fas fa-exclamation-triangle').addClass('travel-damage-warning'));
		else if (rollMin.total < this.travel.acceptableDamage)
			dmWarn?.remove();


	}

	_updateInputs(ev) {
		const name = ev.target.name;
		const value = parseFloat(ev.target.value) || 0; // new value
		if (TravelOptions.debug) console.log('NewValue:', name, '=', value);
		switch (name) {
			case 'hit-dice':
				this.party.hitDice = clamp(Math.round(value), 1, 500);
				break;
			case 'hit-points':
				this.party.hitPoints = clamp(Math.round(value), 0, 500000);
				this.travel.acceptableDamage = Math.floor(this.party.hitPoints / 2);
				this.jq.find('input[name="acceptable-damage"]').val(this.travel.acceptableDamage);
				break;
			case 'distance':
				this.travel.distance = clamp(value, 0, 50000);
				break;
			case 'speed-overland':
				this.party.speedOverland = clamp(value, 1, 500);
				this.party.speedOverlandActual = this.party.speedOverland * this.travel.multiplier;
				this.party.speedTactical = this.party.speedOverland / 0.8;
				this.jq.find('input[name="speed-tactical"]').val(this.party.speedTactical);
				break;
			case 'speed-tactical':
				this.party.speedTactical = clamp(value, 5, 500);
				this.party.speedOverland = this.party.speedTactical * 0.8;
				this.party.speedOverlandActual = this.party.speedOverland * this.travel.multiplier;
				this.jq.find('input[name="speed-overland"]').val(this.party.speedOverland);
				break;
			case 'speed-multiplier':
				this.travel.multiplier = clamp(value, 0, 10);
				this.party.speedOverlandActual = this.party.speedOverland * this.travel.multiplier;
				break;
			case 'acceptable-damage':
				this.travel.acceptableDamage = clamp(value, 0, this.party.hitPoints - 1);
				if (value !== this.travel.acceptableDamage)
					this.jq.find('input[name="acceptable-damage"]').val(this.travel.acceptableDamage);
				break;
			case 'resting':
				this.camp.resting = clamp(value, 0, SafetyOptions.MaxCamping);
				break;
			case 'preparation':
				this.camp.preparing = clamp(value, 0, SafetyOptions.MaxCamping);
				break;
			case 'setup':
				this.camp.setup = clamp(value, 0, SafetyOptions.MaxCamping);
				break;
			case 'other':
				this.camp.other = clamp(value, 0, SafetyOptions.MaxCamping);
				break;
			case 'party-size':
				this.party.size = clamp(value, 1, 10);
				this.camp.resting = nightWatches[clamp(this.party.size, 1, 6)].totalTime.rounded ?? 11;
				this.jq.find('input[name="resting"]').val(this.camp.resting);
				break;
			case 'hustle':
				this.travel.hustle = clamp(Math.round(value), 0, SafetyOptions.MaxTravel);
				{
					const evt = $(ev.target);
					const hustleWarn = evt.parent().find('.travel-hustle-time-warning');
					if (this.travel.hustle > this.travel.dailyHoursPlan && hustleWarn.length === 0) {
						evt.before(faIcon('fas fa-exclamation-triangle').addClass('travel-hustle-time-warning'));
						evt.css({ 'background-color': 'rgba(255, 0, 0, 0.1)' });
					}
					else if (this.travel.hustle <= this.travel.dailyHoursPlan) {
						hustleWarn?.remove();
						evt.css({ 'background-color': '' });
					}
				}
				break;
			case 'force-march':
				this.travel.forcedMarch = clamp(Math.round(value), 0, SafetyOptions.MaxTravel);
				break;
		}

		this._updateTravel();
	}

	activateListeners(html) {
		super.activateListeners(html);
		this.jq = html;
		html.find('input.readonly').prop('readonly', true);
		html.find('.travel-input').on('change', this._updateInputs.bind(this));
		this._updateTravel();
	}
}

{
	const party = new PartyInfo();
	if (canvas.tokens.controlled.length > 0) {
		// Autofill party's weakest link
		party.hitPoints = Number.POSITIVE_INFINITY;
		party.hitDice = Number.POSITIVE_INFINITY;
		party.speedTactical = Number.POSITIVE_INFINITY;
		party.size = 0;
		canvas.tokens.controlled.map(t => t.actor).forEach(a => {
			if (!a) return;
			party.size++;
			party.hitDice = Math.min(a.data.data.attributes.hd.total, party.hitDice);
			party.hitPoints = Math.min(a.data.data.attributes.hp.value, party.hitPoints);
			party.speedTactical = Math.min(a.data.data.attributes.speed.land.total, party.speedTactical);
		});
		party.speedOverland = party.speedTactical * 0.8;
		party.speedOverlandActual = party.speedOverland;
	}

	TravelDialog.open({ p: party });
}
