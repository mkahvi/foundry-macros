## Pathfinder 1e

Macros specifically meant for PF1e.

These are not expected to work with any other game system without major overhauls.
