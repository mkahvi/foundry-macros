/**
 * Roll stats for D&D 5e
 *
 * Compatibility: Foundry v11
 */

(async () => {
	// Default configuration
	const rollStatsCfg = {
		skipPrompt: true, // Skip prompt in case you feel this config here is good for all cases
		maxIdle: 120, // Assume end of rolls when this many minutes have passed between messages.
		maxDays: 3, // Don't look back more than this many days no matter what.
		minStreak: 2, // Minimum streak length to report. Values under 2 are ignored.
	};

	class RollStats {
		/** @type {number[]} */
		rolls = [];

		size = 20;

		get nat1s() { return this.rolls.filter(r => r === 1).length; }
		get nat20s() { return this.rolls.filter(r => r === 20).length; }

		get best() { return Math.max(...this.rolls); }
		get worst() { return Math.min(...this.rolls); }

		get spread() {
			const base = {};
			for (let i = 1; i < this.size + 1; i++) base[i] = 0;
			return this.rolls.reduce((split, cur) => {
				split[cur]++;
				return split;
			}, base);
		}

		get mode() {
			const entries = Object.entries(this.spread);
			const [mode, _count] = entries.sort(([_a, a], [_b, b]) => b - a)[0];
			return parseInt(mode);
		}

		get modeCount() {
			const entries = Object.entries(this.spread);
			const [_mode, count] = entries.sort(([_a, a], [_b, b]) => b - a)[0];
			return count;
		}

		get average() { return this.rolls.length ? Math.roundDecimals(this.rolls.reduce((prev, cur) => cur + prev) / this.rolls.length, 2) : NaN; }

		/**
		 * @param {number} rolls
		 */
		add(...rolls) {
			// console.log('Adding:', ...rolls);
			this.rolls.push(...rolls);
		}

		finalize() {
			this.rolls.reverse();
		}

		constructor(size = 20) {
			this.size = size;
		}
	}

	class UserStats {
		#id;
		user;

		streaks = [];
		get longestStreak() { return this.streaks[0]; }
		_lastStreak = { num: 0, count: 0 };

		// Specific rolls
		specific = {
			100: new RollStats(100), 20: new RollStats(20), 12: new RollStats(12), 10: new RollStats(10), 8: new RollStats(8), 6: new RollStats(6), 4: new RollStats(4), 3: new RollStats(3), 2: new RollStats(2)
		};

		// All d20 rolls
		get checks() { return this.specific[20]; }

		saves = new RollStats();
		ability = new RollStats();
		skills = new RollStats();
		attacks = new RollStats();
		damage = new RollStats();
		attacksAll = new RollStats();
		attacksConfirms = new RollStats();
		unidentified = new RollStats();

		constructor(userId) {
			this.#id = userId;
			this.user = game.users.get(userId);
		}

		get name() {
			return this.user.name;
		}

		add(rolldata) {
			console.log('Adding:', rolldata);
			for (const [_size, rolls] of Object.entries(rolldata)) {
				const size = parseInt(_size);
				if (this.specific[size] === undefined) {
					console.warn('Unsupported die size:', size);
					continue;
				}
				this.specific[size].add(...rolls);
				if (size === 20) {
					for (const r of rolls) {
						if (this._lastStreak.num === r) this._lastStreak.count++;
						else {
							// Push and reset on streak end
							if (this._lastStreak.count > 1) this.streaks.push(this._lastStreak);
							this._lastStreak = { num: r, count: 1 };
						}
					}
				}
			}
		}

		finalize() {
			// Push lingering streaks in
			if (this._lastStreak.count > 1) this.streaks.push(this._lastStreak);
			delete this._lastStreak;

			if (rollStatsCfg.minStreak > 1) this.streaks = this.streaks.filter(s => s.count >= rollStatsCfg.minStreak);

			// Sort streaks
			this.streaks.sort((a, b) => {
				const diff = b.count - a.count;
				if (diff != 0) return diff;
				return a.num - b.num;
			});

			// Create compressed streak display with repeat streaks
			this.superStreaks = [];
			for (const streak of this.streaks) {
				const last = this.superStreaks.at(-1);
				if (last) {
					if (last.count === streak.count && last.num === streak.num) {
						last.repeat ??= 1;
						last.repeat += 1;
						continue;
					}
				}
				this.superStreaks.push(streak);
			}

			// Restore order in time
			Object.values(this).forEach(p => p instanceof RollStats ? p.finalize() : null);
			Object.values(this.specific).forEach(p => p.finalize());
		}
	}

	// Unpack active dice from a roll
	function getDice(roll) {
		return roll?.dice.reduce((dtotal, dt) => {
			const rs = dt.results.reduce((total, r) => { if (r.active) total.push(r.result); return total; }, []);
			if (rs.length) {
				dtotal[dt.faces] ??= [];
				dtotal[dt.faces].push(...rs);
			}
			return dtotal;
		}, {});
	}

	function scanChatMessages() {
		/** @type {ChatMessage[]} */
		const allMessages = game.messages.contents.reverse(); // get messages and reverse them so they're oldest first

		/** @type {Record<string,UserStats>} */
		const users = {};
		const now = Date.now(); // use last message timestamp
		let lastStamp = allMessages[0].timestamp;
		let lastMsg = allMessages[allMessages.length - 1];
		let includedMessages = 0;
		for (const cm of allMessages) {
			if (!cm.isContentVisible) continue; // Skip hidden messages
			// console.log('Processing:', cm);

			const roll = cm.rolls?.[0];

			// Init user
			const userId = cm.user.id;
			if (!userId) {
				console.warn('- Bad user ID:', cm);
				continue;
			}
			users[userId] ??= new UserStats(userId);
			/** @type {UserStats} */
			const user = users[userId];

			// CHECK TIMESTAMPS
			const stamp = cm.timestamp;
			const nowDelta = now - stamp;
			if (nowDelta > rollStatsCfg.maxDays * 86_400_000) {
				console.log('- Too old message', Math.roundDecimals(nowDelta / 86_400_000, 2), 'days\n', cm);
				break;
			}
			const lastMsgDelta = lastStamp - stamp;
			if (lastMsgDelta > rollStatsCfg.maxIdle * 60_000) {
				console.log('- Idle time exceeded', Math.roundDecimals(lastMsgDelta / 60_000, 2), 'minutes\n', cm);
				break;
			}
			lastStamp = stamp;

			// Unpack die rolls into an array
			const rolls = getDice(roll);

			const metadata = cm.getFlag('dnd5e', 'roll');
			const subject = metadata?.type;
			console.log(subject, 'Rolls:', rolls);

			if (rolls) {
				switch (subject) {
					case 'damage':
						// user.damage.add(...rolls);
						break;
					case 'attack':
						user.attacks.add(...rolls[20] ?? []);
						break;
					case 'ability':
						if (rolls[20]?.length > 0) {
							user.ability.add(...rolls[20]);
						}
						break;
					case 'save':
						if (rolls[20]?.length > 0) {
							user.saves.add(...rolls[20]);
						}
						break;
					case 'skill':
						user.skills.add(...rolls[20]);
						break;
					default:
						if (rolls[20]?.length > 0)
							user.unidentified.add(...rolls[20]);
						break;
				}

				user.add(rolls);
			}

			lastMsg = cm;
			includedMessages++;
		}

		for (const user of Object.values(users))
			user.finalize();

		const stats = {
			oldest: lastMsg,
			newest: allMessages[0],
			messages: {
				included: includedMessages,
				total: allMessages.length
			}
		}

		console.log('DONE SCANNING');
		return { users, stats };
	}

	function generateStatDialog(users, stats) {
		const hbs = `
<style>
.app.dialog.roll-stats .main-layout {
	gap: 3px;
	display: grid;
	grid-template-rows: repeat(16, auto);
	grid-auto-flow: column;
}
.app.dialog.roll-stats .users {
	display:contents;
}
.app.dialog.roll-stats label,
.app.dialog.roll-stats details summary {
	white-space: nowrap;
}
</style>
<div class='config'>
	<label>Max days: <b>{{config.maxDays}}</b>.</label>
	<label>Max idle between messages: <b>{{config.maxIdle}}</b>.</label>
	<br>
	<label>Observed time period: <b>{{period}}</b> {{periodUnits}}.</label>
	<label>Observed messages: <b>{{messages.included}}/{{messages.total}}</b>.</label>
	<br>
	<label>Oldest message: <b>{{lastTimestamp}}</b>.</label>
</div>
<div class='main-layout'>
	<div class='users'>
		<h2>User:</h2>
		<h3 style='border-bottom: 1px solid #c3acac;'><b>d20</b></h3>
		<label style='background-color:rgba(0,0,0,0.05);'>Average:</label>
		<label>Mode:</label>
		<label style='background-color:rgba(0,0,0,0.05);'>Nat 20s:</label>
		<label>Nat 1s:</label>
		<label style='background-color:rgba(0,0,0,0.05);'>Best – Worst:</label>
		<label>Longest streak:</label>
		<label style='background-color:rgba(0,0,0,0.05);'>Streaks:</label>
		<div>Spread:</div>

		<h3 style='border-bottom: 1px solid #c3acac;'><b>By Subject</b></h3>
		<label style='background-color:rgba(0,0,0,0.05);'>Attacks:</label>
		<label>Ability:</label>
		<label style='background-color:rgba(0,0,0,0.05);'>Skills:</label>
		<label>Saves:</label>
		<label style='background-color:rgba(0,0,0,0.05);'>Other:</label>
	</div>

	{{#each users as |user id|}}
	<div style='display:contents;background-color:rgba(0,0,0,0.02);'>
		<h2>{{user.name}}</h2>
		<h3 style='border-bottom: 1px solid #c3acac;'>{{checks.rolls.length}}</h3>
		<label style='background-color:rgba(0,0,0,0.05);{{#if (gt checks.average 12)}}color:seagreen;{{/if}}{{#if (lt checks.average 8)}}color:maroon;{{/if}}'>{{checks.average}}</label>
		<label>{{#if (gt checks.modeCount 1)}}<b>{{checks.mode}}</b> ×{{checks.modeCount}} [{{rollStats-macro-percentile checks.modeCount checks.rolls.length}}%]{{else}}n/a{{/if}}</label>
		<label style='background-color:rgba(0,0,0,0.05);'>{{checks.nat20s}} [{{rollStats-macro-percentile checks.nat20s checks.rolls.length}}%]</label>
		<label>{{checks.nat1s}} [{{rollStats-macro-percentile checks.nat1s checks.rolls.length}}%]</label>
		<label style='background-color:rgba(0,0,0,0.05);'>{{checks.best}} – {{checks.worst}}</label>
		<label {{#if (eq checks.rolls.length 0)}}style='opacity:0.4;'{{/if}}>{{#if longestStreak}}<b>{{longestStreak.num}}</b> × {{longestStreak.count}}{{else}}n/a{{/if}}</label>
		<details {{#if (eq streaks.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary style='background-color:rgba(0,0,0,0.05);'><b>{{streaks.length}}</b> streak(s)</summary>
			<div>
			{{#each superStreaks}}
				<div class='flexrow' style='gap:0.3em;{{#unless (rollStats-macro-every-nth @index 2)}}background-color:rgba(0,0,0,0.05);{{/unless}}{{#if (eq count 0)}}color:gray;{{/if}}'>
					<label style='flex:0 2em;text-align: right;'><b>{{num}}</b></label>
					×<label style='flex:0 1em;'>{{count}}</label>
					{{#if (gt repeat 1)}}
					×<label style='flex:0 1em;'>{{repeat}}</label>
					{{/if}}
				</div>
			{{/each}}
			</div>
		</details>
		<details {{#if (eq checks.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<div style='display:grid;grid-template-rows: repeat(20, auto);'>
			{{#each checks.spread as |count size|}}
				<div class='flexrow' style='gap:0.3em;{{#if (rollStats-macro-every-nth @index 2)}}background-color:rgba(0,0,0,0.05);{{/if}}{{#if (eq count 0)}}color:gray;{{/if}}'>
					<label style='flex:0 2em;text-align: right;'>{{size}}</label> ×<label style='flex:0 2em;'>{{count}}</label>
				</div>
			{{/each}}
			</div>
		</details>

		<label></label>
		<details {{#if (eq attacks.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary style='background-color:rgba(0,0,0,0.05);'><b>{{attacks.rolls.length}}</b> checks </summary>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Average:</label> <label style='flex:1 4em;'>{{attacks.average}}</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Mode:</label> <label style='flex:1 4em;'><b>{{attacks.mode}}</b> ×{{attacks.modeCount}} [{{rollStats-macro-percentile attacks.modeCount attacks.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 20s:</label> <label style='flex:1 4em;'>{{attacks.nat20s}} [{{rollStats-macro-percentile attacks.nat20s attacks.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 1s:</label> <label style='flex:1 4em;'>{{attacks.nat1s}} [{{rollStats-macro-percentile attacks.nat1s attacks.rolls.length}}%]</label>
			</div>
		</details>
		<details {{#if (eq ability.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary><b>{{ability.rolls.length}}</b> checks </summary>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Average:</label> <label style='flex:1 4em;white-space:nowrap;'>{{ability.average}}</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Mode:</label> <label style='flex:1 4em;white-space:nowrap;'><b>{{ability.mode}}</b> ×{{ability.modeCount}} [{{rollStats-macro-percentile ability.modeCount ability.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 20s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{ability.nat20s}} [{{rollStats-macro-percentile ability.nat20s ability.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 1s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{ability.nat1s}} [{{rollStats-macro-percentile ability.nat1s ability.rolls.length}}%]</label>
			</div>
		</details>
		<details {{#if (eq skills.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary style='background-color:rgba(0,0,0,0.05);'><b>{{skills.rolls.length}}</b> checks</summary>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Average:</label> <label style='flex:1 4em;white-space:nowrap;'>{{skills.average}}</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Mode:</label> <label style='flex:1 4em;white-space:nowrap;'><b>{{skills.mode}}</b> ×{{skills.modeCount}} [{{rollStats-macro-percentile skills.modeCount skills.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 20s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{skills.nat20s}} [{{rollStats-macro-percentile skills.nat20s skills.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 1s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{skills.nat1s}} [{{rollStats-macro-percentile skills.nat1s skills.rolls.length}}%]</label>
			</div>
		</details>
		<details {{#if (eq saves.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary><b>{{saves.rolls.length}}</b> checks</summary>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Average:</label> <label style='flex:1 4em;white-space:nowrap;'>{{saves.average}}</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Mode:</label> <label style='flex:1 4em;white-space:nowrap;'><b>{{saves.mode}}</b> ×{{saves.modeCount}} [{{rollStats-macro-percentile saves.modeCount saves.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 20s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{saves.nat20s}} [{{rollStats-macro-percentile saves.nat20s saves.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 1s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{saves.nat1s}} [{{rollStats-macro-percentile saves.nat1s saves.rolls.length}}%]</label>
			</div>
		</details>
		<details {{#if (eq unidentified.rolls.length 0)}}style='opacity:0.4;'{{/if}}>
			<summary style='background-color:rgba(0,0,0,0.05);'><b>{{unidentified.rolls.length}}</b> checks</summary>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Average:</label> <label style='flex:1 4em;white-space:nowrap;'>{{unidentified.average}}</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Mode:</label> <label style='flex:1 4em;white-space:nowrap;'><b>{{unidentified.mode}}</b> ×{{unidentified.modeCount}} [{{rollStats-macro-percentile unidentified.modeCount unidentified.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 20s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{unidentified.nat20s}} [{{rollStats-macro-percentile unidentified.nat20s unidentified.rolls.length}}%]</label>
			</div>
			<div class='flexrow' style='gap:0.3em;'>
				<label style='flex:0 4em;'>Nat 1s:</label> <label style='flex:1 4em;white-space:nowrap;'>{{unidentified.nat1s}} [{{rollStats-macro-percentile unidentified.nat1s unidentified.rolls.length}}%]</label>
			</div>
		</details>
	</div>
	{{/each}}
</div>
<hr>
`;

		function everyNth(index, nth) {
			return index % nth === 0;
		}

		function percentile(num, total) {
			return Math.roundDecimals(num / total * 100, 1);
		}

		Handlebars.registerHelper('rollStats-macro-every-nth', everyNth);
		Handlebars.registerHelper('rollStats-macro-percentile', percentile);
		let content;
		try {
			const template = Handlebars.compile(hbs);
			const lastDate = new Date(stats.oldest.timestamp);
			const today = new Date(Date.now()).toLocaleDateString() === lastDate.toLocaleDateString();

			let period = (stats.newest.timestamp - stats.oldest.timestamp) / 1_000;
			let periodUnits = 'seconds';
			if (period >= 86_400) {
				period /= 86_400;
				periodUnits = 'days';
			}
			else if (period >= 3_600) {
				period /= 3_600;
				periodUnits = 'hours';
			}
			else if (period >= 300) {
				period /= 60;
				periodUnits = 'minutes';
			}

			period = Math.roundDecimals(period, 2);
			const templatedata = {
				users,
				period,
				periodUnits,
				messages: stats.messages,
				config: rollStatsCfg,
				lastTimestamp: (today ? '' : lastDate.toLocaleDateString() + ' ') + lastDate.toLocaleTimeString(),
			};
			content = template(templatedata, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
		}
		finally {
			Handlebars.unregisterHelper('rollStats-macro-every-nth');
			Handlebars.unregisterHelper('rollStats-macro-percentile');
		}

		class StatDialog extends Dialog {
			/**
			 * @param {JQuery} jq
			 */
			activateListeners(jq) {
				super.activateListeners(jq);
				const html = jq[0].parentElement;
				const app = html.closest('.app.dialog');
				app.style.minHeight = '21rem';
				app.style.minWidth = '28rem';
				const buttons = html?.querySelector('.dialog-buttons');
				buttons.style.height = 'min-content';
				buttons?.querySelectorAll('button').forEach(el => el.style.height = 'min-content');
			}
		}

		StatDialog.wait({
			title: 'Roll Stats',
			content,
			buttons: {
				dismiss: {
					label: 'OK',
					callback: () => null,
				}
			},
			close: () => null,
			default: 'dismiss'
		},
		{
			classes: ['dialog', 'roll-stats'],
			resizable: true
		});
	}

	async function getNewConfiguration() {
		const hbs = `
<form autocomplete='off'>
	<div class='form-group'>
		<label>Max idle minutes:</label> <input name='maxIdle' type='number' data-dtype='Number' step='1' min='5' value='{{maxIdle}}'>
		<p class='hint'>If period between messages is longer than this, stop searching.</p>
	</div>
	<div class='form-group'>
		<label>Max backlog days:</label> <input name='maxDays' type='number' data-dtype='Number' step='1' min='1' value='{{maxDays}}'>
		<p class='hint'>Don't look at messages older than this.</p>
	</div>
</form>
<hr>
`;

		const template = Handlebars.compile(hbs);
		const content = template(rollStatsCfg, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
		return Dialog.prompt({
			title: 'Roll Stats Configuration',
			content,
			callback: html => {
				const form = html[0].querySelector('form');
				const fd = new FormDataExtended(form).object;
				Object.assign(rollStatsCfg, fd);
				return fd;
			},
			rejectClose: true,
		});
	}

	const isShift = game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.SHIFT);
	if (!rollStatsCfg.skipPrompt && !isShift || rollStatsCfg.skipPrompt && isShift)
		await getNewConfiguration();

	const { users, stats } = scanChatMessages();

	console.log(users, stats);

	generateStatDialog(users, stats);
})();
